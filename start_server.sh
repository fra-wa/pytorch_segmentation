#!/bin/bash
# Check for virtual env
source ./virtual_env/bin/activate

if [ $? -eq 1 ]; then
  source ./linux_installer.sh
else
  python install_requirements.py --update_venv=True
  python manage.py migrate
  gnome-terminal -- ./background_runner.sh
  echo "Waiting 5 seconds for the background_runner to set up the server."
  sleep 5
  xdg-open http://127.0.0.1:8000/
  exit 1
fi