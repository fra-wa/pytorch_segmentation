import argparse
import coverage
import os
import sys
import unittest

module_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django

django.setup()


class SkipTestLoader(unittest.TestLoader):
    def __init__(self):
        super(SkipTestLoader, self).__init__()

    def discover(self, start_dir, pattern='test*.py', top_level_dir=None, patterns_to_omit=None):
        """
        Custom discover function: executes like discover but removes tests, if the test file name is in patterns_to_omit

        Args:
            start_dir:
            pattern:
            top_level_dir:
            patterns_to_omit (list): list of strings, if the string is in a test_xxx.py file name, the related test is
                                     skipped

        Find and return all test modules from the specified start
        directory, recursing into subdirectories to find them and return all
        tests found within them. Only test files that match the pattern will
        be loaded. (Using shell style pattern matching.)

        All test modules must be importable from the top level of the project.
        If the start directory is not the top level directory then the top
        level directory must be specified separately.

        If a test package name (directory with '__init__.py') matches the
        pattern then the package will be checked for a 'load_tests' function. If
        this exists then it will be called with (loader, tests, pattern) unless
        the package has already had load_tests called from the same discovery
        invocation, in which case the package module object is not scanned for
        tests - this ensures that when a package uses discover to further
        discover child tests that infinite recursion does not happen.

        If load_tests exists then discovery does *not* recurse into the package,
        load_tests is responsible for loading all tests in the package.

        The pattern is deliberately not stored as a loader attribute so that
        packages can continue discovery themselves. top_level_dir is stored so
        load_tests does not need to pass this argument in to loader.discover().

        Paths are sorted before being imported to ensure reproducible execution
        order even on filesystems with non-alphabetical ordering like ext3/4.
        """
        set_implicit_top = False
        if top_level_dir is None and self._top_level_dir is not None:
            # make top_level_dir optional if called from load_tests in a package
            top_level_dir = self._top_level_dir
        elif top_level_dir is None:
            set_implicit_top = True
            top_level_dir = start_dir

        top_level_dir = os.path.abspath(top_level_dir)

        if not top_level_dir in sys.path:
            # all test modules must be importable from the top level directory
            # should we *unconditionally* put the start directory in first
            # in sys.path to minimise likelihood of conflicts between installed
            # modules and development versions?
            sys.path.insert(0, top_level_dir)
        self._top_level_dir = top_level_dir

        is_not_importable = False
        is_namespace = False
        tests = []
        if os.path.isdir(os.path.abspath(start_dir)):
            start_dir = os.path.abspath(start_dir)
            if start_dir != top_level_dir:
                is_not_importable = not os.path.isfile(os.path.join(start_dir, '__init__.py'))
        else:
            # support for discovery from dotted module names
            try:
                __import__(start_dir)
            except ImportError:
                is_not_importable = True
            else:
                the_module = sys.modules[start_dir]
                top_part = start_dir.split('.')[0]
                try:
                    start_dir = os.path.abspath(
                        os.path.dirname((the_module.__file__)))
                except AttributeError:
                    # look for namespace packages
                    try:
                        spec = the_module.__spec__
                    except AttributeError:
                        spec = None

                    if spec and spec.loader is None:
                        if spec.submodule_search_locations is not None:
                            is_namespace = True

                            for path in the_module.__path__:
                                if (not set_implicit_top and
                                        not path.startswith(top_level_dir)):
                                    continue
                                self._top_level_dir = \
                                    (path.split(the_module.__name__
                                                .replace(".", os.path.sep))[0])
                                tests.extend(self._find_tests(path, pattern, namespace=True))
                    elif the_module.__name__ in sys.builtin_module_names:
                        # builtin module
                        raise TypeError('Can not use builtin modules as dotted module names') from None
                    else:
                        raise TypeError('don\'t know how to discover from {!r}'.format(the_module)) from None

                if set_implicit_top:
                    if not is_namespace:
                        self._top_level_dir = self._get_directory_containing_module(top_part)
                        sys.path.remove(top_level_dir)
                    else:
                        sys.path.remove(top_level_dir)

        if is_not_importable:
            raise ImportError('Start directory is not importable: %r' % start_dir)

        if not is_namespace:
            tests = list(self._find_tests(start_dir, pattern))

            # Omitting some tests here
            if patterns_to_omit:
                indexes_to_pop = []
                for i, test in enumerate(tests):
                    if any(pattern_to_omit in str(test._tests) for pattern_to_omit in patterns_to_omit):
                        indexes_to_pop.append(i)
                indexes_to_pop.sort(reverse=True)

                for idx in indexes_to_pop:
                    tests.pop(idx)

        return self.suiteClass(tests)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 'True', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'False', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--omit_models", type=str2bool, default='False',
                        help="If True, the tests related to dl_models are omitted")
    parser.add_argument("--omit_augmentation", type=str2bool, default='False',
                        help="If True, the tests related to augmentation are omitted")
    parser.add_argument("--use_coverage", type=str2bool, default='True',
                        help="If False, no coverage report will be created")
    args = parser.parse_args()

    current_dir = os.path.dirname(os.path.abspath(__file__))
    sys.path.extend([current_dir])

    patterns_to_omit = []

    if args.omit_models:
        patterns_to_omit.append('test_models')
        patterns_to_omit.append('test_load_and_save_model')

    if args.omit_augmentation:
        patterns_to_omit.append('test_augmentation')

    coverage_dir = os.path.join(current_dir, 'test_coverage')

    cov = None

    if args.use_coverage:
        cov = coverage.Coverage(
            data_file=None,
            source=[
                current_dir,
            ],
            omit=[
                # files
                '*__init__.py',
                '*continue_training.py',
                '*execute_tests.py',
                '*install_requirements.py',
                '*integration_test.py',
                '*model_base_tests.py',
                '*start_training.py',
                '*start_prediction.py',
                '*save_decoding_colors.py',
                '*test_*',
                # folders
                '/hpc/*',
            ]
        )
        cov.start()

    print('\nHello! I am glad to see you executing tests :)')
    if args.use_coverage and not args.omit_models and not args.omit_augmentation:
        print('If you want to omit some tests, pass --omit_models=True or --omit_augmentation=True\n'
              'If you do not want a coverage for this run, use --use_coverage=False.\n')
        input('Press any key to continue ...\n')
    print(f'\n'
          f'Skipping model tests:        {args.omit_models}')
    print(f'Skipping augmentation tests: {args.omit_augmentation}')
    print(f'Using coverage:              {args.use_coverage}')

    print('\nSome tests might need some time. Be patient and take a coffee, this will take a while :)\n')

    testsuite = SkipTestLoader().discover(
        start_dir=os.path.join(current_dir, 'pytorch_segmentation/tests'),
        patterns_to_omit=patterns_to_omit,
    )
    result = unittest.TextTestRunner(verbosity=1).run(testsuite)

    if not result.errors and not result.failures:
        print(
            '\nCongratulations, your tests passed :)\n\n'
        )

    if args.use_coverage:
        cov.stop()
        cov.save()
        if not os.path.exists(coverage_dir):
            os.makedirs(coverage_dir)
        cov.html_report(directory=coverage_dir)
