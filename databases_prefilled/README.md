This folder contains some databases with data.

How to use:
- install the framework
- run the server
- stop the server
- copy a selected database into the newly created db folder (same level as the LICENSE file)
- rename the copied database to db.sqlite and (re-)move the old one