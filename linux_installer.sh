#!/bin/bash
echo "Python == 3.8 required. (Pip and python3-venv have to be installed as well!)"
echo ""
read -p "Press enter to continue"
echo ""
echo "The installation will be inside this folder. You want to uninstall? Remove the folder virtual_env or the whole project"
echo ""

# Check for Python Installation
echo ""
echo "Checking if the environment exists:"
echo ""

source ./virtual_env/bin/activate

if [ $? -eq 0 ]; then
  # Reaching here means Python Venv was already created
  echo "Found existing virtual environment"
  python install_requirements.py
elif [ $? -eq 1 ]; then
  echo ""
  echo "Creating a new environment."
  echo ""
  python3 -m venv virtual_env
  if [ $? -eq 1 ]; then
    echo "No correct python found! Install python 3.8 (also pip and python3-venv if needed)"
    read -p "Press enter to exit"
    exit 1
  fi
  echo "created virtual environment, installing packages."
  source ./virtual_env/bin/activate
  python install_requirements.py
fi

echo ""
echo "Finished installation."
echo ""
echo "If everything worked, you can now run 'source start_server.sh'"
echo ""
