import logging
import warnings

import numpy as np


class MetricStorage:
    def __init__(self, classes):
        self.num_classes = classes

        self.overall_accuracy_list = []
        self.accuracy_per_class_list = []
        self.true_positives_ratio_list = []
        self.true_negatives_ratio_list = []
        self.false_positives_ratio_list = []
        self.false_negatives_ratio_list = []

        self.precision_list = []
        self.mean_iou_list = []
        self.dice_coefficient_list = []

        self.class_suited_for_calculation_list = []
        self.all_pixels_list = []
        self.pixels_per_class_list = []

        # class vise value storage
        self.class_suited_for_calculation = np.zeros(self.num_classes, dtype=int)  # 0 and 1 --> 1: is usable
        self.pixels_per_class = np.zeros(self.num_classes, dtype=np.uint32)

        self.accuracy_per_class = np.zeros(self.num_classes)
        self.true_positives_ratio_per_class = np.zeros(self.num_classes)
        self.true_negatives_ratio_per_class = np.zeros(self.num_classes)
        self.false_positives_ratio_per_class = np.zeros(self.num_classes)
        self.false_negatives_ratio_per_class = np.zeros(self.num_classes)

        self.precision_per_class = np.zeros(self.num_classes)
        self.mean_iou_per_class = np.zeros(self.num_classes)
        self.dice_coefficient_per_class = np.zeros(self.num_classes)

        # storage summed up (for multiclass, otherwise only foreground)
        self.all_pixels = 0
        self.overall_accuracy = 0
        self.true_positives_ratio = 0
        self.true_negatives_ratio = 0
        self.false_positives_ratio = 0
        self.false_negatives_ratio = 0
        self.precision = 0
        self.mean_iou = 0
        self.dice_coefficient = 0
        self.calculation_reasonable = True

    def store_calculator(self, calculator):
        if not isinstance(calculator, MetricsCalculator):
            raise ValueError('Calculator mus be a MetricsCalculator instance!')

        self.overall_accuracy_list.append(calculator.overall_accuracy)
        self.accuracy_per_class_list.append(calculator.accuracy_per_class)
        self.true_positives_ratio_list.append(calculator.tp_ratio)
        self.true_negatives_ratio_list.append(calculator.tn_ratio)
        self.false_positives_ratio_list.append(calculator.fp_ratio)
        self.false_negatives_ratio_list.append(calculator.fn_ratio)

        self.precision_list.append(calculator.precision)
        self.mean_iou_list.append(calculator.mean_iou)
        self.dice_coefficient_list.append(calculator.dice_coefficient)

        self.class_suited_for_calculation_list.append(calculator.class_suited_for_calculation)
        self.all_pixels_list.append(calculator.all_pixels)
        self.pixels_per_class_list.append(calculator.pixels_per_class)

    def combine_storages(self, storage):
        if not isinstance(storage, MetricStorage):
            raise ValueError('storage mus be a MetricsStorage instance!')

        self.overall_accuracy_list += storage.overall_accuracy_list
        self.accuracy_per_class_list += storage.accuracy_per_class_list
        self.true_positives_ratio_list += storage.true_positives_ratio_list
        self.true_negatives_ratio_list += storage.true_negatives_ratio_list
        self.false_positives_ratio_list += storage.false_positives_ratio_list
        self.false_negatives_ratio_list += storage.false_negatives_ratio_list

        self.precision_list += storage.precision_list
        self.mean_iou_list += storage.mean_iou_list
        self.dice_coefficient_list += storage.dice_coefficient_list

        self.class_suited_for_calculation_list += storage.class_suited_for_calculation_list
        self.all_pixels_list += storage.all_pixels_list
        self.pixels_per_class_list += storage.pixels_per_class_list

    def __set_class_to_not_usable(self, class_id):
        self.class_suited_for_calculation[class_id] = 0

        self.pixels_per_class[class_id] = 0
        self.accuracy_per_class[class_id] = 0
        self.true_positives_ratio_per_class[class_id] = 0
        self.true_negatives_ratio_per_class[class_id] = 0
        self.false_positives_ratio_per_class[class_id] = 0
        self.false_negatives_ratio_per_class[class_id] = 0
        self.precision_per_class[class_id] = 0
        self.mean_iou_per_class[class_id] = 0
        self.dice_coefficient_per_class[class_id] = 0

    def calculate_result(self, exclude_background=False):
        """
        Iterate over classes
        calculate results for each class with weight
        magic
        """

        pixels_per_class_array = np.array(self.pixels_per_class_list)
        class_suited_for_calculation_array = np.array(self.class_suited_for_calculation_list)
        accuracy_per_class_array = np.array(self.accuracy_per_class_list)
        true_positives_ratio_array = np.array(self.true_positives_ratio_list)
        true_negatives_ratio_array = np.array(self.true_negatives_ratio_list)
        false_positives_ratio_array = np.array(self.false_positives_ratio_list)
        false_negatives_ratio_array = np.array(self.false_negatives_ratio_list)
        precision_array = np.array(self.precision_list)
        mean_iou_array = np.array(self.mean_iou_list)
        dice_coefficient_array = np.array(self.dice_coefficient_list)

        self.all_pixels = sum(self.all_pixels_list)

        overall_acc_array = np.array(self.overall_accuracy_list)
        all_px_array = np.array(self.all_pixels_list)
        pixel_weights = all_px_array / self.all_pixels
        overall_acc_array_weighted = pixel_weights * overall_acc_array
        self.overall_accuracy = sum(overall_acc_array_weighted)

        for class_id in range(self.num_classes):
            pixels_per_class = pixels_per_class_array[:, class_id]
            self.pixels_per_class[class_id] = np.sum(pixels_per_class)

            # set pixels per class to 0 for any image with bad distribution --> will be used as weight = 0
            pixels_per_class = pixels_per_class * class_suited_for_calculation_array[:, class_id]
            # what is: self.class_suited_for_calculation[:, class_id] --> zero and ones: 1 is usable else no
            # see MetricsCalculator

            max_pixels_per_class = np.max(pixels_per_class)
            if max_pixels_per_class == 0:
                self.__set_class_to_not_usable(class_id)
                continue
            # cleaned weights:
            self.class_suited_for_calculation[class_id] = 1
            weights = pixels_per_class / max_pixels_per_class
            divisor = np.sum(weights)

            self.accuracy_per_class[class_id] = np.sum(
                weights * accuracy_per_class_array[:, class_id]) / divisor

            self.true_positives_ratio_per_class[class_id] = np.sum(
                weights * true_positives_ratio_array[:, class_id]) / divisor

            self.true_negatives_ratio_per_class[class_id] = np.sum(
                weights * true_negatives_ratio_array[:, class_id]) / divisor

            self.false_positives_ratio_per_class[class_id] = np.sum(
                weights * false_positives_ratio_array[:, class_id]) / divisor

            self.false_negatives_ratio_per_class[class_id] = np.sum(
                weights * false_negatives_ratio_array[:, class_id]) / divisor

            self.precision_per_class[class_id] = np.sum(
                weights * precision_array[:, class_id]) / divisor

            self.mean_iou_per_class[class_id] = np.sum(
                weights * mean_iou_array[:, class_id]) / divisor

            self.dice_coefficient_per_class[class_id] = np.sum(
                weights * dice_coefficient_array[:, class_id]) / divisor

        # we are interested in class vise values, no background
        if self.num_classes == 2 or exclude_background:
            class_weights = self.pixels_per_class / (self.all_pixels - self.pixels_per_class[0])
            class_weights[0] = 0
        else:
            class_weights = self.pixels_per_class / self.all_pixels

        self.true_positives_ratio = np.sum(class_weights * self.true_positives_ratio_per_class)
        self.true_negatives_ratio = np.sum(class_weights * self.true_negatives_ratio_per_class)
        self.false_positives_ratio = np.sum(class_weights * self.false_positives_ratio_per_class)
        self.false_negatives_ratio = np.sum(class_weights * self.false_negatives_ratio_per_class)
        self.precision = np.sum(class_weights * self.precision_per_class)
        self.mean_iou = np.sum(class_weights * self.mean_iou_per_class)
        self.dice_coefficient = np.sum(class_weights * self.dice_coefficient_per_class)

    def __array_to_info_text(self, array, unit=''):
        array_list = list(array)
        classes_list = list(range(self.num_classes))
        class_and_array = zip(classes_list, array_list)
        if array.dtype == int:
            string_list = [
                f'{class_id_and_val[0]}: {class_id_and_val[1]} {unit}' for class_id_and_val in class_and_array
            ]
        else:
            string_list = [
                f'{class_id_and_val[0]}: {class_id_and_val[1]:.3f} {unit}' for class_id_and_val in class_and_array
            ]
        return f'{", ".join(string_list)}'

    def log_multiclass_results(self):
        logging.info('Classes:')
        logging.info(f'{", ".join([str(class_id) for class_id in range(self.num_classes)])}')
        logging.info('Class suited for calculation (1 yes, 0: this class had no occurrence):')
        text = self.__array_to_info_text(self.class_suited_for_calculation)
        logging.info(text)
        logging.info('Pixel percentage per class:')
        pixel_ratio = self.pixels_per_class / self.all_pixels * 100
        text = self.__array_to_info_text(pixel_ratio, unit='%')
        logging.info(text)
        logging.info('Accuracy per class:')
        text = self.__array_to_info_text(self.accuracy_per_class * 100, unit='%')
        logging.info(text)
        logging.info('TP ratio per class:')
        text = self.__array_to_info_text(self.true_positives_ratio_per_class)
        logging.info(text)
        logging.info('TN ratio per class:')
        text = self.__array_to_info_text(self.true_negatives_ratio_per_class)
        logging.info(text)
        logging.info('FP ratio per class:')
        text = self.__array_to_info_text(self.false_positives_ratio_per_class)
        logging.info(text)
        logging.info('FN ratio per class:')
        text = self.__array_to_info_text(self.false_negatives_ratio_per_class)
        logging.info(text)
        logging.info('Precision per class:')
        text = self.__array_to_info_text(self.precision_per_class)
        logging.info(text)
        logging.info('Mean IoU per class:')
        text = self.__array_to_info_text(self.mean_iou_per_class)
        logging.info(text)
        logging.info('DICE per class:')
        text = self.__array_to_info_text(self.dice_coefficient_per_class)
        logging.info(text)

    def show_result(self):
        logging.info(f'Finished testing.')
        if self.num_classes == 2:
            logging.info(f'Results:')
            logging.info(f'Binary segmentation --> values representing foreground class.')
            logging.info('Class 0 is treated as background.')
        else:
            logging.info(f'Global results:')
            logging.info(f'Multiclass segmentation --> values representing weighted sum of all classes.')

        logging.info(f'Overall accuracy: {self.overall_accuracy * 100:.2f}%')
        logging.info('')
        skipped_data_count = self.class_suited_for_calculation.shape[0] - np.sum(self.class_suited_for_calculation)
        logging.info(
            f'Metrics (only well distributed data included). '
            f'{self.class_suited_for_calculation.shape[0] - skipped_data_count}/'
            f'{self.class_suited_for_calculation.shape[0]} are valid.'
        )
        if skipped_data_count == self.class_suited_for_calculation.shape[0]:
            logging.info('No calculation possible: The testing data is not well distributed!')
        else:
            logging.info(f'True positives rate (Also called sensitivity or recall)')
            logging.info(f'{self.true_positives_ratio:.5f}')
            logging.info(f'True negatives rate (Also called specificity)')
            logging.info(f'{self.true_negatives_ratio:.5f}')
            logging.info(f'False positives rate (Also called fallout, 1 - specificity)')
            logging.info(f'{self.false_positives_ratio:.5f}')
            logging.info(f'False negatives rate (1 - sensitivity)')
            logging.info(f'{self.false_negatives_ratio:.5f}')
            logging.info(f'Precision (Also called Positive Predicted Value)')
            logging.info(f'{self.precision:.5f}')
            logging.info(f'IoU (Also called jaccard index.)')
            logging.info(f'{self.mean_iou:.5f}')
            logging.info(f'DICE coefficient (Also called overlap index or F1-measure)')
            logging.info(f'{self.dice_coefficient:.5f}')

        if self.num_classes > 2:
            logging.info('')
            self.log_multiclass_results()
            logging.info('')

        logging.info(
            'Attention: IoU and DICE measure the same aspects and provide the same system ranking. Therefore, it does '
            'not provide additional information to select both of them together as validation metrics'
        )


class MetricsCalculator:
    def __init__(self, decoded_network_output, ground_truth, classes):
        """

        Args:
            decoded_network_output: contains image in range 0 to classes - 1! Not colored or scaled from 0 to 255
            ground_truth: contains image in range 0 to classes - 1! Not colored or scaled from 0 to 255
            classes: how many classes to segment into
        """
        if not decoded_network_output.shape:
            raise ValueError('Network output is empty')

        self.is_binary = True
        gt_unique = np.unique(ground_truth)
        if gt_unique.shape[0] > 2:
            self.is_binary = False

        out_unique = np.unique(decoded_network_output)
        if out_unique.shape[0] > classes or gt_unique.shape[0] > classes:
            raise ValueError(
                f'Prediction ({out_unique}) or ground truth {gt_unique} contains more than {classes} classes!'
            )

        if not np.all(out_unique < classes):
            raise ValueError(
                f'Unique output classes are not as expected:\n'
                f'at least one value is > than {classes - 1} (classes ({classes}) - 1, first class is 0):\n'
                f'Unique: {", ".join([str(class_id) for class_id in list(out_unique)])}'
            )
        if not np.all(gt_unique < classes):
            raise ValueError(
                f'Unique ground truth classes are not as expected:\n'
                f'at least one value is > than {classes - 1} (classes ({classes}) - 1, first class is 0):\n'
                f'Unique: {", ".join([str(class_id) for class_id in list(gt_unique)])}'
            )

        if len(decoded_network_output.shape) != len(ground_truth.shape):
            raise ValueError(
                f'Shapes of predicted and Ground truth volume are different.\n'
                f'GT shape: {ground_truth.shape}, Predicted shape: {decoded_network_output.shape}'
            )

        for predicted_dim, target_dim in zip(decoded_network_output.shape, ground_truth.shape):
            if predicted_dim != target_dim:
                raise ValueError(
                    f'Dimension missmatch: Dimension of ground truth and predicted volume are different:\n'
                    f'GT shape: {ground_truth.shape}, Predicted shape: {decoded_network_output.shape}'
                )

        self.network_output = decoded_network_output
        self.ground_truth = ground_truth
        self.num_classes = classes

        self.all_pixels = 1
        for shape in self.network_output.shape:
            self.all_pixels = shape * self.all_pixels

        self.overall_accuracy = None  # 0 to 1

        # True positives; Also called sensitivity or recall
        self.tp = np.zeros(self.num_classes, dtype=np.uint32)
        self.tp_ratio = np.zeros(self.num_classes)

        # True negatives; Also called specificity
        self.tn = np.zeros(self.num_classes, dtype=np.uint32)  # True negatives; Also called specificity
        self.tn_ratio = np.zeros(self.num_classes)

        # False positives; Also called fallout
        self.fp = np.zeros(self.num_classes, dtype=np.uint32)
        self.fp_ratio = np.zeros(self.num_classes)

        # False negatives; 1 - sensitivity
        self.fn = np.zeros(self.num_classes, dtype=np.uint32)
        self.fn_ratio = np.zeros(self.num_classes)

        # Positive Predicted Value; Also called Precision
        self.precision = np.zeros(self.num_classes)

        # Mean IoU
        # Also called jaccard index.
        self.mean_iou = np.zeros(self.num_classes)

        # Dice
        # Also called overlap index or F1-measure
        self.dice_coefficient = np.zeros(self.num_classes)

        # if tp for a class = 0, iou, dice, precision, .. are useless.
        self.class_suited_for_calculation = np.zeros(self.num_classes, dtype=int)

        # in the end, we need to weight the results. why: suppose class 1 has only 10 pixels in gt of this image
        # now a second image contains 10.000 pixels of class 1.
        # suppose the network classifies 1 out of 10 pixels in sample 1
        # suppose the network classifies 9.000 out of 10.000 pixels in sample 2
        # it gets a bit unfair to weight 1:1 using 10 pixels compared to 10.000 right?
        self.pixels_per_class = np.zeros(self.num_classes, dtype=np.uint32)
        self.accuracy_per_class = np.zeros(self.num_classes)

    def calculate(self):
        true_false = self.network_output == self.ground_truth
        self.overall_accuracy = np.sum(true_false) / self.all_pixels

        # compute tp, fn, fp, tn and acc per class
        for class_id in range(self.num_classes):
            gt_true_false = self.ground_truth == class_id
            result_map = gt_true_false.astype(int)
            gt_true_count = np.sum(result_map == 1)
            gt_false_count = np.sum(result_map == 0)

            if gt_true_count != 0:
                self.class_suited_for_calculation[class_id] = 1
                self.pixels_per_class[class_id] = gt_true_count
            else:
                # will result in some nan values --> are caught below. nan values cannot be stored to sqlite db!
                continue

            out_true_false = self.network_output == class_id

            self.accuracy_per_class[class_id] = np.sum(gt_true_false == out_true_false) / self.all_pixels

            # calculate True Positives; Also called sensitivity or recall
            out_result_map = out_true_false.astype(int)
            out_result_map[out_result_map == 0] = 2  # for easier comparison in next line -> gives true positives
            self.tp[class_id] = np.sum(result_map == out_result_map)

            # calculate True Negatives; Also called specificity
            out_result_map[out_result_map == 1] = 3  # set true to 3 to remove trues from comparison
            out_result_map[out_result_map == 2] = 0  # set false to 0 and compare against gt -> gives true negatives
            self.tn[class_id] = np.sum(result_map == out_result_map)

            # False Positives
            self.fp[class_id] = gt_false_count - self.tn[class_id]

            # False Negatives
            self.fn[class_id] = gt_true_count - self.tp[class_id]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")  # if division by 0
            # Positive Predicted Value; Also called Precision
            self.precision = self.tp / (self.tp + self.fp)
            self.precision[np.where(np.isnan(self.precision))] = 0  # only background

            # True Positives rate; Also called sensitivity or recall
            self.tp_ratio = self.tp / (self.fn + self.tp)
            self.tp_ratio[np.where(np.isnan(self.tp_ratio))] = 0  # only foreground

            # True Negatives Rate; Also called specificity
            self.tn_ratio = self.tn / (self.tn + self.fp)
            self.tn_ratio[np.where(np.isnan(self.tn_ratio))] = 0  # only foreground

            # False Positives Rate; Also called fallout, 1 - specificity = 1 - TNR
            self.fp_ratio = 1 - self.tn_ratio

            # False Negatives Rate; 1 - sensitivity = 1 - TPR
            self.fn_ratio = 1 - self.tp_ratio

            # Dice
            # Also called overlap index or F1-measure
            # The Dice coefficient [13] (DICE), also called the overlap index, is the most used metric in validating
            # medical volume segmentations. In addition to the direct comparison between automatic and ground truth
            # segmentations, it is common to use the DICE to measure reproducibility (repeatability). Zou et al. [1]
            # used the DICE as a measure of the reproducibility as a statistical validation of manual annotation where
            # segmenters repeatedly annotated the same MRI image, then the pair-wise overlap of the repeated
            # segmentations is calculated using the DICE.
            # [https://bmcmedimaging.biomedcentral.com/articles/10.1186/s12880-015-0068-x formula 6]
            # TP: true positive
            # FP: false positive
            # FN: false negative
            # DICE = 2 * TP / (2 * TP + FP + FN)
            self.dice_coefficient = 2 * self.tp / (2 * self.tp + self.fp + self.fn)
            self.dice_coefficient[np.where(np.isnan(self.dice_coefficient))] = 0

            # Mean IoU
            # Also called jaccard index.
            # IoU and DICE measure the same aspects and provide the same system ranking. Therefore, it does
            # not provide additional information to select both of them together as validation metrics:
            #
            # DICE = 2 * IoU / (1 + IoU)
            # IoU = DICE / (2 - DICE) = TP / (TP + FP + FN)
            self.mean_iou = self.dice_coefficient / (2 - self.dice_coefficient)
            self.mean_iou[np.where(np.isnan(self.mean_iou))] = 0
