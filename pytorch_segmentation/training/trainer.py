import logging
import numpy as np
import os
import random
import time
import torch
import torch.distributed as dist

from torch.nn.parallel import DistributedDataParallel as DDP
from decimal import Decimal
from django.utils import timezone

from pytorch_segmentation import constants
from pytorch_segmentation.dl_models.backward_compatibility import BACKWARD_COMPATIBLE_NETWORKS
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, remove_file
from pytorch_segmentation.temporary_parameters.user_information import show_best_five_stats
from pytorch_segmentation.training.saving import save_history
from pytorch_segmentation.utils import show_memory, get_time_spent_string, get_time_remaining_string, \
    set_up_reproducibility, get_human_time_string_from_seconds


class Trainer:

    def __init__(self, train_preparer, multi_gpu_training=False):
        """

        Args:
            train_preparer:
            multi_gpu_training:
        """

        self.train_preparer = train_preparer

        self.log_file_instance = self.train_preparer.log_file_instance
        self.logger = self.train_preparer.logger

        # User training parameters
        self.classes = self.train_preparer.classes
        self.epochs = self.train_preparer.epochs

        if multi_gpu_training:
            self.device = torch.device(f'cuda:{dist.get_rank()}')
            self.num_gpus = dist.get_world_size()
            self.gpu_id = dist.get_rank()
        else:
            self.device = self.train_preparer.device
            self.num_gpus = None
            self.gpu_id = None

        self.save_every = self.train_preparer.save_every if self.train_preparer.save_every > 0 else self.epochs
        self.show_ram = self.train_preparer.show_ram

        # default parameters or parameters that will be calculated during setup
        self.start_epoch = self.train_preparer.start_epoch
        self.model_analysis = self.train_preparer.model_analysis

        # model, scaler, loss, ...
        self.model = self.train_preparer.model
        if self.gpu_id is not None:
            self.model.to(self.device)

            if self.model.__class__ in BACKWARD_COMPATIBLE_NETWORKS:
                # significantly slower
                find_unused_parameters = True
            else:
                find_unused_parameters = False

            self.model = DDP(self.model, device_ids=[self.gpu_id], find_unused_parameters=find_unused_parameters)
        self.loss_func = self.train_preparer.loss_func.to(self.device)

        # deactivating amp: causing loss to get nan in some instances. (BCE with logits Loss)
        # see: https://github.com/pytorch/pytorch/issues/40497#issuecomment-764538341
        # the thread is interesting - Nvidias Apex AMP seems to work though
        if torch.device(self.device).type == 'cpu' or True:
            self.scaler = torch.cuda.amp.GradScaler(enabled=False)
            self.amp_enabled = False
        else:
            self.scaler = torch.cuda.amp.GradScaler()
            self.amp_enabled = True

        self.optimizer = self.train_preparer.optimizer
        self.profiler = self.train_preparer.profiler
        self.training_started_time = self.train_preparer.training_started_time

        # DATA:
        self.training_loader = self.train_preparer.training_loader
        self.validation_loader = self.train_preparer.validation_loader

    def reseed(self):
        """When using DDP, you need to reseed in each sub process"""
        if self.train_preparer.reproduce and self.train_preparer.multi_gpu_training:
            seed_multiplier = 1 + self.gpu_id
            # No python, torch and numpy cpu state setting - otherwise, all states are the same of process with rank=0.
            # Getting access of other processes states without communication is not easy. Therefore, only cuda states
            # are saved at the checkpoint.
            # When continuing, the cpu states differ, but now using the same seed multiplied by (1 + gpu_id).
            # Ensures reproducible randomness by the loss of continuous random states
            try:
                # different machine may have less gpus or is now running with more gpus on same machine.
                set_up_reproducibility(self.train_preparer.torch_cuda_states[self.gpu_id], self.device, seed_multiplier)
            except IndexError:
                set_up_reproducibility(seed_multiplier)

    def get_targets_and_outputs(self, inputs, targets):
        """
        Used by test loop
        Returns output batch with channels always at out_batch.shape[1]
        """
        inputs = inputs.to(self.device, dtype=torch.float)

        # long for multiclass else we could get too large numbers resulting in nan
        masks_type = torch.float32 if self.classes == 1 else torch.long
        targets = targets.to(self.device, dtype=masks_type)

        with torch.cuda.amp.autocast(enabled=self.amp_enabled):
            outputs = self.model(inputs)
        return targets, outputs

    def get_targets_outputs_and_loss(self, input_batch, target_batch):
        """
        Used by train and validation loop
        Returns output batch with channels always at out_batch.shape[1]
        """
        input_batch = input_batch.to(self.device, dtype=torch.float)

        # long for multiclass else we could get too large numbers resulting in nan
        masks_type = torch.float32 if self.classes == 2 else torch.long
        target_batch = target_batch.to(self.device, dtype=masks_type)

        with torch.cuda.amp.autocast(enabled=self.amp_enabled):
            output_batch = self.model(input_batch)

            if self.classes == 2:
                loss = self.loss_func(output_batch.squeeze(dim=1), target_batch)
            else:
                loss = self.loss_func(output_batch, target_batch)

        return target_batch, output_batch, loss

    @staticmethod
    def compute_accuracy(output_logits, targets):
        # output_logits of shape: B, C, ....
        if output_logits.size(1) == 1:
            output_probabilities = torch.sigmoid(output_logits)
            outs = (output_probabilities.squeeze(1) > 0.5).float()
            correct = outs.eq(targets.squeeze()).sum().item()
        else:
            values, predictions = torch.max(output_logits, dim=1)
            correct = predictions.eq(targets).sum().item()

        total = targets.nelement()  # sums up all pixels (batch_size x width x height)
        accuracy = (correct / total) * targets.size(0)

        return accuracy

    def reduce_loss_and_acc(self, loss, acc, dataset_length):
        # loss was multiplied by each batch size --> divide by all training samples
        if self.gpu_id is not None:
            # reduce -> sums all values up on rank 0 to get real loss and acc  -> nccl
            # all_reduce -> sums all values up on all ranks  -> gloo and nccl
            # see: https://pytorch.org/tutorials/intermediate/dist_tuto.html
            # and: https://pytorch.org/docs/stable/distributed.html#

            # tensors to be sharded have to be on gpu
            loss = torch.tensor(loss).to(self.device)
            acc = torch.tensor(acc).to(self.device)

            dist.all_reduce(loss, op=dist.ReduceOp.SUM)  # default op is SUM
            dist.all_reduce(acc, op=dist.ReduceOp.SUM)

            loss = loss.item()
            acc = acc.item()

        acc = acc / dataset_length
        loss = loss / dataset_length
        return loss, acc

    def train_loop(self, epoch):
        self.model.train()

        train_loss = 0.0
        train_acc = 0.0

        if self.gpu_id == 0 or self.gpu_id is None:
            logging.info('Training loop:')
        try:
            for i, (input_batch, target_batch) in enumerate(self.training_loader):
                if self.gpu_id == 0 or self.gpu_id is None:
                    logging.info(
                        f'Epoch {epoch + 1}/{self.epochs}, train loop: Batch {i + 1} / {len(self.training_loader)}'
                    )
                if i <= 3 or i % 30 == 0:
                    if self.gpu_id == 0:
                        show_memory(self.device.type, self.num_gpus)
                    elif self.num_gpus is None:
                        show_memory(self.device, self.num_gpus)

                targets, outputs, loss = self.get_targets_outputs_and_loss(input_batch, target_batch)

                self.scaler.scale(loss).backward()
                self.scaler.step(self.optimizer)
                self.scaler.update()
                # Setting gradient to None has a slightly different numerical behavior than setting it to zero
                # see: https://pytorch.org/docs/master/optim.html#torch.optim.Optimizer.zero_grad
                # This will in general have lower memory footprint, and can modestly improve performance. However, it
                # changes certain behaviors. For example: 1. When the user tries to access a gradient and perform manual
                # ops on it, a None attribute or a Tensor full of 0s will behave differently. 2. If the user requests
                # zero_grad(set_to_none=True) followed by a backward pass, .grads are guaranteed to be None for params
                # that did not receive a gradient. 3. torch.optim optimizers have a different behavior if the gradient
                # is 0 or None (in one case it does the step with a gradient of 0 and in the other it skips the step
                # altogether).
                self.optimizer.zero_grad(set_to_none=True)

                # if last batch has fewer elements, last batch must be weighted different. Therefore: loss x batch_size
                train_loss += loss.item() * targets.size(0)
                train_acc += self.compute_accuracy(outputs, targets)

        except (RuntimeError, ValueError) as e:
            non_intuitive_vram_errors = [
                'CUDA out of memory',
                'CUDA error: out of memory',
                'Unable to find a valid cuDNN algorithm to run convolution',
                'cuDNN error: CUDNN_STATUS_NOT_SUPPORTED',
            ]
            for non_intuitive_error in non_intuitive_vram_errors:
                if non_intuitive_error in e.args[0]:
                    raise RuntimeError(f'\n!!! Not enough memory to compute !!!\nError: {e}') from e
            raise RuntimeError(f'Got an error during training:\n{e}') from e

        return self.reduce_loss_and_acc(train_loss, train_acc, len(self.training_loader.dataset))

    def valid_loop(self, epoch):
        valid_loss = 0.0
        valid_acc = 0.0

        if self.gpu_id == 0 or self.gpu_id is None:
            logging.info(f'Validation loop:')
        self.model.eval()
        with torch.no_grad():
            for i, (input_batch, target_batch) in enumerate(self.validation_loader):
                if self.gpu_id is None or self.gpu_id == 0:
                    logging.info(
                        f'Epoch {epoch + 1}/{self.epochs}, valid loop: Batch {i + 1} / {len(self.validation_loader)}'
                    )
                if i <= 3 or i % 30 == 0:
                    if self.gpu_id == 0:
                        show_memory(self.device.type, self.num_gpus)
                    elif self.gpu_id is None:
                        show_memory(self.device, self.num_gpus)

                targets, outputs, loss = self.get_targets_outputs_and_loss(input_batch, target_batch)

                valid_loss += loss.item() * targets.size(0)
                valid_acc += self.compute_accuracy(outputs, targets)

        return self.reduce_loss_and_acc(valid_loss, valid_acc, len(self.validation_loader.dataset))

    def show_stats(self, history, start_time):
        epoch, train_loss, valid_loss, train_acc, valid_acc = history[-1][:5]

        human_time_spent = get_time_spent_string(start_time)
        human_time_remaining = get_time_remaining_string(
            start_time,
            total_count=self.epochs - self.start_epoch,
            current_idx=epoch - 1 - self.start_epoch,
        )
        percent_processed = int(round(epoch / self.epochs * 100))

        logging.info(
            f'Processed: {percent_processed}%, time spent: {human_time_spent}, time remaining: {human_time_remaining}'
        )

        logging.info(f'Training: Loss: {train_loss:.6f}, Accuracy: {train_acc:.6f}%')
        logging.info(f'Validation: Loss: {valid_loss:.6f}, Accuracy: {valid_acc:.6f}%')

    def save_checkpoint(self, history):
        if not isinstance(history, list):
            raise ValueError('history must be a list!')

        if isinstance(self.model, DDP):
            model = self.model.module
        else:
            model = self.model

        python_state = random.getstate()
        numpy_state = np.random.get_state()
        torch_cpu_state = torch.get_rng_state()
        if self.device.type == 'cuda' and not isinstance(self.model, DDP):
            torch_cuda_states = [torch.cuda.get_rng_state(device=self.device)]
        elif isinstance(self.model, DDP):
            torch_cuda_states = []
            for i in range(self.num_gpus):
                torch_cuda_states.append(torch.cuda.get_rng_state(device=torch.device(f'cuda:{i}')))
        else:
            torch_cuda_states = None

        saving_dict = {
            'architecture': self.train_preparer.architecture,
            'image_channels': self.train_preparer.channels,
            'classes': self.classes,
            'image_input_size': self.train_preparer.input_size,
            'history': history,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'scaler_state_dict': self.scaler.state_dict(),
            'normalization': self.train_preparer.norm,
            'dataset_mean': self.train_preparer.dataset_mean,
            'dataset_std': self.train_preparer.dataset_std,
            'online_aug': self.train_preparer.online_aug,
            'aug_strength': self.train_preparer.aug_strength,
            'aug_start_epoch': self.train_preparer.aug_start_epoch,
            'dataset_name': self.train_preparer.dataset_name,
            'batch_size': self.train_preparer.batch_size,
            'python_state': python_state,
            'numpy_state': numpy_state,
            'torch_cpu_state': torch_cpu_state,
            'torch_cuda_state': torch_cuda_states,
            'training_started_time': timezone.localtime(self.model_analysis.created_datetime),
            'auto_weight': self.train_preparer.auto_weight,
            'weight_per_class': self.train_preparer.weight_per_class,
        }

        if constants.ARCHITECTURES_AND_BACKBONES[self.train_preparer.architecture]:
            saving_dict['backbone'] = self.train_preparer.backbone

        if self.train_preparer.architecture in constants.THREE_D_NETWORKS:
            saving_dict['input_depth'] = self.train_preparer.input_depth

        if self.train_preparer.online_aug:
            saving_dict['online_aug'] = True
            saving_dict['aug_start_epoch'] = self.train_preparer.aug_start_epoch
            saving_dict['online_aug_kwargs'] = self.train_preparer.online_aug_kwargs

        file_number = str(history[-1][0]).zfill(len(str(self.epochs)))
        file_path = os.path.join(self.train_preparer.model_folder, 'checkpoint_epoch_' + str(file_number) + '.pt')

        best_epoch_data_val_acc = sorted(history, key=lambda x: x[4], reverse=True)[0]
        best_epoch_data_val_loss = sorted(history, key=lambda x: x[2])[0]

        best_val_acc_epoch = best_epoch_data_val_acc[0]
        best_val_loss_epoch = best_epoch_data_val_loss[0]

        # removing previous files to reduce space except for last checkpoint
        epoch = history[-1][0]  # epoch start at 1 at history
        if self.epochs != epoch and epoch % self.save_every != 0:
            # only remove others if we save a new best model.
            checkpoints = get_file_paths_in_folder(self.train_preparer.model_folder, extension='.pt')
            for c in checkpoints:
                checkpoint_epoch = int(os.path.basename(c).split('checkpoint_epoch_')[1].split('.pt')[0])
                if checkpoint_epoch % self.save_every == 0:
                    # do not remove "save_every" models!
                    continue

                # keep the best validation accuracy and best validation loss epochs
                if checkpoint_epoch == best_val_acc_epoch:
                    continue
                if checkpoint_epoch == best_val_loss_epoch:
                    continue

                logging.info(f'Removing checkpoint: {c}')
                remove_file(c)
        logging.info(f'Saved checkpoint {file_path}')
        torch.save(saving_dict, file_path)

        self.model_analysis.storage_size = Decimal(round(os.stat(file_path).st_size / 1000 / 1000, 2))
        self.model_analysis.training_loss = Decimal(best_epoch_data_val_loss[1])
        self.model_analysis.validation_loss = Decimal(best_epoch_data_val_loss[2])
        self.model_analysis.training_accuracy = Decimal(best_epoch_data_val_loss[3])
        self.model_analysis.validation_accuracy = Decimal(best_epoch_data_val_loss[4])
        self.model_analysis.save()

    def start_training(self):
        history, header = self.profiler.get_history()

        epoch = 0
        best_loss = 10
        best_val_acc = 0
        start_time = time.time()
        analysis_10_epochs_start_time = 0

        if self.gpu_id is not None:
            logging.info(f'Rank {self.gpu_id}: Start time: {get_human_time_string_from_seconds(start_time, False)}')
        else:
            logging.info(f'Start time: {get_human_time_string_from_seconds(start_time, False)}')

        for epoch in range(self.start_epoch, self.epochs):
            if self.gpu_id is not None:
                logging.info(f'Rank {self.gpu_id}: Epoch: {epoch + 1}/{self.epochs}')
            else:
                logging.info(f'Epoch: {epoch + 1}/{self.epochs}')
            if self.gpu_id == 0 or self.gpu_id is None:
                if epoch == self.train_preparer.aug_start_epoch:
                    self.training_loader.dataset.online_aug_active = True
                if epoch - self.start_epoch == 2:
                    analysis_10_epochs_start_time = time.time()
                if epoch - self.start_epoch == 12:
                    self.model_analysis.training_duration_10_epochs = time.time() - analysis_10_epochs_start_time
                    self.model_analysis.save()

            if self.gpu_id is not None:
                dist.barrier()

            train_loss, train_acc = self.train_loop(epoch)
            valid_loss, valid_acc = self.valid_loop(epoch)

            if self.gpu_id == 0 or self.gpu_id is None:

                self.profiler.update(epoch + 1, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc)

                history, header = self.profiler.get_history()

                self.model_analysis.total_epochs = epoch + 1
                self.model_analysis.save()
                self.model_analysis.save_history(history, header)

                if ((epoch + 1) % self.save_every == 0 or epoch + 1 == self.epochs or valid_loss < best_loss or
                        valid_acc > best_val_acc):
                    if valid_loss < best_loss:
                        logging.info('New best model (validation loss).')
                        best_loss = valid_loss

                    if valid_acc > best_val_acc:
                        logging.info('New best model (validation accuracy).')
                        best_val_acc = valid_acc

                    self.save_checkpoint(history)

                self.show_stats(history, start_time)

            if self.gpu_id is not None:
                dist.barrier()

        end_time = time.time()
        if self.gpu_id is not None and self.gpu_id == 0:
            logging.info(f'Rank {self.gpu_id}: End time: {get_human_time_string_from_seconds(end_time, False)}')
            logging.info(f'Rank {self.gpu_id}: Seconds spent: {end_time - start_time}')
        else:
            logging.info(f'End time: {get_human_time_string_from_seconds(end_time, False)}')
            logging.info(f'Seconds spent: {end_time - start_time}')

        # store real used time, not just approximation.
        if self.gpu_id == 0 or self.gpu_id is None:
            self.model_analysis.training_duration_10_epochs = (time.time() - start_time) / \
                                                              (self.epochs - self.start_epoch) * 10
            self.model_analysis.save()
            self.model_analysis.save_history(history, header)

        if epoch >= 1 and (self.gpu_id == 0 or self.gpu_id is None):
            save_history(history, self.train_preparer.model_folder, header=header)
            show_best_five_stats(history)
