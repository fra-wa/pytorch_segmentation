import datetime
import logging

import numpy as np
import os
import psutil
import torch
import torch.multiprocessing as mp
import socket
import sys

from contextlib import closing
from django.core.exceptions import ObjectDoesNotExist
from torch.distributed import init_process_group, destroy_process_group

from .data_loading import get_dataset_mean_and_std, set_up_loaders, NotEnoughTrainingDataError
from .profiler import TrainingProfiler
from .saving import get_trained_model_folder
from .testing import run_test_loop
from .trainer import Trainer
from .training_utils import get_best_stored_checkpoint, create_online_aug_kwargs_from_db, setup_online_aug_pipe
from .. import constants
from ..dl_models.backward_compatibility import BACKWARD_COMPATIBLE_NETWORKS
from ..dl_models.loading import get_model
from ..models import LogFile, TrainingDataset, ModelAnalysis
from ..temporary_parameters.parameters import FixedParameters
from ..train_data_preprocessing.auto_weighting import get_weights
from ..train_data_preprocessing.dataset_loading import prepare_dataset
from ..utils import timezone_datetime, set_up_reproducibility, show_memory


def get_log_object():
    process = psutil.Process(os.getpid())
    log_files = LogFile.objects.filter(
        process_start_datetime=timezone_datetime(datetime.datetime.fromtimestamp(round(process.create_time()))),
        process_id=os.getpid(),
        execution_process=constants.DL_EXECUTION_TRAIN,
    )
    if log_files:
        if log_files.count() > 1:
            logging.info('Found multiple log files, live view disabled.')
            return None
        else:
            return log_files[0]
    return None


class TrainPreparer:
    def __init__(self,
                 architecture,
                 backbone,
                 norm,
                 pretrained,
                 channels,
                 dataset_name,
                 classes,
                 auto_weight,
                 weight_per_class,
                 epochs,
                 lr,
                 batch_size,
                 input_size,
                 input_depth,
                 device,
                 online_aug,
                 aug_strength,
                 aug_start_epoch,
                 reproduce,
                 save_every,
                 show_ram,
                 logger,
                 log_file_instance,
                 multi_gpu_training,
                 model_path=None,
                 num_gpus=None,
                 ):
        self.log_file_instance = log_file_instance
        self.logger = logger
        self.multi_gpu_training = multi_gpu_training

        # User training parameters
        self.architecture = architecture
        self.backbone = backbone
        self.norm = norm
        self.pretrained = pretrained
        self.channels = channels

        self.dataset_name = dataset_name
        self.classes = classes
        self.auto_weight = auto_weight
        self.weight_per_class = weight_per_class

        self.epochs = epochs
        self.lr = lr
        self.batch_size = batch_size

        self.input_size = input_size
        self.input_depth = input_depth
        self.device = torch.device(device)
        self.num_gpus = num_gpus if num_gpus is not None else 1

        self.online_aug = online_aug
        self.aug_strength = aug_strength if online_aug else 0
        self.aug_start_epoch = aug_start_epoch

        self.reproduce = reproduce
        if self.reproduce and multi_gpu_training:
            logging.warning('Multi GPU and reproducibility is not supported. Deactivating reproducibility.')
            self.reproduce = False

        self.save_every = save_every if save_every > 0 else self.epochs
        self.show_ram = show_ram

        # paths
        self.model_path = model_path
        self.model_folder = get_trained_model_folder(dataset_name, architecture, backbone, model_path)

        # default parameters or parameters that will be calculated during setup
        self.drop_last = False
        self.online_aug_kwargs = {}
        self.augmentation_pipeline = None
        self.dataset_mean = None
        self.dataset_std = None
        self.start_epoch = 0
        self.model_analysis = None

        # model, scaler, loss, ... that will be set during setup
        self.model = None
        self.loss_func = None
        if torch.device(self.device).type == 'cpu':
            self.scaler = torch.cuda.amp.GradScaler(enabled=False)
            self.amp_enabled = False
        else:
            self.scaler = torch.cuda.amp.GradScaler()
            self.amp_enabled = True
        self.gpu_id = None
        self.optimizer = None
        self.profiler = None
        self.training_started_time = None

        # DATA:
        self.training_loader = None
        self.validation_loader = None
        self.image_paths = None
        self.mask_paths = None
        self.valid_images_paths = None
        self.valid_masks_paths = None

        # SETUP
        self.setup_done = False
        self.loaders_setup_done = False
        self.database_setup_done = False

        # REPRODUCIBILITY during DDP
        self.torch_cuda_states = None

    def _setup(self):
        if self.reproduce:
            set_up_reproducibility()

        if self.show_ram:
            logging.info(f'Initial ram usage of device: {self.device}')
            show_memory(self.device)

        self._prepare_model()
        self._prepare_data()
        self._prepare_loss()

        # sanity check
        not_none_parameters = {
            'dataset_mean': self.dataset_mean,
            'dataset_std': self.dataset_std,
            'model': self.model,
            'loss_func': self.loss_func,
            'optimizer': self.optimizer,
            'profiler': self.profiler,
            'training_started_time': self.training_started_time,  # actually process start time
            'image_paths': self.image_paths,
            'mask_paths': self.mask_paths,
            'valid_images_paths': self.valid_images_paths,
            'valid_masks_paths': self.valid_masks_paths,
        }
        error_parameters = []
        for name, parameter in not_none_parameters.items():
            if parameter is None:
                error_parameters.append(name)
        if error_parameters:
            error_text = ', '.join(error_parameters)
            raise ValueError(f'Setup error! The following variables are still None: {error_text}')

        self.setup_done = True

    def _setup_continuation(self):

        checkpoint = torch.load(self.model_path, map_location=self.device)
        checkpoint_keys = checkpoint.keys()

        try:
            self.input_size = checkpoint['image_input_size']
            self.classes = checkpoint['classes']
            self.architecture = checkpoint['architecture']
            self.channels = checkpoint['image_channels']
        except KeyError as e:
            raise ValueError("This model can't be continued. Missing some keys at checkpoint") from e

        if 'backbone' in checkpoint_keys:
            self.backbone = checkpoint['backbone']
        if 'depth_3d' in checkpoint_keys or 'input_depth' in checkpoint_keys:
            try:
                self.input_depth = checkpoint['input_depth']
            except KeyError:
                # compatibility to old checkpoints
                self.input_depth = checkpoint['depth_3d']
        if 'normalization' in checkpoint_keys:
            self.norm = checkpoint['normalization']
        else:
            self.norm = 'bn'  # very old checkpoints
        if 'drop_last' in checkpoint_keys:
            self.drop_last = checkpoint['drop_last']
        if 'aug_strength' in checkpoint_keys:
            self.aug_strength = checkpoint['aug_strength']
        if 'aug_start_epoch' in checkpoint_keys:
            self.aug_start_epoch = checkpoint['aug_start_epoch']
        if 'online_aug' in checkpoint_keys:
            self.online_aug = checkpoint['online_aug']
        if 'online_aug_kwargs' in checkpoint_keys:
            self.online_aug_kwargs = checkpoint['online_aug_kwargs']
        if 'dataset_mean' in checkpoint_keys:
            self.dataset_mean = checkpoint['dataset_mean']
        if 'dataset_std' in checkpoint_keys:
            self.dataset_std = checkpoint['dataset_std']
        if 'batch_size' in checkpoint_keys:
            if self.batch_size is not None:
                if self.batch_size != checkpoint['batch_size']:
                    logging.info(
                        f'WARNING: Checkpoint was trained with a batch size of: {checkpoint["batch_size"]} '
                        f'but you defined: {self.batch_size}!'
                    )
                    logging.info(f'WARNING: Continuing with new batch size: {self.batch_size}!')
            else:
                self.batch_size = checkpoint['batch_size']

        if 'auto_weight' in checkpoint_keys:
            self.auto_weight = checkpoint['auto_weight']
            self.weight_per_class = checkpoint['weight_per_class']
        else:
            self.auto_weight = False
            self.weight_per_class = self.classes * [1.0]
            checkpoint['auto_weight'] = self.auto_weight
            checkpoint['weight_per_class'] = self.weight_per_class
            torch.save(checkpoint, self.model_path)

        if 'scaler_state_dict' in checkpoint_keys:
            self.scaler = torch.cuda.amp.GradScaler()
            self.scaler = self.scaler.load_state_dict(checkpoint["scaler_state_dict"])
        else:
            self.scaler = torch.cuda.amp.GradScaler(enabled=False)

        if self.aug_strength is None:
            logging.info('Could not find: aug_strength in checkpoint. Using default: 0')
            self.aug_strength = 0

        if self.aug_start_epoch is None:
            logging.info('Could not find: aug_start_epoch in checkpoint. Using default: 0')
            self.aug_start_epoch = 0

        if self.online_aug is None:
            logging.info('Could not find: online_aug in checkpoint. Using default: False')
            self.online_aug = False

        self.pretrained = False

        if self.reproduce and not self.multi_gpu_training:
            if 'python_state' in checkpoint_keys:
                python_state = checkpoint['python_state']
            else:
                python_state = None
            if 'numpy_state' in checkpoint_keys:
                numpy_state = checkpoint['numpy_state']
            else:
                numpy_state = None
            if 'torch_cpu_state' in checkpoint_keys:
                torch_cpu_state = checkpoint['torch_cpu_state']
            else:
                torch_cpu_state = None
            if 'torch_cuda_state' in checkpoint_keys and self.device.type == 'cuda':
                torch_cuda_state = checkpoint['torch_cuda_state']
                if isinstance(torch_cuda_state, list):
                    # single gpu is default. multi gpu will be set later. See at Trainer class.
                    torch_cuda_state = torch_cuda_state[0]
                self.torch_cuda_states = checkpoint['torch_cuda_state']
                if not isinstance(self.torch_cuda_states, list):
                    self.torch_cuda_states = [self.torch_cuda_states]
            else:
                torch_cuda_state = None

            set_up_reproducibility(python_state, numpy_state, torch_cpu_state, torch_cuda_state, self.device)

        log_parameters = [
            ('architecture', self.architecture),
            ('backbone', self.backbone),
            ('norm', self.norm),
            ('pretrained', self.pretrained),
            ('channels', self.channels),
            ('dataset_name', self.dataset_name),
            ('classes', self.classes),
            ('auto_weight', self.auto_weight),
            ('weight_per_class', self.weight_per_class),
            ('epochs', self.epochs),
            ('lr', self.lr),
            ('batch_size', self.batch_size),
            ('input_size', self.input_size),
            ('input_depth', self.input_depth),
            ('device', self.device),
            ('online_aug', self.online_aug),
            ('aug_strength', self.aug_strength),
            ('aug_start_epoch', self.aug_start_epoch),
            ('reproduce', self.reproduce),
            ('save_every', self.save_every),
            ('show_ram', self.show_ram),
            ('multi_gpu_training', self.multi_gpu_training),
            ('model_path', self.model_path),
            ('num_gpus', self.num_gpus),
        ]
        log_parameters = sorted(log_parameters, key=lambda x: x[0])
        logging.info('Loaded checkpoint with following parameters:')
        for name, value in log_parameters:
            if value is None:
                logging.info(f'{name}: /')
            else:
                logging.info(f'{name}: {value}')

        self.model = get_model(
            model_name=self.architecture, channels=self.channels, classes=self.classes,
            device=self.device, backbone=self.backbone, pretrained=False, normalization=self.norm,
            in_size=self.input_size, dataset_mean=self.dataset_mean, dataset_std=self.dataset_std
        )

        try:
            self.model.load_state_dict(checkpoint['model_state_dict'])
        except KeyError:
            if self.multi_gpu_training:
                logging.info('############# WARNING ################')
                logging.info('Multi gpu is enabled but this is an old network containing unused parameters!')
                logging.info('Go to pytorch_segmentation/pytorch_segmentation/training/trainer.py')
                logging.info('Search and adjust the following line:')
                logging.info('self.model = DDP(self.model, device_ids=[ddp_gpu_id], find_unused_parameters=False)')
                logging.info('to:')
                logging.info('self.model = DDP(self.model, device_ids=[ddp_gpu_id], find_unused_parameters=True)')
                logging.info('############# WARNING ################')
            self.model = get_model(
                model_name=self.architecture, channels=self.channels, classes=self.classes,
                device=self.device, backbone=self.backbone, pretrained=False, normalization=self.norm,
                in_size=self.input_size, dataset_mean=self.dataset_mean, dataset_std=self.dataset_std,
                model_classes=BACKWARD_COMPATIBLE_NETWORKS,
            )
            self.model.load_state_dict(checkpoint['model_state_dict'])

        self.training_started_time = None
        if 'training_started_time' in checkpoint_keys:
            self.training_started_time = checkpoint['training_started_time']

        optim_state = checkpoint['optimizer_state_dict']
        self.optimizer = torch.optim.Adam(self.model.parameters())
        self.optimizer.load_state_dict(optim_state)

        # crucial!!
        # issue: using more VRam when loading from checkpoint than starting from scratch:
        # since loading is in external func, there is no problem but the state dict is heavy!
        # https://discuss.pytorch.org/t/gpu-memory-usage-increases-by-90-after-torch-load/9213/9
        # 0/11441 MiB  --> initial
        # 6270/11441 MiB  --> loaded checkpoint
        # 6270/11441 MiB  --> set up optimizer
        # 2060/11441 MiB  --> deleted optimizer_state dict
        del optim_state
        torch.cuda.empty_cache()

        history = checkpoint['history']

        # compatibility with older checkpoints
        if len(history[0]) == 14:
            aug_strength = [line[13] for line in history]
        elif len(history[0]) == 6:
            aug_strength = [line[5] for line in history]
        else:
            aug_strength = [0.0 for line in history]

        history_dict = {
            'epochs': [line[0] for line in history],  # epoch of history starts at 1

            'train_loss': [line[1] for line in history],
            'valid_loss': [line[2] for line in history],
            'train_acc': [line[3] for line in history],
            'valid_acc': [line[4] for line in history],
            'aug_strength': aug_strength,
        }
        return history_dict

    def _prepare_data(self):
        """
        Preprocesses the data for further use.
            - Subdivision into patches
            - Dataset mean and std
            - Calculating weights regarding each class - passed to the loss.
        """
        self.image_paths, self.mask_paths, self.valid_images_paths, self.valid_masks_paths = prepare_dataset(
            self.architecture, self.dataset_name, self.input_size, self.channels, self.classes, self.logger
        )

        valid_count = len(self.valid_images_paths)
        train_count = len(self.image_paths)

        logging.info(f'Validating data size against batch size')
        if any([train_count < self.batch_size * self.num_gpus, valid_count < self.batch_size * self.num_gpus]):
            if valid_count < self.batch_size * self.num_gpus:
                data = 'Validation'
            else:
                data = 'Train'

            if self.multi_gpu_training:
                raise NotEnoughTrainingDataError(
                    f'{data} data has only {valid_count} elements. You are using {self.num_gpus} GPUs and each has a '
                    f'batch size of: {self.batch_size}. You need at least {self.num_gpus * self.batch_size} images!'
                    f'Reduce batch size accordingly!'
                )
            else:
                raise NotEnoughTrainingDataError(
                    f'{data} data has only {valid_count} elements but batch size is: {self.batch_size}. '
                    f'Reduce batch size accordingly!'
                )

        # double check here in case someone deleted the std and mean file --> we do not recalculate everything
        if self.model.dataset_mean is not None and self.model.dataset_std is not None:
            self.dataset_mean, self.dataset_std = self.model.dataset_mean, self.model.dataset_std
        else:
            self.dataset_mean, self.dataset_std = get_dataset_mean_and_std(
                self.dataset_name, self.input_size, self.channels, logger=self.logger
            )

        if self.auto_weight:
            dataset_folder = os.path.join(constants.DATASET_FOLDER, self.dataset_name)
            self.weight_per_class = get_weights(
                dataset_folder, self.mask_paths, self.classes, self.input_size, logger=self.logger
            )
        else:
            self.weight_per_class = np.ones(self.classes)

    def _prepare_loss(self):
        weights = self.weight_per_class
        if weights is None:
            raise ValueError('No weights passed to loss!')
        if not isinstance(weights, np.ndarray):
            if isinstance(weights, list):
                weights = np.array(weights)
            else:
                raise ValueError('params.weight_per_class must be a list or np.ndarray!')
        weights = torch.from_numpy(weights)

        if self.classes == 2:
            # pos_weight: weighting the positives --> weight of ones, weight of zeros = 1
            if not self.auto_weight:
                weights = [None, None]
            loss = torch.nn.BCEWithLogitsLoss(pos_weight=weights[1]).to(self.device)
        else:
            if not self.auto_weight:
                weights = None
            loss = torch.nn.CrossEntropyLoss(weight=weights.float(), reduction='mean').to(self.device)
        self.loss_func = loss

    def _prepare_model(self):
        """
        call after data preparation!
        prepares all model related parameters like loss
        """
        history_dict = {
            'epochs': [],  # epoch of history will start at 1, noz 0
            'train_loss': [],
            'valid_loss': [],
            'train_acc': [],
            'valid_acc': [],
            'aug_strength': [],
        }
        if self.model_path:
            history_dict = self._setup_continuation()  # continuing -> model, scaler etc. are set there
            self.start_epoch = history_dict['epochs'][-1]
            self.epochs += self.start_epoch
        else:
            if self.online_aug:
                self.online_aug_kwargs = create_online_aug_kwargs_from_db(self.architecture)
                self.aug_strength = self.online_aug_kwargs['global_strength']
                self.aug_start_epoch = self.online_aug_kwargs['online_aug_start_epoch']
            self.model = get_model(
                model_name=self.architecture, channels=self.channels, classes=self.classes,
                device=self.device, backbone=self.backbone, pretrained=self.pretrained,
                normalization=self.norm, in_size=self.input_size, log_information=True
            )
            if self.lr is not None:
                self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.lr)
            else:
                self.optimizer = torch.optim.Adam(self.model.parameters())

            process = psutil.Process(os.getpid())
            process_started_timestamp = round(process.create_time())
            self.training_started_time = timezone_datetime(datetime.datetime.fromtimestamp(process_started_timestamp))

        self.profiler = TrainingProfiler(
            history_dict=history_dict,
            aug_strength=self.aug_strength if self.online_aug else 0,
            aug_start_epoch=self.aug_start_epoch if self.online_aug else 0,
        )

        if self.online_aug:
            logging.info('#####################################################')
            logging.info('Using the following online aug parameters:')
            for key, value in self.online_aug_kwargs.items():
                logging.info(f'Parameter: {key}: {value}')
            logging.info('#####################################################')

        logging.info(
            f'The model has {round(self.model.get_parameter_count() / 1000000, 1)} mio parameters '
            f'({self.model.get_parameter_count()}).'
        )
        if self.show_ram:
            logging.info(f'Ram usage after model and parameter loading of device: {self.device}')
            show_memory(self.device)

    def _setup_loaders(self, rank, max_workers=None):
        if not self.setup_done:
            raise RuntimeError('Call prepare_data first!')

        self.training_loader, self.validation_loader = set_up_loaders(
            image_paths=self.image_paths,
            mask_paths=self.mask_paths,
            architecture=self.architecture,
            channels=self.channels,
            classes=self.classes,
            batch_size=self.batch_size,
            reproduce=self.reproduce,
            online_aug=self.online_aug,
            input_size=self.input_size,
            input_depth=self.input_depth,
            valid_images_paths=self.valid_images_paths,
            valid_masks_paths=self.valid_masks_paths,
            dataset_mean=self.dataset_mean,
            dataset_std=self.dataset_std,
            multi_gpu_training=self.multi_gpu_training,
            num_gpus=self.num_gpus,
            max_workers=max_workers,  # on hpc, too many workers may fail.
            log_info=True if rank == 0 else False,
        )

        if self.online_aug:
            self.augmentation_pipeline = setup_online_aug_pipe(
                self.online_aug_kwargs, self.architecture, self.input_size, self.input_depth
            )

        self.training_loader.dataset.aug_pipeline = self.augmentation_pipeline
        if self.start_epoch >= self.aug_start_epoch:
            self.training_loader.dataset.online_aug_active = True

        self.loaders_setup_done = True

    def _setup_database(self):
        if not self.setup_done:
            raise RuntimeError('Call prepare_data first!')
        if not self.loaders_setup_done:
            raise RuntimeError('Call _setup_loaders first or use prepare_loaders!')

        if not isinstance(self.training_started_time, datetime.datetime):
            raise ValueError('training_started_time needs to be a tz aware datetime object')
        if self.training_started_time.tzinfo is None:
            raise ValueError('training_started_time needs to be a tz aware datetime object')

        try:
            dataset = TrainingDataset.objects.get(folder_name_on_device=self.dataset_name)
            if dataset.number_training_images != len(self.training_loader.dataset):
                dataset.number_training_images = len(self.training_loader.dataset)
                dataset.save()
            elif dataset.number_validation_images != len(self.validation_loader.dataset):
                dataset.number_validation_images = len(self.validation_loader.dataset)
                dataset.save()
            logging.info('Found database training dataset instance.')
        except (ObjectDoesNotExist, ValueError):
            dataset = TrainingDataset.objects.create(
                name=self.dataset_name,
                folder_name_on_device=self.dataset_name,
                number_training_images=len(self.training_loader.dataset),
                number_validation_images=len(self.validation_loader.dataset),
            )
            logging.info('Created new database training dataset instance.')

        try:
            model_analysis = ModelAnalysis.objects.filter(
                architecture=self.architecture,
                backbone=self.backbone,
                channels=self.channels,
                classes=self.classes,
                input_height=self.input_size,
                input_width=self.input_size,
                input_depth=self.input_depth,
                online_aug_kwargs_string=str(self.online_aug_kwargs),
                created_datetime=self.training_started_time,  # this should be enough to filter correctly locally
            )[0]
            if not model_analysis.normalization:
                model_analysis.normalization = self.norm
                model_analysis.save()
            logging.info('Found related analysis entry, continuing.')
        except IndexError:
            model_analysis = ModelAnalysis.objects.create(
                created_datetime=self.training_started_time,
                architecture=self.architecture,
                backbone=self.backbone,
                dataset=dataset,
                total_epochs=0,
                batch_size=self.batch_size,
                channels=self.channels,
                classes=self.classes,
                input_height=self.input_size,
                input_width=self.input_size,
                input_depth=self.input_depth if self.architecture in constants.THREE_D_NETWORKS else None,
                online_aug_kwargs_string=str(self.online_aug_kwargs),
                pretrained=self.pretrained if self.backbone else None,
                normalization=constants.NORMALIZATION_DICT[self.norm],
            )
            logging.info('Created analysis database entry.')

        if self.log_file_instance is not None:
            self.log_file_instance.model_analysis = model_analysis
            self.log_file_instance.save()
        else:
            logging.info('Could not find the related log file object. No live view available.')

        self.model_analysis = model_analysis
        self.database_setup_done = True

    def prepare_data(self):
        self._setup()

    def prepare_loaders(self, rank, max_workers=None):
        self._setup_loaders(rank, max_workers)
        if rank == 0:
            self._setup_database()


def ddp_setup(rank, world_size, port):
    """
    init ddp process group - each gpu runs 1 process
    this is needed for them to communicate

    Args:
        rank: unique identifier for each process, from 0 to world_size - 1
        world_size: total number of processes - each gpu should have one
        port: local hosts port
    """

    os.environ["MASTER_ADDR"] = "127.0.0.1"
    os.environ["MASTER_PORT"] = port

    if sys.platform == 'win32':
        init_process_group(backend='gloo', rank=rank, world_size=world_size)
    else:
        init_process_group(backend='nccl', rank=rank, world_size=world_size)


def init_multi_gpu_training(rank, world_size, queue, train_preparer, process_logger, port, max_workers):
    process_logger.set_up_root_logger()  # Set up the root logger to send messages to events queue
    torch.cuda.set_device(rank)
    ddp_setup(rank, world_size, port)
    train_preparer.prepare_loaders(rank=rank, max_workers=max_workers)
    trainer = Trainer(train_preparer, multi_gpu_training=True)
    trainer.reseed()
    trainer.start_training()
    if rank == 0:
        queue.put((trainer.profiler, trainer.model_analysis.pk))
    destroy_process_group()


def find_free_port():
    """ https://stackoverflow.com/questions/1365265/on-localhost-how-do-i-pick-a-free-port-number """

    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return str(s.getsockname()[1])


def log_fixed_training_parameters(parameters):
    if not isinstance(parameters, FixedParameters):
        raise ValueError('parameters must be a FixedParameters instance')

    log_parameters = [
        ('architecture', parameters.architecture),
        ('backbone', parameters.backbone),
        ('norm', parameters.norm),
        ('pretrained', parameters.pretrained),
        ('channels', parameters.channels),
        ('dataset_name', parameters.dataset_name),
        ('classes', parameters.classes),
        ('auto_weight', parameters.auto_weight),
        ('weight_per_class', parameters.weight_per_class),
        ('epochs', parameters.epochs),
        ('lr', parameters.lr),
        ('batch_size', parameters.batch_size),
        ('input_size', parameters.input_size),
        ('input_depth', parameters.input_depth),
        ('device', parameters.device),
        ('online_aug', parameters.online_aug),
        ('aug_strength', parameters.aug_strength),
        ('aug_start_epoch', parameters.aug_start_epoch),
        ('reproduce', parameters.reproduce),
        ('save_every', parameters.save_every),
        ('show_ram', parameters.show_ram),
        ('multi_gpu_training', parameters.multi_gpu_training),
        ('model_path', parameters.model_path),
        ('num_gpus', parameters.num_gpus),
    ]

    log_parameters = sorted(log_parameters, key=lambda x: x[0])

    for name, value in log_parameters:
        if value is None:
            logging.info(f'{name}: /')
        else:
            logging.info(f'{name}: {value}')


def start_training(parameters, logger=None, execute_test=True):
    """

    Args:
        parameters: Parameters instance (user interaction)
        logger: needed for multiprocessing
        execute_test: whether to execute the automatic test at the end of a training

    Returns:

    """

    if not isinstance(parameters, FixedParameters):
        raise ValueError('Parameters must be FixedParameters instance.')

    if not parameters.model_path:
        log_fixed_training_parameters(parameters)

    # outside ddp processes
    log_file = get_log_object()

    if parameters.multi_gpu_training:
        logging.info(f'Preparing model training on {parameters.num_gpus} GPUs.')
    train_preparer = TrainPreparer(
        architecture=parameters.architecture,
        backbone=parameters.backbone,
        norm=parameters.norm,
        pretrained=parameters.pretrained,
        channels=parameters.channels,
        dataset_name=parameters.dataset_name,
        classes=parameters.classes,
        auto_weight=parameters.auto_weight,
        weight_per_class=parameters.weight_per_class,
        epochs=parameters.epochs,
        lr=parameters.lr,
        batch_size=parameters.batch_size,
        input_size=parameters.input_size,
        input_depth=parameters.input_depth,
        device=parameters.device,
        online_aug=parameters.online_aug,
        aug_strength=parameters.aug_strength,
        aug_start_epoch=parameters.aug_start_epoch,
        reproduce=parameters.reproduce,
        save_every=parameters.save_every,
        show_ram=parameters.show_ram,
        logger=logger,
        log_file_instance=log_file,
        multi_gpu_training=parameters.multi_gpu_training,
        model_path=parameters.model_path,  # if this is not none, continuation is triggered
        num_gpus=parameters.num_gpus,
    )

    train_preparer.prepare_data()
    world_size = parameters.num_gpus

    if parameters.multi_gpu_training:
        port = find_free_port()
        manager = mp.Manager()
        queue = manager.Queue()
        if parameters.num_gpus < 4:
            max_workers = 4
        elif parameters.num_gpus < 8:
            max_workers = 2
        else:
            max_workers = 1

        mp.spawn(
            init_multi_gpu_training,
            args=(world_size, queue, train_preparer, logger, port, max_workers),
            nprocs=world_size,
            join=True,
        )
        profiler, analysis_pk = queue.get()[:]

    else:
        train_preparer.prepare_loaders(rank=0)
        trainer = Trainer(train_preparer)
        trainer.start_training()

        profiler = trainer.profiler
        analysis_pk = trainer.model_analysis.pk

    if execute_test:
        if parameters.model_path:
            parameters.input_size = train_preparer.input_size
            parameters.input_depth = train_preparer.input_depth
            parameters.architecture = train_preparer.architecture
            parameters.channels = train_preparer.channels
            parameters.classes = train_preparer.classes
            parameters.batch_size = train_preparer.batch_size
            parameters.device = train_preparer.device

        history, header = profiler.get_history()

        model_analysis = ModelAnalysis.objects.get(pk=analysis_pk)

        best_saved_checkpoint, best_saved_epoch_data = get_best_stored_checkpoint(
            train_preparer.model_folder, history, model_analysis
        )
        model_analysis.refresh_from_db()

        testing_dir = os.path.join(constants.DATASET_FOLDER, parameters.dataset_name, 'test')

        if os.path.isdir(testing_dir) and best_saved_checkpoint is not None:
            storage = run_test_loop(parameters, best_saved_checkpoint, testing_dir, logger=logger)
            if storage is not None:
                model_analysis.save_test_storage(storage, best_saved_epoch_data[0])

        model_analysis.save()

