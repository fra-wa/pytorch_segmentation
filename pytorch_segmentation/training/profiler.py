class TrainingProfiler:
    def __init__(self,
                 history_dict=None,
                 aug_strength=None,
                 aug_start_epoch=None,
                 ):
        """
        Args:
            history_dict: Recent training history.
                Shape: [[epoch + 1, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc], ...]
            aug_strength: Augmentation strength (global)
            aug_start_epoch: Epoch, from which the online augmentation is activated. Currently deprecated
        """
        if aug_start_epoch is not None and aug_start_epoch < 0:
            raise ValueError('augmentation start epoch must be 0 or greater')

        self.epochs = []

        self.train_losses = []
        self.valid_losses = []
        self.train_accuracies = []
        self.valid_accuracies = []

        self.augmentation_strengths = []

        self.aug_start_epoch = aug_start_epoch
        self.aug_strength = aug_strength

        self.history = history_dict

        if self.history is not None and self.history['epochs']:
            self.epochs = history_dict['epochs']

            self.train_losses = history_dict['train_loss']
            self.valid_losses = history_dict['valid_loss']
            self.train_accuracies = history_dict['train_acc']
            self.valid_accuracies = history_dict['valid_acc']

            self.augmentation_strengths = history_dict['aug_strength']

    def update(self, epoch, train_loss, val_loss, train_acc, val_acc):
        self.epochs.append(epoch)
        self.train_losses.append(train_loss)
        self.valid_losses.append(val_loss)
        self.train_accuracies.append(train_acc)
        self.valid_accuracies.append(val_acc)

        self.augmentation_strengths.append(self.aug_strength)

    def get_history(self):
        history = list(zip(
            self.epochs,

            self.train_losses,
            self.valid_losses,
            self.train_accuracies,
            self.valid_accuracies,

            self.augmentation_strengths,
        ))

        header = f'epochs, train_loss, val_loss, train_acc, val_acc, augmentation_strengths'

        return history, header
