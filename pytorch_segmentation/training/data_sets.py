import cv2
import logging
import os

import numpy as np
import torch

from torch.utils.data.dataset import Dataset

from .transformations import get_gray_image_transformation
from .transformations import get_rgb_image_transformation

from pytorch_segmentation import constants


class Dataset2D(Dataset):
    def __init__(self, img_paths, mask_paths, channels, dataset_mean=None, dataset_std=None):
        """

        Args:
            img_paths:
            mask_paths:
            channels: 1 for gray, 3 rgb, ..
        """
        if channels not in [1, 3]:
            raise ValueError('Channels must be 1 or 3')
        self.channels = channels

        self.img_paths = img_paths
        self.mask_paths = mask_paths

        self.aug_pipeline = None
        self.online_aug_active = False

        if len(self.img_paths) != len(self.mask_paths):
            raise RuntimeError(f'Length of images ({len(img_paths)}) and masks ({len(mask_paths)}) is not equal!')

        for img, mask in zip(img_paths, mask_paths):
            try:
                assert os.path.isfile(img)
                assert os.path.isfile(mask)
            except AssertionError as e:
                logging.info(f'\nFailed at img: \n{img}\nmask:\n{mask}.\nAt least one file is missing.')
                raise AssertionError('Could not find some files') from e

        if channels == 1:
            self.img_transform = get_gray_image_transformation(dataset_mean, dataset_std)
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation(dataset_mean, dataset_std)
        else:
            raise NotImplementedError(f'{channels} channels are not supported.')

    def __getitem__(self, i):
        img_path = self.img_paths[i]
        mask_path = self.mask_paths[i]
        if self.channels == 1:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        elif self.channels == 3:
            img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

        if self.aug_pipeline is not None and self.online_aug_active:
            img, mask = self.aug_pipeline(img, mask)

        if self.channels == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = self.img_transform(img)
        mask = torch.tensor(mask)

        return img, mask

    def __len__(self):
        return len(self.img_paths)


class Dataset3D(Dataset):
    """
    Dataset for volumes or time sequences
    """

    def __init__(self,
                 input_paths,
                 target_paths,
                 classes,
                 channels,
                 load_c_d_h_w=True,  # true for volumes, false for sequences
                 dataset_mean=None,
                 dataset_std=None,
                 ):
        """

        Args:
            input_paths:
            target_paths:
            classes:
            channels:
            load_c_d_h_w:
                Volumes: channels, depth, height, width [C, D, H, W]
                Sequences: frames, channels, height, width [F, C, H, W]
            dataset_mean: mean of the actual dataset. If None, a default mean will be used
            dataset_std: standard deviation of the dataset. If None, a default std will be used
        """

        if not isinstance(input_paths, list) or not isinstance(target_paths, list):
            raise ValueError('Images and mask paths need to be lists')

        if not isinstance(input_paths[0], list) or not isinstance(target_paths[0], list):
            raise ValueError('Images and mask need to contain sublist representing volumes')

        if channels not in [1, 3]:
            raise ValueError('Channels must be 1 or 3')

        if dataset_mean is not None and not isinstance(dataset_mean, list):
            raise ValueError('mean must be a list')

        if dataset_std is not None and not isinstance(dataset_std, list):
            raise ValueError('std must be a list')

        self.channels = channels
        self.classes = classes

        self.inputs = input_paths
        self.targets = target_paths
        self.load_C_D_H_W = load_c_d_h_w

        self.aug_pipeline = None
        self.online_aug_active = False

        if channels == 1:
            self.img_transform = get_gray_image_transformation()
            self.mean = constants.DEFAULT_GRAY_MEAN if dataset_mean is None else dataset_mean
            self.std = constants.DEFAULT_GRAY_STD if dataset_std is None else dataset_std
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation()
            # rgb
            self.mean = constants.DEFAULT_RGB_MEAN if dataset_mean is None else dataset_mean
            self.std = constants.DEFAULT_RGB_STD if dataset_std is None else dataset_std
        else:
            raise NotImplementedError(f'Can not load images with {channels} channels.')

    def __len__(self):
        return len(self.inputs)

    def normalize_and_to_tensor(self, sequence_or_volume):
        """
        converts sequence or volume from bgr to rgb
        normalizes the volume against global values (important!)
        converts to tensor --> shape: [D, H, W, C] it's up to you to change that!
        """
        if self.channels == 3:
            # normalize and to rgb format
            normalized = np.zeros_like(sequence_or_volume, dtype=np.float32)
            normalized[:, :, :, 0] = (sequence_or_volume[:, :, :, 2] - self.mean[0]) / self.std[0]
            normalized[:, :, :, 1] = (sequence_or_volume[:, :, :, 1] - self.mean[1]) / self.std[1]
            normalized[:, :, :, 2] = (sequence_or_volume[:, :, :, 0] - self.mean[2]) / self.std[2]

        else:
            normalized = ((sequence_or_volume - self.mean[0]) / self.std[0]).astype(np.float32)
            normalized = np.expand_dims(normalized, 3)

        normalized = torch.from_numpy(normalized)
        return normalized

    def __getitem__(self, item):

        input_volume = []
        if self.channels == 1:
            for image in self.inputs[item]:
                input_volume.append(cv2.imread(image, cv2.IMREAD_GRAYSCALE))
        else:
            for image in self.inputs[item]:
                input_volume.append(cv2.imread(image, cv2.IMREAD_COLOR))

        target_volume = []
        for mask in self.targets[item]:
            target_volume.append(cv2.imread(mask, cv2.IMREAD_GRAYSCALE))

        input_volume = np.array(input_volume)  # d, h, w, c
        target_volume = np.array(target_volume)  # d, h, w

        if self.aug_pipeline is not None and self.online_aug_active:
            input_volume, target_volume = self.aug_pipeline(input_volume, target_volume)

        if self.channels == 3:
            # bgr to rgb
            input_volume = input_volume[..., ::-1]

        input_volume = self.normalize_and_to_tensor(input_volume)
        # input_volume is of shape: d, h, w, c. we want: c, d, h, w or f, c, h, w

        if self.load_C_D_H_W:
            # [D, H, W, C] --> [C, D, H, W]
            input_volume = input_volume.permute(3, 0, 1, 2)
        else:
            # [D, H, W, C] --> [F, C, H, W]
            input_volume = input_volume.permute(0, 3, 1, 2)

        return input_volume, target_volume
