import logging
import os

from .. import constants
from ..data_augmentation.online_augmentation_pipelines import ImageAugmentationPipeline
from ..data_augmentation.online_augmentation_pipelines import VolumeAugmentationPipeline
from ..file_handling.utils import get_file_paths_in_folder
from ..models import OnlineImageAugParameters, OnlineVolumeAugParameters


def get_best_stored_checkpoint(model_dir, history, model_analysis):
    """
    Depending on the valid loss and stored checkpoints

    Args:
        model_dir:
        history:
        model_analysis:

    Returns: checkpoint, related_data
    """
    checkpoints = get_file_paths_in_folder(model_dir, extension='.pt')
    if not checkpoints:
        return None
    # named like: checkpoint_epoch_xyz.pt --> split at _, select xyz.pt, split, profit
    checkpoint_epochs = [int(os.path.basename(checkpoint).split('_')[-1].split('.')[0]) for checkpoint in checkpoints]

    checkpoint_indexes = [epoch - 1 for epoch in checkpoint_epochs]
    reduced_history = [history[index] for index in checkpoint_indexes]
    best_stored_epoch_loss = sorted(reduced_history, key=lambda x: x[2])[0]
    best_actual_epoch_loss = sorted(history, key=lambda x: x[2])[0]

    best_stored_epoch_acc = sorted(reduced_history, key=lambda x: x[4], reverse=True)[0]
    best_actual_epoch_acc = sorted(history, key=lambda x: x[4], reverse=True)[0]

    model_analysis.best_epoch = best_actual_epoch_loss[0]
    model_analysis.save()

    logging.info(f'Best epoch (loss): {best_actual_epoch_loss[0]}')
    logging.info(f'Best epoch (accuracy): {best_actual_epoch_acc[0]}')
    if best_stored_epoch_loss[0] != best_actual_epoch_loss[0]:
        logging.info(f'Best saved epoch (loss): {best_stored_epoch_loss[0]}')
    if best_stored_epoch_acc[0] != best_actual_epoch_acc[0]:
        logging.info(f'Best saved epoch (accuracy): {best_stored_epoch_acc[0]}')
    logging.info(f'Values of best epoch (loss): {best_stored_epoch_loss[0]}')
    logging.info(
        f'train_loss: {best_stored_epoch_loss[1]:.6f}, valid_loss: {best_stored_epoch_loss[2]:.4f}, '
        f'train_acc: {best_stored_epoch_loss[3]:.6f}, valid_acc {best_stored_epoch_loss[4]:.4f}'
    )
    logging.info(f'This checkpoint (loss) should be treated as the best overall model of your training!')
    epochs = history[-1][0]
    file_number = str(best_stored_epoch_loss[0]).zfill(len(str(epochs)))
    file_name = f'checkpoint_epoch_{file_number}.pt'
    checkpoint_path = os.path.join(model_dir, file_name)
    if not os.path.isfile(checkpoint_path):
        raise RuntimeError('Mistakes were made')

    return checkpoint_path, best_stored_epoch_loss


def create_online_aug_kwargs_from_db(architecture):
    if architecture in constants.TWO_D_NETWORKS:
        if OnlineImageAugParameters.objects.count() == 0:
            OnlineImageAugParameters.objects.create()
        online_aug_parameters = OnlineImageAugParameters.objects.last()
    else:
        if OnlineVolumeAugParameters.objects.count() == 0:
            OnlineVolumeAugParameters.objects.create()
        online_aug_parameters = OnlineVolumeAugParameters.objects.last()

    fields = online_aug_parameters._meta.fields
    online_aug_kwargs = {}
    for field in fields:
        if field.attname == 'id':
            continue
        online_aug_kwargs[field.attname] = online_aug_parameters.__getattribute__(field.attname)

    return online_aug_kwargs


def setup_online_aug_pipe(online_aug_kwargs, architecture, input_size, input_depth=None, log_info=True):
    global_strength = online_aug_kwargs['global_strength'] / 100
    max_augmentations = online_aug_kwargs['max_augmentations']
    max_rot_angle = online_aug_kwargs['max_rot_angle']
    resizing_scale = (
        float(online_aug_kwargs['resizing_scale_lower']),
        float(online_aug_kwargs['resizing_scale_upper'])
    )
    squeeze_scale = (
        float(online_aug_kwargs['squeeze_scale_lower']),
        float(online_aug_kwargs['squeeze_scale_upper'])
    )
    tilting_start_end_factor = (
        float(online_aug_kwargs['tilting_start_factor']),
        float(online_aug_kwargs['tilting_end_factor'])
    )

    if architecture in constants.TWO_D_NETWORKS:
        augmentation_pipeline = ImageAugmentationPipeline(
            policy=online_aug_kwargs['policy'],
            expected_input_size=input_size,
            global_strength=global_strength,
            max_augmentations=max_augmentations,
            max_rot_angle=max_rot_angle,
            resizing_scale=resizing_scale,
            squeeze_scale=squeeze_scale,
            tilting_start_end_factor=tilting_start_end_factor,

            brightness_prob=online_aug_kwargs['brightness_prob'] / 100,
            contrast_prob=online_aug_kwargs['contrast_prob'] / 100,
            gaussian_filter_prob=online_aug_kwargs['gaussian_filter_prob'] / 100,
            gaussian_noise_prob=online_aug_kwargs['gaussian_noise_prob'] / 100,
            random_erasing_prob=online_aug_kwargs['random_erasing_prob'] / 100,
            channel_shuffle_prob=online_aug_kwargs['channel_shuffle_prob'] / 100,
            color_to_hsv_prob=online_aug_kwargs['color_to_hsv_prob'] / 100,
            iso_noise_prob=online_aug_kwargs['iso_noise_prob'] / 100,
            random_fog_prob=online_aug_kwargs['random_fog_prob'] / 100,
            random_rain_prob=online_aug_kwargs['random_rain_prob'] / 100,
            random_shadow_prob=online_aug_kwargs['random_shadow_prob'] / 100,
            random_snow_prob=online_aug_kwargs['random_snow_prob'] / 100,
            random_sun_flair_prob=online_aug_kwargs['random_sun_flair_prob'] / 100,
            rgb_shift_prob=online_aug_kwargs['rgb_shift_prob'] / 100,
            solarize_prob=online_aug_kwargs['solarize_prob'] / 100,
            to_gray_prob=online_aug_kwargs['to_gray_prob'] / 100,
            to_sepia_prob=online_aug_kwargs['to_sepia_prob'] / 100,

            elastic_distortion_prob=online_aug_kwargs['elastic_distortion_prob'] / 100,
            flip_prob=online_aug_kwargs['flip_prob'] / 100,
            grid_distortion_prob=online_aug_kwargs['grid_distortion_prob'] / 100,
            optical_distortion_prob=online_aug_kwargs['optical_distortion_prob'] / 100,
            random_crop_prob=online_aug_kwargs['random_crop_prob'] / 100,
            resize_prob=online_aug_kwargs['resize_prob'] / 100,
            rotate_by_angle_prob=online_aug_kwargs['rotate_by_angle_prob'] / 100,
            tilt_prob=online_aug_kwargs['tilt_prob'] / 100,
            squeeze_prob=online_aug_kwargs['squeeze_prob'] / 100,
            grid_shuffle_prob=online_aug_kwargs['grid_shuffle_prob'] / 100,
        )
    else:
        if input_depth is None:
            raise ValueError('Input depth cannot be None!')
        augmentation_pipeline = VolumeAugmentationPipeline(
            policy=online_aug_kwargs['policy'],
            expected_input_size=input_size,
            expected_input_depth=input_depth,
            global_strength=global_strength,
            max_augmentations=max_augmentations,
            max_rot_angle=max_rot_angle,
            resizing_scale=resizing_scale,
            squeeze_scale=squeeze_scale,
            tilting_start_end_factor=tilting_start_end_factor,
            depth_flip_allowed=online_aug_kwargs['depth_flip_allowed'],

            add_blur_3d_prob=online_aug_kwargs['add_blur_3d_prob'] / 100,
            add_noise_3d_prob=online_aug_kwargs['add_noise_3d_prob'] / 100,
            brightness_3d_prob=online_aug_kwargs['brightness_3d_prob'] / 100,
            contrast_3d_prob=online_aug_kwargs['contrast_3d_prob'] / 100,
            random_erasing_3d_prob=online_aug_kwargs['random_erasing_3d_prob'] / 100,
            sharpen_3d_with_blur_prob=online_aug_kwargs['sharpen_3d_with_blur_prob'] / 100,
            random_shadow_3d_prob=online_aug_kwargs['random_shadow_3d_prob'] / 100,

            flip_prob=online_aug_kwargs['flip_prob'] / 100,
            random_crop_3d_prob=online_aug_kwargs['random_crop_3d_prob'] / 100,
            random_resize_3d_prob=online_aug_kwargs['random_resize_3d_prob'] / 100,
            rotate_z_3d_prob=online_aug_kwargs['rotate_z_3d_prob'] / 100,
            squeeze_prob=online_aug_kwargs['squeeze_prob'] / 100,
            tilt_prob=online_aug_kwargs['tilt_prob'] / 100,
        )

        if log_info:
            logging.info('-------------------------')
            logging.info('Volume Augmentation is active!')
            logging.info(
                'Attention: to get the best results, '
                'ROTATE BEFORE: rotate initial data by 90, 180 and 270 degrees around x and y'
            )
            logging.info('You can use the Volume Augmentation Tool and skip all other augmentations.')
            logging.info('Explanation:')
            logging.info('The online augmentation only rotates around z since the depth of a network might be totally '
                         'different from height and width.')
            logging.info('h and w are most likely a constraint by a networks architecture')
            logging.info('e.g. UNet3D--> h and w must be divisible')
            logging.info("by two**n (n = depth of UNet3D -> 64/(2**4) for depth = 4)) and the depth isn't.")
            logging.info('The depth might be 16, 20, 32, 40, ... and is still valid.')
            logging.info('-------------------------')

    return augmentation_pipeline
