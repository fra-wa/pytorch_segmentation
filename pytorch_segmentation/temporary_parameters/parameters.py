"""
Deprecated but still needed for hpc. Todo: remove and use django parameters
"""
try:
    import logging
    import os

    from pytorch_segmentation import constants
    from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, get_sub_folders
    from pytorch_segmentation.temporary_parameters.utils import confirm_input_by_type

except ImportError:
    import logging
    import os
    import sys

    current_dir = os.path.dirname(os.path.abspath(__file__))
    module_dir = os.path.dirname(current_dir)
    sys.path.extend([module_dir])

    from pytorch_segmentation import constants
    from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, get_sub_folders
    from pytorch_segmentation.temporary_parameters.utils import confirm_input_by_type


def confirm_training_dataset_exists(folder_name):
    """
    Confirms that the training dataset contains images and masks or if
    multiple training datasets are given, every dataset contains images and masks

    Args:
        folder_name: dataset name like "some_folder_name_with_sub_folders" folder name!! easier for user!

    Returns: Folder path or False

    """
    print('Confirming dataset...')
    training_folder = os.path.join(constants.DATASET_FOLDER, folder_name)
    if not os.path.exists(training_folder):
        print(f'The training folder at {training_folder}\ndoes not exist, try again or press ctrl + c to abort.')
        return False

    image_and_mask_folders = get_sub_folders(training_folder, depth=1)

    if not image_and_mask_folders:
        print('There are no images and masks. Please read readme first')
        return False

    if len(image_and_mask_folders) < 2:
        print('The training folder must contain two sub folders! Please read Readme first.')
        return False

    image_sub_folders = get_sub_folders(os.path.join(training_folder, 'images'), depth=1)
    mask_sub_folders = get_sub_folders(os.path.join(training_folder, 'masks'), depth=1)

    if image_sub_folders or mask_sub_folders:
        if len(image_sub_folders) != len(mask_sub_folders):
            print('Amount of image and mask folder is inconsistent!')
            if len(image_sub_folders) > len(mask_sub_folders):
                print('There are more image folders')
            else:
                print('There are more mask folders')

        for img_sub_folder, mask_sub_folder in zip(image_sub_folders, mask_sub_folders):

            images = get_file_paths_in_folder(img_sub_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
            masks = get_file_paths_in_folder(mask_sub_folder, extension=constants.SUPPORTED_MASK_TYPES)

            if len(images) != len(masks):
                print(f'The dataset of {os.path.basename(images)} is inconsistent! '
                      f'Masks and images are not of equal length.')
                return False

            if not images or not masks:
                print(f'Masks and/or images of dataset {os.path.basename(images)} are missing.')
                return False
    else:
        images = get_file_paths_in_folder(
            os.path.join(training_folder, 'images'),
            extension=constants.SUPPORTED_INPUT_IMAGE_TYPES
        )
        masks = get_file_paths_in_folder(
            os.path.join(training_folder, 'masks'),
            extension=constants.SUPPORTED_MASK_TYPES
        )
        if len(images) != len(masks):
            print('The dataset is inconsistent! Masks and images are not of equal length.')
            return False

        if not images or not masks:
            print('Masks and/or images are empty folders.')
            return False

    print('Confirmed, data is valid.')
    return folder_name


class FixedParameters:
    """
    Class to store fixed parameters after user confirmation for easier usage
    This is old code, refactoring would be needed to fit django but no time. Todo!
    """
    # training parameters given by user
    # adaptive_aug = None  # impact not enough to justify using this
    architecture = None
    aug_strength = None
    aug_start_epoch = None
    auto_train_stop = None
    backbone = None
    batch_size = None
    use_gru = None
    pretrained = None
    channels = None
    classes = None
    dataset_name = None
    input_depth = None
    device = None
    multi_gpu_training = None
    num_gpus = None
    epochs = None
    input_size = None
    lr = None
    norm = None
    online_aug = None
    reproduce = None
    rnn_sequence_size = None
    save_every = None
    show_ram = None
    gpu = None
    auto_weight = None
    weight_per_class = None

    # volume augmentation parameters given by user
    # classes = None
    function_names_as_folder = None
    images_folder = None
    masks_folder = None
    dataset_folder = None
    max_augmentations = None
    total_augmentations = None
    min_dimension_x = None
    min_dimension_y = None
    min_dimension_z = None
    out_folder = None
    rotation_angle = None
    skip_list = None

    # image augmentation parameters given by user
    # channels = None
    # classes = None
    colored_masks = None
    # images_folder = None
    # masks_folder = None
    # max_augmentations = None
    # total_augmentations = None
    min_dimension = None
    # out_folder = None
    show_details = None
    use_multicore = None

    # prediction parameters set by user or stored at checkpoint
    # batch_size = None
    data_folder = None
    # input_depth = None
    # device = None
    # input_size = None
    model_path = None
    nice_color = None
    save_color = None
    overlap = None
    log_thresh = None  # training used 0.5
    weighting_strategy = None

    # dataset related and for prediction. stored at checkpoint
    dataset_mean = None
    dataset_std = None

    def __init__(self):
        pass

    def get_attribute_name_list(self):
        return sorted([attr for attr in dir(self) if not attr.startswith('__') and not callable(getattr(self, attr))])

    def show_parameters(self):
        frame_string = '#####################################################'
        logging.info('')
        logging.info(frame_string)

        attribute_list = self.get_attribute_name_list()
        # for console usage:
        # longest_attribute = max(
        #     [len(attr_name) for attr_name in attribute_list if self.__getattribute__(attr_name) is not None]
        # )  # might be used to align everything but font of text field does not fit.
        # tabs_needed = math.ceil(longest_attribute / 4)
        logging.info('Set parameters and values:')

        excluded_attributes = [
            'use_gru',
            'rnn_sequence_size',
        ]
        if self.architecture in constants.TWO_D_NETWORKS:
            excluded_attributes.append('input_depth')
        if self.backbone is None:
            excluded_attributes.append('backbone')  # is indirectly skipped as well by: if value is None or not value:

        for attribute in attribute_list:
            if attribute in excluded_attributes:
                continue
            name = ' '.join(attribute.split('_'))
            value = self.__getattribute__(attribute)
            if value is None or not value:
                continue
            if name == 'norm' or name == 'normalization':
                name = 'normalization'
                values = [choice[0] for choice in constants.NORMALIZATION_CHOICES]
                choice_index = values.index(value)
                value = constants.NORMALIZATION_CHOICES[choice_index][1]
            logging.info(f'{name}: {value}')

        logging.info(frame_string)
        logging.info('')


class Parameter:
    def __init__(self, name, value, index,  help_string='',):
        """

        Args:
            name: name of this parameter
            value: value of this parameter
            index: index of parameter --> decides position when printing all attributes
            help_string: help for this parameter
        """
        if not isinstance(name, str):
            raise ValueError('name must be a string')
        if not isinstance(help_string, str):
            raise ValueError('question must be a string')

        self.name = name
        self.value = value
        self.help_string = help_string
        self.value_type = type(value)
        self.index = index


class Parameters:
    epochs = None

    def __init__(self, attribute_name_list, attribute_value_list, attribute_help_list=None):
        """

        Args:
            attribute_name_list (list): list of attribute names, which will be set to this class
            attribute_value_list (list): list of initial default attribute values
            attribute_help_list (list): list containing help for each attribute

        Usage:
            automated interaction guide: call parameters.start_interaction_guide()
                This will return a FixedParameters instance where you can call:
                    fixed_parameters.attribute_name
                to get the value.

            Attention! If passing a path, you might want to extend it to your need since a path is just a string.
            Maybe use .utils.confirm_path_input() in that case.

            you can also only:
                - show_parameters()
                - let the user_update_attribute by index or name of the attribute
                - start_interaction_guide
                - call len(parameters)
                - get_attribute_name_list()
                - iterate like: parameters[i]
        """
        if not isinstance(attribute_name_list, list) or not isinstance(attribute_value_list, list):
            raise ValueError(f'Pass lists the Parameters class!\n'
                             f'Names are of type: {type(attribute_name_list)}\n'
                             f'Values are ot type: {type(attribute_value_list)}')

        if attribute_help_list is None:
            attribute_help_list = len(attribute_name_list) * ['']
        elif not isinstance(attribute_help_list, list):
            raise ValueError(f'Pass lists the Parameters class not {type(attribute_help_list)}')

        if not (len(attribute_name_list) == len(attribute_value_list) == len(attribute_help_list)):
            raise ValueError('attribute names, values and helps must be of same length')

        for idx, name in enumerate(attribute_name_list):
            value = attribute_value_list[idx]
            help_string = attribute_help_list[idx]
            self.__setattr__(name, Parameter(name, value, help_string=help_string, index=idx))

    def __getitem__(self, i):
        """
        You can iterate over this class

        Args:
            i: index

        Returns: object of class: Parameter
        """
        return getattr(self, self.get_attribute_name_list()[i])

    def __len__(self):
        return len(self.get_attribute_name_list())

    def get_attribute_name_list(self, return_all_attributes=False):
        """
        Args:
            return_all_attributes: if True, all attributes are returned, even if they are not instance of Parameter
        """
        all_attributes = [attr for attr in dir(self) if not attr.startswith('__') and not callable(getattr(self, attr))]
        attributes_and_indexes = []
        for name in all_attributes:
            current_attr = self.__getattribute__(name)
            if isinstance(current_attr, Parameter):
                attributes_and_indexes.append((name, current_attr.index))
            elif return_all_attributes:
                attributes_and_indexes.append((name, None))

        if not return_all_attributes:
            attributes_and_indexes = sorted(attributes_and_indexes, key=lambda x: x[1])

        return [attr_and_idx[0] for attr_and_idx in attributes_and_indexes]

    def user_update_attribute(self, attribute_name_or_index):
        """
        Leads the user through an attribute updating

        Args:
            attribute_name_or_index: index of the attribute at get_attribute_name_list() or the name of the attr
        """
        error_message = None
        try:
            attribute_name = self[attribute_name_or_index].name
        except TypeError:
            attribute_name = attribute_name_or_index
        except IndexError:
            error_message = f'Parameter number {attribute_name_or_index} does not exist!'
            return error_message

        if not isinstance(attribute_name, str):
            raise ValueError('Pass an attribute name or the index of the parameter')

        try:
            attribute = self.__getattribute__(attribute_name)

            if not isinstance(attribute, Parameter):
                raise RuntimeError(f'The attribute {attribute_name} must be a Parameter instance!')

            new_value = input(f'Pass a value for {attribute.name}\n')

            new_value = confirm_input_by_type(new_value, attribute.value_type)
            attribute.value = new_value

        except AttributeError:
            print(f'There is no attribute called {attribute_name}.')

        return error_message

    def show_attributes_and_values(self, ask_help=True):
        if ask_help:
            help_me = True
        else:
            help_me = False

        frame_string = '#####################################################'
        logging.info('')
        logging.info(frame_string)
        logging.info('')

        attribute_list = self.get_attribute_name_list()
        longest_attribute = max([len(attr_name) for attr_name in attribute_list])

        if help_me:
            print('Attributes and help:')
            for i in range(len(self)):
                parameter = self[i]

                if not isinstance(parameter, Parameter):
                    continue

                name_string = parameter.name + (longest_attribute - len(parameter.name)) * ' '
                space = (len(str(len(self))) - len(str(i))) * ' '

                if parameter.help_string:
                    help_string_list = parameter.help_string.split('\n')
                    fill_length = len(f'Parameter {i} {space}: {name_string} --> ')
                    fill_string = f'\n{fill_length * " "}'
                    help_string = f'{fill_string}'.join(help_string_list)
                    print(f'Parameter {i} {space}: {name_string} --> {help_string}')
                else:
                    print(f'Parameter {i} {space}: {name_string} --> no help provided.')

            print('\n')

        logging.info('Current attributes and related values:')
        for i in range(len(self)):
            parameter = self[i]
            if not isinstance(parameter, Parameter):
                continue
            name_string = parameter.name + (longest_attribute - len(parameter.name)) * ' '
            space = (len(str(len(self))) - len(str(i))) * ' '
            logging.info(f'Parameter {i} {space}: {name_string} --> {parameter.value}')

        logging.info('')
        logging.info(frame_string)
        logging.info('')

    def get_fixed_parameters(self):
        """
        Returns: clean class with all Parameters where you can call an attribute and directly get it's value.

        In other words:
        parameters.attribute_name.value
        can be replaced with
        this_return_class.attribute_name
        """
        fixed_parameters = FixedParameters()
        for parameter_name in self.get_attribute_name_list(return_all_attributes=True):
            parameter = getattr(self, parameter_name)
            if isinstance(parameter, Parameter):
                fixed_parameters.__setattr__(parameter_name, parameter.value)
            else:
                fixed_parameters.__setattr__(parameter_name, parameter)

        return fixed_parameters

    def start_interaction_guide(self):
        self.show_attributes_and_values()

        input_was_a_parameter = True
        user_changed_something = False  # if user changed something, print values again

        # user can now decide to update a parameter or not
        while input_was_a_parameter:

            parameter_to_update = input('Pass a number to change the parameter or type s to start.\n')

            parameter_to_update = confirm_input_by_type(parameter_to_update, int, exception_key='s')
            if parameter_to_update == 's':
                break

            # calling update sequence
            error_msg = self.user_update_attribute(parameter_to_update)
            if error_msg is not None:
                print(error_msg)
            else:
                user_changed_something = True
                self.show_attributes_and_values(ask_help=False)

        if user_changed_something:
            self.show_attributes_and_values(ask_help=False)

        fixed_parameters = self.get_fixed_parameters()

        return fixed_parameters
