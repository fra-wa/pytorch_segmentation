import os

from django import forms

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.models import InferenceParameters


class InferenceInputForm(forms.ModelForm):
    class Meta:
        model = InferenceParameters
        fields = [
            'model_path',
            'data_folder',
            'device',
            'save_color',
            'log_thresh',
            'overlap',
            'weighting_strategy',
        ]
        widgets = {
            'model_path': forms.TextInput(attrs={'class': 'form-control'}),
            'data_folder': forms.TextInput(attrs={'class': 'form-control'}),

            'device': forms.Select(attrs={'class': 'form-control'}),
            'save_color': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'log_thresh': forms.NumberInput(attrs={'class': 'form-control'}),
            'overlap': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),

            'weighting_strategy': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
            'model_path',
            'data_folder',
            'log_thresh',
            'overlap',
            'weighting_strategy',
        ]

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True

    def clean(self):
        fcd = super().clean()

        if not os.path.isfile(fcd['model_path']):
            self.add_error('model_path', 'The model path does not exist.')
        elif not fcd['model_path'].endswith('.pt'):
            self.add_error('model_path', 'The model must be a .pt file.')

        if not os.path.isdir(fcd['data_folder']):
            self.add_error('data_folder', 'There is no such directory.')
        if not get_file_paths_in_folder(fcd['data_folder'], extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
            self.add_error(
                'data_folder',
                f'There is no data to segment. Supported file types are: '
                f'{", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
            )
