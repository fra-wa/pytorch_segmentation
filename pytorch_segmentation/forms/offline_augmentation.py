import os

from django import forms
from django.contrib import messages
from django.utils.safestring import mark_safe

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_sub_folders, get_file_paths_in_folder
from pytorch_segmentation.models import OfflineImageAugmentationParameters
from pytorch_segmentation.models import OfflineVolumeAugmentationParameters


class OfflineImageAugForm(forms.ModelForm):
    dataset_choice = forms.ChoiceField(
        label='Dataset',
        choices=([]),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = OfflineImageAugmentationParameters
        fields = [
            'images_folder',
            'masks_folder',

            'dataset_choice',  # replaces dataset_folder
            'output_folder',

            'min_dimension',
            'max_rot_angle',

            'max_augmentations',
            'total_augmentations',

            # collapsed
            'do_rotate',
            'do_resize',
            'do_tilt_backwards',
            'do_tilt_forward',

            'do_tilt_left',
            'do_tilt_right',
            'do_elastic_distortion',
            'do_grid_distortion',

            'do_optical_distortion',
            'do_random_crop',
            'do_random_grid_shuffle',
            'do_squeeze_height',

            'do_squeeze_width',
            'do_flip_horizontal',
            'do_flip_vertical',
            'do_gaussian_noise',

            'do_brightness',
            'do_contrast',
            'do_gaussian_filter',
            'do_sharpen',

            'do_to_gray_and_histogram_normalization',
            'do_to_gray_and_adaptive_histogram',
            'do_to_gray_and_non_local_means_filter',
            'do_random_erasing',

            'do_channel_shuffle',
            'do_color_to_hsv',
            'do_iso_noise',
            'do_random_fog',

            'do_random_rain',
            'do_random_shadow',
            'do_random_snow',
            'do_random_sun_flair',

            'do_rgb_shift',
            'do_solarize',
            'do_to_gray',
            'do_to_sepia',
        ]
        widgets = {
            'images_folder': forms.TextInput(attrs={'class': 'form-control'}),
            'masks_folder': forms.TextInput(attrs={'class': 'form-control'}),
            'output_folder': forms.TextInput(attrs={'class': 'form-control'}),
            'channels': forms.Select(attrs={'class': 'form-control'}),
            'classes': forms.NumberInput(attrs={'class': 'form-control'}),
            'colored_masks': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'min_dimension': forms.NumberInput(attrs={'class': 'form-control'}),
            'max_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),
            'total_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),
            'max_rot_angle': forms.NumberInput(attrs={'class': 'form-control', 'unit': '°'}),

            'do_rotate': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_resize': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_tilt_backwards': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_tilt_forward': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_tilt_left': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_tilt_right': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_elastic_distortion': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_grid_distortion': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_optical_distortion': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_crop': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_grid_shuffle': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_squeeze_height': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_squeeze_width': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_flip_horizontal': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_flip_vertical': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_to_gray_and_adaptive_histogram': forms.CheckboxInput(
                attrs={'class': 'form-check-input', 'role': 'switch'}
            ),

            'do_brightness': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_contrast': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_gaussian_filter': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_gaussian_noise': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_to_gray_and_histogram_normalization': forms.CheckboxInput(
                attrs={'class': 'form-check-input', 'role': 'switch'}
            ),
            'do_to_gray_and_non_local_means_filter': forms.CheckboxInput(
                attrs={'class': 'form-check-input', 'role': 'switch'}
            ),
            'do_sharpen': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_erasing': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_channel_shuffle': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_color_to_hsv': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_iso_noise': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_fog': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_random_rain': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_shadow': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_snow': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_sun_flair': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_rgb_shift': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_solarize': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_to_gray': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_to_sepia': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
            'output_folder',
        ]

        dataset_choices = []
        datasets = get_sub_folders(constants.DATASET_FOLDER, depth=1)
        for folder in datasets:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_base_names = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_base_names and 'masks' in sub_base_names and get_file_paths_in_folder(sub_folders[0]):
                dataset_choices.append((os.path.basename(folder), os.path.basename(folder)))

        if self.instance is not None and self.instance.pk is not None:
            if self.instance.dataset_folder and \
                    self.instance.dataset_folder not in [dataset_choice[0] for dataset_choice in dataset_choices]:
                dataset_choices.append(('__deleted__', f'{self.instance.dataset_folder} (deleted!)'))

        dataset_choices.insert(0, constants.EMPTY_CHOICE)

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True

            if field == 'dataset_choice':
                if not dataset_choices or (len(dataset_choices) == 1 and dataset_choices[0][0] == '__deleted__'):
                    messages.add_message(
                        request,
                        messages.INFO,
                        mark_safe(
                            'There are no datasets to choose from. Read '
                            '<a class="form-a" '
                            'href="https://gitlab.com/fra-wa/pytorch_segmentation/-/blob/master/README.md">Readme</a> '
                            'how to setup your data correctly!'
                        ),
                    )
                self.fields[field].help_text = mark_safe(
                    'Leave the images and masks folder empty and select a 3D dataset to '
                    'augment (located inside segmentation_datasets/ as described in the '
                    '<a class="form-a" href="https://gitlab.com/fra-wa/pytorch_segmentation/-/blob/master/README.md">'
                    'Readme</a>).'
                )
                self.fields[field].choices = dataset_choices
                if self.instance and self.instance.pk is not None:
                    self.fields[field].initial = self.instance.dataset_folder

    def clean(self):
        fcd = super().clean()
        if 'dataset_choice' in fcd and fcd['dataset_choice'] == '__deleted__':
            self.add_error(
                'dataset_choice',
                'The dataset does no more exist!'
            )

        if all(['images_folder' in fcd, 'masks_folder' in fcd, 'dataset_choice' in fcd]):
            if not fcd['dataset_choice'] and (not fcd['images_folder'] or not fcd['masks_folder']):
                self.add_error(
                    'dataset_choice',
                    'Pass either a 3D dataset or images and masks folder.'
                )
                self.add_error(
                    'images_folder',
                    'Pass either images and masks folder or select a 3D dataset.'
                )
                self.add_error(
                    'masks_folder',
                    'Pass either images and masks folder or select a 3D dataset.'
                )

        all_augmentations = [fcd[field] for field in self.fields if field.startswith('do_')]

        if not any(all_augmentations):
            self.add_error(
                'do_rotate',
                'At least one augmentation needs to be selected!'
            )
        return fcd

    def save(self, commit=True):
        parameters = super().save(commit)

        parameters.dataset_folder = self.cleaned_data['dataset_choice']

        if commit:
            parameters.save()

        return parameters


class OfflineVolumeAugForm(forms.ModelForm):
    dataset_choice = forms.ChoiceField(
        label='Dataset',
        choices=([]),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = OfflineVolumeAugmentationParameters
        fields = [
            'images_folder',
            'masks_folder',

            'dataset_choice',  # replaces dataset_folder
            'output_folder',

            'min_dimension_x',
            'min_dimension_y',

            'min_dimension_z',
            'rotation_angle',

            'max_augmentations',
            'total_augmentations',

            # collapsed
            'do_rotate_x',
            'do_rotate_y',
            'do_rotate_z',
            'do_resize',

            'do_random_flip',
            'do_random_tilt',
            'do_random_squeeze',
            'do_random_crop',

            'do_noise',
            'do_blur',
            'do_sharpen',
            'do_contrast',

            'do_brightness',
            'do_random_shadow',
            'do_random_erasing',
        ]
        widgets = {
            'images_folder': forms.TextInput(attrs={'class': 'form-control'}),
            'masks_folder': forms.TextInput(attrs={'class': 'form-control'}),
            'output_folder': forms.TextInput(attrs={'class': 'form-control'}),

            'min_dimension_x': forms.NumberInput(attrs={'class': 'form-control'}),
            'min_dimension_y': forms.NumberInput(attrs={'class': 'form-control'}),

            'min_dimension_z': forms.NumberInput(attrs={'class': 'form-control'}),
            'rotation_angle': forms.NumberInput(attrs={'class': 'form-control'}),

            'max_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),
            'total_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),

            'do_rotate_x': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_rotate_y': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_rotate_z': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_resize': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_random_flip': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_tilt': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_squeeze': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_crop': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_noise': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_blur': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_sharpen': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_contrast': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'do_brightness': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_shadow': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            'do_random_erasing': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
            'output_folder',
        ]

        dataset_choices = []
        datasets = get_sub_folders(constants.DATASET_FOLDER, depth=1)
        for folder in datasets:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_base_names = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_base_names and 'masks' in sub_base_names and get_sub_folders(sub_folders[0]):
                dataset_choices.append((os.path.basename(folder), os.path.basename(folder)))

        if self.instance is not None and self.instance.pk is not None:
            if self.instance.dataset_folder and \
                    self.instance.dataset_folder not in [dataset_choice[0] for dataset_choice in dataset_choices]:
                dataset_choices.append(('__deleted__', f'{self.instance.dataset_folder} (deleted!)'))

        dataset_choices.insert(0, constants.EMPTY_CHOICE)

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True

            if field == 'dataset_choice':
                if not dataset_choices or (len(dataset_choices) == 1 and dataset_choices[0][0] == '__deleted__'):
                    messages.add_message(
                        request,
                        messages.INFO,
                        mark_safe(
                            'There are no datasets to choose from. Read '
                            '<a class="form-a" '
                            'href="https://gitlab.com/fra-wa/pytorch_segmentation/-/blob/master/README.md">Readme</a> '
                            'how to setup your data correctly!'
                        ),
                    )
                self.fields[field].help_text = mark_safe(
                    'Leave the images and masks folder empty and select a 3D dataset to '
                    'augment (located inside segmentation_datasets/ as described in the '
                    '<a class="form-a" href="https://gitlab.com/fra-wa/pytorch_segmentation/-/blob/master/README.md">'
                    'Readme</a>).'
                )
                self.fields[field].choices = dataset_choices
                if self.instance and self.instance.pk is not None:
                    self.fields[field].initial = self.instance.dataset_folder

    def clean(self):
        fcd = super().clean()
        if 'dataset_choice' in fcd and fcd['dataset_choice'] == '__deleted__':
            self.add_error(
                'dataset_choice',
                'The dataset does no more exist!'
            )

        if all(['images_folder' in fcd, 'masks_folder' in fcd, 'dataset_choice' in fcd]):
            if not fcd['dataset_choice'] and (not fcd['images_folder'] or not fcd['masks_folder']):
                self.add_error(
                    'dataset_choice',
                    'Pass either a 3D dataset or images and masks folder.'
                )
                self.add_error(
                    'images_folder',
                    'Pass either images and masks folder or select a 3D dataset.'
                )
                self.add_error(
                    'masks_folder',
                    'Pass either images and masks folder or select a 3D dataset.'
                )

    def save(self, commit=True):
        parameters = super().save(commit)

        parameters.dataset_folder = self.cleaned_data['dataset_choice']

        if commit:
            parameters.save()

        return parameters
