import os

from django import forms

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_sub_folders
from pytorch_segmentation.models import TrainingDataset


class TrainingDatasetForm(forms.ModelForm):
    folder_choice = forms.ChoiceField(
        label='Dataset folders on this PC',
        choices=([]),
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
        help_text='Select a folder located inside "../segmentation_datasets/xx".',
    )

    class Meta:
        model = TrainingDataset
        fields = [
            'name',
            'folder_choice',
            'number_training_images',
            'number_validation_images',
        ]
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'number_training_images': forms.NumberInput(attrs={'class': 'form-control'}),
            'number_validation_images': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        dataset_choices = []
        self.deleted_choice = '__deleted__'
        dataset_folders = sorted(get_sub_folders(constants.DATASET_FOLDER, depth=1))
        for folder in dataset_folders:
            sub_folders = get_sub_folders(folder, depth=1)
            sub_folders = [os.path.basename(sub_folder) for sub_folder in sub_folders]
            if 'images' in sub_folders and 'masks' in sub_folders:
                dataset_choices.append((os.path.basename(folder), os.path.basename(folder)))

        dataset_choice_keys = [dataset_choice[0] for dataset_choice in dataset_choices]
        if self.instance and self.instance.pk is not None:
            if self.instance.folder_name_on_device and self.instance.folder_name_on_device not in dataset_choice_keys:
                dataset_choices.append((
                    f'__deleted__',
                    f'{self.instance.folder_name_on_device} (not on this pc)'
                ))

        dataset_choices.insert(0, constants.EMPTY_CHOICE)

        for field in self.fields:
            if field == 'folder_choice':
                self.fields[field].choices = dataset_choices

                if self.instance and self.instance.pk is not None:
                    if self.instance.folder_name_on_device in dataset_choice_keys:
                        self.fields[field].initial = self.instance.folder_name_on_device
                    else:
                        self.fields[field].initial = '__deleted__'

            if field in ['number_training_images', 'number_validation_images']:
                self.fields[field].help_text = self.fields[field].help_text + ' Set to 0 if unknown.'
                if self.instance and self.instance.pk is None:
                    self.fields[field].initial = 0

            if field == 'name':
                self.fields[field].help_text = 'This is the readable name to be displayed. Choose a dataset-folder ' \
                                               'from the dropdown below if one exists (only if a dataset exists, the ' \
                                               'model can be tested).'

    def clean(self):
        fcd = super(TrainingDatasetForm, self).clean()
        if fcd['folder_choice'] and 'folder_choice' in self.changed_data:
            datasets = TrainingDataset.objects.filter(folder_name_on_device=fcd['folder_choice'])
            if datasets.count() > 0:
                self.add_error('folder_choice', 'A dataset related to this folder does already exist!')
    
    def save(self, commit=True):
        initial_folder = self.instance.folder_name_on_device
        training_dataset = super().save(commit=False)
        if training_dataset.folder_name_on_device == self.deleted_choice:
            training_dataset.folder_name_on_device = initial_folder

        if 'folder_choice' in self.cleaned_data and self.cleaned_data['folder_choice'] != self.deleted_choice:
            training_dataset.folder_name_on_device = self.cleaned_data['folder_choice']

        if commit:
            training_dataset.save()
        
        return training_dataset
