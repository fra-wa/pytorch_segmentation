import os

from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.safestring import mark_safe

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_sub_folders
from pytorch_segmentation.models import OnlineImageAugParameters, TrainingDataset
from pytorch_segmentation.models import OnlineVolumeAugParameters


class OnlineImageAugForm(forms.ModelForm):
    class Meta:
        model = OnlineImageAugParameters
        fields = [
            'policy',

            'global_strength',
            'online_aug_start_epoch',
            'max_augmentations',

            'max_rot_angle',
            'vertical_flip_allowed',

            'resizing_scale_upper',
            'squeeze_scale_upper',
            'tilting_start_factor',

            'resizing_scale_lower',
            'squeeze_scale_lower',
            'tilting_end_factor',

            # geometric
            'rotate_by_angle_prob',
            'resize_prob',
            'squeeze_prob',

            'tilt_prob',
            'flip_prob',
            'elastic_distortion_prob',

            'grid_distortion_prob',
            'optical_distortion_prob',
            'random_crop_prob',

            'grid_shuffle_prob',

            # pixel manipulation
            'brightness_prob',
            'contrast_prob',
            'gaussian_filter_prob',

            'gaussian_noise_prob',
            'random_erasing_prob',
            'channel_shuffle_prob',

            'color_to_hsv_prob',
            'iso_noise_prob',
            'random_fog_prob',

            'random_rain_prob',
            'random_shadow_prob',
            'random_snow_prob',

            'random_sun_flair_prob',
            'rgb_shift_prob',
            'solarize_prob',

            'to_gray_prob',
            'to_sepia_prob',
        ]
        widgets = {
            'policy': forms.Select(attrs={'class': 'form-control'}),

            'vertical_flip_allowed': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'global_strength': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'online_aug_start_epoch': forms.NumberInput(attrs={'class': 'form-control'}),
            'max_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),
            'max_rot_angle': forms.NumberInput(attrs={'class': 'form-control', 'unit': '°'}),
            'resizing_scale_lower': forms.NumberInput(attrs={'class': 'form-control'}),
            'resizing_scale_upper': forms.NumberInput(attrs={'class': 'form-control'}),
            'squeeze_scale_lower': forms.NumberInput(attrs={'class': 'form-control'}),
            'squeeze_scale_upper': forms.NumberInput(attrs={'class': 'form-control'}),
            'tilting_start_factor': forms.NumberInput(attrs={'class': 'form-control'}),
            'tilting_end_factor': forms.NumberInput(attrs={'class': 'form-control'}),

            'elastic_distortion_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'flip_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'grid_distortion_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'optical_distortion_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_crop_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'resize_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'rotate_by_angle_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'tilt_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'squeeze_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'grid_shuffle_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),

            'brightness_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'contrast_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'gaussian_filter_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'gaussian_noise_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_erasing_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'channel_shuffle_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'color_to_hsv_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'iso_noise_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_fog_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_rain_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_shadow_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_snow_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_sun_flair_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'rgb_shift_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'solarize_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'to_gray_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'to_sepia_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
        ]

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True


class OnlineVolumeAugForm(forms.ModelForm):
    class Meta:
        model = OnlineVolumeAugParameters
        fields = [
            'policy',

            'global_strength',
            'online_aug_start_epoch',
            'max_augmentations',

            'max_rot_angle',
            'depth_flip_allowed',

            'resizing_scale_upper',
            'squeeze_scale_upper',
            'tilting_start_factor',

            'resizing_scale_lower',
            'squeeze_scale_lower',
            'tilting_end_factor',

            # geometric
            'flip_prob',
            'random_crop_3d_prob',
            'random_resize_3d_prob',

            'rotate_z_3d_prob',
            'squeeze_prob',
            'tilt_prob',

            # voxel manipulation
            'add_blur_3d_prob',
            'add_noise_3d_prob',
            'brightness_3d_prob',

            'contrast_3d_prob',
            'random_erasing_3d_prob',
            'sharpen_3d_with_blur_prob',

            'random_shadow_3d_prob',
        ]
        widgets = {
            'policy': forms.Select(attrs={'class': 'form-control'}),

            'depth_flip_allowed': forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),

            'global_strength': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'max_augmentations': forms.NumberInput(attrs={'class': 'form-control'}),
            'max_rot_angle': forms.NumberInput(attrs={'class': 'form-control', 'unit': '°'}),
            'online_aug_start_epoch': forms.NumberInput(attrs={'class': 'form-control'}),
            'resizing_scale_lower': forms.NumberInput(attrs={'class': 'form-control'}),
            'resizing_scale_upper': forms.NumberInput(attrs={'class': 'form-control'}),
            'squeeze_scale_lower': forms.NumberInput(attrs={'class': 'form-control'}),
            'squeeze_scale_upper': forms.NumberInput(attrs={'class': 'form-control'}),
            'tilting_start_factor': forms.NumberInput(attrs={'class': 'form-control'}),
            'tilting_end_factor': forms.NumberInput(attrs={'class': 'form-control'}),

            'flip_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_crop_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_resize_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'rotate_z_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'squeeze_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'tilt_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),

            'add_blur_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'add_noise_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'brightness_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'contrast_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_erasing_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'sharpen_3d_with_blur_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
            'random_shadow_3d_prob': forms.NumberInput(attrs={'class': 'form-control', 'unit': '%'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        required_fields = [
        ]

        for field in self.fields:
            if field in required_fields:
                self.fields[field].required = True


class OnlineAugExampleForm(forms.Form):
    number_of_samples = forms.IntegerField(
        label='Create',
        validators=[MinValueValidator(1), MaxValueValidator(100)],
        initial=10,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'unit': 'images'}),
        help_text='Create this many new examples (1 to 100).'
    )

    random = forms.BooleanField(
        initial=True,
        label='Random',
        help_text='Randomly select images/volumes or use the first x images/volumes.',
        widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(OnlineAugExampleForm, self).__init__(*args, **kwargs)
