from django import forms

from pytorch_segmentation.models import TrainingDataset


class ExportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ExportForm, self).__init__(*args, **kwargs)

        datasets = TrainingDataset.objects.all()
        for dataset in datasets:
            self.fields[f'dataset_{dataset.pk}'] = forms.BooleanField(
                label=f'{dataset.name}',
                required=False,
                initial=False,
                widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'role': 'switch'}),
            )
