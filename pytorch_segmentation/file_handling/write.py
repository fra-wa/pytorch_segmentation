import cv2
import numpy as np
import os

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import convert_np_array_to_binary, create_folder
from pytorch_segmentation.file_handling.utils import get_overwritten_file_path
from pytorch_segmentation.multicore_utils import slice_volume_equally, multiprocess_function


def create_csv_string(element, fmt):
    if isinstance(element, (str, int, bool)) or element is None or element == np.nan:
        csv_string = '{}, '.format(element)
    else:
        csv_string = f'{element:{fmt}}, '
    return csv_string


def create_csv_line(line, fmt):
    """
    Dynamically creates a csv line based on the input type (text float whatever).
    Applies the given format if type of element is not int or string.

    Returns: csv line
    """
    csv_line = ''
    for element in line:
        if isinstance(element, list) or isinstance(element, tuple):
            for sub_element in element:
                if isinstance(element, list) or isinstance(element, tuple):
                    raise ValueError('Dimension of array or list is higher than 2. Please specify data.\n'
                                     'May use save_numpy_array_to_binary() and the related reading.')
                csv_line += create_csv_string(sub_element, fmt)
        else:
            csv_line += create_csv_string(element, fmt)

    csv_line = csv_line.rstrip(', ')

    csv_line += '\n'
    return csv_line


def save_csv(file_path, data, header, overwrite=True, number_format='.3f'):
    """
    Args:
        data: list or numpy array
        file_path: path to save to
        header: pass a header for your file describing the columns (string, separate by comma)
        overwrite: default True, set to false, then the current datetime will be added to the file name
        number_format: specify format of numbers
    """

    if not overwrite:
        file_path = get_overwritten_file_path(file_path)

    # since formatting and keeping everything mostly universal usable is hard with np array
    # (especially when str is involved), i use lists
    if type(data) == np.ndarray:
        data = list(data)

    csv_to_save = '{}\n'.format(header)
    for idx, line_or_element in enumerate(data):
        try:
            csv_to_save += create_csv_line(line_or_element, number_format)
        except TypeError:
            # only 1D
            csv_to_save += create_csv_string(line_or_element, number_format)
            if idx + 1 == len(data):
                csv_to_save = csv_to_save.rstrip(', ')

    with open(file_path, 'w') as f:
        f.write(csv_to_save)


def save_numpy_array_to_binary(array, file_path):
    """
    Saves any numpy array to a binary file
    Args:
        array: np.ndarray object
        file_path: file path to save the volume
    """
    binary_data = convert_np_array_to_binary(array)

    with open(file_path, 'wb') as binary_file:
        binary_file.write(binary_data)


def __multi_save_volume_images(sub_volume, start_index, max_slices, folder, extension='.png'):
    zero_fills = len(str(max_slices))
    if not extension.startswith('.'):
        extension = f'.{extension}'
    extension = extension.lower()
    extension = extension.lower()

    if extension not in constants.SUPPORTED_INPUT_IMAGE_TYPES:
        raise ValueError(
            f'{extension} is not in supported extensions: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}'
        )

    for z in range(sub_volume.shape[0]):
        current_id = z + start_index
        img = sub_volume[z]
        file_nr = str(current_id).zfill(zero_fills)
        file_name = f'slice_{file_nr}{extension}'
        cv2.imwrite(os.path.join(folder, file_name), img)


def save_volume(volume, folder, multiprocessing=True, extension='.png', start_index=0, max_slices=None):
    """

    Args:
        volume:
        folder:
        multiprocessing: using multicore processing to save - use this for larger volumes!
        extension:
        start_index: starts naming using this index
        max_slices: If given, zero padding will be adjusted by this number

    Returns:

    """
    create_folder(folder)

    if not extension.startswith('.'):
        extension = f'.{extension}'
    extension = extension.lower()

    if extension not in constants.SUPPORTED_INPUT_IMAGE_TYPES:
        raise ValueError(
            f'{extension} is not in supported extensions: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}'
        )

    if max_slices is None:
        max_slices = volume.shape[0]

    if multiprocessing:
        volumes_map = slice_volume_equally(volume)
        depth_map = [vol.shape[0] for vol in volumes_map]
        indexes_map = [start_index]
        for depth in depth_map[:-1]:
            indexes_map.append(indexes_map[-1] + depth)
        max_slices_map = len(volumes_map) * [max_slices]
        folders_map = len(volumes_map) * [folder]
        extension_map = len(volumes_map) * [extension]
        maps = [volumes_map, indexes_map, max_slices_map, folders_map, extension_map]
        multiprocess_function(__multi_save_volume_images, maps)
    else:
        __multi_save_volume_images(volume, start_index, max_slices, folder, extension=extension)
