import csv
import os.path

import cv2
import numpy as np
import scipy.io as sio

from .utils import convert_binary_to_np_array
from .utils import get_file_paths_in_folder
from .. import constants
from ..multicore_utils import slice_multicore_parts, multiprocess_function


def convert_line_to_list(line):
    """
    Assuming that the line is a list of strings, this tries to convert numbers back to float and let strings be strings.
    """
    line_list = []
    for element in line:
        # remove whitespace
        element = element.lstrip()

        try:
            element = float(element)
        except ValueError:
            pass

        line_list.append(element)

    return line_list


def read_csv_to_list(path, ignore_header=True):
    """
    You can pass a txt as well. But the values must be separated by a comma!
    """

    csv_list = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for line in csv_reader:
            line_list = convert_line_to_list(line)
            if line_list:
                csv_list.append(line_list)

    if ignore_header:
        csv_list.pop(0)

    return csv_list


def read_csv_to_array(path, delimiter=', ', dtype=None, encoding="utf8", skip_header=1):
    """
    Args:
        path: path of csv file
        delimiter: default ", "
        dtype: numpy dtype
        encoding: default "utf8"
        skip_header: default 1, set to 0 to keep first line

    Returns: np array

    """
    return np.genfromtxt(path, delimiter=delimiter, dtype=dtype, encoding=encoding, skip_header=skip_header)


def read_mat_file(path):
    """
    Reads .mat local_files in a given folder (if encountering some problems at decoding, add a flag for the version)

    Returns: list with file data
    """
    try:
        return sio.loadmat(path)
    except NotImplementedError:
        raise NotImplementedError('Matlabs file format is v7.3, try this: '
                                  'https://stackoverflow.com/questions/17316880/reading-v-7-3-mat-file-in-python')


def read_mat_files_in_folder(folder):
    """
    Reads .mat local_files in a given folder
    Returns: list containing mat data
    """
    mat_files = get_file_paths_in_folder(folder, extension='.mat')
    data = []

    for file in mat_files:
        data.append(read_mat_file(file))

    return data


def read_csv_files_in_folder(folder, delimiter=', ', dtype=None, encoding="utf8", skip_header=1):
    """
    Reads .csv local_files in a given folder

    path: path of csv file
        delimiter: default ", "
        dtype: numpy dtype
        encoding: default "utf8"
        skip_header: default 1, set to 0 to keep first line

    Returns: list of np arrays
    """
    csv_files = get_file_paths_in_folder(folder, extension='.csv')
    data = []

    for file in csv_files:
        data.append(read_csv_to_array(file, delimiter, dtype, encoding, skip_header))

    return data


def read_binary_to_numpy_array(file_path):
    """
    Reads a numpy ndarray saved as binary back to np.ndarray

    Args:
        file_path:

    Returns:

    """
    with open(file_path, 'rb') as binary_file:
        data = binary_file.read()
    array = convert_binary_to_np_array(data)
    return array


def create_gray_volume_from_files(files, order):
    h, w = cv2.imread(files[0], cv2.IMREAD_GRAYSCALE).shape[:2]
    d = len(files)

    volume = np.zeros((d, h, w), dtype=np.uint8)

    for z, file in enumerate(files):
        img = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
        volume[z] = img

    return volume, order


def create_color_volume_from_files(files, order):
    h, w, c = cv2.imread(files[0], cv2.IMREAD_COLOR).shape[:3]
    d = len(files)

    volume = np.zeros((d, h, w, c), dtype=np.uint8)

    for z, file in enumerate(files):
        img = cv2.imread(file, cv2.IMREAD_COLOR)
        volume[z] = img

    return volume, order


def load_volume_from_folder(folder, gray=True, multiprocessing=True, threads=None):
    if not os.path.isdir(folder):
        raise FileNotFoundError(f"Couldn't find folder: {folder}")
    files = get_file_paths_in_folder(
        folder, extension=constants.SUPPORTED_MASK_TYPES + constants.SUPPORTED_INPUT_IMAGE_TYPES
    )

    if not files:
        supported_formats = ', '.join(constants.SUPPORTED_MASK_TYPES + constants.SUPPORTED_INPUT_IMAGE_TYPES)
        raise FileNotFoundError(f'Folder does not contain images of type: {supported_formats}')

    if multiprocessing:
        files_map = slice_multicore_parts(files, total_maps=threads)
        order_map = list(range(len(files_map)))

        if gray:
            volumes_and_orders = multiprocess_function(create_gray_volume_from_files, [files_map, order_map])
            c = 1
        else:
            volumes_and_orders = multiprocess_function(create_color_volume_from_files, [files_map, order_map])
            c = 3

        volumes_and_orders = sorted(volumes_and_orders, key=lambda x: x[1])

        d, h, w = volumes_and_orders[0][0].shape[:3]
        d = sum([volumes_and_orders[i][0].shape[0] for i in range(len(volumes_and_orders))])

        if c == 3:
            volume = np.zeros((d, h, w, c), dtype=np.uint8)
        else:
            volume = np.zeros((d, h, w), dtype=np.uint8)

        start_index = 0
        for sub_volume, order in volumes_and_orders:
            end_index = start_index + sub_volume.shape[0]
            volume[start_index:end_index] = sub_volume
            start_index += sub_volume.shape[0]
    else:
        if gray:
            volume, _ = create_gray_volume_from_files(files, 0)
        else:
            volume, _ = create_color_volume_from_files(files, 0)

    return volume
