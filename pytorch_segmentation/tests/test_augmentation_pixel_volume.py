import cv2
import io
import numpy as np
import os
import sys

from unittest import TestCase
from unittest import mock

from pytorch_segmentation import constants
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import add_blur_3d
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import add_noise
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import brightness
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import contrast
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import histogram_normalization_3d
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import random_erasing
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import random_shadow_3d
from pytorch_segmentation.data_augmentation.pixel_volume_transformations import sharpen_3d_with_blur
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder


def custom_func(img_2d, mask_2d):
    img = np.zeros_like(img_2d)
    return img, img


class MockedRandomFirstVal:
    @staticmethod
    def randint(val1, val2):
        return val1


class MockedRandomSecondVal:
    @staticmethod
    def randint(val1, val2):
        return val2


class GeometricVolumeManipulationTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.volume_folder = os.path.join(files_path, 'data_3d', 'images', 'volume_1')
        self.masks_folder = os.path.join(files_path, 'data_3d', 'masks', 'volume_1')

        self.volume_paths = get_file_paths_in_folder(
            self.volume_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES
        )[:10]
        self.mask_paths = get_file_paths_in_folder(
            self.masks_folder, extension=constants.SUPPORTED_MASK_TYPES
        )[:10]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

    @staticmethod
    def create_volume_from_files(files):
        h, w = cv2.imread(files[0]).shape[:2]
        volume = np.zeros((len(files), h, w), dtype=np.uint8)

        for z, img in enumerate(files):
            volume[z] = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
        smaller_volume = np.zeros((10, 100, 100), dtype=np.uint8)
        smaller_volume[:, :, :] = volume[:, :100, :100]

        return smaller_volume

    def test_add_noise(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)

        self.assertEqual(len(np.unique(volume[0, :, :])), 1)
        new_vol, new_mask = add_noise(volume, volume)
        self.assertNotEqual(len(np.unique(new_vol[0, :, :])), 1)

    def test_add_blur(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)

        volume[:, :, 15:] = 255

        self.assertEqual(len(np.unique(volume[0, :, :])), 2)
        new_vol, new_mask = add_blur_3d(volume, volume)
        self.assertNotEqual(len(np.unique(new_vol[0, :, :])), 2)

    def test_sharpen_3d_with_blur(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)

        volume[:, :, 15:] = 255

        self.assertEqual(len(np.unique(volume[0, :, :])), 2)
        new_vol, new_mask = add_blur_3d(volume, volume)
        blurred_unique = [0, 47, 100, 154, 207, 255]

        # high values should increase, low decrease at border
        new_vol, new_mask = sharpen_3d_with_blur(new_vol, new_mask)
        sharpened_unique = np.unique(new_vol)
        expected_statements = [False, True, True, False, False, False]
        for sharp, blur, expected_statement in zip(sharpened_unique, blurred_unique, expected_statements):
            statement = blur > sharp
            self.assertEqual(statement, expected_statement)

    def test_contrast_and_brightness(self):

        volume = np.zeros((10, 20, 30), dtype=np.uint8)

        volume[:, :, :15] = 100
        volume[:, :, 15:] = 150

        factor = 1.5
        mean = np.mean(volume)
        expected_value_100 = np.around((100 - mean) * factor + mean)
        expected_value_150 = np.around((150 - mean) * factor + mean)
        new_volume, new_mask = contrast(volume, volume, contrast_factor=factor)

        gray_values = np.unique(new_volume)
        self.assertIn(expected_value_100, gray_values)
        self.assertIn(expected_value_150, gray_values)

        with mock.patch(
                'pytorch_segmentation.data_augmentation.pixel_volume_transformations.random', MockedRandomFirstVal):
            new_volume, new_mask = contrast(volume, volume)
            factor = 0.5
            mean = np.mean(volume)
            expected_value_100 = np.around((100 - mean) * factor + mean)
            expected_value_150 = np.around((150 - mean) * factor + mean)

            gray_values = np.unique(new_volume)
            self.assertIn(expected_value_100, gray_values)
            self.assertIn(expected_value_150, gray_values)

        new_volume, new_mask = brightness(volume, volume, brightness_offset=30)

        gray_values = np.unique(new_volume)
        self.assertIn(130, gray_values)
        self.assertIn(180, gray_values)

        volume = np.zeros((5, 5, 5), dtype=np.uint8)
        volume[:, :, :2] = 20
        volume[:, :, 2:] = 31

        for i in range(50):
            new_volume, new_mask = brightness(volume, volume)
            self.assertTrue((new_volume[:, :, :] != 0).any())
            self.assertTrue((new_volume[:, :, :] != 255).any())
            self.assertNotEqual(len(np.unique(new_volume)), 1)

    def test_histogram_normalization(self):
        volume = np.zeros((10, 20, 30), dtype=np.uint8)

        volume[:, :, :15] = 100
        volume[:, :, 15:] = 150
        new_volume, new_mask = histogram_normalization_3d(volume, volume)

        gray_values = np.unique(new_volume)
        self.assertIn(128, gray_values)
        self.assertIn(255, gray_values)

    def test_random_shadow(self):
        volume = np.full((10, 20, 30), 255, dtype=np.uint8)
        self.assertEqual(len(np.unique(volume[0, :, :])), 1)

        new_volume, new_mask = random_shadow_3d(volume, volume)
        self.assertEqual(len(np.unique(new_volume[:, :, :])), 2)

    def test_random_erasing(self):
        volume = np.full((10, 20, 30), 255, dtype=np.uint8)
        self.assertEqual(len(np.unique(volume[0, :, :])), 1)

        with mock.patch(
                'pytorch_segmentation.data_augmentation.pixel_volume_transformations.random', MockedRandomFirstVal):
            new_volume, new_mask = random_erasing(volume, volume)
        unique, counts = np.unique(new_volume, return_counts=True)
        self.assertEqual(len(unique), 2)
        for value, count in zip(unique, counts):
            if value == 0:
                self.assertEqual(count, 8)
                break

