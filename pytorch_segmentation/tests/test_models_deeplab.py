from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import DeepLabV3Plus


class DeepLabV3PlusTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DeepLabV3Plus', DeepLabV3Plus, backbone='resnet101')
