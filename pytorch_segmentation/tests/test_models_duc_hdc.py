from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import DeepLabDUCHDC


class DeepLabDUCHDCTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DeepLab_DUC_HDC', DeepLabDUCHDC)
