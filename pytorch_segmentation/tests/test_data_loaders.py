import cv2
import io
import os
import sys
import torch

from unittest import TestCase

from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.training.data_loaders import BatchedSequenceLoader
from pytorch_segmentation.training.data_loaders import VolumeLoader
from pytorch_segmentation.training.data_loading import create_train_valid_sequences_or_volumes
from pytorch_segmentation.training.transformations import get_rgb_image_transformation


class LoaderBaseTests(TestCase):
    def setUp(self):
        # Testing with BatchedSequenceLoader to get the base implementations
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.train_images = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))]
        self.train_masks = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'masks', 'volume_1'))]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        # assuming, that data_loading works
        # this already slices correct, but sequence loader can do this, too. Therefore i changed unet3d depth at loader
        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            self.train_images, self.train_masks, depth_or_sequence_size=5, is_3d=True,
        )
        self.batched_loader = BatchedSequenceLoader(
            input_paths=train,
            target_paths=target,
            batch_size=5,
            classes=6,
            channels=1,
            rnn_sequence_size=5
        )

    def tearDown(self):
        sys.stdout = self.previous_out

    def test_drop_last_batch_of_sequence(self):
        func = BatchedSequenceLoader.drop_last_batch_of_sequence

        input_sequence = [[1, 2, 3], [4]]
        target_sequence = [[1, 2, 3], [4]]

        in_dropped, target_dropped = func(input_sequence, target_sequence)
        self.assertEqual(len(in_dropped), 1)
        self.assertEqual(len(target_dropped), 1)

        input_sequence = [self.train_images[0][:3]]
        target_sequence = [self.train_masks[0][:3]]
        in_dropped, target_dropped = func(input_sequence, target_sequence)
        self.assertEqual(len(in_dropped), 0)
        self.assertEqual(len(target_dropped), 0)

    def test_shuffle_batches(self):
        self.batched_loader.shuffle_batches()
        shuffled_input_batches = self.batched_loader.input_batches
        shuffled_target_batches = self.batched_loader.target_batches

        for in_batch, target_batch in zip(shuffled_input_batches, shuffled_target_batches):
            for in_vol, tar_vol in zip(in_batch, target_batch):
                for img, mask in zip(in_vol, tar_vol):
                    img_name = os.path.basename(img)
                    mask_name = os.path.basename(mask)
                    self.assertEqual(img_name, mask_name)
                    # checking folders
                    img_name = img.replace('images', 'masks')
                    self.assertEqual(mask, img_name)

        self.batched_loader.shuffle_batches()
        shuffled_input_batches = self.batched_loader.input_batches
        shuffled_target_batches = self.batched_loader.target_batches

        for in_batch, tar_batch in zip(shuffled_input_batches, shuffled_target_batches):
            for in_sequence, tar_sequence in zip(in_batch, tar_batch):
                for img, mask in zip(in_sequence, tar_sequence):
                    img_name = os.path.basename(img)
                    mask_name = os.path.basename(mask)
                    self.assertEqual(img_name, mask_name)

    def test_load_sequence_targets_to_tensor(self):
        func = self.batched_loader.load_sequence_targets_to_tensor

        mask_paths = self.train_masks[0][:5]
        stacked = func(mask_paths)
        self.assertEqual(len(stacked.shape), 3)
        self.assertEqual(stacked.shape[0], 5)
        self.assertEqual(stacked.shape[1], 256)
        self.assertEqual(stacked.shape[2], 256)


class LoaderBase3DTests(TestCase):
    def setUp(self):
        # Testing with BatchedSequenceLoader to get the base implementations
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.train_images = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))]
        self.train_masks = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'masks', 'volume_1'))]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        # assuming, that data_loading works
        # this already slices correct, but sequence loader can do this, too. Therefore i changed unet3d depth at loader
        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            self.train_images, self.train_masks, depth_or_sequence_size=4, is_3d=True, train_data_percentage=70,
        )
        self.batched_loader = BatchedSequenceLoader(
            input_paths=train,
            target_paths=target,
            batch_size=5,
            classes=6,
            channels=1,
            rnn_sequence_size=4
        )

    def tearDown(self):
        sys.stdout = self.previous_out

    def test_load_batch_tensor(self):
        img_transformation = self.batched_loader.img_transform
        input_tensor, target_tensor = self.batched_loader.load_batch_tensor(0)
        input_data = self.batched_loader.input_batches[0]
        target_data = self.batched_loader.target_batches[0]

        for img, mask in zip(input_data[0], target_data[0]):
            img_name = os.path.basename(img)
            mask_name = os.path.basename(mask)
            self.assertEqual(img_name, mask_name)

        self.assertEqual(len(input_tensor.shape), 5)
        self.assertEqual(len(target_tensor.shape), 4)

        self.assertEqual(input_tensor[0].shape[0], target_tensor[0].shape[0])
        self.assertEqual(input_tensor[0].shape[2], target_tensor[0].shape[1])
        self.assertEqual(input_tensor[0].shape[3], target_tensor[0].shape[2])

        for i in range(input_tensor[0].shape[0]):
            img = input_tensor[0, i, 0, :, :]
            expected_img = img_transformation(cv2.imread(input_data[0][i], cv2.IMREAD_GRAYSCALE))
            self.assertTrue(torch.eq(img, expected_img).all())

        for i in range(target_tensor[0].shape[0]):
            mask = target_tensor[0, i, :, :]
            expected_mask = torch.tensor(cv2.imread(target_data[0][i], cv2.IMREAD_GRAYSCALE))
            self.assertTrue(torch.eq(mask, expected_mask).all())

    def test_create_volume_or_rnn_sequences(self):
        self.assertEqual(self.batched_loader.depth_or_sequence_size, 4)
        self.assertEqual(len(self.train_images[0]), 256)
        expected_sequences = len(self.batched_loader.images)

        input_sequences, target_sequences = self.batched_loader.create_volume_or_rnn_sequences()
        self.assertEqual(len(input_sequences), expected_sequences)
        self.assertEqual(len(target_sequences), expected_sequences)

        self.batched_loader.depth_or_sequence_size = 5
        input_sequences, target_sequences = self.batched_loader.create_volume_or_rnn_sequences()
        self.assertEqual(len(input_sequences), 0)
        self.assertEqual(len(target_sequences), 0)

    def test_create_batches(self):
        self.assertEqual(self.batched_loader.depth_or_sequence_size, 4)
        self.assertEqual(self.batched_loader.batch_size, 5)
        self.assertEqual(len(self.batched_loader.images), 45)

        self.batched_loader.drop_last = False
        self.batched_loader.batch_size = 6  # 45/6 = 7.5

        self.batched_loader.create_batches()
        self.assertEqual(len(self.batched_loader.input_batches), 8)
        self.assertEqual(len(self.batched_loader.dataset), 45)

        for idx, batch in enumerate(self.batched_loader.input_batches):
            if idx == 7:
                # last batch has only 3 members
                self.assertEqual(len(batch), 3)
            else:
                self.assertEqual(len(batch), self.batched_loader.batch_size)

            for sequence in batch:
                self.assertEqual(len(sequence), self.batched_loader.depth_or_sequence_size)

        self.batched_loader.drop_last = True
        self.batched_loader.create_batches()
        self.assertEqual(len(self.batched_loader.input_batches), 7)
        self.assertEqual(len(self.batched_loader.dataset), 42)  # minus last not complete batch

        for batch in self.batched_loader.input_batches:
            self.assertEqual(len(batch), self.batched_loader.batch_size)
            for sequence in batch:
                self.assertEqual(len(sequence), self.batched_loader.depth_or_sequence_size)


class VolumeLoaderTests(TestCase):
    def setUp(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.train_images = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))]
        self.train_masks = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'masks', 'volume_1'))]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        # assuming, that data_loading works
        # this already slices correct, but sequence loader can do this, too. Therefore i changed unet3d depth at loader
        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            self.train_images, self.train_masks, is_3d=True, depth_or_sequence_size=32,
        )
        self.volume_loader = VolumeLoader(
            input_paths=train,
            target_paths=target,
            batch_size=5,
            classes=6,
            channels=1,
            depth=5
        )

    def tearDown(self):
        sys.stdout = self.previous_out

    def test_load_sequence_inputs_to_tensor(self):
        tensor = self.volume_loader.load_sequence_inputs_to_tensor(self.volume_loader.images[0][:5])
        self.assertEqual(len(tensor.shape), 4)
        self.assertEqual(tensor.shape[0], 1)
        self.assertEqual(tensor.shape[1], 5)
        self.assertEqual(tensor.shape[2], 256)
        self.assertEqual(tensor.shape[3], 256)

        self.volume_loader.channels = 3
        self.volume_loader.img_transform = get_rgb_image_transformation()
        tensor = self.volume_loader.load_sequence_inputs_to_tensor(self.volume_loader.images[0][:5])
        self.assertEqual(len(tensor.shape), 4)
        self.assertEqual(tensor.shape[0], 3)
        self.assertEqual(tensor.shape[1], 5)
        self.assertEqual(tensor.shape[2], 256)
        self.assertEqual(tensor.shape[3], 256)


class BatchedSequenceLoaderTests(TestCase):
    def setUp(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.train_images = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))]
        self.train_masks = [get_file_paths_in_folder(os.path.join(train_folder_3d, 'masks', 'volume_1'))]

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

        # assuming, that data_loading works
        # this already slices correct, but sequence loader can do this, too. Therefore i changed unet3d depth at loader
        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            self.train_images, self.train_masks, is_3d=True, depth_or_sequence_size=5,
        )
        self.batched_loader = BatchedSequenceLoader(
            input_paths=train,
            target_paths=target,
            batch_size=5,
            classes=6,
            channels=1,
            rnn_sequence_size=5
        )

    def tearDown(self):
        sys.stdout = self.previous_out

    def test_load_sequence_inputs_to_tensor(self):
        tensor = self.batched_loader.load_sequence_inputs_to_tensor(self.batched_loader.images[0][:5])
        self.assertEqual(len(tensor.shape), 4)
        self.assertEqual(tensor.shape[0], 5)
        self.assertEqual(tensor.shape[1], 1)
        self.assertEqual(tensor.shape[2], 256)
        self.assertEqual(tensor.shape[3], 256)

        self.batched_loader.channels = 3
        self.batched_loader.img_transform = get_rgb_image_transformation()
        tensor = self.batched_loader.load_sequence_inputs_to_tensor(self.batched_loader.images[0][:5])
        self.assertEqual(len(tensor.shape), 4)
        self.assertEqual(tensor.shape[0], 5)
        self.assertEqual(tensor.shape[1], 3)
        self.assertEqual(tensor.shape[2], 256)
        self.assertEqual(tensor.shape[3], 256)
