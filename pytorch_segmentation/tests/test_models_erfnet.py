from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import ERFNet


class ERFNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('ERFNet', ERFNet)
