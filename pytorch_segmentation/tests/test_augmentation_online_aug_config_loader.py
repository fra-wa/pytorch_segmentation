import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation.constants import create_folder
from pytorch_segmentation.data_augmentation.online_aug_config_loader import load_parameters_to_dict, \
    load_online_aug_config
from pytorch_segmentation.file_handling.utils import copy_or_move_file_or_files, remove_file


class ConfigLoaderTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        configs_path = os.path.join(files_path, 'augmentation', 'configs')
        self.img_config = os.path.join(configs_path, 'fake_img_config_parameters.csv')
        self.volume_config = os.path.join(configs_path, 'fake_volume_config_parameters.csv')

        module_dir = os.path.dirname(os.path.dirname(current_dir))
        configs_folder = os.path.join(module_dir, 'configs')
        create_folder(configs_folder)
        online_aug_folder = os.path.join(configs_folder, 'online_aug')
        create_folder(online_aug_folder)
        copy_or_move_file_or_files(self.img_config, online_aug_folder)
        copy_or_move_file_or_files(self.volume_config, online_aug_folder)
        self.fake_img_config = os.path.join(online_aug_folder, 'fake_img_config_parameters.csv')
        self.fake_volume_config = os.path.join(online_aug_folder, 'fake_volume_config_parameters.csv')
        self.img_dataset_name = 'fake_img_config_parameters'
        self.volume_dataset_name = 'fake_volume_config_parameters'

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        remove_file(self.fake_img_config)
        remove_file(self.fake_volume_config)
        sys.stdout = self.previous_out

    def test_load_parameters(self):
        expected_image_parameters = {
            'brightness_prob': 0.15,
            'contrast_prob': 0.15,
            'gaussian_filter_prob': 0.03,
            'gaussian_noise_prob': 0.05,
            'random_erasing_prob': 0.05,
            'channel_shuffle_prob': 0.05,
            'color_to_hsv_prob': 0.05,
            'iso_noise_prob': 0.05,
            'random_fog_prob': 0.02,
            'random_rain_prob': 0.02,
            'random_shadow_prob': 0.02,
            'random_snow_prob': 0.02,
            'random_sun_flair_prob': 0.02,
            'rgb_shift_prob': 0.05,
            'solarize_prob': 0.05,
            'to_gray_prob': 0.05,
            'to_sepia_prob': 0.05,
            'elastic_distortion_prob': 0.05,
            'flip_prob': 0.1,
            'grid_distortion_prob': 0.05,
            'optical_distortion_prob': 0.05,
            'random_crop_prob': 0.05,
            'resize_prob': 0.15,
            'rotate_by_angle_prob': 0.15,
            'tilt_prob': 0.05,
        }
        expected_volume_parameters = {
            'add_blur_3d_prob': 0.01,
            'add_noise_3d_prob': 0.05,
            'brightness_3d_prob': 0.1,
            'contrast_3d_prob': 0.1,
            'histogram_normalization_3d_prob': 0.00,
            'random_erasing_3d_prob': 0.00,
            'random_shadow_3d_prob': 0.00,
            'sharpen_3d_with_blur_prob': 0.05,
            'flip_prob': 0.05,
            'random_crop_3d_prob': 0.05,
            'random_resize_3d_prob': 0.05,
            'rotate_z_3d_prob': 0.05,
            'squeeze_prob': 0.05,
            'tilt_prob': 0.05,
        }
        image_kwargs = load_parameters_to_dict(self.fake_img_config)
        self.assertEqual(len(expected_image_parameters.keys()), len(image_kwargs.keys()))

        for key, value in image_kwargs.items():
            self.assertTrue(key in expected_image_parameters.keys())
            self.assertEqual(expected_image_parameters[key], value)

        volume_kwargs = load_parameters_to_dict(self.fake_volume_config)
        self.assertEqual(len(expected_volume_parameters.keys()), len(volume_kwargs.keys()))
        for key, value in volume_kwargs.items():
            self.assertTrue(key in expected_volume_parameters.keys())
            self.assertEqual(expected_volume_parameters[key], value)

    def test_load_online_aug_config(self):
        expected_image_parameters = {
            'brightness_prob': 0.15,
            'contrast_prob': 0.15,
            'gaussian_filter_prob': 0.03,
            'gaussian_noise_prob': 0.05,
            'random_erasing_prob': 0.05,
            'channel_shuffle_prob': 0.05,
            'color_to_hsv_prob': 0.05,
            'iso_noise_prob': 0.05,
            'random_fog_prob': 0.02,
            'random_rain_prob': 0.02,
            'random_shadow_prob': 0.02,
            'random_snow_prob': 0.02,
            'random_sun_flair_prob': 0.02,
            'rgb_shift_prob': 0.05,
            'solarize_prob': 0.05,
            'to_gray_prob': 0.05,
            'to_sepia_prob': 0.05,
            'elastic_distortion_prob': 0.05,
            'flip_prob': 0.1,
            'grid_distortion_prob': 0.05,
            'optical_distortion_prob': 0.05,
            'random_crop_prob': 0.05,
            'resize_prob': 0.15,
            'rotate_by_angle_prob': 0.15,
            'tilt_prob': 0.05,
        }
        expected_volume_parameters = {
            'add_blur_3d_prob': 0.01,
            'add_noise_3d_prob': 0.05,
            'brightness_3d_prob': 0.1,
            'contrast_3d_prob': 0.1,
            'histogram_normalization_3d_prob': 0.00,
            'random_erasing_3d_prob': 0.00,
            'random_shadow_3d_prob': 0.00,
            'sharpen_3d_with_blur_prob': 0.05,
            'flip_prob': 0.05,
            'random_crop_3d_prob': 0.05,
            'random_resize_3d_prob': 0.05,
            'rotate_z_3d_prob': 0.05,
            'squeeze_prob': 0.05,
            'tilt_prob': 0.05,
        }
        image_kwargs = load_online_aug_config(self.img_dataset_name)
        self.assertEqual(len(expected_image_parameters.keys()), len(image_kwargs.keys()))

        for key, value in image_kwargs.items():
            self.assertTrue(key in expected_image_parameters.keys())
            self.assertEqual(expected_image_parameters[key], value)

        volume_kwargs = load_online_aug_config(self.volume_dataset_name)
        self.assertEqual(len(expected_volume_parameters.keys()), len(volume_kwargs.keys()))
        for key, value in volume_kwargs.items():
            self.assertTrue(key in expected_volume_parameters.keys())
            self.assertEqual(expected_volume_parameters[key], value)

        kwargs = load_online_aug_config('not_a_real_dataset_name')
        self.assertEqual(len(kwargs.keys()), 0)
