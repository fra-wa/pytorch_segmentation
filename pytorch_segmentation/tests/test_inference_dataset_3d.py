import os

from unittest import TestCase

from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.inference.datasets.inference_dataset_3d import Section


class SectionTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        train_folder_3d = os.path.join(files_path, 'data_3d')
        self.images = get_file_paths_in_folder(os.path.join(train_folder_3d, 'images', 'volume_1'))

    def test_setup(self):
        input_size = 100
        total_images = 100
        input_depth = 20
        padding = 1
        overlap = 2 * padding + 0

        section_depths = []
        for d in range(0, total_images - input_depth, input_depth - overlap):
            section_depths.append((d, d + input_depth))
        section_depths.append((total_images - input_depth, total_images - input_depth + input_depth))

        section = Section(
            self.images[:input_depth],
            d_start=0,
            input_size=input_size,
            overlap_d=2,
            overlap_h=50,
            overlap_w=50,
            total_images=total_images,
            padding=1,
        )
        self.assertEqual(section.section_id, 0)
        self.assertEqual(section.ends_sections, [0])

        section = Section(
            self.images[:input_depth],
            d_start=18,
            input_size=input_size,
            overlap_d=2,
            overlap_h=50,
            overlap_w=50,
            total_images=100,
            padding=1,
        )
        self.assertEqual(section.section_id, 1)
        self.assertEqual(section.ends_sections, [1])

        section = Section(
            self.images[:input_depth],
            d_start=0,
            input_size=input_size,
            overlap_d=0,
            overlap_h=50,
            overlap_w=50,
            total_images=100,
            padding=0,
        )
        self.assertEqual(section.section_id, 0)
        self.assertEqual(section.ends_sections, [0])

        padding = 1
        overlap = round((input_depth - 2 * padding) * 75 / 100 + padding * 2)

        section_depths = []
        for d in range(0, total_images - input_depth, input_depth - overlap):
            section_depths.append((d, d + input_depth))
        section_depths.append((total_images - input_depth, total_images - input_depth + input_depth))

        section = Section(
            self.images[:input_depth],
            d_start=16,
            input_size=input_size,
            overlap_d=16,
            overlap_h=50,
            overlap_w=50,
            total_images=100,
            padding=1,
        )
        self.assertEqual(section.section_id, 4)
        self.assertEqual(section.ends_sections, [0])

        input_depth = 128
        input_size = 128
        padding = 1
        overlap_d = 50
        overlap = round((input_depth - 2 * padding) * overlap_d / 100 + padding * 2)
        total_images = 512

        section_depths = []
        for d in range(0, total_images - input_depth, input_depth - overlap):
            section_depths.append((d, d + input_depth))
        section_depths.append((total_images - input_depth, total_images - input_depth + input_depth))

        section = Section(
            self.images[:input_depth],
            d_start=126,
            input_size=input_size,
            overlap_d=overlap,
            overlap_h=50,
            overlap_w=50,
            total_images=total_images,
            padding=1,
        )
        self.assertEqual(section.section_id, 2)
        self.assertEqual(section.ends_sections, [1])

        section = Section(
            self.images[:128],
            d_start=378,
            input_size=input_size,
            overlap_d=overlap,
            overlap_h=50,
            overlap_w=50,
            total_images=512,
            padding=1,
        )
        self.assertEqual(section.section_id, 6)
        self.assertEqual(section.ends_sections, [])

        section = Section(
            self.images[:128],
            d_start=384,
            input_size=input_size,
            overlap_d=overlap,
            overlap_h=50,
            overlap_w=50,
            total_images=512,
            padding=1,
        )
        self.assertEqual(section.section_id, 7)
        self.assertEqual(section.ends_sections, [5, 6, 7])
