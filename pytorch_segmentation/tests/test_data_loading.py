import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.training.data_loading import create_train_valid_sequences_or_volumes
from pytorch_segmentation.training.data_loading import NotEnoughTrainingDataError
from pytorch_segmentation.training.data_loading import set_up_loaders
from pytorch_segmentation.training.data_loading import split_volume_sequences
from pytorch_segmentation.training.data_loading import slice_sub_volumes_to_train_data
from pytorch_segmentation.training.data_loading import slice_sequence_or_files
from pytorch_segmentation.training.data_loading import slice_data_to_sub_volumes_or_sequences


class FakeParams:
    def __init__(self,
                 architecture,
                 bs=1,
                 in_size=256,
                 classes=6,
                 channels=1,
                 drop_last=True,
                 depth_or_rnn=12,
                 reproduce=True):
        self.architecture = architecture
        self.batch_size = bs
        self.input_size = in_size
        self.classes = classes
        self.channels = channels
        self.drop_last = drop_last
        self.depth_3d = depth_or_rnn
        self.rnn_sequence_size = depth_or_rnn
        self.reproduce = reproduce
        self.adaptive_aug = False
        self.online_aug = False


class DataLoadingTests(TestCase):
    def setUp(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.train_masks = os.path.join(files_path, 'data_2d', 'masks')

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        sys.stdout = self.previous_out

    def test_slice_sequence(self):
        sequence = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        train, valid = slice_sequence_or_files(sequence, training_percentage=70)
        self.assertEqual(len(train), 7)
        self.assertEqual(len(valid), 3)

        with self.assertRaises(NotEnoughTrainingDataError):
            sequence = [1]
            slice_sequence_or_files(sequence)

        sequence = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        train, valid = slice_sequence_or_files(sequence, training_percentage=50)
        self.assertEqual(len(train), 5)
        self.assertEqual(len(valid), 5)

    def test_slice_sequences_to_volume(self):
        sequences = [get_file_paths_in_folder(self.train_images)[:9]]
        depth = 10
        with self.assertRaises(NotEnoughTrainingDataError):
            slice_data_to_sub_volumes_or_sequences(sequences, depth)

        sequences = [get_file_paths_in_folder(self.train_images)[:30]]

        volumes = slice_data_to_sub_volumes_or_sequences(sequences, depth)
        self.assertEqual(len(volumes), 3)

    def test_split_volume_sequences(self):
        sequences = []
        for i in range(10):
            sequence = [i * 10 + j + 1 for j in range(10)]
            sequences.append(sequence)

        train, remaining = split_volume_sequences(sequences, 70)
        self.assertEqual(len(train), 7)
        self.assertEqual(len(remaining), 3)

        valid, test = split_volume_sequences(remaining, 50)
        self.assertEqual(len(valid), 2)
        self.assertEqual(len(test), 1)

        for sub_list in test:
            for element in sub_list:
                self.assertFalse(any(element in sl for sl in train))
                self.assertFalse(any(element in sl for sl in valid))

        for sub_list in valid:
            for element in sub_list:
                self.assertFalse(any(element in sl for sl in train))

    def test_slice_sub_volumes_to_train_data(self):
        sequences = []
        for i in range(2):
            sequence = [i * 10 + j + 1 for j in range(10)]
            sequences.append(sequence)

        train, valid = slice_sub_volumes_to_train_data(sequences)
        self.assertEqual(len(train), 1)
        self.assertEqual(len(valid), 1)

        sequences = []
        for i in range(4):
            sequence = [i * 10 + j + 1 for j in range(10)]
            sequences.append(sequence)

        train, valid = slice_sub_volumes_to_train_data(sequences, train_data_percentage=25)
        self.assertEqual(len(train), 1)
        self.assertEqual(len(valid), 3)

        with self.assertRaises(NotEnoughTrainingDataError):
            sequences = []
            for i in range(1):
                sequence = [i * 10 + j + 1 for j in range(10)]
                sequences.append(sequence)

            slice_sub_volumes_to_train_data(sequences)

    def test_create_train_valid_test_sequences(self):
        input_sequences = []
        target_sequences = []

        volume_count = 10
        volume_depth = 100

        for i in range(volume_count):
            sequence = [i * volume_depth + j + 1 for j in range(volume_depth)]
            input_sequences.append(sequence)
            sequence = [-1 * (i * volume_depth + j + 1) for j in range(volume_depth)]
            target_sequences.append(sequence)

        # creates list of input paths
        # if a volume has 100 slices, then input paths for data loader will be sized like:
        # train 70% valid 15% test 15% --> 70 / 15 / 15 slices
        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            input_sequences, target_sequences, is_3d=True, depth_or_sequence_size=10, train_data_percentage=70
        )

        self.assertEqual(len(train), 70)
        self.assertEqual(len(target), 70)
        self.assertEqual(len(val_train), 30)
        self.assertEqual(len(val_target), 30)

        for input_, target_ in zip(train, target):
            for in_element, target_element in zip(input_, target_):
                self.assertEqual(in_element, -1 * target_element)

        for input_, target_ in zip(val_train, val_target):
            for in_element, target_element in zip(input_, target_):
                self.assertEqual(in_element, -1 * target_element)

        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            input_sequences, target_sequences, is_3d=True, depth_or_sequence_size=10, shuffle_data_split=True
        )
        train_data = [train, target]
        valid_data = [val_train, val_target]

        for input_, target_ in zip(train_data[0], train_data[1]):
            for in_element, target_element in zip(input_, target_):
                self.assertEqual(in_element, -1 * target_element)

        for input_, target_ in zip(valid_data[0], valid_data[1]):
            for in_element, target_element in zip(input_, target_):
                self.assertEqual(in_element, -1 * target_element)

        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            input_sequences, target_sequences, is_3d=False
        )
        train_data = [train, target]
        valid_data = [val_train, val_target]
        self.assertEqual(len(train_data[0]), 10)
        self.assertEqual(len(train_data[1]), 10)
        self.assertEqual(len(valid_data[0]), 10)
        self.assertEqual(len(valid_data[1]), 10)

        train, target, val_train, val_target = create_train_valid_sequences_or_volumes(
            input_sequences, target_sequences, is_3d=False, shuffle_data_split=True
        )
        train_data = [train, target]
        valid_data = [val_train, val_target]

        for input_, target in zip(train_data[0], train_data[1]):
            target_list = [-1 * element for element in target]
            self.assertListEqual(input_, target_list)

        for input_, target in zip(valid_data[0], valid_data[1]):
            target_list = [-1 * element for element in target]
            self.assertListEqual(input_, target_list)

    def test_setup_loaders(self):
        image_paths = get_file_paths_in_folder(self.train_images)
        mask_paths = get_file_paths_in_folder(self.train_images)

        # 4 * 10 = 40, train has 180 images, drop last is true -> total length is 4
        train, valid = set_up_loaders(
            [image_paths],
            [mask_paths],
            architecture=constants.ResidualUNet3D_NAME,
            channels=1,
            classes=6,
            batch_size=4,
            reproduce=True,
            online_aug=False,
            input_size=256,
            input_depth=10,
            test_case=True,
            valid_images_paths=None,
            valid_masks_paths=None,
            dataset_mean=None,
            dataset_std=None,
            train_data_percentage=70,
            max_workers=None,
            multi_gpu_training=False,
            drop_last=True,
        )
        self.assertEqual(train.batch_size, 4)  # B
        self.assertEqual(train.dataset.channels, 1)  # C
        self.assertEqual(len(train), 4)  # 256//10 = 25, math.ceil(0.7 * 25) = 18 -> //4 = 4, drop last true

        train, valid = set_up_loaders(
            [image_paths],
            [mask_paths],
            architecture=constants.ResidualUNet3D_NAME,
            channels=1,
            classes=6,
            batch_size=4,
            reproduce=True,
            online_aug=False,
            input_size=256,
            input_depth=10,
            test_case=True,
            valid_images_paths=None,
            valid_masks_paths=None,
            dataset_mean=None,
            dataset_std=None,
            train_data_percentage=70,
            max_workers=None,
            multi_gpu_training=False,
            drop_last=True,
        )
        self.assertEqual(len(train), 5)  # 256//10 = 25, math.ceil(0.7 * 25) = 18 -> /4 = 4.5, drop last False -> 5

        train, valid = set_up_loaders(
            [image_paths],
            [mask_paths],
            architecture=constants.ResidualUNet3D_NAME,
            channels=1,
            classes=6,
            batch_size=6,
            reproduce=True,
            online_aug=False,
            input_size=256,
            input_depth=5,
            test_case=True,
            valid_images_paths=None,
            valid_masks_paths=None,
            dataset_mean=None,
            dataset_std=None,
            train_data_percentage=70,
            max_workers=None,
            multi_gpu_training=False,
            drop_last=True,
        )
        self.assertEqual(len(train), 6)  # 256//5 = 51, math.ceil(0.7*51) = 36 sub volumes -->/6 = 6

        train, valid = set_up_loaders(
            [image_paths],
            [mask_paths],
            architecture=constants.ResidualUNet3D_NAME,
            channels=1,
            classes=6,
            batch_size=12,
            reproduce=True,
            online_aug=False,
            input_size=256,
            input_depth=10,
            test_case=True,
            valid_images_paths=None,
            valid_masks_paths=None,
            dataset_mean=None,
            dataset_std=None,
            train_data_percentage=70,
            max_workers=None,
            multi_gpu_training=False,
            drop_last=True,
        )
        self.assertEqual(len(train), 2)  # 256//10 = 25, math.ceil(0.7*25) = 18, 18/12 = 1,.. --> len 2 drop last false

        train, valid = set_up_loaders(
            image_paths,
            mask_paths,
            architecture=constants.ResidualUNet3D_NAME,
            channels=1,
            classes=6,
            batch_size=1,
            reproduce=True,
            online_aug=False,
            input_size=256,
            input_depth=12,
            test_case=True,
            valid_images_paths=None,
            valid_masks_paths=None,
            dataset_mean=None,
            dataset_std=None,
            train_data_percentage=70,
            max_workers=None,
            multi_gpu_training=False,
            drop_last=True,
        )
        expected_train_size = round(0.7 * 256)
        self.assertEqual(len(train), expected_train_size)
        self.assertEqual(len(valid), 256 - expected_train_size)

        for i, inputs_target in enumerate(train):
            if i == 1:
                break
            self.assertEqual(len(inputs_target[0].shape), 4)
            self.assertEqual(inputs_target[0].shape[0], 1)
            self.assertEqual(inputs_target[0].shape[1], 1)
            self.assertEqual(inputs_target[0].shape[2], 256)
            self.assertEqual(inputs_target[0].shape[3], 256)

            self.assertEqual(len(inputs_target[1].shape), 3)
            self.assertEqual(inputs_target[1].shape[0], 1)
            self.assertEqual(inputs_target[1].shape[1], 256)
            self.assertEqual(inputs_target[1].shape[2], 256)

        for i, inputs_target in enumerate(valid):
            if i == 1:
                break
            self.assertEqual(len(inputs_target[0].shape), 4)
            self.assertEqual(inputs_target[0].shape[0], 1)
            self.assertEqual(inputs_target[0].shape[1], 1)
            self.assertEqual(inputs_target[0].shape[2], 256)
            self.assertEqual(inputs_target[0].shape[3], 256)

            self.assertEqual(len(inputs_target[1].shape), 3)
            self.assertEqual(inputs_target[1].shape[0], 1)
            self.assertEqual(inputs_target[1].shape[1], 256)
            self.assertEqual(inputs_target[1].shape[2], 256)
