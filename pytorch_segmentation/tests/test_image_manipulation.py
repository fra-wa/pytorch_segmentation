import cv2
import io
import os
import sys

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import copy_dir, copy_or_move_file_or_files, remove_file
from pytorch_segmentation.file_handling.utils import create_folder
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.file_handling.utils import remove_dir
from pytorch_segmentation.train_data_preprocessing.image_manipulation import multiprocess_images_and_masks_simple, \
    convert_color_masks_to_label_masks, get_mapped_label_to_color_or_gray
from pytorch_segmentation.train_data_preprocessing.image_manipulation import slice_image_and_mask


class ImageManipulationTests(TestCase):
    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        self.files_path = os.path.join(current_dir, 'files')
        manipulation_folder = os.path.join(self.files_path, 'data_2d', 'image_manipulation')

        self.images_folder = os.path.join(manipulation_folder, 'images')
        self.masks_folder = os.path.join(manipulation_folder, 'masks')
        self.color_masks_folder = os.path.join(manipulation_folder, 'color_masks')

        create_folder(self.images_folder)
        create_folder(self.masks_folder)
        create_folder(self.color_masks_folder)

        self.images_base = os.path.join(manipulation_folder, 'images_base')
        self.color_masks_base = os.path.join(manipulation_folder, 'color_masks_base')
        self.masks_base = os.path.join(manipulation_folder, 'masks_base')

        # copy and delete afterwards. Do not manipulate the base folders!
        copy_dir(self.color_masks_base, self.color_masks_folder)
        copy_dir(self.images_base, self.images_folder)
        copy_dir(self.masks_base, self.masks_folder)

        # for image slicing and such
        self.out_images_folder = os.path.join(manipulation_folder, 'out_images')
        self.out_masks_folder = os.path.join(manipulation_folder, 'out_masks')

        create_folder(self.out_images_folder)
        create_folder(self.out_masks_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self) -> None:
        remove_dir(self.color_masks_folder)
        remove_dir(self.masks_folder)
        remove_dir(self.images_folder)
        remove_dir(self.out_images_folder)
        remove_dir(self.out_masks_folder)

        different_masks_folder = os.path.join(self.files_path, 'masks')
        remove_dir(os.path.join(different_masks_folder, 'tmp'))
        remove_dir(os.path.join(different_masks_folder, 'original'))

        sys.stdout = self.previous_out

    def test_slice_single_image_and_mask(self):
        img_path = get_file_paths_in_folder(self.images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)[0]
        mask_path = get_file_paths_in_folder(self.masks_folder, extension=constants.SUPPORTED_MASK_TYPES)[0]

        input_size = 512

        img = cv2.imread(img_path)
        mask = cv2.imread(mask_path, -1)
        new_image_paths, new_mask_paths = slice_image_and_mask(
            img, mask, img_path, mask_path, input_size, self.out_images_folder, self.out_masks_folder
        )
        self.assertEqual(len(new_image_paths), len(new_mask_paths))
        self.assertEqual(len(new_image_paths), 2)

    def test_slice_image_and_mask(self):
        images = get_file_paths_in_folder(self.images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        masks = get_file_paths_in_folder(self.masks_folder, extension=constants.SUPPORTED_MASK_TYPES)

        input_size = 512

        for i_path, m_path in zip(images[:2], masks[:2]):
            img = cv2.imread(i_path)
            mask = cv2.imread(m_path, -1)
            new_image_paths, new_mask_paths = slice_image_and_mask(
                img, mask, i_path, m_path, input_size, self.out_images_folder, self.out_masks_folder
            )
            self.assertEqual(len(new_image_paths), len(new_mask_paths))
            self.assertEqual(len(new_image_paths), 2)

        input_size = 400

        for i_path, m_path in zip(images[:2], masks[:2]):
            img = cv2.imread(i_path)
            mask = cv2.imread(m_path, -1)
            new_image_paths, new_mask_paths = slice_image_and_mask(
                img, mask, i_path, m_path, input_size, self.out_images_folder, self.out_masks_folder
            )
            self.assertEqual(len(new_image_paths), len(new_mask_paths))
            self.assertEqual(len(new_image_paths), 4)

        with self.assertRaises(RuntimeError):
            img = cv2.imread(images[-1])
            mask = cv2.imread(masks[-1], -1)
            slice_image_and_mask(
                img, mask, images[-1], masks[-1], input_size, self.out_images_folder, self.out_masks_folder
            )

    def test_multiprocess_images_and_masks_simple(self):
        new_image_paths, new_mask_paths = multiprocess_images_and_masks_simple(
            image_paths=get_file_paths_in_folder(self.images_folder),
            mask_paths=get_file_paths_in_folder(self.masks_folder),
            images_folder=self.out_images_folder,
            masks_folder=self.out_masks_folder,
            load_gray=True,
            input_size=512,
            process_nr=0,  # no prints
        )
        self.assertEqual(len(new_image_paths), len(new_mask_paths))
        self.assertEqual(len(new_image_paths), 6)

    def test_get_mapped_label_to_color_or_gray(self):
        masks_folder = os.path.join(self.files_path, 'masks')
        masks = get_file_paths_in_folder(masks_folder)
        self.assertNotEqual(len(masks), 0)

        tmp_folder = os.path.join(masks_folder, 'tmp')
        create_folder(tmp_folder)
        copy_or_move_file_or_files(masks, tmp_folder)

        for mask in masks:
            if 'color_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 9, None)
                self.assertEqual(type(mapped), dict)
                self.assertEqual(len(mapped), 9)
            elif 'gray_0_1_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 2, None)
                self.assertEqual(type(mapped), dict)
                self.assertEqual(len(mapped), 2)
            elif 'gray_0_255_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 2, None)
                self.assertEqual(type(mapped), dict)
                self.assertEqual(len(mapped), 2)

    def test_convert_color_masks_to_label_masks(self):
        masks_folder = os.path.join(self.files_path, 'masks')
        masks = get_file_paths_in_folder(masks_folder)
        self.assertNotEqual(len(masks), 0)

        tmp_folder = os.path.join(masks_folder, 'tmp')
        create_folder(tmp_folder)
        copy_or_move_file_or_files(masks, tmp_folder)

        for mask in masks:
            mapped = None
            if 'color_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 9, None)
            elif 'gray_0_1_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 2, None)
            elif 'gray_0_255_mask.png' in mask:
                mapped = get_mapped_label_to_color_or_gray([mask], 2, None)

            if mapped is not None:
                if mapped is not None:
                    backup_folder = os.path.join(masks_folder, 'original')
                    create_folder(backup_folder)
                    convert_color_masks_to_label_masks([mask], mapped, 0)
                    self.assertTrue(os.path.isfile(os.path.join(backup_folder, os.path.basename(mask))))
                    remove_dir(backup_folder)

                    mask_name = os.path.basename(mask)
                    tmp_backup_file = os.path.join(tmp_folder, mask_name)
                    self.assertTrue(os.path.isfile(tmp_backup_file))

                    remove_file(mask)
                    self.assertFalse(os.path.isfile(mask))

                    copy_or_move_file_or_files(tmp_backup_file, masks_folder)
                    self.assertTrue(os.path.isfile(mask))
