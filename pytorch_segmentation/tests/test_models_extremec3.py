from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import ExtremeC3Net


class ExtremeC3NetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('ExtremeC3Net', ExtremeC3Net)
