import numpy as np

from unittest import TestCase

from pytorch_segmentation.inference.utils import get_logits_weight


class InferenceUtilsTests(TestCase):
    def setUp(self) -> None:
        pass

    def test_get_logits_weight(self):

        h = 4
        w = 4
        d = 2
        expected = np.ones((h, w))
        logits = get_logits_weight('sum', h, w)
        self.assertTrue(np.array_equal(logits, expected))
        expected = np.ones((d, h, w))
        logits = get_logits_weight('sum', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array([
            [1, 1, 1, 1],
            [1, 2, 2, 1],
            [1, 2, 2, 1],
            [1, 1, 1, 1],
        ])
        logits = get_logits_weight('linear', h, w)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array([
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
        ])
        logits = get_logits_weight('linear', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        d = 4
        expected = np.array([
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
            [
                [2, 2, 2, 2],
                [2, 4, 4, 2],
                [2, 4, 4, 2],
                [2, 2, 2, 2],
            ],
            [
                [2, 2, 2, 2],
                [2, 4, 4, 2],
                [2, 4, 4, 2],
                [2, 2, 2, 2],
            ],
            [
                [1, 1, 1, 1],
                [1, 2, 2, 1],
                [1, 2, 2, 1],
                [1, 1, 1, 1],
            ],
        ])
        logits = get_logits_weight('linear', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        expected = np.array(
            [[28, 50, 50, 28],
             [50, 87, 87, 50],
             [50, 87, 87, 50],
             [28, 50, 50, 28]]
        )
        logits = get_logits_weight('gauss', h, w)
        self.assertTrue(np.array_equal(logits, expected))

        d = 2
        expected = np.array([
            [
                [21, 37, 37, 21],
                [37, 66, 66, 37],
                [37, 66, 66, 37],
                [21, 37, 37, 21],
            ],
            [
                [21, 37, 37, 21],
                [37, 66, 66, 37],
                [37, 66, 66, 37],
                [21, 37, 37, 21],
            ],
        ])
        logits = get_logits_weight('gauss', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))

        d = 4
        expected = np.array([
            [
                [15, 26, 26, 15],
                [26, 46, 46, 26],
                [26, 46, 46, 26],
                [15, 26, 26, 15],
            ],
            [
                [26, 46, 46, 26],
                [46, 81, 81, 46],
                [46, 81, 81, 46],
                [26, 46, 46, 26],
            ],
            [
                [26, 46, 46, 26],
                [46, 81, 81, 46],
                [46, 81, 81, 46],
                [26, 46, 46, 26],
            ],
            [
                [15, 26, 26, 15],
                [26, 46, 46, 26],
                [26, 46, 46, 26],
                [15, 26, 26, 15],
            ],
        ])
        logits = get_logits_weight('gauss', h, w, depth=d)
        self.assertTrue(np.array_equal(logits, expected))
