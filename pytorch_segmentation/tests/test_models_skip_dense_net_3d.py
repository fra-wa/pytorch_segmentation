from pytorch_segmentation.dl_models import SkipDenseNet3D
from pytorch_segmentation.tests.model_base_tests import ModelBaseTests3D


class SkipDenseNet3DTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('SkipDenseNet3D', SkipDenseNet3D)
