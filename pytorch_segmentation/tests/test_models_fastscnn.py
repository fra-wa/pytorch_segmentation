from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import FastSCNN


class FastSCNNTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('FastSCNN', FastSCNN)
