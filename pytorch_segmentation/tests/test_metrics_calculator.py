import warnings

import numpy as np

from unittest import TestCase

from pytorch_segmentation.training.metrics import MetricsCalculator
from pytorch_segmentation.training.testing import MetricStorage


class OldCalculatorUsedAsBinaryTestReference:
    def __init__(self, decoded_network_output, ground_truth):
        if not decoded_network_output.shape:
            raise ValueError('Network output is empty')

        unique = np.unique(ground_truth)
        if unique.shape[0] > 2:
            raise ValueError('Ground truth volume contains more than two classes!')

        unique = np.unique(decoded_network_output)
        if unique.shape[0] > 2:
            raise ValueError('Predicted volume contains more than two classes!')

        if not len(decoded_network_output.shape) == len(ground_truth.shape):
            raise ValueError(
                f'Shapes of predicted and Ground truth volume are different.\n'
                f'GT shape: {ground_truth.shape}, Predicted shape: {decoded_network_output.shape}'
            )

        for predicted_dim, target_dim in zip(decoded_network_output.shape, ground_truth.shape):
            if predicted_dim != target_dim:
                raise ValueError(
                    f'Dimension missmatch: Dimension of ground truth and predicted volume are different:\n'
                    f'GT shape: {ground_truth.shape}, Predicted shape: {decoded_network_output.shape}'
                )

        self.network_output = decoded_network_output
        self.ground_truth = ground_truth

        self.network_output[self.network_output != 0] = 255
        self.ground_truth[self.ground_truth != 0] = 255

        assert np.min(self.network_output) in [0, 255]
        assert np.max(self.network_output) in [0, 255]
        assert np.max(self.ground_truth) in [0, 255]
        assert np.max(self.ground_truth) in [0, 255]

        self.all_logits = 1
        for shape in self.network_output.shape:
            self.all_logits = shape * self.all_logits

        self.overall_accuracy = None  # 0 to 1

        # True positives; Also called sensitivity or recall
        self.true_positives = None
        self.true_positives_ratio = None

        # True negatives; Also called specificity
        self.true_negatives = None
        self.true_negatives_ratio = None

        # False positives; Also called fallout
        self.false_positives = None
        self.false_positives_ratio = None

        # False negatives; 1 - sensitivity
        self.false_negatives = None
        self.false_negatives_ratio = None

        # Positive Predicted Value; Also called Precision
        self.precision = None

        # Mean IoU
        # Also called jaccard index.
        self.mean_iou = None

        # Dice
        # Also called overlap index or F1-measure
        self.dice_coefficient = None

        # if almost the whole volume belongs to a single class, iou, dice, precision, .. are useless.
        # Data should be equal distributed for perfect test!
        self.true_distribution = np.sum(ground_truth == 255) / self.all_logits
        self.gt_distribution_is_balanced = True if 0.025 < self.true_distribution < 0.975 else False
        # invert dice and co? don't know why I had this
        self.invert_dice_and_co = True if 0.025 > self.true_distribution else False

    def calculate_binary(self):
        """0 is background, 255 is foreground!"""
        true_false = self.network_output == self.ground_truth
        self.overall_accuracy = np.sum(true_false) / self.all_logits

        ground_truth_true_count = np.sum(self.ground_truth == 255)
        ground_truth_false_count = np.sum(self.ground_truth == 0)

        # True positives; Also called sensitivity or recall
        only_network_true = np.copy(self.network_output).astype(np.float16)
        only_network_true[only_network_true == 0] = -1
        self.true_positives = np.sum(only_network_true == self.ground_truth)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")  # if division by 0
            self.true_positives_ratio = self.true_positives / ground_truth_true_count
            if np.isnan(self.true_positives_ratio):
                # there is no foreground in ground truth --> n / 0
                self.true_positives_ratio = 1

        # True negatives; Also called specificity
        only_network_false = np.copy(self.network_output).astype(np.float16)
        only_network_false[only_network_false == 255] = -1
        self.true_negatives = np.sum(only_network_false == self.ground_truth)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")  # if division by 0
            self.true_negatives_ratio = self.true_negatives / ground_truth_false_count
            if np.isnan(self.true_negatives_ratio):
                # there is no background in ground truth --> n / 0
                self.true_negatives_ratio = 1

        # False positives; Also called fallout, 1 - specificity
        self.false_positives_ratio = 1 - self.true_negatives_ratio
        self.false_positives = ground_truth_false_count - self.true_negatives

        # False negatives; 1 - sensitivity
        self.false_negatives_ratio = 1 - self.true_positives_ratio
        self.false_negatives = ground_truth_true_count - self.true_positives

        if self.invert_dice_and_co:
            # invert and calculate again
            ground_truth_true_count = np.sum(self.ground_truth == 0)
            ground_truth_false_count = np.sum(self.ground_truth == 255)
            only_network_true = np.copy(self.network_output).astype(np.float16)
            only_network_true[only_network_true == 255] = -1
            true_positives = np.sum(only_network_true == self.ground_truth)
            only_network_false = np.copy(self.network_output).astype(np.float16)
            only_network_false[only_network_false == 0] = -1
            true_negatives = np.sum(only_network_false == self.ground_truth)
            false_positives = ground_truth_false_count - true_negatives
            false_negatives = ground_truth_true_count - true_positives
        else:
            true_positives = self.true_positives
            false_positives = self.false_positives
            false_negatives = self.false_negatives

        # Positive Predicted Value; Also called Precision
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")  # if division by 0
            self.precision = true_positives / (true_positives + false_positives)
        if true_positives == 0:
            if ground_truth_true_count > 0:
                self.precision = 0
            else:
                self.precision = 1

        # Dice
        # Also called overlap index or F1-measure
        # The Dice coefficient [13] (DICE), also called the overlap index, is the most used metric in validating medical
        # volume segmentations. In addition to the direct comparison between automatic and ground truth segmentations,
        # it is common to use the DICE to measure reproducibility (repeatability). Zou et al. [1] used the DICE as a
        # measure of the reproducibility as a statistical validation of manual annotation where segmenters repeatedly
        # annotated the same MRI image, then the pair-wise overlap of the repeated segmentations is calculated using
        # the DICE.
        # [https://bmcmedimaging.biomedcentral.com/articles/10.1186/s12880-015-0068-x formula 6]
        # TP: true positive
        # FP: false positive
        # FN: false negative
        # DICE = 2 * TP / (2 * TP + FP + FN)
        self.dice_coefficient = 2 * true_positives / (2 * true_positives + false_positives + false_negatives)

        # Mean IoU
        # Also called jaccard index.
        # IoU and DICE measure the same aspects and provide the same system ranking. Therefore, it does
        # not provide additional information to select both of them together as validation metrics:
        #
        # DICE = 2 * IoU / (1 + IoU)
        # IoU = DICE / (2 - DICE) = TP / (TP + FP + FN)
        #
        # IoU is always larger than DICE
        self.mean_iou = self.dice_coefficient / (2 - self.dice_coefficient)


class OldMetricStorage:
    def __init__(self):
        self.overall_accuracy_list = []
        self.true_positives_list = []
        self.true_positives_ratio_list = []
        self.true_negatives_list = []
        self.true_negatives_ratio_list = []
        self.false_positives_list = []
        self.false_positives_ratio_list = []
        self.false_negatives_list = []
        self.false_negatives_ratio_list = []
        self.precision_list = []
        self.mean_iou_list = []
        self.dice_coefficient_list = []

        # list of true or false --> false if gt only contains background or only foreground
        self.gt_distribution_is_balanced_list = []
        self.logits_list = []

        self.all_logits = 0
        self.overall_accuracy = 0
        self.true_positives = 0
        self.true_positives_ratio = 0
        self.true_negatives = 0
        self.true_negatives_ratio = 0
        self.false_positives = 0
        self.false_positives_ratio = 0
        self.false_negatives = 0
        self.false_negatives_ratio = 0
        self.precision = 0
        self.mean_iou = 0
        self.dice_coefficient = 0
        self.gt_distribution_is_balanced = True

    def store_calculator_results(self, calculator):
        if not isinstance(calculator, (OldCalculatorUsedAsBinaryTestReference, )):
            raise ValueError('Calculator mus be a any Metrics Calculator instance!')

        self.overall_accuracy_list.append(calculator.overall_accuracy)
        self.true_positives_list.append(calculator.true_positives)
        self.true_positives_ratio_list.append(calculator.true_positives_ratio)
        self.true_negatives_list.append(calculator.true_negatives)
        self.true_negatives_ratio_list.append(calculator.true_negatives_ratio)
        self.false_positives_list.append(calculator.false_positives)
        self.false_positives_ratio_list.append(calculator.false_positives_ratio)
        self.false_negatives_list.append(calculator.false_negatives)
        self.false_negatives_ratio_list.append(calculator.false_negatives_ratio)
        self.precision_list.append(calculator.precision)
        self.mean_iou_list.append(calculator.mean_iou)
        self.dice_coefficient_list.append(calculator.dice_coefficient)

        self.gt_distribution_is_balanced_list.append(calculator.gt_distribution_is_balanced)

        self.logits_list.append(calculator.all_logits)

    def combine_storages(self, storage):
        if not isinstance(storage, (OldMetricStorage,)):
            raise ValueError('Calculator mus be a MetricsStorage instance!')

        self.overall_accuracy_list += storage.overall_accuracy_list
        self.true_positives_list += storage.true_positives_list
        self.true_positives_ratio_list += storage.true_positives_ratio_list
        self.true_negatives_list += storage.true_negatives_list
        self.true_negatives_ratio_list += storage.true_negatives_ratio_list
        self.false_positives_list += storage.false_positives_list
        self.false_positives_ratio_list += storage.false_positives_ratio_list
        self.false_negatives_list += storage.false_negatives_list
        self.false_negatives_ratio_list += storage.false_negatives_ratio_list
        self.precision_list += storage.precision_list
        self.mean_iou_list += storage.mean_iou_list
        self.dice_coefficient_list += storage.dice_coefficient_list
        self.gt_distribution_is_balanced_list += storage.gt_distribution_is_balanced_list
        self.logits_list += storage.logits_list

    def calculate_result(self):
        self.all_logits = sum(self.logits_list)
        self.true_positives = sum(self.true_positives_list)
        self.true_negatives = sum(self.true_negatives_list)
        self.false_positives = sum(self.false_positives_list)
        self.false_negatives = sum(self.false_negatives_list)

        # sum(ratio * weighting) -> weight applied by pixels per image or voxels per volume
        # dealing with big numbers here can cause RuntimeWarning in long_scalars --> weight by logits
        max_logits = max(self.logits_list)
        weights = [logits / max_logits for logits in self.logits_list]
        divisor = sum(weights)
        for i, (weight, gt_is_balanced) in enumerate(zip(weights, self.gt_distribution_is_balanced_list)):
            self.overall_accuracy += self.overall_accuracy_list[i] * weight

            if gt_is_balanced:
                # add current result
                self.true_positives_ratio += self.true_positives_ratio_list[i] * weight
                self.true_negatives_ratio += self.true_negatives_ratio_list[i] * weight
                self.false_positives_ratio += self.false_positives_ratio_list[i] * weight
                self.false_negatives_ratio += self.false_negatives_ratio_list[i] * weight
                self.precision += self.precision_list[i] * weight
                self.mean_iou += self.mean_iou_list[i] * weight
                self.dice_coefficient += self.dice_coefficient_list[i] * weight
            else:
                # divisor must be reduced by current weight
                divisor -= weight

        # divide by all logits
        self.overall_accuracy = self.overall_accuracy / sum(weights)
        if divisor != 0:
            # otherwise, gt was not balanced
            self.true_positives_ratio = self.true_positives_ratio / divisor
            self.true_negatives_ratio = self.true_negatives_ratio / divisor
            self.false_positives_ratio = self.false_positives_ratio / divisor
            self.false_negatives_ratio = self.false_negatives_ratio / divisor
            self.precision = self.precision / divisor
            self.mean_iou = self.mean_iou / divisor
            self.dice_coefficient = self.dice_coefficient / divisor


class MetricsCalculatorTests(TestCase):

    def test_calculation(self):
        gt = np.zeros((2, 2))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        classes = 2

        with self.assertRaises(ValueError):
            MetricsCalculator(out, gt, 1)
        with self.assertRaises(ValueError):
            tmp_out = np.zeros((2, 2))
            tmp_out[0:, :] = 2
            MetricsCalculator(tmp_out, gt, classes)
        with self.assertRaises(ValueError):
            tmp_gt = np.zeros((2, 2))
            tmp_gt[0:, :] = 2
            MetricsCalculator(out, tmp_gt, classes)
        with self.assertRaises(ValueError):
            tmp_out = np.zeros((2, 2, 2))
            MetricsCalculator(tmp_out, gt, classes)
        with self.assertRaises(ValueError):
            tmp_out = np.zeros((2, 3))
            MetricsCalculator(tmp_out, gt, classes)

        calculator = MetricsCalculator(out, gt, classes)
        calculator.calculate()

        self.assertEqual(calculator.class_suited_for_calculation[0], 1)
        self.assertEqual(calculator.class_suited_for_calculation[1], 1)

        self.assertEqual(calculator.all_pixels, 4)
        self.assertEqual(calculator.overall_accuracy, 0.5)
        self.assertEqual(calculator.tp[0], 2)
        self.assertEqual(calculator.tp[1], 0)
        self.assertEqual(calculator.tp_ratio[0], 1)
        self.assertEqual(calculator.tp_ratio[1], 0)

        self.assertEqual(calculator.tn[0], 0)
        self.assertEqual(calculator.tn[1], 2)
        self.assertEqual(calculator.tn_ratio[0], 0)
        self.assertEqual(calculator.tn_ratio[1], 1)

        self.assertEqual(calculator.fp[0], 2)
        self.assertEqual(calculator.fp[1], 0)
        self.assertEqual(calculator.fp_ratio[0], 1)
        self.assertEqual(calculator.fp_ratio[1], 0)

        self.assertEqual(calculator.fn[0], 0)
        self.assertEqual(calculator.fn[1], 2)
        self.assertEqual(calculator.fn_ratio[0], 0)
        self.assertEqual(calculator.fn_ratio[1], 1)

        self.assertEqual(calculator.accuracy_per_class[0], 0.5)
        self.assertEqual(calculator.accuracy_per_class[1], 0.5)

        self.assertEqual(calculator.precision[0], 0.5)
        self.assertEqual(calculator.precision[1], 0)

        self.assertEqual(round(calculator.dice_coefficient[0], 2), 0.67)
        self.assertEqual(round(calculator.dice_coefficient[1], 2), 0.0)

        gt = np.zeros((10, 10))
        out = np.zeros_like(gt)
        out[5:, :] = 1

        calculator = MetricsCalculator(out, gt, classes)
        calculator.calculate()
        self.assertEqual(calculator.class_suited_for_calculation[0], 1)
        self.assertEqual(calculator.class_suited_for_calculation[1], 0)

        gt = np.zeros((3, 3))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        gt[2:, :] = 2
        out[1:, :] = 1
        out[2:, :] = 2
        classes = 3

        calculator = MetricsCalculator(out, gt, classes)
        calculator.calculate()

        self.assertEqual(calculator.class_suited_for_calculation[0], 1)
        self.assertEqual(calculator.class_suited_for_calculation[1], 1)
        self.assertEqual(calculator.class_suited_for_calculation[2], 1)
        self.assertEqual(calculator.overall_accuracy, 1)

        gt = np.zeros((3, 3))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        gt[2:, :] = 2
        out[1:, :] = 1
        out[2:, :] = 2
        out[2, -1] = 1
        classes = 3

        calculator = MetricsCalculator(out, gt, classes)
        calculator.calculate()
        self.assertEqual(calculator.class_suited_for_calculation[0], 1)
        self.assertEqual(calculator.class_suited_for_calculation[1], 1)
        self.assertEqual(calculator.class_suited_for_calculation[2], 1)
        self.assertEqual(round(calculator.overall_accuracy, 2), 0.89)

        self.assertEqual(calculator.precision[0], 1)
        self.assertEqual(calculator.precision[1], 0.75)  # 3 correct, 1 not
        self.assertEqual(calculator.precision[2], 1)  # 2 out of 3 correct

        self.assertEqual(calculator.tp_ratio[0], 1)
        self.assertEqual(calculator.tp_ratio[1], 1)
        self.assertEqual(round(calculator.tp_ratio[2], 2), 0.67)

        self.assertEqual(calculator.fn_ratio[0], 0)
        self.assertEqual(calculator.fn_ratio[1], 0)
        self.assertEqual(round(calculator.fn_ratio[2], 2), 0.33)

        self.assertEqual(calculator.dice_coefficient[0], 1)
        self.assertEqual(round(calculator.dice_coefficient[1], 2), 0.86)
        self.assertEqual(calculator.dice_coefficient[2], 0.8)

        self.assertEqual(calculator.mean_iou[0], 1)
        self.assertEqual(calculator.mean_iou[1], 0.75)  # 4 of 3
        self.assertEqual(round(calculator.mean_iou[2], 2), 0.67)  # 2 of 3


class MetricStorageTests(TestCase):
    def setUp(self):
        self.classes = 3
        gt = np.zeros((3, 3))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        gt[2:, :] = 2
        out[1:, :] = 1
        out[2:, :] = 2

        self.calculator_1 = MetricsCalculator(out, gt, self.classes)
        self.calculator_1.calculate()

        gt = np.copy(gt)
        out = np.copy(out)
        out[2, -1] = 1
        self.calculator_2 = MetricsCalculator(out, gt, self.classes)
        self.calculator_2.calculate()

        gt = np.zeros((2, 2))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        self.calculator_3 = MetricsCalculator(out, gt, self.classes)
        self.calculator_3.calculate()

    def test_storing_and_calculation(self):
        storage = MetricStorage(self.classes)
        storage.store_calculator(self.calculator_1)
        storage.store_calculator(self.calculator_2)
        storage.store_calculator(self.calculator_3)

        storage.calculate_result()

        gt_1 = self.calculator_1.ground_truth
        out_1 = self.calculator_1.network_output

        gt_2 = self.calculator_2.ground_truth
        out_2 = self.calculator_2.network_output

        gt_3 = self.calculator_3.ground_truth
        out_3 = self.calculator_3.network_output

        all_px_1 = gt_1.shape[0] * gt_1.shape[1]
        all_px_2 = gt_2.shape[0] * gt_2.shape[1]
        all_px_3 = gt_3.shape[0] * gt_3.shape[1]

        acc_1 = np.sum(gt_1 == out_1) / all_px_1
        acc_2 = np.sum(gt_2 == out_2) / all_px_2
        acc_3 = np.sum(gt_3 == out_3) / all_px_3

        all_pixels = all_px_1 + all_px_2 + all_px_3
        w_1 = all_px_1 / all_pixels
        w_2 = all_px_2 / all_pixels
        w_3 = all_px_3 / all_pixels

        acc_1_weighted = w_1 * acc_1
        acc_2_weighted = w_2 * acc_2
        acc_3_weighted = w_3 * acc_3

        expected_acc = acc_1_weighted + acc_2_weighted + acc_3_weighted

        self.assertEqual(round(storage.overall_accuracy, 2), round(expected_acc, 2))

    def test_against_old_with_binary(self):
        classes = 2
        gt = np.zeros((3, 3))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        out[1:, :] = 1
        calculator_1 = MetricsCalculator(out, gt, classes)
        calculator_1.calculate()

        gt = np.copy(gt)
        out = np.copy(out)
        out[2, -1] = 0
        calculator_2 = MetricsCalculator(out, gt, classes)
        calculator_2.calculate()

        gt = np.zeros((2, 2))
        out = np.zeros_like(gt)
        gt[1:, :] = 1
        calculator_3 = MetricsCalculator(out, gt, classes)
        calculator_3.calculate()

        storage = MetricStorage(classes)
        storage.store_calculator(calculator_1)
        storage.store_calculator(calculator_2)
        storage.store_calculator(calculator_3)
        storage.calculate_result()

        gt_1 = calculator_1.ground_truth
        out_1 = calculator_1.network_output

        gt_2 = calculator_2.ground_truth
        out_2 = calculator_2.network_output

        gt_3 = calculator_3.ground_truth
        out_3 = calculator_3.network_output

        all_px_1 = gt_1.shape[0] * gt_1.shape[1]
        all_px_2 = gt_2.shape[0] * gt_2.shape[1]
        all_px_3 = gt_3.shape[0] * gt_3.shape[1]

        acc_1 = np.sum(gt_1 == out_1) / all_px_1
        acc_2 = np.sum(gt_2 == out_2) / all_px_2
        acc_3 = np.sum(gt_3 == out_3) / all_px_3

        all_pixels = all_px_1 + all_px_2 + all_px_3
        w_1 = all_px_1 / all_pixels
        w_2 = all_px_2 / all_pixels
        w_3 = all_px_3 / all_pixels

        acc_1_weighted = w_1 * acc_1
        acc_2_weighted = w_2 * acc_2
        acc_3_weighted = w_3 * acc_3

        expected_acc = acc_1_weighted + acc_2_weighted + acc_3_weighted

        self.assertEqual(round(storage.overall_accuracy, 2), round(expected_acc, 2))

        classes = 2
        gt_1 = np.zeros((2, 2))
        out_1 = np.zeros_like(gt_1)
        gt_1[1:, :] = 1
        out_1[1:, :] = 1
        calculator_1 = MetricsCalculator(out_1, gt_1, classes)
        calculator_1.calculate()

        gt_2 = np.copy(gt)
        out_2 = np.copy(out)
        out_2[1, -1] = 0
        calculator_2 = MetricsCalculator(out_2, gt_2, classes)
        calculator_2.calculate()

        storage = MetricStorage(classes)
        storage.store_calculator(calculator_1)
        storage.store_calculator(calculator_2)
        storage.calculate_result()

        old_c_1 = OldCalculatorUsedAsBinaryTestReference(out_1, gt_1)
        old_c_1.calculate_binary()
        old_c_2 = OldCalculatorUsedAsBinaryTestReference(out_2, gt_2)
        old_c_2.calculate_binary()

        old_storage = OldMetricStorage()
        old_storage.store_calculator_results(old_c_1)
        old_storage.store_calculator_results(old_c_2)
        old_storage.calculate_result()

        self.assertEqual(storage.true_positives_ratio, old_storage.true_positives_ratio)
        self.assertEqual(storage.true_negatives_ratio, old_storage.true_negatives_ratio)
        self.assertEqual(storage.false_positives_ratio, old_storage.false_positives_ratio)
        self.assertEqual(storage.false_negatives_ratio, old_storage.false_negatives_ratio)
        self.assertEqual(storage.precision, old_storage.precision)
        self.assertEqual(storage.mean_iou, old_storage.mean_iou)
        self.assertEqual(storage.dice_coefficient, old_storage.dice_coefficient)
