from unittest import TestCase

from pytorch_segmentation.inference.predictors.volume_predictor import get_section_index_values


class FakeSection:  # see inference.datasets.inference_dataset_3d.Section
    """Class behaves like a Section but without images and height/width calculation"""
    def __init__(self, d_start, d_end, input_depth, padding, overlap_pct, vol_depth):
        self.d_start = d_start
        self.d_end = d_end
        self.input_depth = input_depth
        self.section_id = None
        self.ends_sections = []

        # following code is copied from inference dataset 3d and is already tested
        # round has to be math.ceil!
        overlap = round((input_depth - 2 * padding) * overlap_pct / 100 + padding * 2)
        # round has to be math.ceil!
        # however, this does not disturb the test.

        section_depths = []
        for d in range(0, vol_depth - input_depth, input_depth - overlap):
            section_depths.append((d, d + input_depth))
        section_depths.append((vol_depth - input_depth, vol_depth - input_depth + input_depth))

        overlaps = []
        for i, (current_start, current_end) in enumerate(section_depths):
            section_overlaps_with = []
            if self.d_start == d_start and current_end == self.d_start + input_depth:
                self.section_id = i

            if current_start != 0:
                current_start += padding
            if self.d_end != vol_depth:
                current_end -= padding
            for j, (start, end) in enumerate(section_depths):
                if start != 0:
                    start += padding
                if end != vol_depth:
                    end -= padding
                if start <= current_end - 1 and end - 1 >= current_start:
                    section_overlaps_with.append(j)
            if i + 1 != len(section_depths):
                overlaps.append([section_overlaps_with[0]])
            else:
                overlaps.append(section_overlaps_with)

        reversed_overlaps = list(reversed(overlaps))
        reversed_end_sections = []
        seen_sections = []
        for overlap_list in reversed_overlaps:
            end_sections = []
            for section_id in overlap_list:
                if section_id not in seen_sections:
                    end_sections.append(section_id)
                    seen_sections.append(section_id)
            reversed_end_sections.append(end_sections)

        end_sections = list(reversed(reversed_end_sections))  # all sections with overlaps
        self.ends_sections = end_sections[self.section_id]


def get_sections(volume_depth, input_depth, overlap_percentage, padding):
    overlap = round((input_depth - 2 * padding) * overlap_percentage / 100 + padding * 2)
    section_depths = []
    for d in range(0, volume_depth - input_depth, input_depth - overlap):
        section_depths.append((d, d + input_depth))
    section_depths.append((volume_depth - input_depth, volume_depth - input_depth + input_depth))

    sections = []
    for d_start, d_end in section_depths:
        sections.append(FakeSection(d_start, d_end, input_depth, padding, overlap_percentage, volume_depth))
    return sections, section_depths


class RecreationProcessTests(TestCase):
    def setUp(self) -> None:
        pass

    def test_get_section_index_values(self):
        volume_depth = 100
        input_depth = 50
        overlap_pct = 0
        padding = 0

        sections, _ = get_sections(volume_depth, input_depth, overlap_pct, padding)

        expected_start_ends = [(0, 50), (50, 100)]
        self.assertEqual(len(sections), len(expected_start_ends))
        for i, (start, end) in enumerate(expected_start_ends):
            self.assertEqual(sections[i].d_start, start)
            self.assertEqual(sections[i].d_end, end)
            self.assertEqual(sections[i].section_id, i)

        for section in sections:
            section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
                section, section.section_id * 50, (section.section_id + 1) * 50, volume_depth, padding
            )
            self.assertEqual(section_d_start, 0)
            self.assertEqual(section_d_end, 50)
            self.assertEqual(result_volume_d_start, 0)

        # real example
        volume_depth = 512
        input_depth = 128
        overlap_pct = 50
        padding = 1

        sections, section_depths = get_sections(volume_depth, input_depth, overlap_pct, padding)
        self.assertEqual(len(sections), 8)
        expected_ends_sections = [
            [],
            [0],
            [1],
            [2],
            [3],
            [4],
            [],
            [5, 6, 7],
        ]

        # expected section_d_start, section_d_end, result_volume_d_start per section needed for recreation
        global_values = [
            [[]],
            [[0, 127], [64, 127]],
            [[127, 190], [127, 190]],
            [[190, 253], [190, 253]],
            [[253, 316], [253, 316]],
            [[316, 379], [316, 379]],
            [[]],
            [[379, 442], [379, 505], [385, 512]],
        ]
        expected_results = [
            [[]],
            [[0, 127, 0], [1, 64, 64]],
            [[64, 127, 0], [1, 64, 0]],
            [[64, 127, 0], [1, 64, 0]],
            [[64, 127, 0], [1, 64, 0]],
            [[64, 127, 0], [1, 64, 0]],
            [[]],
            [[64, 127, 0], [1, 127, 0], [1, 128, 6]],
        ]
        expected_global_logits_start_end = [
            (None, None),
            (0, 127),
            (127, 190),
            (190, 253),
            (253, 316),
            (316, 379),
            (None, None),
            (379, 512),
        ]
        for i, section in enumerate(sections):
            self.assertEqual(section.ends_sections, expected_ends_sections[i])
            if section.ends_sections:
                # fraction of main code, therefore needed to be tested as well
                target_section_ids = section.ends_sections
                target_sections = sections[section.ends_sections[0]:section.ends_sections[-1] + 1]
                sections_for_recreation = sections[target_section_ids[0]: section.section_id + 1]

                if min(target_section_ids) - 1 >= 0:
                    logits_volume_d_start = sections[min(target_section_ids) - 1].d_end - padding
                else:
                    logits_volume_d_start = 0
                # fraction end

                # fraction start
                logits_d_end_global = target_sections[-1].d_end
                if logits_d_end_global != volume_depth:
                    logits_d_end_global -= padding

                out_logits_depth = logits_d_end_global - logits_volume_d_start
                # fraction end
                global_start, global_end = expected_global_logits_start_end[i]
                self.assertEqual(out_logits_depth, global_end - global_start)

                for j, re_section in enumerate(sections_for_recreation):
                    section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
                        re_section, global_start, global_end, volume_depth, padding
                    )
                    expected_start, expected_end, expected_d_vol = expected_results[section.section_id][j]

                    try:
                        self.assertEqual(section_d_start, expected_start)
                        self.assertEqual(section_d_end, expected_end)
                        self.assertEqual(result_volume_d_start, expected_d_vol)
                    except AssertionError as e:
                        print(f'Main       section {section.section_id}')
                        print(f'Recreation section: {re_section.section_id}')
                        print(f'Volume size: {out_logits_depth}')
                        print(f'Expected: start: {expected_start}, end: {expected_end}, d_vol: {expected_d_vol}')
                        print(f'Is      : start: {section_d_start}, end: {section_d_end}, d_vol: '
                              f'{result_volume_d_start}')
                        raise AssertionError(e)

        volume_depth = 100
        input_depth = 50
        overlap_pct = 75
        padding = 1

        sections, _ = get_sections(volume_depth, input_depth, overlap_pct, padding)

        expected_start_ends = [(0, 50), (12, 62), (24, 74), (36, 86), (48, 98), (50, 100)]
        self.assertEqual(len(sections), len(expected_start_ends))
        for i, (start, end) in enumerate(expected_start_ends):
            self.assertEqual(sections[i].d_start, start)
            self.assertEqual(sections[i].d_end, end)
            self.assertEqual(sections[i].section_id, i)

        for section in sections:
            if section.section_id != len(sections) - 1:
                section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
                    section, section.section_id * 12, (section.section_id + 1) * 12 + 50, volume_depth, padding
                )
            else:
                section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
                    section, 51, 100, volume_depth, padding
                )

            if section.section_id == 0:
                self.assertEqual(section_d_start, 0)
                self.assertEqual(section_d_end, 49)
            elif section.section_id == len(sections) - 1:
                self.assertEqual(section_d_start, 1)
                self.assertEqual(section_d_end, 50)
            else:
                self.assertEqual(section_d_start, 1)
                self.assertEqual(section_d_end, 49)

        # another real world example
        volume_depth = 512
        input_depth = 128
        overlap_pct = 75
        padding = 1

        sections, _ = get_sections(volume_depth, input_depth, overlap_pct, padding)

        expected_start_ends = [
            (0, 128),
            (32, 160),
            (64, 192),
            (96, 224),
            (128, 256),
            (160, 288),
            (192, 320),
            (224, 352),
            (256, 384),
            (288, 416),
            (320, 448),
            (352, 480),
            (384, 512),
        ]
        self.assertEqual(len(sections), len(expected_start_ends))
        for i, (start, end) in enumerate(expected_start_ends):
            self.assertEqual(sections[i].d_start, start)
            self.assertEqual(sections[i].d_end, end)
            self.assertEqual(sections[i].section_id, i)

        expected_ends_sections = [
            [],
            [],
            [],
            [0],
            [1],
            [2],
            [3],
            [4],
            [5],
            [6],
            [7],
            [8],
            [9, 10, 11, 12],
        ]

        # expected section_d_start, section_d_end, result_volume_d_start per section needed for recreation
        global_values = [
            [],                                                             # (0, 128), 0
            [],                                                             # (32, 160), 1
            [],                                                             # (64, 192), 2
            [[0, 128], [32, 160], [64, 192], [96, 224]],                    # (96, 224), 3
            [[32, 160], [64, 192], [96, 224], [128, 256]],                  # (128, 256), 4
            [[64, 192], [96, 224], [128, 256], [160, 288]],                 # (160, 288), 5
            [[96, 224], [128, 256], [160, 288], [192, 320]],                # (192, 320), 6
            [[128, 256], [160, 288], [192, 320], [224, 352]],               # (224, 352), 7
            [[160, 288], [192, 320], [224, 352], [256, 384]],               # (256, 384), 8
            [[192, 320], [224, 352], [256, 384], [288, 416]],               # (288, 416), 9
            [[224, 352], [256, 384], [288, 416], [320, 448]],               # (320, 448), 10
            [[256, 384], [288, 416], [320, 448], [352, 480]],               # (352, 480), 11
            [[288, 416], [320, 448], [352, 480], [384, 512]],               # (384, 512), 12
        ]

        expected_global_logits_start_end = [
            (None, None),
            (None, None),
            (None, None),
            (0, 127),
            (127, 159),
            (159, 191),
            (191, 223),
            (223, 255),
            (255, 287),
            (287, 319),
            (319, 351),
            (351, 383),
            (383, 512),
        ]

        expected_results = []
        for i, (global_start, global_stop) in enumerate(expected_global_logits_start_end):
            sub_list = []

            if global_start is not None:
                for start, end in global_values[i]:
                    idx_start = global_start - start
                    if 0 <= idx_start < padding or idx_start <= 0:
                        idx_start = padding
                    idx_end = input_depth - padding
                    if end > global_stop:
                        idx_end = global_stop - start
                        if idx_end > input_depth - padding:
                            idx_end = input_depth - padding

                    if global_start - start < 0:
                        start_in_log_vol = abs(global_start - start) + idx_start
                    elif start + padding > global_start:
                        start_in_log_vol = start + padding - global_start
                    else:
                        start_in_log_vol = 0

                    if start == 0:
                        idx_start = 0
                        start_in_log_vol = 0
                    if end == volume_depth:
                        idx_end = end - start

                    sub_list.append([idx_start, idx_end, start_in_log_vol])
            expected_results.append(sub_list)

        for i, section in enumerate(sections):
            self.assertEqual(section.ends_sections, expected_ends_sections[i])
            if section.ends_sections:
                # fraction of main code, therefore needed to be tested as well
                target_section_ids = section.ends_sections
                target_sections = sections[section.ends_sections[0]:section.ends_sections[-1] + 1]
                sections_for_recreation = sections[target_section_ids[0]: section.section_id + 1]

                if min(target_section_ids) - 1 >= 0:
                    logits_volume_d_start = sections[min(target_section_ids) - 1].d_end - padding
                else:
                    logits_volume_d_start = 0
                # fraction end

                # fraction start
                logits_d_end_global = target_sections[-1].d_end
                if logits_d_end_global != volume_depth:
                    logits_d_end_global -= padding

                out_logits_depth = logits_d_end_global - logits_volume_d_start
                # fraction end
                global_start, global_end = expected_global_logits_start_end[i]
                self.assertEqual(out_logits_depth, global_end - global_start)

                for j, re_section in enumerate(sections_for_recreation):
                    if re_section.section_id == 9 and section.section_id == 12:
                        a = 0
                    section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
                        re_section, global_start, global_end, volume_depth, padding
                    )
                    expected_start, expected_end, expected_d_vol = expected_results[section.section_id][j]
                    section_global_start, section_global_end = global_values[section.section_id][j]
                    self.assertEqual(section_global_start, re_section.d_start)
                    self.assertEqual(section_global_end, re_section.d_end)

                    try:
                        self.assertEqual(section_d_start, expected_start)
                        self.assertEqual(section_d_end, expected_end)
                        self.assertEqual(result_volume_d_start, expected_d_vol)
                    except AssertionError as e:
                        print(f'Main       section {section.section_id}')
                        print(f'Recreation section: {re_section.section_id}')
                        print(f'Volume size: {out_logits_depth}')
                        print(f'sect global start: {section_global_start}, end: {section_global_end}')
                        print(f'part global start: {global_start}, end: {global_end}')
                        print('Indexes:')
                        print(f'Expected: start: {expected_start}, end: {expected_end}, d_vol: {expected_d_vol}')
                        print(f'Is      : start: {section_d_start}, end: {section_d_end}, d_vol: '
                              f'{result_volume_d_start}')
                        raise AssertionError(e)
