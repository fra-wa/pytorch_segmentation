import cv2
import os

from unittest import TestCase

import numpy as np

from pytorch_segmentation.data_augmentation.rgb_manipulation import channel_shuffle
from pytorch_segmentation.data_augmentation.rgb_manipulation import color_to_hsv
from pytorch_segmentation.data_augmentation.rgb_manipulation import iso_noise
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_fog
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_rain
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_shadow
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_snow
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_sun_flair
from pytorch_segmentation.data_augmentation.rgb_manipulation import rgb_shift
from pytorch_segmentation.data_augmentation.rgb_manipulation import solarize
from pytorch_segmentation.data_augmentation.rgb_manipulation import to_gray
from pytorch_segmentation.data_augmentation.rgb_manipulation import to_sepia
from pytorch_segmentation.file_handling.utils import create_folder


class RGBManipulationTests(TestCase):
    def setUp(self) -> None:
        # Testing with BatchedSequenceLoader to get the base implementations
        self.save_files = False

        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        augmentation_folder = os.path.join(files_path, 'augmentation')
        self.result_folder = os.path.join(augmentation_folder, 'results')

        self.file_path = os.path.join(augmentation_folder, 'small.png')
        self.pattern = cv2.imread(self.file_path)
        self.mask = cv2.imread(self.file_path, cv2.IMREAD_GRAYSCALE)
        self.mask[self.mask >= 200] = 255
        self.mask[self.mask < 200] = 10

    def test_channel_shuffle(self):
        folder = os.path.join(self.result_folder, 'channel_shuffle')
        if self.save_files:
            create_folder(folder)

        color_pattern = np.copy(self.pattern)
        color_pattern[:, :, 1] = 128

        img, mask = channel_shuffle(color_pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'shuffled.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_color_to_hsv(self):
        folder = os.path.join(self.result_folder, 'hsv')
        if self.save_files:
            create_folder(folder)

        img, mask = color_to_hsv(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'hsv.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_iso_noise(self):
        folder = os.path.join(self.result_folder, 'iso_noise')
        if self.save_files:
            create_folder(folder)

        img, mask = iso_noise(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'iso_noise.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_random_fog(self):
        folder = os.path.join(self.result_folder, 'random_fog')
        if self.save_files:
            create_folder(folder)

        img, mask = random_fog(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'random_fog.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_random_rain(self):
        folder = os.path.join(self.result_folder, 'random_rain')
        if self.save_files:
            create_folder(folder)

        img, mask = random_rain(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'random_rain.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_random_shadow(self):
        folder = os.path.join(self.result_folder, 'random_shadow')
        if self.save_files:
            create_folder(folder)

        img, mask = random_shadow(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'random_shadow.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_random_snow(self):
        folder = os.path.join(self.result_folder, 'random_snow')
        if self.save_files:
            create_folder(folder)

        img, mask = random_snow(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'random_snow.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_random_sun_flair(self):
        folder = os.path.join(self.result_folder, 'random_sun_flair')
        if self.save_files:
            create_folder(folder)

        img, mask = random_sun_flair(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'random_sun_flair.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_rgb_shift(self):
        folder = os.path.join(self.result_folder, 'rgb_shift')
        if self.save_files:
            create_folder(folder)

        img, mask = rgb_shift(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'rgb_shift.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_solarize(self):
        folder = os.path.join(self.result_folder, 'solarize')
        if self.save_files:
            create_folder(folder)

        img, mask = solarize(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'solarize.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_to_gray(self):
        folder = os.path.join(self.result_folder, 'to_gray')
        if self.save_files:
            create_folder(folder)

        color_pattern = np.copy(self.pattern)
        color_pattern[:, :, 1] = 128

        img, mask = to_gray(color_pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'to_gray.png'
            cv2.imwrite(os.path.join(folder, file_name), img)

    def test_to_sepia(self):
        folder = os.path.join(self.result_folder, 'to_sepia')
        if self.save_files:
            create_folder(folder)

        img, mask = to_sepia(self.pattern, self.mask)
        self.assertEqual(np.unique(mask).shape[0], 2)

        if self.save_files:
            file_name = 'to_sepia.png'
            cv2.imwrite(os.path.join(folder, file_name), img)
