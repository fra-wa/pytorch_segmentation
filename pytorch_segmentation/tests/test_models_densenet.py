from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import DenseNet57
from pytorch_segmentation.dl_models import DenseNet67
from pytorch_segmentation.dl_models import DenseNet103


class DenseNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('DenseNet57', DenseNet57)

        try:
            self.start_test('DenseNet67', DenseNet67)
        except RuntimeError:
            print(f'\nNot enough VRam for DenseNet67!\n')
        try:
            self.start_test('DenseNet103', DenseNet103)
        except RuntimeError:
            print(f'\nNot enough VRam for DenseNet103!\n')
