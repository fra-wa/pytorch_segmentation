import cv2
import os
import torch

from unittest import TestCase

from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.dl_models import ResidualUNet3D, model_constants
from pytorch_segmentation.dl_models import UNet3D


class ResidualUnet3DTests(TestCase):

    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

    def test_forward(self):
        with torch.no_grad():
            unet = ResidualUNet3D(1, 6)

            image_paths = get_file_paths_in_folder(self.train_images)
            images = []
            for i in range(16):
                images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

            image_batch = torch.stack(images)
            image_batch = torch.unsqueeze(torch.unsqueeze(image_batch, dim=0), dim=0)

            image_batch = image_batch.to(self.device)
            unet.to(self.device)

            out = unet(image_batch.float())
            self.assertEqual(out.shape[0], 1)
            self.assertEqual(out.shape[1], 6)
            self.assertEqual(out.shape[2], 16)
            self.assertEqual(out.shape[3], 256)
            self.assertEqual(out.shape[4], 256)

    def test_correct_normalizations(self):
        for norm in model_constants.NORMALIZATIONS:
            if norm == 'gn':
                excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
            elif norm == 'bn':
                excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm2d]
            else:
                raise NotImplementedError()
            unet = ResidualUNet3D(1, 6, normalization=norm)

            for name, module in unet.named_modules():
                for excluded_norm in excluded_norms:
                    self.assertFalse(isinstance(module, excluded_norm))


class UNet3DTests(TestCase):
    def setUp(self) -> None:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.train_images = os.path.join(files_path, 'data_2d', 'images')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

    def test_forward(self):
        with torch.no_grad():
            unet = UNet3D(1, 6)

            image_paths = get_file_paths_in_folder(self.train_images)
            images = []
            for i in range(16):
                images.append(torch.from_numpy(cv2.imread(image_paths[i], cv2.IMREAD_GRAYSCALE)))

            image_batch = torch.stack(images)
            image_batch = torch.unsqueeze(torch.unsqueeze(image_batch, dim=0), dim=0)

            image_batch = image_batch.to(self.device)
            unet.to(self.device)

            out = unet(image_batch.float())
            self.assertEqual(out.shape[0], 1)
            self.assertEqual(out.shape[1], 6)
            self.assertEqual(out.shape[2], 16)
            self.assertEqual(out.shape[3], 256)
            self.assertEqual(out.shape[4], 256)

    def test_correct_normalizations(self):
        for norm in model_constants.NORMALIZATIONS:
            if norm == 'gn':
                excluded_norms = [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]
            elif norm == 'bn':
                excluded_norms = [torch.nn.GroupNorm, torch.nn.BatchNorm2d]
            else:
                raise NotImplementedError()
            unet = UNet3D(1, 6, normalization=norm)

            for name, module in unet.named_modules():
                for excluded_norm in excluded_norms:
                    self.assertFalse(isinstance(module, excluded_norm))
