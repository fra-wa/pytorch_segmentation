from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import AttentionUNet
from pytorch_segmentation.dl_models import R2UNet
from pytorch_segmentation.dl_models import R2AttentionUNet


class R2UNetTests(ModelBaseTests2D):
    def test_forward(self):
        # t = 2 needs more than 8b ram
        self.start_test('AttentionUNet', AttentionUNet)
        self.start_test('R2UNet', R2UNet, t=1)
        self.start_test('R2AttentionUNet', R2AttentionUNet, t=1)
