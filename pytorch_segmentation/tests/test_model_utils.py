import warnings

from unittest import TestCase

from pytorch_segmentation.dl_models import model_constants
from pytorch_segmentation.dl_models.utils import get_group_norm_groups


class ModelUtilsTests(TestCase):
    def test_get_group_norm_groups(self):
        constant_values = sorted(model_constants.GROUP_NORM_LOOKUP.values())
        channels_to_test = [
            16,
            32,
            64,
            256,
            512,
            4096,
            8192,
        ]
        for channel in channels_to_test:
            self.assertIn(get_group_norm_groups(channel), constant_values)

        channels_to_test = [
            1,
            2,
            3,
            8,
        ]

        for channel in channels_to_test:
            with warnings.catch_warnings(record=True) as w:
                get_group_norm_groups(channel)
                self.assertEqual(len(w), 1)
            w = []  # empty w. otherwise caught warnings will always contain the same one warning.

        channels_to_test = [
            24,
            48,
            128,
            320,
            1024,
            6144,
            12288,
        ]

        for channel, expected_groups in zip(channels_to_test, constant_values):
            self.assertEqual(get_group_norm_groups(channel), expected_groups)

        with warnings.catch_warnings(record=True) as w:
            self.assertEqual(get_group_norm_groups(31), 1)
            self.assertEqual(len(w), 1)

        self.assertEqual(get_group_norm_groups(50), 2)
        self.assertEqual(get_group_norm_groups(50), 2)
