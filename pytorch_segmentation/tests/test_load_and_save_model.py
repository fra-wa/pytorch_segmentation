import io
import os
import sys
import warnings

import torch

from unittest import TestCase

from pytorch_segmentation import constants
from pytorch_segmentation.file_handling.utils import create_folder, remove_dir
from pytorch_segmentation.dl_models import PSPDenseNet
from pytorch_segmentation.dl_models import UNet
from pytorch_segmentation.dl_models import SUPPORTED_MODEL_CLASSES
from pytorch_segmentation.dl_models.loading import instantiate_model_class
from pytorch_segmentation.dl_models.loading import get_model
from pytorch_segmentation.training.saving import _get_base_5_tick, get_tick_sizes


class ModelLoadingTests(TestCase):
    def setUp(self):
        # Testing with BatchedSequenceLoader to get the base implementations
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files_path = os.path.join(current_dir, 'files')
        self.checkpoint_folder = os.path.join(files_path, 'checkpoints')
        create_folder(self.checkpoint_folder)

        self.printed_output = io.StringIO()
        self.previous_out = sys.stdout
        sys.stdout = self.printed_output

    def tearDown(self):
        sys.stdout = self.previous_out
        remove_dir(self.checkpoint_folder)

    def test_instantiate_model_class(self):
        sys.stdout = self.previous_out
        print('')
        total_models = len(SUPPORTED_MODEL_CLASSES)
        for idx, model_class in enumerate(SUPPORTED_MODEL_CLASSES):
            if len(constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__]) > 1:
                backbones = [b for b in constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__] if b]
                for backbone in backbones:
                    print(f'Testing instantiation {idx + 1}/{total_models} '
                          f'{model_class.__name__} (Backbone: {backbone})                                       ',
                          end='\r')
                    try:
                        loaded_model = instantiate_model_class(model_class=model_class, image_channels=1, num_classes=2,
                                                               normalization='bn', backbone=backbone, pretrained=True,
                                                               use_gru=True, device='cuda', in_size=256,
                                                               log_information=False)
                        self.assertTrue(isinstance(loaded_model, model_class))
                    except NotImplementedError:
                        self.fail(f'Failed to instantiate {model_class.__name__}. Got NotImplementedError!')
            else:
                print(f'Testing instantiation {idx + 1}/{total_models} '
                      f'{model_class.__name__} (Backbone: /)                                       ',
                      end='\r')
                try:
                    loaded_model = instantiate_model_class(model_class=model_class, image_channels=1, num_classes=2,
                                                           normalization='bn', backbone=None, pretrained=True,
                                                           use_gru=True, device='cuda', in_size=256,
                                                           log_information=False)
                    self.assertTrue(isinstance(loaded_model, model_class))
                except NotImplementedError:
                    self.fail(f'Failed to instantiate {model_class.__name__}. Got NotImplementedError!')
            print(f'Testing model instantiation {idx + 1}/{total_models} '
                  f'finished.                                                        ',
                  end='\r')
        print('')

    def test_instantiate_model_with_group_norm(self):
        sys.stdout = self.previous_out
        with warnings.catch_warnings(record=True):
            warnings.simplefilter('ignore')
            print('')
            total_models = len(SUPPORTED_MODEL_CLASSES)
            print(f'Testing GroupNorm. PSPDenseNet is not supported and skipped here!')
            for idx, model_class in enumerate(SUPPORTED_MODEL_CLASSES):
                if model_class == PSPDenseNet:
                    continue
                if constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__]:
                    backbone = constants.ARCHITECTURES_AND_BACKBONES[model_class.__name__][0]
                    print(f'Testing GroupNorm {idx + 1}/{total_models} '
                          f'{model_class.__name__} (Backbone: {backbone})                                       ',
                          end='\r')
                    try:
                        loaded_model = instantiate_model_class(model_class=model_class, image_channels=1, num_classes=2,
                                                               normalization='gn', backbone=backbone, pretrained=True,
                                                               use_gru=True, device='cuda', in_size=256,
                                                               log_information=False)
                        for name, module in loaded_model.named_modules():
                            for excluded_norm in [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]:
                                self.assertFalse(isinstance(module, excluded_norm))
                    except AssertionError:
                        self.fail(f'Failed to use GroupNorm at {model_class.__name__}!')
                else:
                    print(f'Testing GroupNorm {idx + 1}/{total_models} '
                          f'{model_class.__name__} (Backbone: /)                                       ',
                          end='\r')
                    try:
                        loaded_model = instantiate_model_class(model_class=model_class, image_channels=1, num_classes=2,
                                                               normalization='gn', backbone=None, pretrained=True,
                                                               use_gru=True, device='cuda', in_size=256,
                                                               log_information=False)
                        for name, module in loaded_model.named_modules():
                            for excluded_norm in [torch.nn.BatchNorm2d, torch.nn.BatchNorm3d]:
                                self.assertFalse(isinstance(module, excluded_norm))
                    except AssertionError:
                        self.fail(f'Failed to use GroupNorm at {model_class.__name__}!')
            print(f'Testing models with GroupNorm {idx + 1}/{total_models} '
                  f'finished.                                                          ',
                  end='\r')
        print('')

    def test_get_model(self):
        model_instance = get_model(model_name='UNet', channels=1, classes=2, device='cuda', normalization='gn')
        self.assertTrue(isinstance(model_instance, UNet))
        fake_tensor = torch.zeros((5, 1, 128, 128)).float()
        fake_tensor = fake_tensor.to('cpu')

        with torch.no_grad():
            with self.assertRaises(RuntimeError):
                # device cuda and cpu tensor should not work
                model_instance(fake_tensor)

    def test_save_and_load_model(self):
        pass

    def test__get_base_5_tick(self):
        tick_size_strings = [
            '0.022',
            '0.1',
            '0.11',
            '0.2',
            '0.7',
            '0.8',
            '1.0',
            '1.2',
            '11.0',
            '11.2',
            '11.3',
        ]
        expected_tick_sizes = [
            0.02,
            0.1,
            0.1,
            0.2,
            0.5,
            1.0,
            1.0,
            1.0,
            10.0,
            11.0,
            11.5,
        ]
        for string, expected_tick in zip(tick_size_strings, expected_tick_sizes):
            tick_size = _get_base_5_tick(string)
            self.assertEqual(tick_size, expected_tick)

    def test_get_tick_sizes(self):
        kwargs = {
            'data_lim_min': 0,
            'data_lim_max': 1.0,
            'expected_major_ticks': 4,
            'expected_minor_ticks': None,
            'round_to_next_5': True,
        }
        major_tick_size, minor_tick_size = get_tick_sizes(**kwargs)
        expected_major = 0.25
        expected_minor = 0.125
        self.assertEqual(major_tick_size, expected_major)
        self.assertEqual(minor_tick_size, expected_minor)

        kwargs = {
            'data_lim_min': 0,
            'data_lim_max': 0.9,
            'expected_major_ticks': 4,
            'expected_minor_ticks': None,
            'round_to_next_5': True,
        }
        major_tick_size, minor_tick_size = get_tick_sizes(**kwargs)
        expected_major = 0.25
        expected_minor = 0.125
        self.assertEqual(major_tick_size, expected_major)
        self.assertEqual(minor_tick_size, expected_minor)

        kwargs = {
            'data_lim_min': 0,
            'data_lim_max': 0.9,
            'expected_major_ticks': 4,
            'expected_minor_ticks': None,
            'round_to_next_5': False,
        }
        major_tick_size, minor_tick_size = get_tick_sizes(**kwargs)
        expected_major = round(0.9 / 4, 2)  # 0.225 --> 0.23
        expected_minor = 0.115
        self.assertEqual(major_tick_size, expected_major)
        self.assertEqual(minor_tick_size, expected_minor)

        kwargs = {
            'data_lim_min': 0,
            'data_lim_max': 0.09,
            'expected_major_ticks': 4,
            'expected_minor_ticks': None,
            'round_to_next_5': False,
        }
        major_tick_size, minor_tick_size = get_tick_sizes(**kwargs)
        expected_major = round(0.09 / 4, 3)  # 0.0225  --> 0.022 (python rounding)
        expected_minor = round(0.09 / 4, 3) / 2
        self.assertEqual(major_tick_size, expected_major)
        self.assertEqual(minor_tick_size, expected_minor)

        kwargs = {
            'data_lim_min': 0,
            'data_lim_max': 0.09,
            'expected_major_ticks': 4,
            'expected_minor_ticks': None,
            'round_to_next_5': True,
        }
        major_tick_size, minor_tick_size = get_tick_sizes(**kwargs)
        expected_major = 0.02  # 0.0225  --> 0.02
        expected_minor = 0.01
        self.assertEqual(major_tick_size, expected_major)
        self.assertEqual(minor_tick_size, expected_minor)
