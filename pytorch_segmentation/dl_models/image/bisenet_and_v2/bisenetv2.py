import torch
import torch.nn as nn

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import group_norm


class ConvBNReLU(nn.Module):

    def __init__(self, in_chan, out_chan, ks=3, stride=1, padding=1,
                 dilation=1, groups=1, bias=False, normalization='bn'):
        super(ConvBNReLU, self).__init__()
        self.conv = nn.Conv2d(
            in_chan,
            out_chan,
            kernel_size=ks,
            stride=stride,
            padding=padding,
            dilation=dilation,
            groups=groups,
            bias=bias
        )
        self.bn = nn.BatchNorm2d(out_chan) if normalization == 'bn' else group_norm(out_chan)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        feat = self.conv(x)
        feat = self.bn(feat)
        feat = self.relu(feat)
        return feat


class UpSample(nn.Module):

    def __init__(self, n_chan, factor=2):
        super(UpSample, self).__init__()
        out_chan = n_chan * factor * factor
        self.proj = nn.Conv2d(n_chan, out_chan, 1, 1, 0)
        self.up = nn.PixelShuffle(factor)

    def forward(self, x):
        feat = self.proj(x)
        feat = self.up(feat)
        return feat


class DetailBranch(nn.Module):

    def __init__(self, image_channels, normalization='bn'):
        super(DetailBranch, self).__init__()
        self.S1 = nn.Sequential(
            ConvBNReLU(image_channels, 64, 3, stride=2, normalization=normalization),
            ConvBNReLU(64, 64, 3, stride=1, normalization=normalization),
        )
        self.S2 = nn.Sequential(
            ConvBNReLU(64, 64, 3, stride=2, normalization=normalization),
            ConvBNReLU(64, 64, 3, stride=1, normalization=normalization),
            ConvBNReLU(64, 64, 3, stride=1, normalization=normalization),
        )
        self.S3 = nn.Sequential(
            ConvBNReLU(64, 128, 3, stride=2, normalization=normalization),
            ConvBNReLU(128, 128, 3, stride=1, normalization=normalization),
            ConvBNReLU(128, 128, 3, stride=1, normalization=normalization),
        )

    def forward(self, x):
        feat = self.S1(x)
        feat = self.S2(feat)
        feat = self.S3(feat)
        return feat


class StemBlock(nn.Module):

    def __init__(self, image_channels, normalization='bn'):
        super(StemBlock, self).__init__()
        self.conv = ConvBNReLU(image_channels, 16, 3, stride=2, normalization=normalization)
        self.left = nn.Sequential(
            ConvBNReLU(16, 8, 1, stride=1, padding=0, normalization=normalization),
            ConvBNReLU(8, 16, 3, stride=2, normalization=normalization),
        )
        self.right = nn.MaxPool2d(
            kernel_size=3, stride=2, padding=1, ceil_mode=False)
        self.fuse = ConvBNReLU(32, 16, 3, stride=1, normalization=normalization)

    def forward(self, x):
        feat = self.conv(x)
        feat_left = self.left(feat)
        feat_right = self.right(feat)
        feat = torch.cat([feat_left, feat_right], dim=1)
        feat = self.fuse(feat)
        return feat


class CEBlock(nn.Module):

    def __init__(self, normalization='bn'):
        super(CEBlock, self).__init__()
        self.bn = nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128)
        self.conv_gap = ConvBNReLU(128, 128, 1, stride=1, padding=0, normalization=normalization)
        # TODO: in paper here is naive conv2d, no bn-relu
        self.conv_last = ConvBNReLU(128, 128, 3, stride=1, normalization=normalization)

    def forward(self, x):
        feat = torch.mean(x, dim=(2, 3), keepdim=True)
        feat = self.bn(feat)
        feat = self.conv_gap(feat)
        feat = feat + x
        feat = self.conv_last(feat)
        return feat


class GELayerS1(nn.Module):

    def __init__(self, in_chan, out_chan, exp_ratio=6, normalization='bn'):
        super(GELayerS1, self).__init__()
        mid_chan = in_chan * exp_ratio
        self.conv1 = ConvBNReLU(in_chan, in_chan, 3, stride=1, normalization=normalization)
        self.dwconv = nn.Sequential(
            nn.Conv2d(
                in_chan, mid_chan, kernel_size=3, stride=1,
                padding=1, groups=in_chan, bias=False),
            nn.BatchNorm2d(mid_chan) if normalization == 'bn' else group_norm(mid_chan),
            nn.ReLU(inplace=True),  # not shown in paper
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(
                mid_chan, out_chan, kernel_size=1, stride=1,
                padding=0, bias=False),
            nn.BatchNorm2d(out_chan) if normalization == 'bn' else group_norm(out_chan),
        )
        self.conv2[1].last_bn = True
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        feat = self.conv1(x)
        feat = self.dwconv(feat)
        feat = self.conv2(feat)
        feat = feat + x
        feat = self.relu(feat)
        return feat


class GELayerS2(nn.Module):

    def __init__(self, in_chan, out_chan, exp_ratio=6, normalization='bn'):
        super(GELayerS2, self).__init__()
        mid_chan = in_chan * exp_ratio
        self.conv1 = ConvBNReLU(in_chan, in_chan, 3, stride=1, normalization=normalization)
        self.dwconv1 = nn.Sequential(
            nn.Conv2d(
                in_chan, mid_chan, kernel_size=3, stride=2,
                padding=1, groups=in_chan, bias=False),
            nn.BatchNorm2d(mid_chan) if normalization == 'bn' else group_norm(mid_chan),
        )
        self.dwconv2 = nn.Sequential(
            nn.Conv2d(
                mid_chan, mid_chan, kernel_size=3, stride=1,
                padding=1, groups=mid_chan, bias=False),
            nn.BatchNorm2d(mid_chan) if normalization == 'bn' else group_norm(mid_chan),
            nn.ReLU(inplace=True),  # not shown in paper
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(
                mid_chan, out_chan, kernel_size=1, stride=1,
                padding=0, bias=False),
            nn.BatchNorm2d(out_chan) if normalization == 'bn' else group_norm(out_chan),
        )
        self.conv2[1].last_bn = True
        self.shortcut = nn.Sequential(
            nn.Conv2d(
                in_chan, in_chan, kernel_size=3, stride=2,
                padding=1, groups=in_chan, bias=False),
            nn.BatchNorm2d(in_chan) if normalization == 'bn' else group_norm(in_chan),
            nn.Conv2d(
                in_chan, out_chan, kernel_size=1, stride=1,
                padding=0, bias=False),
            nn.BatchNorm2d(out_chan) if normalization == 'bn' else group_norm(out_chan),
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        feat = self.conv1(x)
        feat = self.dwconv1(feat)
        feat = self.dwconv2(feat)
        feat = self.conv2(feat)
        shortcut = self.shortcut(x)
        feat = feat + shortcut
        feat = self.relu(feat)
        return feat


class SegmentBranch(nn.Module):

    def __init__(self, image_channels, normalization='bn'):
        super(SegmentBranch, self).__init__()
        self.S1S2 = StemBlock(image_channels, normalization=normalization)
        self.S3 = nn.Sequential(
            GELayerS2(16, 32, normalization=normalization),
            GELayerS1(32, 32, normalization=normalization),
        )
        self.S4 = nn.Sequential(
            GELayerS2(32, 64, normalization=normalization),
            GELayerS1(64, 64, normalization=normalization),
        )
        self.S5_4 = nn.Sequential(
            GELayerS2(64, 128, normalization=normalization),
            GELayerS1(128, 128, normalization=normalization),
            GELayerS1(128, 128, normalization=normalization),
            GELayerS1(128, 128, normalization=normalization),
        )
        self.S5_5 = CEBlock(normalization=normalization)

    def forward(self, x):
        feat2 = self.S1S2(x)
        feat3 = self.S3(feat2)
        feat4 = self.S4(feat3)
        feat5_4 = self.S5_4(feat4)
        feat5_5 = self.S5_5(feat5_4)
        return feat2, feat3, feat4, feat5_4, feat5_5


class BGALayer(nn.Module):

    def __init__(self, normalization='bn'):
        super(BGALayer, self).__init__()
        self.left1 = nn.Sequential(
            nn.Conv2d(
                128, 128, kernel_size=3, stride=1,
                padding=1, groups=128, bias=False),
            nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128),
            nn.Conv2d(
                128, 128, kernel_size=1, stride=1,
                padding=0, bias=False),
        )
        self.left2 = nn.Sequential(
            nn.Conv2d(
                128, 128, kernel_size=3, stride=2,
                padding=1, bias=False),
            nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128),
            nn.AvgPool2d(kernel_size=3, stride=2, padding=1, ceil_mode=False)
        )
        self.right1 = nn.Sequential(
            nn.Conv2d(
                128, 128, kernel_size=3, stride=1,
                padding=1, bias=False),
            nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128),
        )
        self.right2 = nn.Sequential(
            nn.Conv2d(
                128, 128, kernel_size=3, stride=1,
                padding=1, groups=128, bias=False),
            nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128),
            nn.Conv2d(
                128, 128, kernel_size=1, stride=1,
                padding=0, bias=False),
        )
        self.up1 = nn.Upsample(scale_factor=4)
        self.up2 = nn.Upsample(scale_factor=4)
        self.conv = nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(128) if normalization == 'bn' else group_norm(128),
            nn.ReLU(inplace=True),  # not shown in paper
        )

    def forward(self, x_d, x_s):
        left1 = self.left1(x_d)
        left2 = self.left2(x_d)
        right1 = self.right1(x_s)
        right2 = self.right2(x_s)
        right1 = self.up1(right1)
        left = left1 * torch.sigmoid(right1)
        right = left2 * torch.sigmoid(right2)
        right = self.up2(right)
        out = self.conv(left + right)
        return out


class SegmentHead(nn.Module):

    def __init__(self, in_chan, mid_chan, n_classes, up_factor=8, aux=True, normalization='bn'):
        super(SegmentHead, self).__init__()
        self.conv = ConvBNReLU(in_chan, mid_chan, 3, stride=1, normalization=normalization)
        self.drop = nn.Dropout(0.1)
        self.up_factor = up_factor

        out_chan = n_classes * up_factor * up_factor
        if aux:
            self.conv_out = nn.Sequential(
                ConvBNReLU(mid_chan, up_factor * up_factor, 3, stride=1, normalization=normalization),
                nn.Conv2d(up_factor * up_factor, out_chan, 1, 1, 0),
                nn.PixelShuffle(up_factor)
            )
        else:
            self.conv_out = nn.Sequential(
                nn.Conv2d(mid_chan, out_chan, 1, 1, 0),
                nn.PixelShuffle(up_factor)
            )

    def forward(self, x):
        feat = self.conv(x)
        feat = self.drop(feat)
        feat = self.conv_out(feat)
        return feat


class BiSeNetV2(BaseModel):

    def __init__(self, image_channels, num_classes, normalization='gn', output_aux=False, **kwargs):
        super(BiSeNetV2, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            # binary!
            num_classes = 1

        self.output_aux = output_aux
        self.detail = DetailBranch(image_channels, normalization=normalization)
        self.segment = SegmentBranch(image_channels, normalization=normalization)
        self.bga = BGALayer(normalization=normalization)

        self.head = SegmentHead(128, 1024, num_classes, up_factor=8, aux=False, normalization=normalization)
        if self.output_aux:
            self.aux2 = SegmentHead(16, 128, num_classes, up_factor=4, normalization=normalization)
            self.aux3 = SegmentHead(32, 128, num_classes, up_factor=8, normalization=normalization)
            self.aux4 = SegmentHead(64, 128, num_classes, up_factor=16, normalization=normalization)
            self.aux5_4 = SegmentHead(128, 128, num_classes, up_factor=32, normalization=normalization)

    def forward(self, x):
        feat_d = self.detail(x)
        feat2, feat3, feat4, feat5_4, feat_s = self.segment(x)
        feat_head = self.bga(feat_d, feat_s)

        logits = self.head(feat_head)
        if self.output_aux:
            logits_aux2 = self.aux2(feat2)
            logits_aux3 = self.aux3(feat3)
            logits_aux4 = self.aux4(feat4)
            logits_aux5_4 = self.aux5_4(feat5_4)
            return logits, logits_aux2, logits_aux3, logits_aux4, logits_aux5_4
        return logits
