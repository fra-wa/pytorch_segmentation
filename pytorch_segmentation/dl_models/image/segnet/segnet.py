import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from math import ceil

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import group_norm
from pytorch_segmentation.dl_models.utils import load_backbone


class SegNet(BaseModel):
    def __init__(self, image_channels, num_classes, pretrained=True, normalization='bn', **kwargs):
        super(SegNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1
        if image_channels != 3 or normalization != 'bn':
            pretrained = False

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        vgg_bn = models.vgg16_bn(weights=models.VGG16_BN_Weights.DEFAULT)
        if norm_layer != nn.BatchNorm2d:
            vgg_bn = batch_norm_to_group_norm(vgg_bn)

        encoder = list(vgg_bn.features.children())

        # Adjust the input size
        if image_channels != 3:
            encoder[0] = nn.Conv2d(image_channels, 64, kernel_size=3, stride=1, padding=1)

        # Encoder, VGG without any maxpooling
        self.stage1_encoder = nn.Sequential(*encoder[:6])
        self.stage2_encoder = nn.Sequential(*encoder[7:13])
        self.stage3_encoder = nn.Sequential(*encoder[14:23])
        self.stage4_encoder = nn.Sequential(*encoder[24:33])
        self.stage5_encoder = nn.Sequential(*encoder[34:-1])
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)

        # Decoder, same as the encoder but reversed, maxpool will not be used
        decoder = encoder
        decoder = [i for i in list(reversed(decoder)) if not isinstance(i, nn.MaxPool2d)]
        # Replace the last conv layer
        decoder[-1] = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        # When reversing, we also reversed conv->batchN->relu, correct it
        decoder = [item for i in range(0, len(decoder), 3) for item in decoder[i:i + 3][::-1]]
        # Replace some conv layers & batchN after them
        for i, module in enumerate(decoder):
            if isinstance(module, nn.Conv2d):
                if module.in_channels != module.out_channels:
                    decoder[i + 1] = norm_layer(module.in_channels)
                    decoder[i] = nn.Conv2d(module.out_channels, module.in_channels, kernel_size=3, stride=1, padding=1)

        self.stage1_decoder = nn.Sequential(*decoder[0:9])
        self.stage2_decoder = nn.Sequential(*decoder[9:18])
        self.stage3_decoder = nn.Sequential(*decoder[18:27])
        self.stage4_decoder = nn.Sequential(*decoder[27:33])
        self.stage5_decoder = nn.Sequential(*decoder[33:],
                                            nn.Conv2d(64, num_classes, kernel_size=3, stride=1, padding=1)
                                            )
        self.unpool = nn.MaxUnpool2d(kernel_size=2, stride=2)

    def forward(self, x):
        # Encoder
        x = self.stage1_encoder(x)
        x1_size = x.size()
        x, indices1 = self.pool(x)

        x = self.stage2_encoder(x)
        x2_size = x.size()
        x, indices2 = self.pool(x)

        x = self.stage3_encoder(x)
        x3_size = x.size()
        x, indices3 = self.pool(x)

        x = self.stage4_encoder(x)
        x4_size = x.size()
        x, indices4 = self.pool(x)

        x = self.stage5_encoder(x)
        x5_size = x.size()
        x, indices5 = self.pool(x)

        # Decoder
        x = self.unpool(x, indices=indices5, output_size=x5_size)
        x = self.stage1_decoder(x)

        x = self.unpool(x, indices=indices4, output_size=x4_size)
        x = self.stage2_decoder(x)

        x = self.unpool(x, indices=indices3, output_size=x3_size)
        x = self.stage3_decoder(x)

        x = self.unpool(x, indices=indices2, output_size=x2_size)
        x = self.stage4_decoder(x)

        x = self.unpool(x, indices=indices1, output_size=x1_size)
        x = self.stage5_decoder(x)

        return x


class DecoderBottleneck(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm2d):
        super(DecoderBottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, in_channels // 4, kernel_size=1, bias=False)
        self.bn1 = norm_layer(in_channels // 4)
        self.conv2 = nn.ConvTranspose2d(in_channels // 4, in_channels // 4, kernel_size=2, stride=2, bias=False)
        self.bn2 = norm_layer(in_channels // 4)
        self.conv3 = nn.Conv2d(in_channels // 4, in_channels // 2, 1, bias=False)
        self.bn3 = norm_layer(in_channels // 2)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = nn.Sequential(
            nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2, bias=False),
            norm_layer(in_channels // 2))

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)

        identity = self.downsample(x)
        out += identity
        out = self.relu(out)
        return out


class LastBottleneck(nn.Module):
    def __init__(self, in_channels, norm_layer=nn.BatchNorm2d):
        super(LastBottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, in_channels // 4, kernel_size=1, bias=False)
        self.bn1 = norm_layer(in_channels // 4)
        self.conv2 = nn.Conv2d(in_channels // 4, in_channels // 4, kernel_size=3, padding=1, bias=False)
        self.bn2 = norm_layer(in_channels // 4)
        self.conv3 = nn.Conv2d(in_channels // 4, in_channels // 4, 1, bias=False)
        self.bn3 = norm_layer(in_channels // 4)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = nn.Sequential(
            nn.Conv2d(in_channels, in_channels // 4, kernel_size=1, bias=False),
            norm_layer(in_channels // 4))

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)

        identity = self.downsample(x)
        out += identity
        out = self.relu(out)
        return out


class SegResNet(BaseModel):
    def __init__(self, image_channels, num_classes, backbone='resnet50', pretrained=True, normalization='bn', **kwargs):
        super(SegResNet, self).__init__(image_channels, num_classes)
        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        supported_backbones = ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101']
        if backbone not in supported_backbones:
            raise NotImplementedError(
                f'This model only supports the following backbones:\n {", ".join(supported_backbones)}')

        resnet, resnet_channels = load_backbone(backbone, pretrained=pretrained)
        if norm_layer != nn.BatchNorm2d:
            resnet = batch_norm_to_group_norm(resnet)

        encoder = list(resnet.children())
        if image_channels != 3:
            encoder[0] = nn.Conv2d(image_channels, 64, kernel_size=3, stride=1, padding=1)
        encoder[3].return_indices = True

        # Encoder
        self.first_conv = nn.Sequential(*encoder[:4])
        resnet_blocks = [resnet.layer1, resnet.layer2, resnet.layer3, resnet.layer4]
        self.encoder = nn.Sequential(*resnet_blocks)

        # Decoder
        resnet_untrained, resnet_channels = load_backbone(backbone, pretrained=False)
        if norm_layer != nn.BatchNorm2d:
            resnet_untrained = batch_norm_to_group_norm(resnet_untrained)

        resnet_blocks = [
            resnet_untrained.layer4, resnet_untrained.layer3, resnet_untrained.layer2, resnet_untrained.layer1
        ]
        decoder = []
        channels = (2048, 1024, 512)
        for i, block in enumerate(resnet_blocks[:-1]):
            new_block = list(block.children())[::-1][:-1]
            decoder.append(nn.Sequential(*new_block, DecoderBottleneck(channels[i], norm_layer=norm_layer)))
        new_block = list(resnet_blocks[-1].children())[::-1][:-1]
        decoder.append(nn.Sequential(*new_block, LastBottleneck(256, norm_layer=norm_layer)))

        self.decoder = nn.Sequential(*decoder)
        self.last_conv = nn.Sequential(
            nn.ConvTranspose2d(64, 64, kernel_size=2, stride=2, bias=False),
            nn.Conv2d(64, num_classes, kernel_size=3, stride=1, padding=1)
        )

    def forward(self, x):
        inputsize = x.size()

        # Encoder
        x, indices = self.first_conv(x)
        x = self.encoder(x)

        # Decoder
        x = self.decoder(x)
        h_diff = ceil((x.size()[2] - indices.size()[2]) / 2)
        w_diff = ceil((x.size()[3] - indices.size()[3]) / 2)
        if indices.size()[2] % 2 == 1:
            x = x[:, :, h_diff:x.size()[2] - (h_diff - 1), w_diff: x.size()[3] - (w_diff - 1)]
        else:
            x = x[:, :, h_diff:x.size()[2] - h_diff, w_diff: x.size()[3] - w_diff]

        x = F.max_unpool2d(x, indices, kernel_size=2, stride=2)
        x = self.last_conv(x)

        if inputsize != x.size():
            h_diff = (x.size()[2] - inputsize[2]) // 2
            w_diff = (x.size()[3] - inputsize[3]) // 2
            x = x[:, :, h_diff:x.size()[2] - h_diff, w_diff: x.size()[3] - w_diff]
            if h_diff % 2 != 0:
                x = x[:, :, :-1, :]
            if w_diff % 2 != 0:
                x = x[:, :, :, :-1]

        return x
