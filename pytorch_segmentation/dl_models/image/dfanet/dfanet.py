""" Deep Feature Aggregation"""
import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_segmentation.dl_models.base_model import BaseModel
from .xception import XceptionA, Enc, FCAttention
from pytorch_segmentation.dl_models.utils import group_norm


class _ConvNormReLU(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0,
                 dilation=1, groups=1, norm_layer=nn.BatchNorm2d):
        super(_ConvNormReLU, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, dilation, groups, bias=False)
        self.bn = norm_layer(out_channels)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class DFANet(BaseModel):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DFANet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm
        encoder = XceptionA(image_channels, norm_layer=norm_layer)
        self.encoder_conv1 = encoder.conv1
        self.encoder_enc2 = encoder.enc2
        self.encoder_enc3 = encoder.enc3
        self.encoder_enc4 = encoder.enc4
        self.encoder_fca = encoder.fca

        self.enc2_2 = Enc(240, 48, 4, norm_layer=norm_layer)
        self.enc3_2 = Enc(144, 96, 6, norm_layer=norm_layer)
        self.enc4_2 = Enc(288, 192, 4, norm_layer=norm_layer)
        self.fca_2 = FCAttention(192, norm_layer=norm_layer)

        self.enc2_3 = Enc(240, 48, 4, norm_layer=norm_layer)
        self.enc3_3 = Enc(144, 96, 6, norm_layer=norm_layer)
        self.enc3_4 = Enc(288, 192, 4, norm_layer=norm_layer)
        self.fca_3 = FCAttention(192, norm_layer=norm_layer)

        self.enc2_1_reduce = _ConvNormReLU(48, 32, 1, norm_layer=norm_layer)
        self.enc2_2_reduce = _ConvNormReLU(48, 32, 1, norm_layer=norm_layer)
        self.enc2_3_reduce = _ConvNormReLU(48, 32, 1, norm_layer=norm_layer)
        self.conv_fusion = _ConvNormReLU(32, 32, 1, norm_layer=norm_layer)

        self.fca_1_reduce = _ConvNormReLU(192, 32, 1, norm_layer=norm_layer)
        self.fca_2_reduce = _ConvNormReLU(192, 32, 1, norm_layer=norm_layer)
        self.fca_3_reduce = _ConvNormReLU(192, 32, 1, norm_layer=norm_layer)
        self.conv_out = nn.Conv2d(32, num_classes, 1)

    def forward(self, x):
        # backbone
        stage1_conv1 = self.encoder_conv1(x)
        stage1_enc2 = self.encoder_enc2(stage1_conv1)
        stage1_enc3 = self.encoder_enc3(stage1_enc2)
        stage1_enc4 = self.encoder_enc4(stage1_enc3)
        stage1_fca = self.encoder_fca(stage1_enc4)
        stage1_out = F.interpolate(stage1_fca, scale_factor=4, mode='bilinear', align_corners=True)

        # stage2
        stage2_enc2 = self.enc2_2(torch.cat([stage1_enc2, stage1_out], dim=1))
        stage2_enc3 = self.enc3_2(torch.cat([stage1_enc3, stage2_enc2], dim=1))
        stage2_enc4 = self.enc4_2(torch.cat([stage1_enc4, stage2_enc3], dim=1))
        stage2_fca = self.fca_2(stage2_enc4)
        stage2_out = F.interpolate(stage2_fca, scale_factor=4, mode='bilinear', align_corners=True)

        # stage3
        stage3_enc2 = self.enc2_3(torch.cat([stage2_enc2, stage2_out], dim=1))
        stage3_enc3 = self.enc3_3(torch.cat([stage2_enc3, stage3_enc2], dim=1))
        stage3_enc4 = self.enc3_4(torch.cat([stage2_enc4, stage3_enc3], dim=1))
        stage3_fca = self.fca_3(stage3_enc4)

        stage1_enc2_decoder = self.enc2_1_reduce(stage1_enc2)
        stage2_enc2_docoder = F.interpolate(self.enc2_2_reduce(stage2_enc2), scale_factor=2,
                                            mode='bilinear', align_corners=True)
        stage3_enc2_decoder = F.interpolate(self.enc2_3_reduce(stage3_enc2), scale_factor=4,
                                            mode='bilinear', align_corners=True)
        fusion = stage1_enc2_decoder + stage2_enc2_docoder + stage3_enc2_decoder
        fusion = self.conv_fusion(fusion)

        stage1_fca_decoder = F.interpolate(self.fca_1_reduce(stage1_fca), scale_factor=4,
                                           mode='bilinear', align_corners=True)
        stage2_fca_decoder = F.interpolate(self.fca_2_reduce(stage2_fca), scale_factor=8,
                                           mode='bilinear', align_corners=True)
        stage3_fca_decoder = F.interpolate(self.fca_3_reduce(stage3_fca), scale_factor=16,
                                           mode='bilinear', align_corners=True)
        fusion = fusion + stage1_fca_decoder + stage2_fca_decoder + stage3_fca_decoder

        out = self.conv_out(fusion)
        out = F.interpolate(out, scale_factor=4, mode='bilinear', align_corners=True)

        return out
