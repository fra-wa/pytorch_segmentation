import torch
import torch.nn.functional as F
from torch import nn
from torchvision import models as torchvision_models

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import group_norm
from pytorch_segmentation.dl_models.utils import load_backbone


class PSPModule(nn.Module):
    def __init__(self, in_channels, bin_sizes, norm_layer=nn.BatchNorm2d):
        super(PSPModule, self).__init__()
        out_channels = in_channels // len(bin_sizes)
        self.norm_layer = norm_layer
        self.stages = nn.ModuleList([self._make_stages(in_channels, out_channels, b_s)
                                     for b_s in bin_sizes])
        self.bottleneck = nn.Sequential(
            nn.Conv2d(in_channels + (out_channels * len(bin_sizes)), out_channels,
                      kernel_size=3, padding=1, bias=False),
            norm_layer(out_channels),
            nn.ReLU(inplace=True),
            nn.Dropout2d(0.1)
        )

    def _make_stages(self, in_channels, out_channels, bin_sz):
        prior = nn.AdaptiveAvgPool2d(output_size=bin_sz)
        conv = nn.Conv2d(in_channels, out_channels, kernel_size=1, bias=False)
        bn = self.norm_layer(out_channels)
        relu = nn.ReLU(inplace=True)
        return nn.Sequential(prior, conv, bn, relu)

    def forward(self, features):
        h, w = features.size()[2], features.size()[3]
        pyramids = [features]
        pyramids.extend([F.interpolate(stage(features), size=(h, w), mode='bilinear',
                                       align_corners=True) for stage in self.stages])
        output = self.bottleneck(torch.cat(pyramids, dim=1))
        return output


class PSPResNetDecoder(nn.Module):
    def __init__(self, in_channels, num_classes):
        """
        Args:
            in_channels:
            num_classes:
        """
        super(PSPResNetDecoder, self).__init__()

        self.up_1 = nn.Sequential(
            nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels, in_channels // 2, 3, 1, 1)
        )
        self.up_2 = nn.Sequential(
            nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels // 2, in_channels // 4, 3, 1, 1)
        )
        self.up_3 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels // 4, num_classes, 3, 1, 1)
        )

    def forward(self, x):
        x = self.up_1(x)
        x = self.up_2(x)
        x = self.up_3(x)
        return x


class PSPNet(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone='resnet152',
                 pretrained=True,
                 normalization='bn',
                 **kwargs):
        super(PSPNet, self).__init__(image_channels, num_classes)
        supported_backbones = ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101']
        if backbone not in supported_backbones:
            raise ValueError(f'{backbone} is not supported!')

        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.backbone, backbone_channels = load_backbone(backbone, pretrained)
        if norm_layer != nn.BatchNorm2d:
            self.backbone = batch_norm_to_group_norm(self.backbone)
        if image_channels != 3:
            self.backbone.conv1 = nn.Conv2d(image_channels, 64, 7, 2, 3, bias=False)

        self.psp_module = PSPModule(backbone_channels, bin_sizes=[1, 2, 3, 6], norm_layer=norm_layer)
        self.decoder = PSPResNetDecoder(backbone_channels // 4, num_classes)

    def forward(self, x):
        x = self.backbone.conv1(x)
        x = self.backbone.bn1(x)
        x = self.backbone.relu(x)
        x = self.backbone.maxpool(x)
        x = self.backbone.layer1(x)
        x = self.backbone.layer2(x)
        x = self.backbone.layer3(x)
        x = self.backbone.layer4(x)

        x = self.psp_module(x)
        x = self.decoder(x)
        return x


class PSPDenseNetDecoder(nn.Module):
    def __init__(self, in_channels, num_classes):
        """
        Args:
            in_channels:
            num_classes:
        """
        super(PSPDenseNetDecoder, self).__init__()

        self.up_1 = nn.Sequential(
            nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels, in_channels // 2, 3, 1, 1)
        )
        self.up_2 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels // 2, num_classes, 3, 1, 1)
        )

    def forward(self, x):
        x = self.up_1(x)
        x = self.up_2(x)
        return x


# PSP with dense net as the backbone
class PSPDenseNet(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone='densenet121',
                 pretrained=True,
                 normalization='bn',
                 **kwargs):
        super(PSPDenseNet, self).__init__(image_channels, num_classes)

        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        if num_classes == 2:
            num_classes = 1

        if normalization == 'gn':
            raise NotImplementedError('Group norm is not supported for PSPDenseNet. Use batch norm.')

        # changing the backbone with batch_norm_to_group_norm takes too much
        norm_layer = nn.BatchNorm2d  # if normalization == 'bn' else group_norm

        model, _ = load_backbone(backbone, pretrained)
        # if norm_layer != nn.BatchNorm2d:
        #     model = batch_norm_to_group_norm(model)

        backbone_channels = model.classifier.in_features

        conv_0_channels = model.features.conv0.out_channels

        if image_channels != 3:
            model.features.conv0 = nn.Conv2d(image_channels, conv_0_channels, 7, 2, 3, bias=False)

        self.block0 = nn.Sequential(*list(model.features.children())[:4])
        self.block1 = model.features.denseblock1
        self.block2 = model.features.denseblock2
        self.block3 = model.features.denseblock3
        self.block4 = model.features.denseblock4

        self.transition1 = model.features.transition1
        # No pooling
        self.transition2 = nn.Sequential(
            *list(model.features.transition2.children())[:-1])
        self.transition3 = nn.Sequential(
            *list(model.features.transition3.children())[:-1])

        for n, m in self.block3.named_modules():
            if 'conv2' in n:
                m.dilation, m.padding = (2, 2), (2, 2)
        for n, m in self.block4.named_modules():
            if 'conv2' in n:
                m.dilation, m.padding = (4, 4), (4, 4)

        bin_sizes = [1, 2, 3, 6]
        self.psp_module = PSPModule(backbone_channels, bin_sizes=bin_sizes, norm_layer=norm_layer)
        self.decoder = PSPDenseNetDecoder(backbone_channels // len(bin_sizes), num_classes)

    def forward(self, x):
        x = self.block0(x)
        x = self.block1(x)
        x = self.transition1(x)
        x = self.block2(x)
        x = self.transition2(x)
        x = self.block3(x)
        x = self.transition3(x)
        x = self.block4(x)

        x = self.psp_module(x)
        x = self.decoder(x)

        return x
