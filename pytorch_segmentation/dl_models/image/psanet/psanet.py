"""
Point-wise Spatial Attention Network
Hengshuang Zhao, et al. "PSANet: Point-wise Spatial Attention Network for Scene Parsing." ECCV-2018.

Inspiration taken from:
https://github.com/Tramac/awesome-semantic-segmentation-pytorch

But the provided network is not correct aligned with the paper.

TODO: Network does not learn correctly after realignment with paper?!
"""
import torch
import torch.nn as nn

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import group_norm
from pytorch_segmentation.dl_models.utils import load_backbone


class ConvBNReLU(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 stride=1,
                 padding=0,
                 dilation=1,
                 groups=1,
                 norm_layer=nn.BatchNorm2d):
        super(ConvBNReLU, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, dilation, groups, bias=False)
        self.bn = norm_layer(out_channels)
        self.relu = nn.ReLU(True)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class AttentionGeneration(nn.Module):
    def __init__(self, in_channels, reduced_channels, height_width, norm_layer=nn.BatchNorm2d):
        super(AttentionGeneration, self).__init__()
        self.conv_reduce = ConvBNReLU(in_channels, reduced_channels, 1, norm_layer=norm_layer)
        self.attention = nn.Sequential(
            ConvBNReLU(reduced_channels, (2 * height_width - 1) * (2 * height_width - 1), 1, norm_layer=norm_layer),
            nn.Conv2d((2 * height_width - 1) * (2 * height_width - 1), height_width * height_width, 1, bias=False))

        self.reduced_channels = reduced_channels

    def forward(self, x):
        reduce_x = self.conv_reduce(x)
        attention = self.attention(reduce_x)
        b, c, h, w = attention.size()
        attention = attention.view(b, c, h * w)
        reduce_x = reduce_x.view(b, self.reduced_channels, h * w)
        fm = torch.bmm(reduce_x, torch.softmax(attention, dim=1))  # result should be 512
        fm = fm.view(b, self.reduced_channels, h, w)

        return fm


class PointWiseSpatialAttention(nn.Module):
    def __init__(self, in_channels, height_width, reduced_channels=512, norm_layer=nn.BatchNorm2d):
        super(PointWiseSpatialAttention, self).__init__()
        self.collect_attention = AttentionGeneration(in_channels, reduced_channels, height_width, norm_layer=norm_layer)
        self.distribute_attention = AttentionGeneration(
            in_channels, reduced_channels, height_width, norm_layer=norm_layer
        )

    def forward(self, x):
        collect_fm = self.collect_attention(x)
        distribute_fm = self.distribute_attention(x)
        psa_fm = torch.cat([collect_fm, distribute_fm], dim=1)
        return psa_fm


class PSAModule(nn.Module):
    def __init__(self, backbone_center_channels, reduced_dimension, reduced_channels=None, norm_layer=nn.BatchNorm2d):
        """

        Args:
            backbone_center_channels:
            reduced_dimension: dimension outcome of the backbone net. (Currently only squared images are supported).
                For ResNet50 to 151 this will be: img_size // (2 ** 5)  --> 256 -> 8, 512: 16, ...
            reduced_channels: to perfectly use the information, this should be backbone_center_channels // 2

        Returns: [b, c, h, w] = [B, 2 * backbone_center_channels, reduced_dimension, reduced_dimension]
        """
        super(PSAModule, self).__init__()
        if reduced_channels is None:
            reduced_channels = backbone_center_channels // 2

        self.psa = PointWiseSpatialAttention(
            backbone_center_channels, reduced_dimension, reduced_channels=reduced_channels, norm_layer=norm_layer
        )

        self.conv_post = ConvBNReLU(reduced_channels * 2, backbone_center_channels, 1, norm_layer=norm_layer)

    def forward(self, x):
        global_feature = self.psa(x)
        out = self.conv_post(global_feature)
        out = torch.cat([x, out], dim=1)
        return out


class Decoder(nn.Module):
    def __init__(self, in_channels, num_classes):
        """
        Example for ResNet 50 to 151 backbone (up 4 times to reduce size):
            Up(4x, bilinear)
            Conv2D(backbone_center_channels, 2048)

            Up(4x, bilinear)
            Conv2D(2048, 1024)

            Up(2x, bilinear)
            Conv2D(backbone_center_channels, )

        Args:
            in_channels:
            num_classes:
        """
        super(Decoder, self).__init__()

        self.up_1 = nn.Sequential(
            nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True),
            nn.Conv2d(in_channels, 1024, 3, 1, 1)
        )
        self.up_2 = nn.Sequential(
            nn.Upsample(scale_factor=4, mode='bilinear', align_corners=True),
            nn.Conv2d(1024, 256, 3, 1, 1)
        )
        self.up_3 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True),
            nn.Conv2d(256, num_classes, 3, 1, 1)
        )

    def forward(self, x):
        x = self.up_1(x)
        x = self.up_2(x)
        x = self.up_3(x)
        return x


class PSANet(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 in_size,
                 backbone='resnet50',
                 pretrained=True,
                 normalization='bn',
                 **kwargs):
        super(PSANet, self).__init__(image_channels, num_classes)
        if isinstance(in_size, tuple) or isinstance(in_size, list):
            raise NotImplementedError('Currently only squared input images are supported..')

        supported_backbones = ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101']
        if backbone not in supported_backbones:
            raise NotImplementedError(
                f'This model only supports the following backbones:\n {", ".join(supported_backbones)}')

        if num_classes == 2:
            num_classes = 1
        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.backbone, backbone_channels = load_backbone(backbone, pretrained=pretrained)
        if norm_layer != nn.BatchNorm2d:
            self.backbone = batch_norm_to_group_norm(self.backbone)
        if image_channels != 3:
            self.backbone.conv1 = nn.Conv2d(image_channels, 64, 7, 2, 3, bias=False)

        self.height_width = in_size
        backbone_encodings = 5
        self.psa_module = PSAModule(
            backbone_channels, reduced_dimension=in_size // (2 ** backbone_encodings), norm_layer=norm_layer
        )

        self.final = Decoder(in_channels=backbone_channels * 2, num_classes=num_classes)

    def forward(self, x):
        x = self.backbone.conv1(x)
        x = self.backbone.bn1(x)
        x = self.backbone.relu(x)
        x = self.backbone.maxpool(x)
        x = self.backbone.layer1(x)
        x = self.backbone.layer2(x)
        x = self.backbone.layer3(x)
        x = self.backbone.layer4(x)

        x = self.psa_module(x)
        x = self.final(x)
        return x
