"""Base Model for Semantic Segmentation"""
import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import group_norm
from pytorch_segmentation.dl_models.utils import load_backbone


class SeparableConv2d(nn.Module):
    def __init__(self, inplanes, planes, kernel_size=3, stride=1, padding=1,
                 dilation=1, bias=False, norm_layer=nn.BatchNorm2d):
        super(SeparableConv2d, self).__init__()
        self.conv = nn.Conv2d(inplanes, inplanes, kernel_size, stride, padding, dilation, groups=inplanes, bias=bias)
        self.bn = norm_layer(inplanes)
        self.pointwise = nn.Conv2d(inplanes, planes, 1, bias=bias)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.pointwise(x)
        return x


# copy from: https://github.com/wuhuikai/FastFCN/blob/master/encoding/nn/customize.py
class JPU(nn.Module):
    """Joint Pyramid Upsampling"""

    def __init__(self, in_channels, width=512, norm_layer=nn.BatchNorm2d, **kwargs):
        super(JPU, self).__init__()

        self.conv5 = nn.Sequential(
            nn.Conv2d(in_channels[-1], width, 3, padding=1, bias=False),
            norm_layer(width),
            nn.ReLU(True))
        self.conv4 = nn.Sequential(
            nn.Conv2d(in_channels[-2], width, 3, padding=1, bias=False),
            norm_layer(width),
            nn.ReLU(True))
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels[-3], width, 3, padding=1, bias=False),
            norm_layer(width),
            nn.ReLU(True))

        self.dilation1 = nn.Sequential(
            SeparableConv2d(3 * width, width, 3, padding=1, dilation=1, bias=False),
            norm_layer(width),
            nn.ReLU(True))
        self.dilation2 = nn.Sequential(
            SeparableConv2d(3 * width, width, 3, padding=2, dilation=2, bias=False),
            norm_layer(width),
            nn.ReLU(True))
        self.dilation3 = nn.Sequential(
            SeparableConv2d(3 * width, width, 3, padding=4, dilation=4, bias=False),
            norm_layer(width),
            nn.ReLU(True))
        self.dilation4 = nn.Sequential(
            SeparableConv2d(3 * width, width, 3, padding=8, dilation=8, bias=False),
            norm_layer(width),
            nn.ReLU(True))

    def forward(self, *inputs):
        feats = [self.conv5(inputs[-1]), self.conv4(inputs[-2]), self.conv3(inputs[-3])]
        size = feats[-1].size()[2:]
        feats[-2] = F.interpolate(feats[-2], size, mode='bilinear', align_corners=True)
        feats[-3] = F.interpolate(feats[-3], size, mode='bilinear', align_corners=True)
        feat = torch.cat(feats, dim=1)
        feat = torch.cat([self.dilation1(feat), self.dilation2(feat), self.dilation3(feat), self.dilation4(feat)],
                         dim=1)

        return inputs[0], inputs[1], inputs[2], feat


class SegBaseModel(BaseModel):
    r"""Base Model for Semantic Segmentation
    Parameters
    ----------
    backbone : string
        Pre-trained dilated backbone network type (default:'resnet50'; 'resnet50',
        'resnet101' or 'resnet152').
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone='resnet50', jpu=False, pretrained=True, normalization='bn', **kwargs):
        super(SegBaseModel, self).__init__(image_channels, num_classes)

        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        if num_classes == 2:
            num_classes = 1
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.nclass = num_classes
        supported_backbones = ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101']
        if backbone not in supported_backbones:
            raise NotImplementedError(
                f'This model only supports the following backbones:\n {", ".join(supported_backbones)}')
        self.backbone = load_backbone(backbone, pretrained=pretrained)[0]

        if norm_layer != nn.BatchNorm2d:
            self.backbone = batch_norm_to_group_norm(self.backbone)
        if image_channels != 3:
            self.backbone.conv1 = nn.Conv2d(image_channels, 64, 7, 2, 3, bias=False)

        self.jpu = JPU([512, 1024, 2048], width=512, norm_layer=norm_layer, **kwargs) if jpu else None

    def base_forward(self, x):
        """forwarding pre-trained network"""
        x = self.backbone.conv1(x)
        x = self.backbone.bn1(x)
        x = self.backbone.relu(x)
        x = self.backbone.maxpool(x)
        c1 = self.backbone.layer1(x)
        c2 = self.backbone.layer2(c1)
        c3 = self.backbone.layer3(c2)
        c4 = self.backbone.layer4(c3)

        if self.jpu:
            return self.jpu(c1, c2, c3, c4)
        else:
            return c1, c2, c3, c4

    def forward(self, **kwargs):
        raise NotImplementedError('implement at your model')
