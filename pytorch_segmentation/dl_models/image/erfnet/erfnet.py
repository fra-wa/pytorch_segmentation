# ERFNet full model definition for Pytorch
# Sept 2017
# Eduardo Romera
#######################

import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import group_norm


class DownsamplerBlock(nn.Module):
    def __init__(self, ninput, noutput, norm_layer=nn.BatchNorm2d):
        super().__init__()

        self.conv = nn.Conv2d(ninput, noutput - ninput, (3, 3), stride=2, padding=1, bias=True)
        self.pool = nn.MaxPool2d(2, stride=2)
        self.bn = norm_layer(noutput, eps=1e-3)

    def forward(self, x):
        output = torch.cat([self.conv(x), self.pool(x)], 1)
        output = self.bn(output)
        return F.relu(output)


class NonBottleneck1D(nn.Module):
    def __init__(self, chann, dropprob, dilated, norm_layer=nn.BatchNorm2d):
        super().__init__()

        self.conv3x1_1 = nn.Conv2d(chann, chann, (3, 1), stride=1, padding=(1, 0), bias=True)

        self.conv1x3_1 = nn.Conv2d(chann, chann, (1, 3), stride=1, padding=(0, 1), bias=True)

        self.bn1 = norm_layer(chann, eps=1e-03)

        self.conv3x1_2 = nn.Conv2d(chann, chann, (3, 1), stride=1, padding=(1 * dilated, 0), bias=True,
                                   dilation=(dilated, 1))

        self.conv1x3_2 = nn.Conv2d(chann, chann, (1, 3), stride=1, padding=(0, 1 * dilated), bias=True,
                                   dilation=(1, dilated))

        self.bn2 = norm_layer(chann, eps=1e-03)

        self.dropout = nn.Dropout2d(dropprob)

    def forward(self, x):
        output = self.conv3x1_1(x)
        output = F.relu(output)
        output = self.conv1x3_1(output)
        output = self.bn1(output)
        output = F.relu(output)

        output = self.conv3x1_2(output)
        output = F.relu(output)
        output = self.conv1x3_2(output)
        output = self.bn2(output)

        if self.dropout.p != 0:
            output = self.dropout(output)

        return F.relu(output + x)  # +input = identity (residual connection)


class Encoder(nn.Module):
    def __init__(self, image_channels, norm_layer=nn.BatchNorm2d):
        super().__init__()
        self.initial_block = DownsamplerBlock(image_channels, 16, norm_layer=norm_layer)

        self.layers = nn.ModuleList()

        self.layers.append(DownsamplerBlock(16, 64, norm_layer=norm_layer))

        for x in range(0, 5):  # 5 times
            self.layers.append(NonBottleneck1D(64, 0.03, 1, norm_layer=norm_layer))

        self.layers.append(DownsamplerBlock(64, 128, norm_layer=norm_layer))

        for x in range(0, 2):  # 2 times
            self.layers.append(NonBottleneck1D(128, 0.3, 2, norm_layer=norm_layer))
            self.layers.append(NonBottleneck1D(128, 0.3, 4, norm_layer=norm_layer))
            self.layers.append(NonBottleneck1D(128, 0.3, 8, norm_layer=norm_layer))
            self.layers.append(NonBottleneck1D(128, 0.3, 16, norm_layer=norm_layer))

    def forward(self, x):
        output = self.initial_block(x)

        for layer in self.layers:
            output = layer(output)
        return output


class UpsamplerBlock(nn.Module):
    def __init__(self, ninput, noutput, norm_layer=nn.BatchNorm2d):
        super().__init__()
        self.conv = nn.ConvTranspose2d(ninput, noutput, 3, stride=2, padding=1, output_padding=1, bias=True)
        self.bn = norm_layer(noutput, eps=1e-3)

    def forward(self, x):
        output = self.conv(x)
        output = self.bn(output)
        return F.relu(output)


class Decoder(nn.Module):
    def __init__(self, num_classes, norm_layer=nn.BatchNorm2d):
        super().__init__()

        self.layers = nn.ModuleList()

        self.layers.append(UpsamplerBlock(128, 64, norm_layer=norm_layer))
        self.layers.append(NonBottleneck1D(64, 0, 1, norm_layer=norm_layer))
        self.layers.append(NonBottleneck1D(64, 0, 1, norm_layer=norm_layer))

        self.layers.append(UpsamplerBlock(64, 16, norm_layer=norm_layer))
        self.layers.append(NonBottleneck1D(16, 0, 1, norm_layer=norm_layer))
        self.layers.append(NonBottleneck1D(16, 0, 1, norm_layer=norm_layer))

        self.output_conv = nn.ConvTranspose2d(16, num_classes, 2, stride=2, padding=0, output_padding=0, bias=True)

    def forward(self, x):
        output = x

        for layer in self.layers:
            output = layer(output)

        output = self.output_conv(output)

        return output


# ERFNet
class ERFNet(BaseModel):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super().__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm

        self.encoder = Encoder(image_channels, norm_layer=norm_layer)
        self.decoder = Decoder(num_classes, norm_layer=norm_layer)

    def forward(self, x):
        output = self.encoder(x)
        return self.decoder(output)
