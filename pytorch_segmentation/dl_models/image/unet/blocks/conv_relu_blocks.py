from torch import nn

from pytorch_segmentation.dl_models.utils import get_group_norm_groups


class DoubleConvReluBlock(nn.Module):
    """
    Conv Norm ReLU Conv Norm ReLU

    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(DoubleConvReluBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d

        if normalization == 'bn':
            norm = nn.BatchNorm3d if is_3d else nn.BatchNorm2d
            self.conv_block = nn.Sequential(
                conv(
                    input_channels,
                    output_channels,
                    kernel_size=kernel_size,
                    padding=padding,
                    stride=stride,
                    bias=False,
                ),
                norm(output_channels),
                nn.ReLU(inplace=True),
                conv(
                    output_channels,
                    output_channels,
                    kernel_size=kernel_size,
                    padding=padding,
                    stride=stride,
                    bias=False,
                ),
                norm(output_channels),
                nn.ReLU(inplace=True)
            )
        else:
            self.conv_block = nn.Sequential(
                conv(input_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride),
                nn.GroupNorm(get_group_norm_groups(output_channels), output_channels),
                nn.ReLU(inplace=True),
                conv(output_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride),
                nn.GroupNorm(get_group_norm_groups(output_channels), output_channels),
                nn.ReLU(inplace=True)
            )

    def forward(self, x):
        x = self.conv_block(x)
        return x


class ConvConvReluBlock(nn.Module):
    """
    I recommend to not use this if using the SlidingWindowPredictor. The class uses the first conv layer to get the
    overlapping. Customize the Predictor if you want to use this!

    Conv Conv ReLU

    Conventional conv relu conv relu can result in overfitting,
    conv conv relu can lead to better results. Must be tested
    https://arxiv.org/pdf/1709.06247.pdf
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(ConvConvReluBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        # inplace=True means that it will modify the input directly, without allocating any additional output.
        # It can sometimes slightly decrease the memory usage, but may not always be a valid operation
        # (because the original input is destroyed).
        conv = nn.Conv3d if is_3d else nn.Conv2d

        if normalization == 'bn':
            norm = nn.BatchNorm3d if is_3d else nn.BatchNorm2d
            self.conv_block = nn.Sequential(
                conv(input_channels, output_channels, kernel_size=(kernel_size, 1, 1), padding=padding, stride=stride),
                conv(output_channels, output_channels, kernel_size=(1, kernel_size, 1), stride=stride),
                conv(output_channels, output_channels, kernel_size=(1, 1, kernel_size), stride=stride),
                norm(output_channels),
                nn.ReLU(inplace=True),
            )
        else:
            self.conv_block = nn.Sequential(
                nn.Conv2d(
                    input_channels, output_channels, kernel_size=(kernel_size, 1), padding=padding, stride=stride
                ),
                nn.Conv2d(output_channels, output_channels, kernel_size=(1, kernel_size), stride=stride),
                nn.GroupNorm(get_group_norm_groups(output_channels), output_channels),
                nn.ReLU(inplace=True),
            )

    def forward(self, x):
        x = self.conv_block(x)
        return x


class ConvReluBlock(nn.Module):
    """
    Conv Norm ReLU
    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super().__init__()

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d

        if normalization == 'bn':
            norm = nn.BatchNorm3d if is_3d else nn.BatchNorm2d
            self.norm = norm(out_channels)
        else:
            self.norm = nn.GroupNorm(get_group_norm_groups(out_channels), out_channels)

        self.conv = conv(
            in_channels,
            out_channels,
            kernel_size=kernel_size,
            padding=padding,
            stride=stride,
            bias=False if normalization == 'bn' else True,
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.relu(x)
        return x


class ResidualDoubleConvBlock(nn.Module):
    """
    out = Conv ReLU (x)
    residual = out
    residual_block = Conv ReLU Conv ReLU (out)
    out = residual + residual_block
    out = ReLU(out)

    - keeps size
    - optimized: https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html#disable-bias-for-convolutions-directly-followed-by-a-batch-norm
    """

    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, is_3d=False, normalization='gn'):
        super(ResidualDoubleConvBlock, self).__init__()
        if kernel_size % 2 == 0:
            raise ValueError('Kernel size must be odd, not even.')
        if kernel_size == 0 or kernel_size < 0:
            raise ValueError('Kernel size must be positive and odd.')

        # to keep size always even
        padding = int((kernel_size - 1) / 2)

        conv = nn.Conv3d if is_3d else nn.Conv2d

        if normalization == 'bn':
            norm = nn.BatchNorm3d if is_3d else nn.BatchNorm2d
            self.norm = norm(output_channels)
            self.conv_1 = nn.Sequential(
                conv(
                    input_channels,
                    output_channels,
                    kernel_size=kernel_size,
                    padding=padding,
                    stride=stride,
                    bias=False,
                ),
                norm(output_channels),
                nn.ReLU(inplace=True),
            )
            self.conv_2 = nn.Sequential(
                conv(
                    output_channels,
                    output_channels,
                    kernel_size=kernel_size,
                    padding=padding,
                    stride=stride,
                    bias=False,
                ),
                norm(output_channels),
                nn.ReLU(inplace=True)
            )
        else:
            self.norm = nn.GroupNorm(get_group_norm_groups(output_channels), output_channels)
            self.conv_1 = nn.Sequential(
                conv(input_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride),
                nn.GroupNorm(get_group_norm_groups(output_channels), output_channels),
                nn.ReLU(inplace=True),
            )
            self.conv_2 = nn.Sequential(
                conv(output_channels, output_channels, kernel_size=kernel_size, padding=padding, stride=stride),
                nn.GroupNorm(get_group_norm_groups(output_channels), output_channels),
                nn.ReLU(inplace=True)
            )

        self.conv_3 = conv(
            output_channels,
            output_channels,
            kernel_size=kernel_size,
            padding=padding,
            stride=stride,
            bias=False if normalization == 'bn' else True,
        )
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv_1(x)
        residual = x

        x = self.conv_2(x)
        x = self.conv_3(x)
        x = self.norm(x)

        x += residual
        return self.relu(x)
