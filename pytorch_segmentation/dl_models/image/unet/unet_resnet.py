import torch
from torch.nn import functional

from pytorch_segmentation.dl_models.base_model import BaseModel
from .blocks.decoders import CRDecoder
from .blocks.decoders import ResidualDecoder
from pytorch_segmentation.dl_models.utils import load_backbone, batch_norm_to_group_norm
from .blocks.centers import CenterInterpolate
from .blocks.conv_relu_blocks import ConvReluBlock


class UNetResNet(BaseModel):
    """
    PyTorch U-Net model using ResNet(18, 34, 50, 101 or 152)/ResNext(50, 101) encoder
    Args:
        backbone (str): Depth of a ResNet encoder (34, 101 or 152).
        num_classes (int): Number of output classes, for binary: 2.
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone,
                 pretrained=True,
                 use_residual_decoder=False,
                 normalization='gn',
                 **kwargs):
        super().__init__(image_channels, num_classes)
        if num_classes == 2:
            # binary
            num_classes = 1

        if use_residual_decoder:
            decoder = ResidualDecoder
        else:
            decoder = CRDecoder

        self.num_classes = num_classes
        self.dropout_2d = 0.2

        # resnet does not support gray so i replaced that and set pretrained to False
        # standard normalization is batch norm, if group norm: pretrained is False as well
        if image_channels == 1 or normalization == 'gn':
            self.encoder, center_channels = load_backbone(backbone, False)
        else:
            self.encoder, center_channels = load_backbone(backbone, pretrained)

        if normalization == 'gn':
            self.encoder = batch_norm_to_group_norm(self.encoder)

        if image_channels == 1:
            # replace encoder first layer
            self.encoder.conv1 = torch.nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)

        self.start = torch.nn.Sequential(
            self.encoder.conv1,
            self.encoder.bn1,
            self.encoder.relu,
            torch.nn.MaxPool2d(2, 2)
        )
        self.enc1 = self.encoder.layer1
        self.enc2 = self.encoder.layer2
        self.enc3 = self.encoder.layer3
        self.enc4 = self.encoder.layer4

        self.center = CenterInterpolate(center_channels, 512, 256, normalization=normalization)

        self.dec4 = decoder(center_channels + 256, 512, 256, normalization=normalization)
        self.dec3 = decoder(center_channels // 2 + 256, 512, 256, normalization=normalization)
        self.dec2 = decoder(center_channels // 4 + 256, 256, 64, normalization=normalization)
        self.dec1 = decoder(center_channels // 8 + 64, 128, 128, normalization=normalization)
        self.dec0 = decoder(128, 128, 32, normalization=normalization)

        self.conv_relu = ConvReluBlock(32, 32, normalization=normalization)
        self.final = torch.nn.Conv2d(32, num_classes, kernel_size=1)

    def forward(self, x):
        x = self.start(x)
        enc_1 = self.enc1(x)
        enc_2 = self.enc2(enc_1)
        enc_3 = self.enc3(enc_2)
        enc_4 = self.enc4(enc_3)

        center = self.center(enc_4)

        dec_4 = self.dec4(torch.cat((center, enc_4), 1))
        dec_3 = self.dec3(torch.cat((dec_4, enc_3), 1))
        dec_2 = self.dec2(torch.cat((dec_3, enc_2), 1))
        dec_1 = self.dec1(torch.cat((dec_2, enc_1), 1))
        x = self.dec0(dec_1)
        x = self.conv_relu(x)

        x = self.final(functional.dropout2d(x, p=self.dropout_2d))
        return x
