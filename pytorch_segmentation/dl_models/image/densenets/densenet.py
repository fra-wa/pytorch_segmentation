import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import group_norm


def center_crop(layer, max_height, max_width):
    # https://github.com/Lasagne/Lasagne/blob/master/lasagne/layers/merge.py#L162
    # Author does a center crop which crops both inputs (skip and upsample) to size of minimum dimension on both w/h
    batch_size, n_channels, layer_height, layer_width = layer.size()
    xy1 = (layer_width - max_width) // 2
    xy2 = (layer_height - max_height) // 2
    return layer[:, :, xy2:(xy2 + max_height), xy1:(xy1 + max_width)]


class DenseLayer(nn.Sequential):
    def __init__(self, in_channels, growth_rate, normalization='bn'):
        super(DenseLayer, self).__init__()
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm
        self.add_module('norm', norm_layer(in_channels))
        self.add_module('relu', nn.ReLU(inplace=True))

        # author's impl - lasange 'same' pads with half
        # filter size (rounded down) on "both" sides
        self.add_module('conv', nn.Conv2d(in_channels=in_channels,
                                          out_channels=growth_rate, kernel_size=3, stride=1,
                                          padding=1, bias=True))

        self.add_module('drop', nn.Dropout2d(0.2))

    def forward(self, x):
        out = super(DenseLayer, self).forward(x)
        return out


class DenseBlock(nn.Module):
    def __init__(self, in_channels, growth_rate, n_layers, reduction_ratio, upsample=False, normalization='bn'):
        super(DenseBlock, self).__init__()
        self.reduction_ratio = reduction_ratio
        self.upsample = upsample
        self.layers = nn.ModuleList([
            DenseLayer(
                in_channels + i * growth_rate, growth_rate, normalization=normalization) for i in range(n_layers)]
        )

        if self.reduction_ratio is not None:
            if self.upsample:
                self.excitation1 = nn.Conv2d(growth_rate * n_layers,
                                             growth_rate * n_layers // self.reduction_ratio, kernel_size=1)
                self.excitation2 = nn.Conv2d(growth_rate * n_layers // self.reduction_ratio,
                                             growth_rate * n_layers, kernel_size=1)
            else:
                self.excitation1 = nn.Conv2d((in_channels + growth_rate * n_layers),
                                             (in_channels + growth_rate * n_layers) // self.reduction_ratio,
                                             kernel_size=1)
                self.excitation2 = nn.Conv2d((in_channels + growth_rate * n_layers) // self.reduction_ratio,
                                             (in_channels + growth_rate * n_layers), kernel_size=1)

    def forward(self, x):
        if self.upsample:
            new_features = []
            # we pass all previous activations into each dense layer normally
            # But we only store each dense layer's output in the new_features array
            for layer in self.layers:
                out = layer(x)
                x = torch.cat([x, out], 1)
                new_features.append(out)
            out = torch.cat(new_features, 1)

            if self.reduction_ratio is not None:
                fm_size = out.size()[2]
                scale_weight = F.avg_pool2d(out, fm_size)
                scale_weight = F.relu(self.excitation1(scale_weight))
                scale_weight = F.sigmoid(self.excitation2(scale_weight))
                out = out * scale_weight.expand_as(out)
            return out
        else:
            for layer in self.layers:
                out = layer(x)
                x = torch.cat([x, out], 1)  # 1 = channel axis

            if self.reduction_ratio is not None:
                fm_size = x.size()[2]
                scale_weight = F.avg_pool2d(x, fm_size)
                scale_weight = F.relu(self.excitation1(scale_weight))
                scale_weight = F.sigmoid(self.excitation2(scale_weight))
                x = x * scale_weight.expand_as(x)
            return x


class TransitionDown(nn.Sequential):
    def __init__(self, in_channels, normalization='bn'):
        super(TransitionDown, self).__init__()
        norm_layer = nn.BatchNorm2d if normalization == 'bn' else group_norm
        self.add_module('norm', norm_layer(in_channels))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(in_channels=in_channels,
                                          out_channels=in_channels, kernel_size=1, stride=1,
                                          padding=0, bias=True))
        self.add_module('drop', nn.Dropout2d(0.2))
        self.add_module('maxpool', nn.MaxPool2d(2))

    def forward(self, x):
        out = super(TransitionDown, self).forward(x)
        return out


class TransitionUp(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(TransitionUp, self).__init__()
        self.convTrans = nn.ConvTranspose2d(
            in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=2, padding=0, bias=True
        )
        # http://lasagne.readthedocs.io/en/latest/modules/layers/conv.html#lasagne.layers.TransposedConv2DLayer
        # self.updample2d = nn.UpsamplingBilinear2d(scale_factor=2)

    def forward(self, x, skip):
        out = self.convTrans(x)
        out = center_crop(out, skip.size(2), skip.size(3))
        out = torch.cat([out, skip], 1)
        return out


class Bottleneck(nn.Sequential):
    def __init__(self, in_channels, growth_rate, n_layers, reduction_ratio, normalization='bn'):
        self.reduction_ratio = reduction_ratio
        super(Bottleneck, self).__init__()
        self.add_module(
            'bottleneck',
            DenseBlock(
                in_channels, growth_rate, n_layers, self.reduction_ratio, upsample=True, normalization=normalization
            )
        )

    def forward(self, x):
        out = super(Bottleneck, self).forward(x)
        return out


class DenseNet(BaseModel):
    """
    Defaults to DenseNet103
    """

    def __init__(self, image_channels, num_classes, reduction_ratio=None, down_blocks=(4, 5, 7, 10, 12),
                 up_blocks=(12, 10, 7, 5, 4), bottleneck_layers=15,
                 growth_rate=16, out_chans_first_conv=48, normalization='bn'):
        super(DenseNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            num_classes = 1

        self.down_blocks = down_blocks
        self.up_blocks = up_blocks
        self.reduction_ratio = reduction_ratio

        # First Convolution #

        self.add_module('firstconv', nn.Conv2d(in_channels=image_channels,
                                               out_channels=out_chans_first_conv, kernel_size=3,
                                               stride=1, padding=1, bias=True))
        cur_channels_count = out_chans_first_conv
        skip_connection_channel_counts = []

        # Downsampling path #

        self.denseBlocksDown = nn.ModuleList([])
        self.transDownBlocks = nn.ModuleList([])
        for i in range(len(down_blocks)):
            self.denseBlocksDown.append(
                DenseBlock(
                    cur_channels_count, growth_rate, down_blocks[i], self.reduction_ratio, normalization=normalization
                )
            )

            cur_channels_count += (growth_rate * down_blocks[i])
            skip_connection_channel_counts.insert(0, cur_channels_count)
            self.transDownBlocks.append(TransitionDown(cur_channels_count, normalization=normalization))

        #     Bottleneck    #

        self.add_module(
            'bottleneck',
            Bottleneck(
                cur_channels_count,
                growth_rate,
                bottleneck_layers,
                self.reduction_ratio,
                normalization=normalization
            )
        )
        prev_block_channels = growth_rate * bottleneck_layers
        cur_channels_count += prev_block_channels

        #   Upsampling path   #

        self.transUpBlocks = nn.ModuleList([])
        self.denseBlocksUp = nn.ModuleList([])
        for i in range(len(up_blocks) - 1):
            self.transUpBlocks.append(TransitionUp(prev_block_channels, prev_block_channels))
            cur_channels_count = prev_block_channels + skip_connection_channel_counts[i]

            self.denseBlocksUp.append(
                DenseBlock(
                    cur_channels_count,
                    growth_rate,
                    up_blocks[i],
                    self.reduction_ratio,
                    upsample=True,
                    normalization=normalization,
                )
            )
            prev_block_channels = growth_rate * up_blocks[i]
            cur_channels_count += prev_block_channels

        # One final dense block
        self.transUpBlocks.append(TransitionUp(
            prev_block_channels, prev_block_channels))
        cur_channels_count = prev_block_channels + skip_connection_channel_counts[-1]

        self.denseBlocksUp.append(DenseBlock(
            cur_channels_count, growth_rate, up_blocks[-1], self.reduction_ratio,
            upsample=False, normalization=normalization))
        cur_channels_count += growth_rate * up_blocks[-1]

        #      Softmax      #

        self.finalConv = nn.Conv2d(in_channels=cur_channels_count,
                                   out_channels=num_classes, kernel_size=1, stride=1,
                                   padding=0, bias=True)

    def forward(self, x):
        out = self.firstconv(x)

        skip_connections = []
        for i in range(len(self.down_blocks)):
            out = self.denseBlocksDown[i](out)
            skip_connections.append(out)
            out = self.transDownBlocks[i](out)

        out = self.bottleneck(out)

        for i in range(len(self.up_blocks)):
            skip = skip_connections.pop()
            out = self.transUpBlocks[i](out, skip)
            out = self.denseBlocksUp[i](out)

        out = self.finalConv(out)
        return out


class DenseNet57(DenseNet):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseNet57, self).__init__(
            image_channels,
            num_classes,
            down_blocks=(4, 4, 4, 4, 4),
            up_blocks=(4, 4, 4, 4, 4),
            bottleneck_layers=4,
            growth_rate=4,
            out_chans_first_conv=48,
            normalization=normalization,
        )


class DenseNet67(DenseNet):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseNet67, self).__init__(
            image_channels,
            num_classes,
            down_blocks=(5, 5, 5, 5, 5),
            up_blocks=(5, 5, 5, 5, 5),
            bottleneck_layers=5,
            growth_rate=16,
            out_chans_first_conv=48,
            normalization=normalization,
        )


class DenseNet103(DenseNet):
    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(DenseNet103, self).__init__(
            image_channels,
            num_classes,
            down_blocks=(4, 5, 7, 10, 12),
            up_blocks=(12, 10, 7, 5, 4),
            bottleneck_layers=15,
            growth_rate=16,
            out_chans_first_conv=48,
            normalization=normalization,
        )
