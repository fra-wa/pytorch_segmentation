from .image.r2_and_attention_unet.r2_and_attention_unet import AttentionUNet
from .image.bisenet_and_v2.bisenet import BiSeNet
from .image.bisenet_and_v2.bisenetv2 import BiSeNetV2
from .image.danet.danet import DANet
from .image.danet.dran import Dran
from .image.deeplabv3plus.deeplab import DeepLabV3Plus
from .image.denseaspp.denseaspp import DenseASPP121
from .image.denseaspp.denseaspp import DenseASPP161
from .image.denseaspp.denseaspp import DenseASPP169
from .image.denseaspp.denseaspp import DenseASPP201
from .image.densenets.densenet import DenseNet57
from .image.densenets.densenet import DenseNet67
from .image.densenets.densenet import DenseNet103
from .image.dfanet.dfanet import DFANet
from .image.dfn.dfn import DFN
from .image.duc_hdc.duc_hdc import DeepLabDUCHDC
from .image.enet.enet import ENet
from .image.erfnet.erfnet import ERFNet
from .image.espnet.espnet import ESPNet
from .image.extremec3.extremec3 import ExtremeC3Net
from .image.fastscnn.fastscnn import FastSCNN
from .image.fcn.fcn import FCN8
from .image.fcn.fcn import FCN16
from .image.fcn.fcn import FCN32
from .image.gcn.gcn import GCN
from .image.gscnn.gscnn import GSCNN
from .image.hrnet.hrnet import HRNet
from .image.icnet.icnet import ICNet
from .image.laddernet.laddernet import LadderNet
from .image.lednet.lednet import LEDNet
from .image.ocnet.ocnet import OCNet
from .image.psanet.psanet import PSANet
from .image.pspnet.pspnet import PSPNet
from .image.pspnet.pspnet import PSPDenseNet
from .image.r2_and_attention_unet.r2_and_attention_unet import R2AttentionUNet
from .image.r2_and_attention_unet.r2_and_attention_unet import R2UNet
from .image.segnet.segnet import SegNet
from .image.segnet.segnet import SegResNet
from .image.sinet.sinet import SINet
from .image.unet.stateful_unet import SFUNet
from .image.unet.unet import UNet
from .image.unet.unet_resnet import UNetResNet
from .image.unet2plus.unet2plus import UNet2Plus
from .image.unet3plus.unet3plus import UNet3Plus
from .image.unet3plus.unet_3_plus_backbone import UNet3PlusBackboned
from .image.upernet.upernet import UPerNet

from .volumetric.dense_voxel_net.dense_voxel_net import DenseVoxelNet
from .volumetric.high_res_net.high_res_net import HighResNet3D
from .volumetric.resnet_3d_med_net.resnet_3d_med_net import ResNetMed3D
from .volumetric.skip_dense_net_3d.skip_dense_net_3d import SkipDenseNet3D
from .volumetric.unet3d.unet_3d import ResidualUNet3D
from .volumetric.unet3d.unet_3d import UNet3D
from .volumetric.unet_3plus_3d.unet3plus_3d import UNet3Plus3D
from .volumetric.vnet.vnet import VNet
from .volumetric.vnet.vnet import VNetLight

from ..constants import SUPPORTED_ARCHITECTURES

# placed here since that is no real constant but must be checked for consistency
# normally i would place that into model_constants.py but this would require a file manipulation
# if one only want to copy a model out of this project.
SUPPORTED_MODEL_CLASSES = [
    # 2D
    AttentionUNet,
    BiSeNet,
    BiSeNetV2,
    DANet,
    Dran,
    DeepLabV3Plus,
    DenseASPP121,
    DenseASPP161,
    DenseASPP169,
    DenseASPP201,
    DenseNet57,
    DenseNet67,
    DenseNet103,
    DFANet,
    DFN,
    DeepLabDUCHDC,
    ENet,
    ERFNet,
    ESPNet,
    ExtremeC3Net,
    FastSCNN,
    FCN8,
    FCN16,
    FCN32,
    GCN,
    GSCNN,
    HRNet,
    ICNet,
    LadderNet,
    LEDNet,
    OCNet,
    PSANet,
    PSPNet,
    PSPDenseNet,
    R2AttentionUNet,
    R2UNet,
    SegNet,
    SegResNet,
    SINet,
    UNet,
    UNetResNet,
    UNet2Plus,
    UNet3Plus,
    UNet3PlusBackboned,
    UPerNet,
    # 3D
    DenseVoxelNet,
    HighResNet3D,
    ResNetMed3D,
    # SFUNet,
    SkipDenseNet3D,
    ResidualUNet3D,
    UNet3D,
    # UNet3Plus3D,
    VNet,
    VNetLight,
]

for model_class in SUPPORTED_MODEL_CLASSES:
    try:
        assert model_class.__name__ in SUPPORTED_ARCHITECTURES
    except AssertionError:
        raise ValueError(
            f'The class {model_class.__name__} is not in /pytorch_segmentation/constants.SUPPORTED_ARCHITECTURES!\n'
            f'Please also check /pytorch_segmentation/models/__init__.py -> SUPPORTED_MODEL_CLASSES')
