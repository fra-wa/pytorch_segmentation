import inspect
import logging
import torch

from . import SUPPORTED_MODEL_CLASSES
from .backward_compatibility import BACKWARD_COMPATIBLE_NETWORKS

from pytorch_segmentation import constants
from ..models import ModelAnalysis
from ..temporary_parameters.parameters import FixedParameters


def instantiate_model_class(model_class,
                            image_channels,
                            num_classes,
                            normalization=None,
                            backbone=None,
                            pretrained=None,
                            use_gru=None,
                            device=None,
                            in_size=None,
                            log_information=True,
                            dataset_mean=None,
                            dataset_std=None,
                            ):

    model_name = model_class.__name__

    if log_information:
        logging.info(f'Instantiating model: {model_name} with')
        logging.info(f'      image_channels={image_channels}')
        logging.info(f'      num_classes=   {num_classes}')
        logging.info(f'      normalization= {normalization}')
        if model_name in constants.TWO_D_NETWORKS:
            logging.info(f'      backbone=      {backbone}')
        logging.info(f'      pretrained=    {pretrained}')
        if device == 'cuda':
            logging.info(f'      device=        {device} (multi GPU)')
        else:
            logging.info(f'      device=        {device}')
        if model_name == constants.PSANet_NAME:
            logging.info(f'      in_size=        {in_size}')

    try:
        model = model_class(
            image_channels=image_channels,
            num_classes=num_classes,
            normalization=normalization,
            backbone=backbone,
            pretrained=pretrained,
            use_gru=use_gru,
            device=device,
            in_size=in_size,
        )
    except TypeError as e:
        logging.info(f'Got following arguments for model: {model_name}')
        logging.info(f'image_channels = {image_channels}')
        logging.info(f'num_classes = {num_classes}')
        logging.info(f'normalization = {normalization}')
        if model_name in constants.TWO_D_NETWORKS:
            logging.info(f'backbone = {backbone}')
        logging.info(f'pretrained = {pretrained}')
        logging.info(f'device = {device}')
        if model_name == constants.PSANet_NAME:
            logging.info(f'in_size = {in_size}')

        raise NotImplementedError(
            f'Trying to load: {model_name} but got an unexpected argument. '
            f'If you added a new model with specific needs, please ensure compatibility!'
            f'\n'
            f'Expected arguments are: {inspect.signature(model_class)}'
        ) from e
    model.dataset_std = dataset_std
    model.dataset_mean = dataset_mean
    return model


def get_model(model_name,
              channels,
              classes,
              device,
              backbone=None,
              pretrained=None,
              use_gru=None,
              normalization=None,
              in_size=None,
              log_information=True,
              dataset_mean=None,
              dataset_std=None,
              model_classes=None,
              ):
    """

    Args:
        model_name: name is the same like class.__name__
        channels: input channels of the image
        classes: number of classes to segment into
        device: cuda or cpu
        backbone: if a backbone is supported, you can pass one. See at readme for supported ones
        pretrained: Ture or False. if no backbone is supported, this is redundant
        use_gru: if you have a stateful or stateless time series model, this will be the rnn cell
        normalization: 'bn' or 'gn' (batch norm or group norm)
        in_size: only for psa net.
        log_information: True -> shows the parameters for initialization, otherwise nothing will be printed
        dataset_mean: Mean of the dataset, the model was trained on. This is a key at the checkpoint.pt
        dataset_std: Standard deviation of the dataset, the model was trained on. This is a key at the checkpoint.pt
        model_classes: For backward compatibility. In those cases, DDP is slowed down via: find_unused_parameters=True
            See: dl_models/backward_compatibility/init.py -> BACKWARD_COMPATIBLE_NETWORKS

    Returns: the loaded model on the passed device

    """
    if model_classes is None:
        model_classes = SUPPORTED_MODEL_CLASSES

    if len(model_classes) != len(constants.SUPPORTED_ARCHITECTURES):
        raise NotImplementedError(
            f'Length of supported model classes: {len(model_classes)} '
            f'is not equal to length of supported models: {len(constants.SUPPORTED_ARCHITECTURES)}!\n'
            f'Please check the SUPPORTED_ARCHITECTURES at:\npytorch_segmentation/constants.py\n'
            f'and the SUPPORTED_MODEL_CLASSES at:\npytorch_segmentation/models/loading.py!'
        )

    for model_class in model_classes:
        try:
            assert model_class.__name__ in constants.SUPPORTED_ARCHITECTURES
        except AssertionError as e:
            raise ValueError(
                f'The model_name {model_class.__name__} is not in constants.SUPPORTED_ARCHITECTURES'
            ) from e

    model_class = None
    for model_class in SUPPORTED_MODEL_CLASSES:
        if model_class.__name__.lower() == model_name.lower():
            model_class = model_class
            break

    if model_class is None:
        raise NotImplementedError(f'The model {model_name} was not found.\n'
                                  f'Please check pytorch_segmentation/models/loading.py')

    model = instantiate_model_class(model_class=model_class, image_channels=channels, num_classes=classes,
                                    normalization=normalization, backbone=backbone, pretrained=pretrained,
                                    use_gru=use_gru, device=device, in_size=in_size, log_information=log_information,
                                    dataset_mean=dataset_mean, dataset_std=dataset_std)

    return model.to(device)


def load_checkpoint_to_run_test(params):
    """
    Loads the model to run a new test.
    """
    if not isinstance(params, FixedParameters):
        raise ValueError('Expected a FixedParameters instance as input')
    if not params.model_path:
        raise RuntimeError('Please pass the path to the saved checkpoint.')

    device = torch.device(params.device)
    checkpoint = torch.load(params.model_path, map_location=device)
    checkpoint_keys = checkpoint.keys()

    try:
        params.input_size = checkpoint['image_input_size']
        params.classes = checkpoint['classes']
        params.architecture = checkpoint['architecture']
        params.channels = checkpoint['image_channels']
    except KeyError as e:
        raise ValueError("This model can't be continued. Missing some keys at checkpoint") from e

    backbone = None
    norm = 'bn'

    if 'backbone' in checkpoint_keys:
        params.backbone = checkpoint['backbone']
        backbone = checkpoint['backbone']
    if 'depth_3d' in checkpoint_keys or 'input_depth' in checkpoint_keys:
        try:
            params.input_depth = checkpoint['depth_3d']
        except KeyError:
            params.input_depth = checkpoint['input_depth']
    if 'normalization' in checkpoint_keys:
        params.norm = checkpoint['normalization']
        norm = checkpoint['normalization']
    if 'dataset_mean' in checkpoint_keys:
        params.dataset_mean = checkpoint['dataset_mean']
    if 'dataset_std' in checkpoint_keys:
        params.dataset_std = checkpoint['dataset_std']
    if 'batch_size' in checkpoint_keys and (not params.batch_size or params.reproduce):
        params.batch_size = checkpoint['batch_size']

    params.pretrained = False

    model = get_model(model_name=params.architecture, channels=params.channels, classes=params.classes,
                      device=params.device, backbone=backbone, pretrained=False, use_gru=False, normalization=norm,
                      in_size=params.input_size, dataset_mean=params.dataset_mean, dataset_std=params.dataset_std)
    try:
        model.load_state_dict(checkpoint['model_state_dict'])
    except KeyError:
        model = get_model(model_name=params.architecture, channels=params.channels, classes=params.classes,
                          device=params.device, backbone=backbone, pretrained=False, use_gru=False, normalization=norm,
                          in_size=params.input_size, dataset_mean=params.dataset_mean, dataset_std=params.dataset_std,
                          model_classes=BACKWARD_COMPATIBLE_NETWORKS)
        model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()

    history = checkpoint['history']

    # compatibility with older checkpoints
    if len(history[0]) == 14:
        aug_strength = [line[13] for line in history]
    elif len(history[0]) == 6:
        aug_strength = [line[5] for line in history]
    else:
        aug_strength = [0.0 for line in history]

    history_dict = {
        'epochs': [line[0] for line in history],  # epoch of history starts at 1
        'train_loss': [line[1] for line in history],
        'valid_loss': [line[2] for line in history],
        'train_acc': [line[3] for line in history],
        'valid_acc': [line[4] for line in history],
        'aug_strength': aug_strength,
    }

    return params, model, history_dict


def load_model_from_checkpoint(model_path, device='cuda', log_instantiation_info=False):
    """
    Loads a model ready to use for some segmentation task.

    Args:
        model_path: path to the model
        device: cuda or cpu, you decide
        log_instantiation_info: during training, this can be useful --> prints some parameters for log files

    Returns: model in eval mode

    """
    checkpoint = torch.load(model_path, map_location=device)
    if checkpoint['architecture'] == 'UperNet':
        # re-saving old checkpoints when loading
        checkpoint['architecture'] = constants.UPerNet_NAME
        torch.save(checkpoint, model_path)
    checkpoint_keys = checkpoint.keys()

    backbone = None
    use_gru = True
    normalization = None
    in_size = None
    dataset_mean = None
    dataset_std = None

    if 'backbone' in checkpoint_keys:
        backbone = checkpoint['backbone']

    if 'use_gru' in checkpoint_keys:
        use_gru = checkpoint['use_gru']

    if 'normalization' in checkpoint_keys:
        normalization = checkpoint['normalization']
    if 'image_input_size' in checkpoint_keys:
        in_size = checkpoint['image_input_size']

    if 'dataset_mean' in checkpoint_keys:
        dataset_mean = checkpoint['dataset_mean']

    if 'dataset_std' in checkpoint_keys:
        dataset_std = checkpoint['dataset_std']

    if 'depth_3d' in checkpoint_keys or 'input_depth' in checkpoint_keys:
        try:
            input_depth = checkpoint['depth_3d']
        except KeyError:
            input_depth = checkpoint['input_depth']
    else:
        input_depth = None

    online_aug_kwargs = {} if 'online_aug_kwargs' not in checkpoint_keys else checkpoint['online_aug_kwargs']
    training_started_time = None if 'training_started_time' not in checkpoint_keys else checkpoint[
        'training_started_time']

    model_analysis_q = ModelAnalysis.objects.filter(
        architecture=checkpoint['architecture'],
        backbone=backbone if backbone else '',
        channels=checkpoint['image_channels'],
        classes=checkpoint['classes'],
        input_height=checkpoint['image_input_size'],
        input_width=checkpoint['image_input_size'],
        input_depth=input_depth,
        online_aug_kwargs_string=str(online_aug_kwargs),
        created_datetime=training_started_time,  # this should be enough to filter correctly locally
    )
    if model_analysis_q.count() == 1:
        model_analysis = model_analysis_q[0]
        if not model_analysis.normalization:
            model_analysis.normalization = checkpoint['normalization']
            model_analysis.save()

    model = get_model(model_name=checkpoint['architecture'], channels=checkpoint['image_channels'],
                      classes=checkpoint['classes'], device=device, backbone=backbone, pretrained=False,
                      use_gru=use_gru, normalization=normalization, in_size=in_size,
                      log_information=log_instantiation_info, dataset_mean=dataset_mean,
                      dataset_std=dataset_std)

    try:
        model.load_state_dict(checkpoint['model_state_dict'])
    except KeyError:
        model = get_model(model_name=checkpoint['architecture'], channels=checkpoint['image_channels'],
                          classes=checkpoint['classes'], device=device, backbone=backbone, pretrained=False,
                          use_gru=use_gru, normalization=normalization, in_size=in_size,
                          log_information=log_instantiation_info, dataset_mean=dataset_mean,
                          dataset_std=dataset_std, model_classes=BACKWARD_COMPATIBLE_NETWORKS)
        model.load_state_dict(checkpoint['model_state_dict'])

    model.eval()

    return model
