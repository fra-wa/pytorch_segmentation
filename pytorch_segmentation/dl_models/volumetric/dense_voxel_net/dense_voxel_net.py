import torch

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import group_norm

"""
Implementation od DenseVoxelNet based on https://arxiv.org/abs/1708.00573
"""


class DenseLayer(torch.torch.nn.Module):
    def __init__(self, in_channels, growth_rate, bn_size, drop_rate=0.2, norm_layer=torch.nn.BatchNorm3d):
        super(DenseLayer, self).__init__()
        out_channels = bn_size * growth_rate

        self.norm = norm_layer(in_channels)
        self.relu = torch.nn.ReLU(inplace=True)
        self.conv = torch.nn.Conv3d(in_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False)

        self.drop_layer = torch.nn.Dropout3d(p=drop_rate)

    def forward(self, x):
        out = self.norm(x)
        out = self.relu(out)
        out = self.conv(out)
        out = self.drop_layer(out)
        return torch.cat([x, out], 1)


class DenseBlock(torch.nn.Sequential):
    """
    to keep the spatial dims o=i, this formula is applied
    o = [i + 2*p - k - (k-1)*(d-1)]/s + 1
    """

    def __init__(self, num_layers, in_channels, bn_size, growth_rate, drop_rate=0.2, norm_layer=torch.nn.BatchNorm3d):
        super(DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = DenseLayer(in_channels + i * growth_rate, growth_rate, bn_size, drop_rate, norm_layer)
            self.add_module('denselayer%d' % (i + 1), layer)


class Transition(torch.nn.Module):
    def __init__(self, in_channels, out_channels, norm_layer=torch.nn.BatchNorm3d):
        super(Transition, self).__init__()
        self.norm = norm_layer(in_channels)
        self.relu = torch.nn.ReLU(inplace=True)
        self.conv3d = torch.nn.Conv3d(in_channels, out_channels, kernel_size=1, padding=0, stride=1)
        self.max_pool = torch.nn.MaxPool3d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv3d(x)
        y = self.max_pool(x)
        return y, x


class UpSampling(torch.nn.Sequential):
    """
    For transpose conv
    o = output, p = padding, k = kernel_size, s = stride, d = dilation
    o = (i -1)*s - 2*p + k + output_padding = (i-1)*2 +2 = 2*i
    """

    def __init__(self, in_channels, out_features, norm_layer=torch.nn.BatchNorm3d):
        super(UpSampling, self).__init__()
        self.tr_conv1_features = 128  # defined in the paper
        self.tr_conv2_features = out_features

        self.add_module(
            'conv',
            torch.nn.Conv3d(in_channels, in_channels, kernel_size=1, stride=1, padding=0, bias=False)
        )
        self.add_module('norm', norm_layer(in_channels))
        self.add_module('relu', torch.nn.ReLU(inplace=True))

        # Transposed convolutions must be un-padded?
        self.add_module(
            'transp_conv_1',
            torch.nn.ConvTranspose3d(
                in_channels,
                self.tr_conv1_features,
                kernel_size=2,
                padding=0,
                output_padding=0,
                stride=2
            )
        )
        self.add_module(
            'transp_conv_2',
            torch.nn.ConvTranspose3d(
                self.tr_conv1_features,
                self.tr_conv2_features,
                kernel_size=2,
                padding=0,
                output_padding=0,
                stride=2
            )
        )


class DenseVoxelNet(BaseModel):
    """
    Implementation based on https://arxiv.org/abs/1708.00573
    """

    def __init__(self, image_channels, num_classes, normalization='gn', **kwargs):
        super(DenseVoxelNet, self).__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1 and image_channels != 3:
            raise ValueError('image channels must be 1 or 3 for gray or RGB')

        if num_classes == 2:
            num_classes = 1

        norm_layer = torch.nn.BatchNorm3d if normalization == 'bn' else group_norm

        num_input_features = 16
        self.dense_1_out_features = 160
        self.dense_2_out_features = 304
        self.up_out_features = 64

        self.conv_init = torch.nn.Conv3d(
            image_channels, num_input_features, kernel_size=1, stride=2, padding=0, bias=False
        )
        self.dense_1 = DenseBlock(
            num_layers=12,
            in_channels=num_input_features,
            bn_size=1,
            growth_rate=12,
            norm_layer=norm_layer,
        )

        self.trans = Transition(self.dense_1_out_features, self.dense_1_out_features, norm_layer=norm_layer)

        self.dense_2 = DenseBlock(
            num_layers=12,
            in_channels=self.dense_1_out_features,
            bn_size=1,
            growth_rate=12,
            norm_layer=norm_layer,
        )
        self.up_block = UpSampling(self.dense_2_out_features, self.up_out_features, norm_layer=norm_layer)
        self.conv_final = torch.nn.Conv3d(self.up_out_features, num_classes, kernel_size=1, padding=0, bias=False)

        # self.transpose = torch.nn.ConvTranspose3d(
        #     self.dense_1_out_features, self.up_out_features, kernel_size=2, padding=0, output_padding=0, stride=2
        # )

    def forward(self, x):
        # Main network path
        x = self.conv_init(x)
        x = self.dense_1(x)
        x, _ = self.trans(x)
        x = self.dense_2(x)
        x = self.up_block(x)
        y1 = self.conv_final(x)

        # Auxiliary mid-layer prediction, kind of long-skip connection
        # t = self.transpose(t)
        # y2 = self.conv_final(t)
        return y1  # , y2
