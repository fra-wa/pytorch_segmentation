import torch.nn as nn

from .buildingblocks import create_decoders
from .buildingblocks import create_encoders
from .buildingblocks import DoubleConv
from .buildingblocks import ExtResNetBlock
from pytorch_segmentation.dl_models.base_model import BaseModel


def number_of_features_per_level(init_channel_number, num_levels):
    return [init_channel_number * 2 ** k for k in range(num_levels)]


class UNet3DBase(BaseModel):
    """
    Base class for standard and residual UNet.

    Args:
        in_channels (int): number of input channels
        out_channels (int): number of output segmentation masks;
            Note that that the of out_channels might correspond to either
            different semantic classes or to different binary segmentation mask.
            It's up to the user of the class to interpret the out_channels and
            use the proper loss criterion during training (i.e. CrossEntropyLoss (multi-class)
            or BCEWithLogitsLoss (two-class) respectively)
        f_maps (int, tuple): number of feature maps at each level of the encoder; if it's an integer the number
            of feature maps is given by the geometric progression: f_maps ^ k, k=1,2,3,4
        basic_module: basic model for the encoder/decoder (DoubleConv, ExtResNetBlock, ....)
        layer_order (string): determines the order of layers
            in `SingleConv` module. e.g. 'crg' stands for Conv3d+ReLU+GroupNorm3d.
            See `SingleConv` for more info
        num_groups (int): number of groups for the GroupNorm
        num_levels (int): number of levels in the encoder/decoder path (applied only if f_maps is an int)
        conv_kernel_size (int or tuple): size of the convolving kernel in the basic_module
        pool_kernel_size (int or tuple): the size of the window
        conv_padding (int or tuple): add zero-padding added to all three sides of the input
    """

    def __init__(self, 
                 in_channels,
                 out_channels,
                 basic_module,
                 f_maps=64,
                 layer_order='gcr',
                 num_groups=8,
                 num_levels=4,
                 conv_kernel_size=3,
                 pool_kernel_size=2,
                 conv_padding=1,
                 **kwargs):
        num_classes = 2 if out_channels == 1 else out_channels
        super(UNet3DBase, self).__init__(in_channels, num_classes)

        if isinstance(f_maps, int):
            f_maps = number_of_features_per_level(f_maps, num_levels=num_levels)

        if not (isinstance(f_maps, list) or isinstance(f_maps, tuple)):
            raise ValueError('f_maps must be a list or tuple!')
        if len(f_maps) < 1:
            raise ValueError("Required at least 2 levels in the U-Net")

        # create encoder path
        self.encoders = create_encoders(
            in_channels, f_maps, basic_module, conv_kernel_size, conv_padding, layer_order, num_groups, pool_kernel_size
        )

        # create decoder path
        self.decoders = create_decoders(
            f_maps, basic_module, conv_kernel_size, conv_padding, layer_order, num_groups, upsample=True
        )

        # in the last layer a 1×1 convolution reduces the number of output
        # channels to the number of labels
        self.final_conv = nn.Conv3d(f_maps[0], out_channels, kernel_size=1)

    def forward(self, x):
        # encoder part
        encoders_features = []
        for encoder in self.encoders:
            x = encoder(x)
            # reverse the encoder outputs to be aligned with the decoder
            encoders_features.insert(0, x)

        # remove the last encoder's output from the list
        # !!remember: it's the 1st in the list
        encoders_features = encoders_features[1:]

        # decoder part
        for decoder, encoder_features in zip(self.decoders, encoders_features):
            # pass the output from the corresponding encoder and the output
            # of the previous decoder
            x = decoder(encoder_features, x)

        x = self.final_conv(x)

        return x


class UNet3D(UNet3DBase):
    """
    3DUnet model from
    `"3D U-Net: Learning Dense Volumetric Segmentation from Sparse Annotation"
        <https://arxiv.org/pdf/1606.06650.pdf>`.

    Uses `DoubleConv` as a basic_module and nearest neighbor upsampling in the decoder
    """

    def __init__(self, 
                 image_channels,
                 num_classes,
                 f_maps=64,
                 normalization='gn',
                 num_groups=8,
                 num_levels=4,
                 conv_padding=1,
                 **kwargs):
        if num_classes == 2:
            num_classes = 1
        layer_order = 'gcr' if normalization == 'gn' else 'bcr'
        super(UNet3D, self).__init__(in_channels=image_channels,
                                     out_channels=num_classes,
                                     basic_module=DoubleConv,
                                     f_maps=f_maps,
                                     layer_order=layer_order,
                                     num_groups=num_groups,
                                     num_levels=num_levels,
                                     conv_padding=conv_padding)


class ResidualUNet3D(UNet3DBase):
    """
    Residual 3DUnet model implementation based on https://arxiv.org/pdf/1706.00120.pdf.
    Uses ExtResNetBlock as a basic building block, summation joining instead
    of concatenation joining and transposed convolutions for upsampling (watch out for block artifacts).
    Since the model effectively becomes a residual net, in theory it allows for deeper UNet.
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 f_maps=64,
                 normalization='gn',
                 num_groups=8,
                 num_levels=5,
                 conv_padding=1,
                 **kwargs):
        if num_classes == 2:
            num_classes = 1
        layer_order = 'gcr' if normalization == 'gn' else 'bcr'
        super(ResidualUNet3D, self).__init__(in_channels=image_channels,
                                             out_channels=num_classes,
                                             basic_module=ExtResNetBlock,
                                             f_maps=f_maps,
                                             layer_order=layer_order,
                                             num_groups=num_groups,
                                             num_levels=num_levels,
                                             conv_padding=conv_padding)
