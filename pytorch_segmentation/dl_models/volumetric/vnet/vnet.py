import torch.nn as nn
import torch

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import group_norm

"""
Modified version from here:
https://github.com/Dawn90/V-Net.pytorch
"""


class ChannelConsistentConvNormReLU(nn.Module):
    def __init__(self, channels, norm_layer=nn.BatchNorm3d):
        super(ChannelConsistentConvNormReLU, self).__init__()

        self.conv = nn.Conv3d(channels, channels, kernel_size=5, padding=2)
        self.norm = norm_layer(channels)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.norm(x)
        x = self.relu(x)
        return x


def n_consistent_convolutions(channels, number_of_convolutions, norm_layer=nn.BatchNorm3d):
    layers = []
    for _ in range(number_of_convolutions):
        layers.append(ChannelConsistentConvNormReLU(channels, norm_layer))
    return nn.Sequential(*layers)


class InputTransition(nn.Module):
    def __init__(self, in_channels, out_channels=16, norm_layer=nn.BatchNorm3d):
        super(InputTransition, self).__init__()

        self.repeat_rate = int(out_channels / in_channels)

        self.conv = nn.Conv3d(in_channels, out_channels, kernel_size=5, padding=2)
        self.norm = norm_layer(out_channels)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.conv(x)
        out = self.norm(out)
        x16 = x.repeat(1, self.repeat_rate, 1, 1, 1)
        return self.relu(torch.add(out, x16))


class DownTransition(nn.Module):
    def __init__(self, in_channels, layer_count, norm_layer=nn.BatchNorm3d, dropout=0.0):
        super(DownTransition, self).__init__()

        out_channels = 2 * in_channels
        self.down_conv = nn.Conv3d(in_channels, out_channels, kernel_size=2, stride=2)
        self.norm = norm_layer(out_channels)
        self.relu = nn.ReLU(inplace=True)

        self.dropout = nn.Dropout3d(p=dropout)
        self.ops = n_consistent_convolutions(out_channels, layer_count, norm_layer)

    def forward(self, x):
        x = self.down_conv(x)
        x = self.norm(x)
        x = self.relu(x)
        out = self.dropout(x)
        out = self.ops(out)
        out = self.relu(torch.add(out, x))
        return out


class UpTransition(nn.Module):
    def __init__(self, in_channels, out_channels, n_convolutions, dropout=0.0, norm_layer=torch.nn.BatchNorm3d):
        super(UpTransition, self).__init__()
        self.up_conv = nn.ConvTranspose3d(in_channels, out_channels // 2, kernel_size=2, stride=2)

        self.norm = norm_layer(out_channels // 2)

        self.dropout1 = nn.Dropout3d(p=dropout)
        self.relu = nn.ReLU(inplace=True)
        self.dropout2 = nn.Dropout3d(p=0.5)
        self.ops = n_consistent_convolutions(out_channels, n_convolutions, norm_layer)

    def forward(self, x, skip_x):
        out = self.dropout1(x)
        out = self.up_conv(out)
        out = self.norm(out)
        out = self.relu(out)

        skip_x = self.dropout2(skip_x)
        x_cat = torch.cat((out, skip_x), 1)
        out = self.ops(x_cat)
        out = self.relu(torch.add(out, x_cat))
        return out


class OutputTransition(nn.Module):
    def __init__(self, in_channels, num_classes, norm_layer=torch.nn.BatchNorm3d):
        super(OutputTransition, self).__init__()
        self.conv1 = nn.Conv3d(in_channels, num_classes, kernel_size=5, padding=2)
        self.norm = norm_layer(num_classes)
        self.relu = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv3d(num_classes, num_classes, kernel_size=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.norm(x)
        x = self.relu(x)
        x = self.conv2(x)
        return x


class VNet(BaseModel):
    """
    Implementations based on the Vnet paper: https://arxiv.org/abs/1606.04797
    """

    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(VNet, self).__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1:
            raise ValueError('image channels must be 1. RGB for 3D is not supported')

        if num_classes == 2:
            num_classes = 1  # binary

        norm_layer = torch.nn.BatchNorm3d if normalization == 'bn' else group_norm

        self.in_tr = InputTransition(image_channels, norm_layer=norm_layer)
        self.down_tr32 = DownTransition(16, 1, norm_layer=norm_layer)
        self.down_tr64 = DownTransition(32, 2, norm_layer=norm_layer)
        self.down_tr128 = DownTransition(64, 3, norm_layer=norm_layer, dropout=0.5)
        self.down_tr256 = DownTransition(128, 2, norm_layer=norm_layer, dropout=0.5)
        self.up_tr256 = UpTransition(256, 256, 2, norm_layer=norm_layer, dropout=0.5)
        self.up_tr128 = UpTransition(256, 128, 2, norm_layer=norm_layer, dropout=0.5)
        self.up_tr64 = UpTransition(128, 64, 1, norm_layer=norm_layer)
        self.up_tr32 = UpTransition(64, 32, 1, norm_layer=norm_layer)
        self.out_tr = OutputTransition(32, num_classes, norm_layer=norm_layer)

    def forward(self, x):
        out16 = self.in_tr(x)
        out32 = self.down_tr32(out16)
        out64 = self.down_tr64(out32)
        out128 = self.down_tr128(out64)
        out256 = self.down_tr256(out128)
        out = self.up_tr256(out256, out128)
        out = self.up_tr128(out, out64)
        out = self.up_tr64(out, out32)
        out = self.up_tr32(out, out16)
        out = self.out_tr(out)
        return out


class VNetLight(BaseModel):
    """
    A lighter version of Vnet that skips down_tr256 and up_tr256 in order to reduce time and space complexity
    """

    def __init__(self, image_channels, num_classes, normalization='bn', **kwargs):
        super(VNetLight, self).__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1:
            raise ValueError('image channels must be 1. RGB for 3D is not supported')

        if num_classes == 2:
            num_classes = 1  # binary

        norm_layer = torch.nn.BatchNorm3d if normalization == 'bn' else group_norm

        self.in_tr = InputTransition(image_channels, norm_layer=norm_layer)
        self.down_tr32 = DownTransition(16, 1, norm_layer=norm_layer)
        self.down_tr64 = DownTransition(32, 2, norm_layer=norm_layer)
        self.down_tr128 = DownTransition(64, 3, norm_layer=norm_layer, dropout=0.5)
        self.up_tr128 = UpTransition(128, 128, 2, norm_layer=norm_layer, dropout=0.5)
        self.up_tr64 = UpTransition(128, 64, 1, norm_layer=norm_layer)
        self.up_tr32 = UpTransition(64, 32, 1, norm_layer=norm_layer)
        self.out_tr = OutputTransition(32, num_classes, norm_layer=norm_layer)

    def forward(self, x):
        out16 = self.in_tr(x)
        out32 = self.down_tr32(out16)
        out64 = self.down_tr64(out32)
        out128 = self.down_tr128(out64)
        out = self.up_tr128(out128, out64)
        out = self.up_tr64(out, out32)
        out = self.up_tr32(out, out16)
        out = self.out_tr(out)
        return out
