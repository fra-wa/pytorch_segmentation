2023-01-19 10:37:19,048 INFO ################## 1
2023-01-19 10:37:19,048 INFO AttentionUNet
2023-01-19 10:37:21,467 INFO ################## 2
2023-01-19 10:37:21,482 INFO BiSeNet
2023-01-19 10:37:21,639 INFO ################## 3
2023-01-19 10:37:21,639 INFO BiSeNetV2
2023-01-19 10:37:22,171 INFO ################## 4
2023-01-19 10:37:22,171 INFO DANet, resnet50
2023-01-19 10:37:22,821 INFO ################## 4
2023-01-19 10:37:22,821 INFO DANet, resnet101
2023-01-19 10:37:23,729 INFO ################## 4
2023-01-19 10:37:23,744 INFO DANet, resnet152
2023-01-19 10:37:24,205 INFO ################## 4
2023-01-19 10:37:24,205 INFO DANet, resnext50
2023-01-19 10:37:25,396 INFO ################## 4
2023-01-19 10:37:25,396 INFO DANet, resnext101
2023-01-19 10:37:25,560 INFO head.conv6.1.weight
2023-01-19 10:37:25,560 INFO head.conv6.1.bias
2023-01-19 10:37:25,560 INFO head.conv7.1.weight
2023-01-19 10:37:25,560 INFO head.conv7.1.bias
2023-01-19 10:37:26,014 INFO ################## 5
2023-01-19 10:37:26,014 INFO Dran, resnet50
2023-01-19 10:37:26,670 INFO ################## 5
2023-01-19 10:37:26,670 INFO Dran, resnet101
2023-01-19 10:37:27,593 INFO ################## 5
2023-01-19 10:37:27,593 INFO Dran, resnet152
2023-01-19 10:37:28,047 INFO ################## 5
2023-01-19 10:37:28,047 INFO Dran, resnext50
2023-01-19 10:37:29,315 INFO ################## 5
2023-01-19 10:37:29,315 INFO Dran, resnext101
2023-01-19 10:37:29,738 INFO ################## 6
2023-01-19 10:37:29,738 INFO DeepLabV3Plus, drn
2023-01-19 10:37:30,291 INFO ################## 6
2023-01-19 10:37:30,291 INFO DeepLabV3Plus, xception
2023-01-19 10:37:30,837 INFO ################## 6
2023-01-19 10:37:30,837 INFO DeepLabV3Plus, resnet101
2023-01-19 10:37:31,152 INFO ################## 7
2023-01-19 10:37:31,168 INFO DenseASPP121
2023-01-19 10:37:31,777 INFO ################## 8
2023-01-19 10:37:31,777 INFO DenseASPP161
2023-01-19 10:37:32,450 INFO ################## 9
2023-01-19 10:37:32,450 INFO DenseASPP169
2023-01-19 10:37:33,030 INFO ################## 10
2023-01-19 10:37:33,030 INFO DenseASPP201
2023-01-19 10:37:33,324 INFO ################## 11
2023-01-19 10:37:33,324 INFO DenseNet57
2023-01-19 10:37:33,685 INFO ################## 12
2023-01-19 10:37:33,685 INFO DenseNet67
2023-01-19 10:37:34,170 INFO ################## 13
2023-01-19 10:37:34,170 INFO DenseNet103
2023-01-19 10:37:34,826 INFO ################## 14
2023-01-19 10:37:34,826 INFO DFANet
2023-01-19 10:37:35,115 INFO pretrained.fc.weight
2023-01-19 10:37:35,131 INFO pretrained.fc.bias
2023-01-19 10:37:35,757 INFO ################## 15
2023-01-19 10:37:35,757 INFO DFN
2023-01-19 10:37:35,897 INFO smooth_heads.0.rrb.conv_1x1.weight
2023-01-19 10:37:35,897 INFO smooth_heads.0.rrb.cbr.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.0.rrb.cbr.bn.weight
2023-01-19 10:37:35,913 INFO smooth_heads.0.rrb.cbr.bn.bias
2023-01-19 10:37:35,913 INFO smooth_heads.0.rrb.conv_refine.weight
2023-01-19 10:37:35,913 INFO smooth_heads.0.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.0.conv.bias
2023-01-19 10:37:35,913 INFO smooth_heads.1.rrb.conv_1x1.weight
2023-01-19 10:37:35,913 INFO smooth_heads.1.rrb.cbr.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.1.rrb.cbr.bn.weight
2023-01-19 10:37:35,913 INFO smooth_heads.1.rrb.cbr.bn.bias
2023-01-19 10:37:35,913 INFO smooth_heads.1.rrb.conv_refine.weight
2023-01-19 10:37:35,913 INFO smooth_heads.1.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.1.conv.bias
2023-01-19 10:37:35,913 INFO smooth_heads.2.rrb.conv_1x1.weight
2023-01-19 10:37:35,913 INFO smooth_heads.2.rrb.cbr.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.2.rrb.cbr.bn.weight
2023-01-19 10:37:35,913 INFO smooth_heads.2.rrb.cbr.bn.bias
2023-01-19 10:37:35,913 INFO smooth_heads.2.rrb.conv_refine.weight
2023-01-19 10:37:35,913 INFO smooth_heads.2.conv.weight
2023-01-19 10:37:35,913 INFO smooth_heads.2.conv.bias
2023-01-19 10:37:35,913 INFO border_pre_rrbs.0.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.0.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.0.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.0.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_pre_rrbs.0.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.1.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.1.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.1.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.1.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_pre_rrbs.1.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.2.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.2.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.2.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.2.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_pre_rrbs.2.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.3.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.3.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.3.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_pre_rrbs.3.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_pre_rrbs.3.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.0.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.0.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.0.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.0.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_aft_rrbs.0.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.1.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.1.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.1.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.1.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_aft_rrbs.1.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.2.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.2.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.2.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.2.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_aft_rrbs.2.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.3.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.3.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.3.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_aft_rrbs.3.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_aft_rrbs.3.conv_refine.weight
2023-01-19 10:37:35,913 INFO border_heads.0.rrb.conv_1x1.weight
2023-01-19 10:37:35,913 INFO border_heads.0.rrb.cbr.conv.weight
2023-01-19 10:37:35,913 INFO border_heads.0.rrb.cbr.bn.weight
2023-01-19 10:37:35,913 INFO border_heads.0.rrb.cbr.bn.bias
2023-01-19 10:37:35,913 INFO border_heads.0.rrb.conv_refine.weight
2023-01-19 10:37:35,929 INFO border_heads.0.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.0.conv.bias
2023-01-19 10:37:35,929 INFO border_heads.1.rrb.conv_1x1.weight
2023-01-19 10:37:35,929 INFO border_heads.1.rrb.cbr.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.1.rrb.cbr.bn.weight
2023-01-19 10:37:35,929 INFO border_heads.1.rrb.cbr.bn.bias
2023-01-19 10:37:35,929 INFO border_heads.1.rrb.conv_refine.weight
2023-01-19 10:37:35,929 INFO border_heads.1.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.1.conv.bias
2023-01-19 10:37:35,929 INFO border_heads.2.rrb.conv_1x1.weight
2023-01-19 10:37:35,929 INFO border_heads.2.rrb.cbr.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.2.rrb.cbr.bn.weight
2023-01-19 10:37:35,929 INFO border_heads.2.rrb.cbr.bn.bias
2023-01-19 10:37:35,929 INFO border_heads.2.rrb.conv_refine.weight
2023-01-19 10:37:35,929 INFO border_heads.2.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.2.conv.bias
2023-01-19 10:37:35,929 INFO border_heads.3.rrb.conv_1x1.weight
2023-01-19 10:37:35,929 INFO border_heads.3.rrb.cbr.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.3.rrb.cbr.bn.weight
2023-01-19 10:37:35,929 INFO border_heads.3.rrb.cbr.bn.bias
2023-01-19 10:37:35,929 INFO border_heads.3.rrb.conv_refine.weight
2023-01-19 10:37:35,929 INFO border_heads.3.conv.weight
2023-01-19 10:37:35,929 INFO border_heads.3.conv.bias
2023-01-19 10:37:36,592 INFO ################## 16
2023-01-19 10:37:36,592 INFO DeepLabDUCHDC
2023-01-19 10:37:36,859 INFO ################## 17
2023-01-19 10:37:36,859 INFO ENet
2023-01-19 10:37:37,062 INFO ################## 18
2023-01-19 10:37:37,062 INFO ERFNet
2023-01-19 10:37:37,172 INFO encoder.output_conv.weight
2023-01-19 10:37:37,172 INFO encoder.output_conv.bias
2023-01-19 10:37:37,203 INFO ################## 19
2023-01-19 10:37:37,203 INFO ESPNet
2023-01-19 10:37:37,390 INFO ################## 20
2023-01-19 10:37:37,390 INFO ExtremeC3Net
2023-01-19 10:37:37,554 INFO ################## 21
2023-01-19 10:37:37,554 INFO FastSCNN
2023-01-19 10:37:37,616 INFO classifier.sconv2.conv1.weight
2023-01-19 10:37:37,616 INFO classifier.sconv2.bn.weight
2023-01-19 10:37:37,616 INFO classifier.sconv2.bn.bias
2023-01-19 10:37:38,385 INFO ################## 22
2023-01-19 10:37:38,385 INFO FCN8
2023-01-19 10:37:39,174 INFO ################## 23
2023-01-19 10:37:39,174 INFO FCN16
2023-01-19 10:37:39,879 INFO ################## 24
2023-01-19 10:37:39,879 INFO FCN32
2023-01-19 10:37:40,050 INFO ################## 25
2023-01-19 10:37:40,050 INFO GCN, resnet18
2023-01-19 10:37:40,320 INFO ################## 25
2023-01-19 10:37:40,320 INFO GCN, resnet34
2023-01-19 10:37:40,620 INFO ################## 25
2023-01-19 10:37:40,620 INFO GCN, resnet50
2023-01-19 10:37:41,168 INFO ################## 25
2023-01-19 10:37:41,168 INFO GCN, resnet101
2023-01-19 10:37:41,894 INFO ################## 25
2023-01-19 10:37:41,894 INFO GCN, resnet152
2023-01-19 10:37:42,239 INFO ################## 25
2023-01-19 10:37:42,239 INFO GCN, resnext50
2023-01-19 10:37:43,239 INFO ################## 25
2023-01-19 10:37:43,239 INFO GCN, resnext101
2023-01-19 10:37:43,380 INFO br1.bn1.weight
2023-01-19 10:37:43,380 INFO br1.bn1.bias
2023-01-19 10:37:43,380 INFO br1.bn2.weight
2023-01-19 10:37:43,380 INFO br1.bn2.bias
2023-01-19 10:37:43,380 INFO br2.bn1.weight
2023-01-19 10:37:43,380 INFO br2.bn1.bias
2023-01-19 10:37:43,380 INFO br2.bn2.weight
2023-01-19 10:37:43,380 INFO br2.bn2.bias
2023-01-19 10:37:43,380 INFO br3.bn1.weight
2023-01-19 10:37:43,380 INFO br3.bn1.bias
2023-01-19 10:37:43,380 INFO br3.bn2.weight
2023-01-19 10:37:43,380 INFO br3.bn2.bias
2023-01-19 10:37:43,380 INFO br4.bn1.weight
2023-01-19 10:37:43,380 INFO br4.bn1.bias
2023-01-19 10:37:43,380 INFO br4.bn2.weight
2023-01-19 10:37:43,380 INFO br4.bn2.bias
2023-01-19 10:37:43,380 INFO br5.bn1.weight
2023-01-19 10:37:43,380 INFO br5.bn1.bias
2023-01-19 10:37:43,380 INFO br5.bn2.weight
2023-01-19 10:37:43,380 INFO br5.bn2.bias
2023-01-19 10:37:43,380 INFO br6.bn1.weight
2023-01-19 10:37:43,380 INFO br6.bn1.bias
2023-01-19 10:37:43,380 INFO br6.bn2.weight
2023-01-19 10:37:43,380 INFO br6.bn2.bias
2023-01-19 10:37:43,380 INFO br7.bn1.weight
2023-01-19 10:37:43,380 INFO br7.bn1.bias
2023-01-19 10:37:43,380 INFO br7.bn2.weight
2023-01-19 10:37:43,380 INFO br7.bn2.bias
2023-01-19 10:37:43,380 INFO br8.bn1.weight
2023-01-19 10:37:43,380 INFO br8.bn1.bias
2023-01-19 10:37:43,380 INFO br8.bn2.weight
2023-01-19 10:37:43,380 INFO br8.bn2.bias
2023-01-19 10:37:43,380 INFO br9.bn1.weight
2023-01-19 10:37:43,380 INFO br9.bn1.bias
2023-01-19 10:37:43,380 INFO br9.bn2.weight
2023-01-19 10:37:43,380 INFO br9.bn2.bias
2023-01-19 10:37:44,178 INFO ################## 26
2023-01-19 10:37:44,178 INFO GSCNN
2023-01-19 10:37:44,350 INFO dsn1.weight
2023-01-19 10:37:44,350 INFO dsn1.bias
2023-01-19 10:37:44,921 INFO ################## 27
2023-01-19 10:37:44,937 INFO HRNet
2023-01-19 10:37:45,530 INFO ################## 28
2023-01-19 10:37:45,530 INFO ICNet, resnet50
2023-01-19 10:37:46,085 INFO ################## 28
2023-01-19 10:37:46,085 INFO ICNet, resnet101
2023-01-19 10:37:46,883 INFO ################## 28
2023-01-19 10:37:46,883 INFO ICNet, resnet152
2023-01-19 10:37:47,211 INFO ################## 28
2023-01-19 10:37:47,211 INFO ICNet, resnext50
2023-01-19 10:37:48,298 INFO ################## 28
2023-01-19 10:37:48,298 INFO ICNet, resnext101
2023-01-19 10:37:48,516 INFO backbone.fc.weight
2023-01-19 10:37:48,516 INFO backbone.fc.bias
2023-01-19 10:37:48,516 INFO head.cff_12.conv_low_cls.weight
2023-01-19 10:37:48,516 INFO head.cff_24.conv_low_cls.weight
2023-01-19 10:37:48,548 INFO ################## 29
2023-01-19 10:37:48,548 INFO LadderNet
2023-01-19 10:37:48,676 INFO ################## 30
2023-01-19 10:37:48,676 INFO LEDNet
2023-01-19 10:37:49,150 INFO ################## 31
2023-01-19 10:37:49,150 INFO OCNet, resnet50
2023-01-19 10:37:49,744 INFO ################## 31
2023-01-19 10:37:49,744 INFO OCNet, resnet101
2023-01-19 10:37:50,549 INFO ################## 31
2023-01-19 10:37:50,564 INFO OCNet, resnet152
2023-01-19 10:37:50,940 INFO ################## 31
2023-01-19 10:37:50,940 INFO OCNet, resnext50
2023-01-19 10:37:52,089 INFO ################## 31
2023-01-19 10:37:52,089 INFO OCNet, resnext101
2023-01-19 10:37:52,167 INFO backbone.fc.weight
2023-01-19 10:37:52,167 INFO backbone.fc.bias
2023-01-19 10:37:52,723 INFO ################## 32
2023-01-19 10:37:52,723 INFO PSANet, resnet50
2023-01-19 10:37:53,491 INFO ################## 32
2023-01-19 10:37:53,491 INFO PSANet, resnet101
2023-01-19 10:37:54,515 INFO ################## 32
2023-01-19 10:37:54,515 INFO PSANet, resnet152
2023-01-19 10:37:55,124 INFO ################## 32
2023-01-19 10:37:55,124 INFO PSANet, resnext50
2023-01-19 10:37:56,430 INFO ################## 32
2023-01-19 10:37:56,430 INFO PSANet, resnext101
2023-01-19 10:37:56,524 INFO backbone.fc.weight
2023-01-19 10:37:56,524 INFO backbone.fc.bias
2023-01-19 10:37:57,031 INFO ################## 33
2023-01-19 10:37:57,031 INFO PSPNet, resnet50
2023-01-19 10:37:57,683 INFO ################## 33
2023-01-19 10:37:57,683 INFO PSPNet, resnet101
2023-01-19 10:37:58,553 INFO ################## 33
2023-01-19 10:37:58,553 INFO PSPNet, resnet152
2023-01-19 10:37:59,022 INFO ################## 33
2023-01-19 10:37:59,022 INFO PSPNet, resnext50
2023-01-19 10:38:00,234 INFO ################## 33
2023-01-19 10:38:00,234 INFO PSPNet, resnext101
2023-01-19 10:38:00,344 INFO backbone.fc.weight
2023-01-19 10:38:00,344 INFO backbone.fc.bias
2023-01-19 10:38:00,547 INFO ################## 34
2023-01-19 10:38:00,547 INFO PSPDenseNet, densenet121
2023-01-19 10:38:01,085 INFO ################## 34
2023-01-19 10:38:01,085 INFO PSPDenseNet, densenet161
2023-01-19 10:38:01,461 INFO ################## 34
2023-01-19 10:38:01,461 INFO PSPDenseNet, densenet169
2023-01-19 10:38:01,954 INFO ################## 34
2023-01-19 10:38:01,954 INFO PSPDenseNet, densenet201
2023-01-19 10:38:02,399 INFO ################## 35
2023-01-19 10:38:02,399 INFO R2AttentionUNet
2023-01-19 10:38:02,698 INFO ################## 36
2023-01-19 10:38:02,698 INFO R2UNet
2023-01-19 10:38:04,051 INFO ################## 37
2023-01-19 10:38:04,051 INFO SegNet
2023-01-19 10:38:04,708 INFO ################## 38
2023-01-19 10:38:04,708 INFO SegResNet, resnet50
2023-01-19 10:38:05,873 INFO ################## 38
2023-01-19 10:38:05,873 INFO SegResNet, resnet101
2023-01-19 10:38:07,545 INFO ################## 38
2023-01-19 10:38:07,545 INFO SegResNet, resnet152
2023-01-19 10:38:08,252 INFO ################## 38
2023-01-19 10:38:08,252 INFO SegResNet, resnext50
2023-01-19 10:38:10,542 INFO ################## 38
2023-01-19 10:38:10,542 INFO SegResNet, resnext101
2023-01-19 10:38:10,964 INFO ################## 39
2023-01-19 10:38:10,964 INFO SINet
2023-01-19 10:38:11,307 INFO ################## 40
2023-01-19 10:38:11,307 INFO UNet
2023-01-19 10:38:11,589 INFO ################## 41
2023-01-19 10:38:11,589 INFO UNetResNet, resnet18
2023-01-19 10:38:11,933 INFO ################## 41
2023-01-19 10:38:11,933 INFO UNetResNet, resnet34
2023-01-19 10:38:12,386 INFO ################## 41
2023-01-19 10:38:12,401 INFO UNetResNet, resnet50
2023-01-19 10:38:13,113 INFO ################## 41
2023-01-19 10:38:13,113 INFO UNetResNet, resnet101
2023-01-19 10:38:14,098 INFO ################## 41
2023-01-19 10:38:14,098 INFO UNetResNet, resnet152
2023-01-19 10:38:14,598 INFO ################## 41
2023-01-19 10:38:14,598 INFO UNetResNet, resnext50
2023-01-19 10:38:15,911 INFO ################## 41
2023-01-19 10:38:15,911 INFO UNetResNet, resnext101
2023-01-19 10:38:16,067 INFO encoder.fc.weight
2023-01-19 10:38:16,067 INFO encoder.fc.bias
2023-01-19 10:38:16,145 INFO ################## 42
2023-01-19 10:38:16,145 INFO UNet2Plus
2023-01-19 10:38:16,442 INFO ################## 43
2023-01-19 10:38:16,442 INFO UNet3Plus
2023-01-19 10:38:16,786 INFO ################## 44
2023-01-19 10:38:16,786 INFO UNet3PlusBackboned, resnet18
2023-01-19 10:38:17,114 INFO ################## 44
2023-01-19 10:38:17,114 INFO UNet3PlusBackboned, resnet34
2023-01-19 10:38:17,496 INFO ################## 44
2023-01-19 10:38:17,511 INFO UNet3PlusBackboned, resnet50
2023-01-19 10:38:18,106 INFO ################## 44
2023-01-19 10:38:18,106 INFO UNet3PlusBackboned, resnet101
2023-01-19 10:38:18,959 INFO ################## 44
2023-01-19 10:38:18,959 INFO UNet3PlusBackboned, resnet152
2023-01-19 10:38:19,365 INFO ################## 44
2023-01-19 10:38:19,365 INFO UNet3PlusBackboned, resnext50
2023-01-19 10:38:20,517 INFO ################## 44
2023-01-19 10:38:20,517 INFO UNet3PlusBackboned, resnext101
2023-01-19 10:38:20,673 INFO encoder.fc.weight
2023-01-19 10:38:20,673 INFO encoder.fc.bias
2023-01-19 10:38:21,009 INFO ################## 45
2023-01-19 10:38:21,009 INFO UPerNet, resnet18
2023-01-19 10:38:21,306 INFO ################## 45
2023-01-19 10:38:21,306 INFO UPerNet, resnet34
2023-01-19 10:38:22,018 INFO ################## 45
2023-01-19 10:38:22,018 INFO UPerNet, resnet50
2023-01-19 10:38:22,948 INFO ################## 45
2023-01-19 10:38:22,948 INFO UPerNet, resnet101
2023-01-19 10:38:24,137 INFO ################## 45
2023-01-19 10:38:24,137 INFO UPerNet, resnet152
2023-01-19 10:38:24,888 INFO ################## 45
2023-01-19 10:38:24,888 INFO UPerNet, resnext50
2023-01-19 10:38:26,469 INFO ################## 45
2023-01-19 10:38:26,469 INFO UPerNet, resnext101
2023-01-19 10:38:26,719 INFO ################## 46
2023-01-19 10:38:26,719 INFO DenseVoxelNet
2023-01-19 10:38:26,908 INFO ################## 47
2023-01-19 10:38:26,908 INFO HighResNet3D
2023-01-19 10:38:27,740 INFO ################## 48
2023-01-19 10:38:27,740 INFO ResNetMed3D, resnet10
2023-01-19 10:38:27,944 INFO ################## 48
2023-01-19 10:38:27,944 INFO ResNetMed3D, resnet18
2023-01-19 10:38:28,321 INFO ################## 48
2023-01-19 10:38:28,321 INFO ResNetMed3D, resnet34
2023-01-19 10:38:28,634 INFO ################## 48
2023-01-19 10:38:28,634 INFO ResNetMed3D, resnet50
2023-01-19 10:38:29,197 INFO ################## 48
2023-01-19 10:38:29,197 INFO ResNetMed3D, resnet101
2023-01-19 10:38:29,965 INFO ################## 48
2023-01-19 10:38:29,965 INFO ResNetMed3D, resnet152
2023-01-19 10:38:30,809 INFO ################## 48
2023-01-19 10:38:30,809 INFO ResNetMed3D, resnet200
2023-01-19 10:38:31,436 INFO ################## 49
2023-01-19 10:38:31,436 INFO SkipDenseNet3D
2023-01-19 10:38:32,526 INFO ################## 50
2023-01-19 10:38:32,526 INFO ResidualUNet3D
2023-01-19 10:38:32,764 INFO ################## 51
2023-01-19 10:38:32,764 INFO UNet3D
2023-01-19 10:38:33,078 INFO ################## 52
2023-01-19 10:38:33,078 INFO VNet
2023-01-19 10:38:33,276 INFO ################## 53
2023-01-19 10:38:33,276 INFO VNetLight
2023-01-19 10:38:33,307 INFO ##################
