###########################################################################
# Created by: Hang Zhang, edited by: Franz Wagner
# Email: zhang.hang@rutgers.edu / franz.wagner@tu-dresden.de
# Copyright (c) 2017
###########################################################################
import torch
from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import load_backbone, batch_norm_to_group_norm


class BaseNet(BaseModel):
    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone,
                 pretrained=True,
                 normalization='bn',
                 *args, **kwargs):
        super(BaseNet, self).__init__(image_channels, num_classes)
        if num_classes == 2:
            # binary!
            num_classes = 1

        self.nclass = num_classes

        if image_channels != 3 or normalization == 'gn':
            pretrained = False
        self.encoder = load_backbone(backbone, pretrained, False)[0]
        if normalization == 'gn':
            self.encoder = batch_norm_to_group_norm(self.pretrained)

        if image_channels != 3:
            self.encoder.conv1 = torch.nn.Conv2d(image_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)

        self.encoder.fc = None

    def base_forward(self, x):
        x = self.encoder.conv1(x)
        x = self.encoder.bn1(x)
        x = self.encoder.relu(x)
        x = self.encoder.maxpool(x)
        c1 = self.encoder.layer1(x)
        c2 = self.encoder.layer2(c1)
        c3 = self.encoder.layer3(c2)
        c4 = self.encoder.layer4(c3)
        return c1, c2, c3, c4

    def forward(self, **kwargs):
        raise NotImplementedError('implement at your model')
