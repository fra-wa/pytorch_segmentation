"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from torch.nn import functional

from ..blocks.decoders import CRDecoder
from ..blocks.decoders import ResidualDecoder
from ..blocks.encoders import DoubleCREncoder
from ..blocks.centers import CenterInterpolate
from ..blocks.conv_relu_blocks import ConvReluBlock
from ..blocks.conv_relu_blocks import DoubleConvReluBlock
from ..blocks.stateful_units import StatefulConvGRU
from ..blocks.stateful_units import StatefulConvLSTM

from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import get_group_norm_groups
from pytorch_segmentation.dl_models.utils import load_backbone


class SFUNetV2(BaseModel):
    """
    Second version of the stateful unet. Contains a stateful unit before the final layer
    """
    def __init__(self,
                 image_channels,
                 num_classes,
                 use_gru=True,
                 backbone=None,
                 pretrained=True,
                 use_residual_decoder=False,
                 recurrent_depth=1,
                 normalization='gn',
                 device='cuda',
                 ):
        """
        Args:
            image_channels: 1 or 3  (1: pretrained is always False!)
            num_classes: also called number of labels to segment, 2 for binary, more for multiclass
            use_gru: if False, LSTM will be used
            backbone: ResNet or ResNext backbone net for encoding
            pretrained: Whether or not the backbone should be pretrained.
                If single channel input: pretrained is set to False anyways since ResNe(x)t only takes rgb images.
            use_residual_decoder: You can use a residual decoder (try out, this has more parameters!)
            recurrent_depth: number of cells used to remember. Rule of thumb: not more than 2.
            normalization: For batch sizes <16 use group norm (default), else batch norm.
        """
        super(SFUNetV2, self).__init__(image_channels, num_classes)

        if image_channels != 1 and image_channels != 3:
            raise ValueError('image channels must be 1 or 3 for gray or RGB')

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')
        if num_classes == 2:
            num_classes = 1

        self.dropout = 0.2
        self.num_classes = num_classes

        if use_residual_decoder:
            decoder = ResidualDecoder
        else:
            decoder = CRDecoder

        if backbone:
            if image_channels == 1 or normalization == 'gn':
                self.encoder, center_channels = load_backbone(backbone, False, False)
            else:
                self.encoder, center_channels = load_backbone(backbone, pretrained, False)

            if normalization == 'gn':
                self.encoder = batch_norm_to_group_norm(self.encoder)

            if image_channels == 1:
                # replace encoder first layer
                self.encoder.conv1 = torch.nn.Conv2d(
                    1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False
                )

            self.start = torch.nn.Sequential(
                self.encoder.conv1,
                self.encoder.bn1,
                self.encoder.relu,
                torch.nn.MaxPool2d(2, 2),
            )
            self.enc_1 = self.encoder.layer1
            self.enc_2 = self.encoder.layer2
            self.enc_3 = self.encoder.layer3
            self.enc_4 = self.encoder.layer4
            if center_channels == 512:
                center_mid = 512
                center_out = 256
            else:
                center_mid = 1024
                center_out = 512
        else:
            center_channels = 1024
            center_mid = 512
            center_out = 256

            self.start = torch.nn.Sequential(
                ConvReluBlock(image_channels, 64, kernel_size=7, stride=2, normalization=normalization),
                torch.nn.MaxPool2d(2, 2),
            )
            # no max pool to imitate first encoder of resnet
            self.enc_1 = DoubleConvReluBlock(64, center_channels // 8, normalization=normalization)
            self.enc_2 = DoubleCREncoder(center_channels // 8, center_channels // 4, normalization=normalization)
            self.enc_3 = DoubleCREncoder(center_channels // 4, center_channels // 2, normalization=normalization)
            self.enc_4 = DoubleCREncoder(center_channels // 2, center_channels, normalization=normalization)

        rnn_class = StatefulConvGRU if use_gru else StatefulConvLSTM
        rnn_out_channels = 32

        if normalization == 'bn':
            self.norm = torch.nn.BatchNorm2d(rnn_out_channels)
        else:
            self.norm = torch.nn.GroupNorm(get_group_norm_groups(rnn_out_channels), rnn_out_channels)

        # Decoding part as follows:
        self.center = CenterInterpolate(center_channels, center_mid, center_out, normalization=normalization)

        self.dec4 = decoder(center_channels + center_out, 512, 512, normalization=normalization)
        self.dec3 = decoder(center_channels // 2 + 512, 512, 256, normalization=normalization)
        self.dec2 = decoder(center_channels // 4 + 256, 256, 64, normalization=normalization)
        self.dec1 = decoder(center_channels // 8 + 64, 128, 128, normalization=normalization)
        self.dec0 = decoder(128, 128, 32, normalization=normalization)

        self.rnn_layer = rnn_class(
            32,
            hidden_channels=[rnn_out_channels for i in range(recurrent_depth)],
            kernel_size=(3, 3),
            device=device,
        )

        self.final = torch.nn.Conv2d(32, num_classes, kernel_size=(1, 1))

    def forward(self, input_frames, reset_state):
        """
        For evaluating:
        - To loop over input images, pass:
            - tensor shaped like B, 1, c, h, w --> represents one single image of each batch
            - reset_state = False

        Args:
            input_frames: Tensor shaped like b, f, c, h, w
                b ... batches
                f ... frames of the batch video/sequence
                c ... channels (1 or 3 --> Gray or RGB)
                h ... height of the frames
                w ... width of the frames
            reset_state: reset hidden state if analyzing a new video/sequence

        Returns: b x f x n x h x w segmentation mask
            b ... batch
            f ... frames containing the maps of each input image
            n ... number of classes
            h ... height of input frames
            w ... width of input frames

        """

        # if new sequence, passing reset to first iteration
        outputs = []

        batch_size, frames, channels, height, width = input_frames.shape

        for frame_nr in range(frames):

            x = input_frames[:, frame_nr, :, :, :]
            x = self.start(x)
            enc_1 = self.enc_1(x)
            enc_2 = self.enc_2(enc_1)
            enc_3 = self.enc_3(enc_2)
            enc_4 = self.enc_4(enc_3)

            center = self.center(enc_4)

            dec_4 = self.dec4(torch.cat((center, enc_4), dim=1))
            dec_3 = self.dec3(torch.cat((dec_4, enc_3), dim=1))
            dec_2 = self.dec2(torch.cat((dec_3, enc_2), dim=1))
            dec_1 = self.dec1(torch.cat((dec_2, enc_1), dim=1))
            x = self.dec0(dec_1)

            x = self.rnn_layer(x, reset_state)
            x = self.norm(x)

            x = self.final(functional.dropout2d(x, p=self.dropout))

            outputs.append(x)

            if reset_state:
                # reset only at first iteration
                reset_state = False

        output_batch = torch.stack(outputs).transpose(0, 1)

        return output_batch
