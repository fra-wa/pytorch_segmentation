"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from .conv_relu_blocks import ConvReluBlock
from .conv_relu_blocks import ResidualDoubleConvBlock
from .stateful_units import StatefulConvGRU
from .stateful_units import StatefulConvLSTM


class SFCRDecoder(torch.nn.Module):
    """
    Stateful Conv ReLu Decoder
    CR TCR Stateful Unit
    """
    def __init__(self,
                 in_channels,
                 middle_channels,
                 out_channels,
                 normalization='gn',
                 use_gru=True,
                 device='cuda',
                 recurrent_depth=1):
        super(SFCRDecoder, self).__init__()

        self.block = torch.nn.Sequential(
            ConvReluBlock(in_channels, middle_channels, normalization=normalization),
            torch.nn.ConvTranspose2d(middle_channels, out_channels, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
            torch.nn.ReLU(inplace=True)
        )
        rnn_class = StatefulConvGRU if use_gru else StatefulConvLSTM
        self.rnn_layer = rnn_class(
            out_channels,
            hidden_channels=[out_channels for i in range(recurrent_depth)],
            kernel_size=(3, 3),
            device=device,
        )

    def forward(self, x, reset_state):
        x = self.block(x)
        x = self.rnn_layer(x, reset_state)
        return x


class SFResidualDecoder(torch.nn.Module):
    def __init__(self,
                 in_channels,
                 middle_channels,
                 out_channels,
                 normalization='gn',
                 use_gru=True,
                 device='cuda',
                 recurrent_depth=1
                 ):
        super(SFResidualDecoder, self).__init__()

        self.block = torch.nn.Sequential(
            ResidualDoubleConvBlock(in_channels, middle_channels, normalization=normalization),
            torch.nn.ConvTranspose2d(middle_channels, out_channels, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1)),
            torch.nn.ReLU(inplace=True)
        )
        rnn_class = StatefulConvGRU if use_gru else StatefulConvLSTM
        self.rnn_layer = rnn_class(
            out_channels,
            hidden_channels=[out_channels for i in range(recurrent_depth)],
            kernel_size=(3, 3),
            device=device,
        )

    def forward(self, x, reset_state):
        x = self.block(x)
        x = self.rnn_layer(x, reset_state)
        return x
