import torch

from ..blocks.conv_relu_blocks import ConvConvReluBlock
from ..blocks.conv_relu_blocks import ConvReluBlock
from ..blocks.conv_relu_blocks import DoubleConvReluBlock
from ..blocks.conv_relu_blocks import ResidualDoubleConvBlock
from pytorch_segmentation.dl_models.utils import get_group_norm_groups


class DecoderBase(torch.nn.Module):
    def __init__(self, in_channels, out_channels, conv_relu_variant, trainable=True, is_3d=False, normalization='gn'):
        """

        Args:
            in_channels:
            out_channels:
            trainable:
            conv_relu_variant: conv relu class to use: DoubleCR or CCR
        """
        super(DecoderBase, self).__init__()
        # Parameters for Deconvolution were chosen to avoid artifacts, following
        # link https://distill.pub/2016/deconv-checkerboard/
        conv_transpose = torch.nn.ConvTranspose3d if is_3d else torch.nn.ConvTranspose2d
        if trainable:
            if normalization == 'bn' or is_3d:
                norm = torch.nn.BatchNorm3d(out_channels) if is_3d else torch.nn.BatchNorm2d(out_channels)
            else:
                norm = torch.nn.GroupNorm(get_group_norm_groups(out_channels), out_channels)

            self.up = torch.nn.Sequential(
                conv_transpose(in_channels, out_channels, kernel_size=4, stride=2, padding=1),
                norm,
                torch.nn.ReLU(inplace=True)
            )
        else:
            # Not trainable, might be better to reduce over fitting
            # https://discuss.pytorch.org/t/difference-between-nn-upsample-and-nn-convtranspose2d/32653
            self.up = torch.nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)

        self.conv = conv_relu_variant(in_channels, out_channels, is_3d=is_3d, normalization=normalization)

    def forward(self, x, skip_connection_block):
        x = self.up(x)
        x = torch.cat((x, skip_connection_block), dim=1)
        x = self.conv(x)
        return x


class CRCRDecoderBlock(DecoderBase):
    """
    ConvTranspose2D (Up) -> Concat (old + (Conv -> ReLU -> Conv -> ReLU))
    """
    def __init__(self, input_channels, output_channels, trainable=True, is_3d=False, normalization='gn'):
        super(CRCRDecoderBlock, self).__init__(
            input_channels, output_channels, DoubleConvReluBlock, trainable, is_3d, normalization
        )


class CCRDecoderBlock(DecoderBase):
    """
    ConvTranspose2D (Up) -> Concat (old + (Conv -> Conv -> ReLU))
    """
    def __init__(self, input_channels, output_channels, trainable=True, is_3d=False, normalization='gn'):
        super(CCRDecoderBlock, self).__init__(
            input_channels, output_channels, ConvConvReluBlock, trainable, is_3d, normalization
        )


class CRDecoder(torch.nn.Module):
    """
    Does not follow above rules
    """
    def __init__(self, in_channels, middle_channels, out_channels, is_3d=False, normalization='gn'):
        super(CRDecoder, self).__init__()

        conv_transpose = torch.nn.ConvTranspose3d if is_3d else torch.nn.ConvTranspose2d
        self.block = torch.nn.Sequential(
            ConvReluBlock(in_channels, middle_channels, is_3d=is_3d, normalization=normalization),
            conv_transpose(middle_channels, out_channels, kernel_size=4, stride=2, padding=1),
            torch.nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.block(x)


class ResidualDecoder(torch.nn.Module):
    def __init__(self, in_channels, middle_channels, out_channels, is_3d=False, normalization='gn'):
        super(ResidualDecoder, self).__init__()

        conv_transpose = torch.nn.ConvTranspose3d if is_3d else torch.nn.ConvTranspose2d
        self.block = torch.nn.Sequential(
            ResidualDoubleConvBlock(in_channels, middle_channels, is_3d=is_3d, normalization=normalization),
            conv_transpose(middle_channels, out_channels, kernel_size=4, stride=2, padding=1),
            torch.nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.block(x)

