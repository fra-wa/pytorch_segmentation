"""
UNet 3+ paper: https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from .nn_blocks import ConvReluBlock
from .nn_blocks import DoubleConvReluBlock
from pytorch_segmentation.dl_models.base_model import BaseModel
from pytorch_segmentation.dl_models.utils import batch_norm_to_group_norm
from pytorch_segmentation.dl_models.utils import load_backbone


class ThreePlusBackboneDecoder(torch.nn.Module):
    def __init__(self, center_out_channels, level, normalization='gn'):
        """
        ONLY for UNet3+ !WITH! a backbone
        Suppose you have one center and 4 encoders.
        Suppose you want to decode at level 3
        Idea here is:
            Init by giving the current decoder level (3) then the operation list will be:
            [
                upscaling_center by 4
                upscaling level_4 by 2
                ConvNormReLu ConvNormReLu on level_3
                downsampling level_2 by 2
                downsampling level_1 by 4
            ]
        At forward, the encoder iterates through the layers:
            *args = [center, enc_4, enc_3, enc_2, enc_1]

        Args:
            center_out_channels: based on the encoder, you will have either 512, 4096
        """
        super(ThreePlusBackboneDecoder, self).__init__()

        self.max_pool = torch.nn.MaxPool2d
        self.unet_depth = 5  # number of encoder operations + center -> 5 for backbone

        self.operation_list = torch.nn.ModuleList()

        center_up_factor = int(2 ** (self.unet_depth - level))
        self.operation_list.append(
            torch.nn.Sequential(
                torch.nn.Upsample(scale_factor=center_up_factor, mode='bilinear', align_corners=True),
                DoubleConvReluBlock(center_out_channels, 64, normalization=normalization),
            )
        )

        # iterate from down level (level=4) to top level (enc 1, level = 1), regard the decoded in channels: 320
        # if level is 1, we need to upsample every decoded part (in_channel=320) --> scale will be: 16, 8, 4, 2
        for i in range(1, self.unet_depth - level):
            up_scaling_factor = int(2 ** (self.unet_depth - level - i))
            in_channels = 320
            self.operation_list.append(
                torch.nn.Sequential(
                    torch.nn.Upsample(scale_factor=up_scaling_factor, mode='bilinear', align_corners=True),
                    DoubleConvReluBlock(in_channels, 64, normalization=normalization),
                )
            )

        current_scale = int(2 ** (self.unet_depth - level))
        current_in_channels = center_out_channels // current_scale
        if level == 1:
            current_in_channels = 64
        self.operation_list.append(torch.nn.Sequential(
            DoubleConvReluBlock(current_in_channels, 64, normalization=normalization)
        ))

        # iterate from down level (center, level=5) to top level (enc 1, level = 1)
        # if level is 1, we need to upsample every decoded part --> scale will be: 16, 8, 4, 2
        for i in range(1, level):
            kernel_size = int(2 ** i)
            in_channels = current_in_channels // int(2 ** i)
            if i == level - 1:
                in_channels = 64  # encoder 1 has 64 as out channels
            self.operation_list.append(
                torch.nn.Sequential(
                    self.max_pool(kernel_size),
                    DoubleConvReluBlock(in_channels, 64, normalization=normalization),
                )
            )

        self.final = ConvReluBlock(64 * self.unet_depth, 64 * self.unet_depth, normalization=normalization)

    def forward(self, *args):
        """
        Suppose you have 4 encoders and one center. Then:
        *args must be: center, enc_4, enc_3, enc_2, enc_1
        """
        if len(args) != self.unet_depth:
            raise ValueError('Only for UNet3+\n'
                             'Pass all encoded elements as follows: center, level_4, level_3, level_2, level_1\n'
                             'Assuming you have 4 encoders and one center.')
        out_list = []
        for idx, element in enumerate(args):
            out_list.append(self.operation_list[idx](element))

        x = torch.cat(out_list, dim=1)

        return self.final(x)


class UNet3PlusBackboned(BaseModel):
    """
    PyTorch U-Net+++ model with ResNet(18, 34, 50, 101 or 152)/ResNext(50, 101) encoder
    UNet3+: https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 backbone,
                 pretrained=False,
                 normalization='gn',
                 **kwargs):
        super().__init__(image_channels, num_classes)

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')

        if image_channels != 1 and image_channels != 3:
            raise ValueError('image channels must be 1 or 3 for gray or RGB')

        if image_channels == 1 or normalization == 'gn':
            self.encoder, self.center_channels = load_backbone(backbone, False, False)
        else:
            self.encoder, self.center_channels = load_backbone(backbone, pretrained, False)

        if normalization == 'gn':
            self.encoder = batch_norm_to_group_norm(self.encoder)

        if image_channels == 1:
            # replace encoder first layer
            self.encoder.conv1 = torch.nn.Conv2d(
                1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False
            )

        if num_classes == 2:
            num_classes = 1

        self.num_classes = num_classes
        self.dropout_2d = 0.2

        self.enc_1 = torch.nn.Sequential(
            self.encoder.conv1,
            self.encoder.bn1,  # don't be confused, bn1 is either group norm or batch norm
            self.encoder.relu,
            # no max pool here
        )
        self.max_pool = torch.nn.MaxPool2d(2, 2)
        self.enc_2 = self.encoder.layer1
        self.enc_3 = self.encoder.layer2
        self.enc_4 = self.encoder.layer3

        self.center = self.encoder.layer4

        self.dec_4 = ThreePlusBackboneDecoder(self.center_channels, 4, normalization=normalization)
        self.dec_3 = ThreePlusBackboneDecoder(self.center_channels, 3, normalization=normalization)
        self.dec_2 = ThreePlusBackboneDecoder(self.center_channels, 2, normalization=normalization)
        self.dec_1 = ThreePlusBackboneDecoder(self.center_channels, 1, normalization=normalization)

        self.up_sample = torch.nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.final = torch.nn.Conv2d(320, num_classes, kernel_size=(3, 3), padding=(1, 1))

    def forward(self, x):
        enc_1 = self.enc_1(x)
        pooled = self.max_pool(enc_1)
        enc_2 = self.enc_2(pooled)
        enc_3 = self.enc_3(enc_2)
        enc_4 = self.enc_4(enc_3)

        center = self.center(enc_4)

        dec_4 = self.dec_4(center, enc_4, enc_3, enc_2, enc_1)
        dec_3 = self.dec_3(center, dec_4, enc_3, enc_2, enc_1)
        dec_2 = self.dec_2(center, dec_4, dec_3, enc_2, enc_1)
        dec_1 = self.dec_1(center, dec_4, dec_3, dec_2, enc_1)

        x = self.up_sample(dec_1)
        x = self.final(x)
        return x
