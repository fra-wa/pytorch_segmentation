import subprocess
import warnings

import cv2
import datetime
import logging
import math
import numpy as np
import os
import psutil
import random
import subprocess as sp
import sys
import time
import torch

from collections import OrderedDict

from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone
from PIL import Image

from pytorch_segmentation import constants


def get_human_time_values_from_seconds(seconds):
    hours = int(seconds / 3600)
    minutes = int(seconds / 60 - 60 * hours)
    seconds = int(seconds - 3600 * hours - 60 * minutes)
    return hours, minutes, seconds


def get_human_time_string_from_h_m_s(hours, minutes, seconds, full_string=True):
    if hours == 0:
        human_hours = f'00'
    elif hours < 10:
        human_hours = f'0{hours}'
    else:
        human_hours = f'{hours}'
    if minutes == 0:
        human_minutes = f'00'
    elif minutes < 10:
        human_minutes = f'0{minutes}'
    else:
        human_minutes = f'{minutes}'
    if seconds == 0:
        human_seconds = f'00'
    elif seconds < 10:
        human_seconds = f'0{seconds}'
    else:
        human_seconds = f'{seconds}'

    if hours or full_string:
        return f'{human_hours}h:{human_minutes}m:{human_seconds}s'
    elif minutes:
        return f'{human_minutes}m:{human_seconds}s'
    else:
        return f'{human_seconds}s'


def get_human_time_string_from_seconds(seconds, full_string=True):
    hours, minutes, seconds = get_human_time_values_from_seconds(seconds)
    days = 0
    if hours > 24:
        days = hours // 24
        hours = hours - days * 24
    time_string = get_human_time_string_from_h_m_s(hours, minutes, seconds, full_string)
    if full_string and days:
        time_string = f'{days}d, {time_string}'
    return time_string


def calculate_human_spent_time_values(start_time):
    current_time = time.time()
    total_time = current_time - start_time
    return get_human_time_values_from_seconds(total_time)


def get_time_spent_string(start_time):
    hours, minutes, seconds = calculate_human_spent_time_values(start_time)
    return get_human_time_string_from_h_m_s(hours, minutes, seconds)


def get_time_remaining_string(start_time, total_count, current_idx):
    current_time = time.time()
    total_seconds_spent = current_time - start_time
    remaining_seconds = (total_seconds_spent * total_count) / (current_idx + 1) - total_seconds_spent
    hours, minutes, seconds = get_human_time_values_from_seconds(remaining_seconds)
    return get_human_time_string_from_h_m_s(hours, minutes, seconds)


def get_human_date(date_format='%d.%m.%Y'):
    """
    Args:
        date_format: build up from:
            %a	Abbreviated weekday name.	Sun, Mon, ...
            %A	Full weekday name.	Sunday, Monday, ...
            %w	Weekday as a decimal number.	0, 1, ..., 6
            %d	Day of the month as a zero-padded decimal.	01, 02, ..., 31
            %-d	Day of the month as a decimal number.	1, 2, ..., 30
            %b	Abbreviated month name.	Jan, Feb, ..., Dec
            %B	Full month name.	January, February, ...
            %m	Month as a zero-padded decimal number.	01, 02, ..., 12
            %-m	Month as a decimal number.	1, 2, ..., 12
            %y	Year without century as a zero-padded decimal number.	00, 01, ..., 99
            %-y	Year without century as a decimal number.	0, 1, ..., 99
            %Y	Year with century as a decimal number.	2013, 2019 etc.
            %Z	Time zone name.
            %j	Day of the year as a zero-padded decimal number.	001, 002, ..., 366
            %-j	Day of the year as a decimal number.	1, 2, ..., 366
            %U	Week number of the year (Sunday as the first day of the week).
                All days in a new year preceding the first Sunday are considered to be in week 0.	00, 01, ..., 53
            %W	Week number of the year (Monday as the first day of the week).
                All days in a new year preceding the first Monday are considered to be in week 0.	00, 01, ..., 53
            %x	Locale’s appropriate date representation.	09/30/13
            %X	Locale’s appropriate time representation.	07:06:05
            %%	A literal '%' character.	%

    Returns: string of current date

    """
    return timezone.localtime(timezone.now()).date().strftime(date_format)


def get_human_datetime(datetime_format='%d.%m.%Y, %H:%M:%S', time_ending=''):
    """

    Args:
        datetime_format: build up from:
            %a	Abbreviated weekday name.	Sun, Mon, ...
            %A	Full weekday name.	Sunday, Monday, ...
            %w	Weekday as a decimal number.	0, 1, ..., 6
            %d	Day of the month as a zero-padded decimal.	01, 02, ..., 31
            %-d	Day of the month as a decimal number.	1, 2, ..., 30
            %b	Abbreviated month name.	Jan, Feb, ..., Dec
            %B	Full month name.	January, February, ...
            %m	Month as a zero-padded decimal number.	01, 02, ..., 12
            %-m	Month as a decimal number.	1, 2, ..., 12
            %y	Year without century as a zero-padded decimal number.	00, 01, ..., 99
            %-y	Year without century as a decimal number.	0, 1, ..., 99
            %Y	Year with century as a decimal number.	2013, 2019 etc.
            %H	Hour (24-hour clock) as a zero-padded decimal number.	00, 01, ..., 23
            %-H	Hour (24-hour clock) as a decimal number.	0, 1, ..., 23
            %I	Hour (12-hour clock) as a zero-padded decimal number.	01, 02, ..., 12
            %-I	Hour (12-hour clock) as a decimal number.	1, 2, ... 12
            %p	Locale’s AM or PM.	AM, PM
            %M	Minute as a zero-padded decimal number.	00, 01, ..., 59
            %-M	Minute as a decimal number.	0, 1, ..., 59
            %S	Second as a zero-padded decimal number.	00, 01, ..., 59
            %-S	Second as a decimal number.	0, 1, ..., 59
            %f	Microsecond as a decimal number, zero-padded on the left.	000000 - 999999
            %z	UTC offset in the form +HHMM or -HHMM.
            %Z	Time zone name.
            %j	Day of the year as a zero-padded decimal number.	001, 002, ..., 366
            %-j	Day of the year as a decimal number.	1, 2, ..., 366
            %U	Week number of the year (Sunday as the first day of the week).
                All days in a new year preceding the first Sunday are considered to be in week 0.	00, 01, ..., 53
            %W	Week number of the year (Monday as the first day of the week).
                All days in a new year preceding the first Monday are considered to be in week 0.	00, 01, ..., 53
            %c	Locale’s appropriate date and time representation.	Mon Sep 30 07:06:05 2013
            %x	Locale’s appropriate date representation.	09/30/13
            %X	Locale’s appropriate time representation.	07:06:05
            %%	A literal '%' character.	%
        time_ending: Defaults to german: " Uhr"  <- note the space before Uhr

    Returns: string of current date and time by default like: 01.06.2021, 10:15:27 Uhr

    """
    return timezone.localtime(timezone.now()).strftime(datetime_format) + time_ending


def resize_cv2_or_pil_image(image, max_width_or_height, use_pil=False, interpolation=None, invert=False):
    """
    Resize an OpenCV or PIL image based on the aspect ratio

    Args:
        image: image to resize
        max_width_or_height: integer, maximum dimension (width or height) in pixels
        use_pil: True to use PIL image as input
        interpolation: choose an interpolation method, default will be nearest
        invert: If invert is true, the max_width_or_height relates to the min_width_or_height

    Returns: resized image

    """
    h_equals_w = False
    if use_pil:
        w = image.size[0]
        h = image.size[1]
        if interpolation is None:
            interpolation = Image.NEAREST
    else:
        try:
            h, w, c = image.shape
        except ValueError:
            h, w = image.shape

        if interpolation is None:
            interpolation = cv2.INTER_NEAREST

    if h == w:
        h_equals_w = True

    height_larger_width = True
    if h > w:
        percentage = max_width_or_height / w if invert else max_width_or_height / h
    else:
        percentage = max_width_or_height / h if invert else max_width_or_height / w
        height_larger_width = False

    if height_larger_width:
        h = int(round(h * percentage, 1)) if invert else max_width_or_height
        w = max_width_or_height if invert else int(round(w * percentage, 1))
    else:
        h = max_width_or_height if invert else int(round(h * percentage, 1))
        w = int(round(w * percentage, 1)) if invert else max_width_or_height

    if invert:
        assert w >= max_width_or_height
        assert h >= max_width_or_height
    else:
        assert w <= max_width_or_height
        assert h <= max_width_or_height

    if h_equals_w:
        assert h == w

    if use_pil:
        image = image.resize((w, h), interpolation)
    else:
        image = cv2.resize(image, (w, h), interpolation=interpolation)
    return image


def get_gpu_memory():
    """
    Returns: list of values for each gpu in MiB
    """
    def _output_to_list(x):
        return x.decode('ascii').split('\n')[:-1]

    used = "nvidia-smi --query-gpu=memory.used --format=csv"
    total = "nvidia-smi --query-gpu=memory.total --format=csv"
    memory_used_info = _output_to_list(sp.check_output(used.split()))[1:]
    memory_total_info = _output_to_list(sp.check_output(total.split()))[1:]
    memory_used_values = [int(x.split()[0]) for i, x in enumerate(memory_used_info)]
    memory_total_values = [int(x.split()[0]) for i, x in enumerate(memory_total_info)]
    return memory_used_values, memory_total_values


def get_cpu_memory(in_gb=False):
    try:
        # hpc TU Dresden
        slurm_job_uid = int(os.environ['SLURM_JOB_UID'])
        slurm_job_id = int(os.environ['SLURM_JOB_ID'])
        ram_file_path = os.path.join(
            'sys', 'fs', 'cgroup', 'memory', 'slurm',
            f'uid_{slurm_job_uid}', f'job_{slurm_job_id}',
            'memory.limit_in_bytes'
        )
        ram_file_path = '/' + ram_file_path
        with open(ram_file_path) as f:
            lines = f.readlines()
        ram_used = 300  # offset
        ram_total = int(lines[0].replace('\n', ''))
    except KeyError:
        ram_used = psutil.virtual_memory().used
        ram_total = psutil.virtual_memory().total

    # **30 is GiB, **20 MiB; technically this returns MB and GB if you use 1024 as base
    if in_gb:
        ram_used = ram_used / (2 ** 30)
        ram_total = ram_total / (2 ** 30)
    else:
        ram_used = ram_used / (2 ** 20)
        ram_total = ram_total / (2 ** 20)
    return ram_used, ram_total


def show_gpu_memory(device=None, num_gpus=None):
    if device is not None:
        if not isinstance(device, torch.device):
            device = torch.device(device)

    if device is None or device.index is None:
        # device.index is none if cuda was passed -> cuda only indicates multi gpu in this project.
        used, total = get_gpu_memory()
        gpu_s = len(used)
        combined = zip(used, total)
        text = []
        end = num_gpus if num_gpus is not None else len(used)
        for i, (used, total) in enumerate(combined):
            if i == end:
                break
            text.append(f'GPU {str(i).rjust(len(str(gpu_s)))}: {used:.0f}/{total:.0f} MiB')
    else:
        used, total = get_gpu_memory()

        try:
            device_nr = device.index
            if device.index > len(used) - 1:
                logging.info(f'GPU {device_nr} does not exist! Your system has {len(used)} GPUs. Defaulting to GPU 0.')
            used = used[device_nr]
            total = total[device_nr]
        except IndexError:
            device_nr = 0
            used = used[0]
            total = total[0]

        text = [f'GPU {device_nr}: {used:.0f}/{total:.0f} MiB']

    logging.info(f'VRAM used: {"; ".join(text)}')


def show_cpu_memory():
    ram_used, ram_total = get_cpu_memory()
    logging.info(f'Ram usage system: {ram_used:.2f}/{ram_total:.2f} MiB')


def show_memory(device, num_gpus=None):
    if device == 'cpu' or device == torch.device('cpu'):
        show_cpu_memory()
    else:
        show_gpu_memory(device, num_gpus)


def get_memory(device):
    """in MiB"""
    if device == 'cpu' or device == torch.device('cpu'):
        used, total = get_cpu_memory()
    else:
        used, total = get_gpu_memory()
        try:
            device = torch.device(device)
            try:
                device_nr = int(device.type.split(':')[1])
            except IndexError:
                device_nr = 0

            if device_nr > len(used) - 1:
                logging.info(f'GPU {device_nr} does not exist! Your system has {len(used)} GPUs. Defaulting to GPU 0.')
            used = used[device_nr]
            total = total[device_nr]
        except IndexError:
            used = used[0]
            total = total[0]

    unit = 'MiB'
    return used, total, unit


def get_possible_process_count(logical, logging_active=True):
    """
    For hpc with slurm: psutil would get the cpu count of the whole node, not only those cores assigned to this job.
    """
    process_count = psutil.cpu_count(logical)
    try:
        job_assigned_cores = int(os.environ['SLURM_CPUS_PER_TASK'])
        if logical:
            if logging_active:
                logging.info(f'You requested multi-threading! SLURM: HPC uses {job_assigned_cores}. '
                             f'Ensure, that your node supports multi-threading')
            process_count = job_assigned_cores * 2
        else:
            process_count = job_assigned_cores
    except KeyError:
        pass
    return process_count


def get_max_threads(logical=False, max_ram_in_gb=None, logging_active=True):
    """
    Project specific maximum number of processes! threads was before.. Todo: rename
    one thread will use around constants.MULTICORE_RAM_RESERVATION GB of RAM (1.5GB)
    spawning too many will fail obviously --> This function prevents that
    """
    ram_used, ram_total = get_cpu_memory(in_gb=True)
    available = ram_total - ram_used
    if sys.platform == 'win32' and available > 128:
        # windows..
        available = 128

    if max_ram_in_gb is not None:
        threads = int(available / max_ram_in_gb)
    else:
        threads = int(available / constants.MULTICORE_RAM_RESERVATION)

    if threads == 0:
        # Do not use multiprocessing! Stay in same process at your function!
        threads = 1

    # logical are physical threads, otherwise hyper threading will be used
    max_thread_count = get_possible_process_count(logical, logging_active)

    if sys.platform == 'win32' and max_thread_count > 60:
        # python bug: max allowed processes in multithreading is 61!: https://bugs.python.org/issue26903
        # In short: multiprocessing is using a thread per process to track the processes, which is quite wasteful on
        # both unix and windows where nonblocking primitives exist, but nevertheless, thats what it is doing. I
        # need to limit this to 60 processes to avoid the limit in question as a result.
        max_thread_count = 60

    if threads > max_thread_count:
        threads = max_thread_count

    return threads


def ceil(number, decimals):
    """
    rounds up to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1030

    Returns: rounded up number

    """

    return math.ceil((10.0 ** decimals) * number) / (10.0 ** decimals)


def floor(number, decimals):
    """
    rounds down to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1020

    Returns: rounded up number

    """

    return math.floor((10.0 ** decimals) * number) / (10.0 ** decimals)


def round_to_base_number(x, base=5):
    """Example: round_to_number(12, 5) = 10; round_to_number(13, 5) = 15"""
    return base * round(x/base)


def timezone_datetime(datetime_object):
    """
    Makes a datetime object timezone aware
    """
    if not isinstance(datetime_object, datetime.datetime):
        raise ValueError('You can only pass a datetime object.')
    year = datetime_object.year
    month = datetime_object.month
    day = datetime_object.day
    hour = datetime_object.hour
    minute = datetime_object.minute
    second = datetime_object.second

    time_object = timezone.make_aware(
        datetime.datetime(year, month, day, hour, minute, second),
        timezone.get_current_timezone()
    )
    return time_object


def set_up_reproducibility(python_state=None,
                           numpy_state=None,
                           torch_cpu_state=None,
                           torch_cuda_state=None,
                           device=None,
                           seed_multiplier=1,
                           ):
    """
    If you continue a training, all parameters must be set (except if you train on the cpu which... no dont do it)

    Args:

        python_state: previous python state if you want to continue a training
        numpy_state: previous numpy state if you want to continue a training
        torch_cpu_state: previous torch.get_rng_state() if you want to continue a training
        torch_cuda_state: previous torch.cuda.get_rng_state() if you want to continue a training
        device: cuda on which the cuda state shall be applied
        seed_multiplier: useful during DDP init - ensures different results for each process using augmentation and
            other random, numpy random and torch random calls.

    Returns:

    """
    logging.info(
        '\n-------------------\n'
        'Attention: Some cuDNN functions implemented are non deterministic anyways.\n'
        'The end result of two runs will be different but close to each other.\n'
        'EXAMPLE Error at cross_entropy_loss:\n'
        "RuntimeError: SpatialClassNLLCriterion_updateOutput does not have a deterministic implementation,\n"
        "but you set 'torch.use_deterministic_algorithms(True)'\n"
        "The code uses deterministic functions if it is possible. If the error above is fixed, create an issue\n"
        '-------------------'
    )

    torch.manual_seed(constants.GLOBAL_SEED * seed_multiplier)
    if torch_cpu_state is not None:
        torch.set_rng_state(torch_cpu_state.to(torch.device('cpu')))
    if torch_cuda_state is not None:
        if device is None:
            raise ValueError('cuda device must be specified when using cuda rng state')
        torch.cuda.set_rng_state(torch_cuda_state.to('cpu'), device)

    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    np.random.seed(constants.GLOBAL_SEED * seed_multiplier)
    if numpy_state is not None:
        np.random.set_state(numpy_state)

    random.seed(constants.GLOBAL_SEED * seed_multiplier)
    if python_state is not None:
        random.setstate(python_state)


def create_sorted_ordered_dict(some_dict):
    """sorts by key"""
    temp_sorting = []
    for key, value in some_dict.items():
        temp_sorting.append((key, value))
    temp_sorting = sorted(temp_sorting)
    sorted_dict = OrderedDict({})
    for element in temp_sorting:
        sorted_dict[element[0]] = element[1]
    return sorted_dict


def create_file_field_upload_image_from_memory_image(image_in_memory):
    _, encoded_image = cv2.imencode('.png', image_in_memory)
    byte_image = encoded_image.tobytes()
    uploaded = SimpleUploadedFile('.png', byte_image, content_type='image/png')
    return uploaded


def validate_basename(basename):
    """
    Validates a file or folder name. Path can be absolute but only the basename will be sanitized

    Returns: valid file path
    """
    if not isinstance(basename, str):
        raise ValueError('folder_name must be a string')

    base_name = basename
    dir_name = ''
    if sys.platform == 'win32':
        if '\\' in basename:
            base_name = os.path.basename(basename)
            dir_name = os.path.dirname(basename)
    else:
        if '/' in basename:
            base_name = os.path.basename(basename)
            dir_name = os.path.dirname(basename)

    for forbidden_char in constants.FORBIDDEN_FILENAME_CHARACTERS:
        if forbidden_char in base_name:
            base_name = base_name.replace(forbidden_char, '_')

    if base_name in constants.RESERVED_FILE_NAMES:
        warnings.warn(f'Invalid folder name occurred: "{base_name}". New folder name: "{base_name}_"')
        base_name += '_'

    base_name = base_name.rstrip().lstrip()
    while base_name.endswith('.'):
        base_name = base_name.rstrip('.')
        base_name = base_name.rstrip()

    return os.path.join(dir_name, base_name)


def open_file(filename):
    """Opens a file or the explorer if it is a folder"""
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])


def log_error_info_and_kill_children():
    current_process = psutil.Process(os.getpid())
    children = current_process.children(recursive=True)
    child_running = False
    for child in children:
        if child.status() == 'running':
            child_running = True
            break
    if child_running:
        logging.info(f'Terminating background processes. This may take a few seconds.')
    for child in reversed(children):  # reverse to go backwards through children
        child.terminate()
    logging.info(
        'If you got an error like:\n'
        'A process in the process pool was terminated abruptly while the future was running or pending.\n'
        'Close some applications and try again. Check the terminal as well.\n'
        'Restart your system to ensure nothing blocks any ram (like a windows update - yes this happened).\n'
        'Create an issue if nothing helps at: https://gitlab.com/fra-wa/pytorch_segmentation/-/issues'
    )


def create_3d_iteration_map():
    """
    Combines those two lists to a map. Example:

    Examples:
        reversed_input_sizes: [256, 196, 128, 64]  # real choices located at constants
        reversed_input_depths: [512, 128, 64, 32, 20]  # real choices located at constants
        -->
        out_map = [
            # (in_size, depth),
            (512, 256),  # input size 512 was not given but will be inserted. volume should be as "cube-like" as possible
            (256, 128),
            (196, 128),
            (128, 128),
            (128, 64),
            (64, 64),
            (64, 32),
            (64, 20),
        ]

    """
    reversed_input_sizes = list(reversed(sorted(
        [size_choice[0] for size_choice in constants.IN_SIZE_CHOICES_3D]
    )))
    reversed_input_depths = list(reversed(sorted(
        [depth_choice[0] for depth_choice in constants.DEPTH_CHOICES_3D]
    )))

    max_shape = max((reversed_input_depths[0], reversed_input_sizes[0]))
    min_shape = min((reversed_input_depths[-1], reversed_input_sizes[-1]))

    if max_shape == reversed_input_sizes[0]:
        pass
    else:
        reversed_input_sizes.insert(0, max_shape)

    if min_shape == reversed_input_depths[-1]:
        pass
    else:
        reversed_input_depths.append(min_shape)

    in_size_in_depth_map = []
    for depth_size in reversed_input_depths:
        for i, input_size in enumerate(reversed_input_sizes):
            if input_size < depth_size:
                break
            if in_size_in_depth_map and input_size > in_size_in_depth_map[-1][0]:
                continue
            in_size_in_depth_map.append((input_size, depth_size))

    return in_size_in_depth_map
