import os

from django.conf import settings as django_settings
from django.db import models

from pytorch_segmentation.file_handling.utils import remove_dir


def image_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/orig_img{}'.format(instance.id, extension)


def mask_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/orig_mask{}'.format(instance.id, extension)


def in_image_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/in_img{}'.format(instance.id, extension)


def in_mask_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/in_mask{}'.format(instance.id, extension)


def out_image_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/out_img{}'.format(instance.id, extension)


def out_mask_path(instance, extension=None):
    """
    filename is just the extension.
    """
    if extension is None:
        extension = '.png'
    return 'online_aug/example_{}/out_mask{}'.format(instance.id, extension)


class OnlineImageAugExamples(models.Model):
    image = models.ImageField(
        verbose_name='Original image',
        upload_to=image_path,
        help_text='Initial image with sub region square.',
        blank=True,
        null=True,
    )
    mask = models.ImageField(
        verbose_name='Original mask',
        upload_to=mask_path,
        help_text='Initial image with sub region square.',
        blank=True,
        null=True,
    )
    sub_image = models.ImageField(
        verbose_name='Sub image',
        upload_to=in_image_path,
        blank=True,
        null=True,
    )
    sub_mask = models.ImageField(
        verbose_name='Sub mask',
        upload_to=in_mask_path,
        blank=True,
        null=True,
    )
    out_image = models.ImageField(
        verbose_name='Augmented image',
        upload_to=out_image_path,
        blank=True,
        null=True,
    )
    out_mask = models.ImageField(
        verbose_name='Augmented mask',
        upload_to=out_mask_path,
        blank=True,
        null=True,
    )

    def delete(self, using=None, keep_parents=False):
        # keep our media folder clean --> delete also folders, not only files
        folder_path = os.path.join(django_settings.MEDIA_ROOT, 'online_aug', f'example_{self.id}')
        super(OnlineImageAugExamples, self).delete(using=using, keep_parents=keep_parents)
        remove_dir(folder_path)
