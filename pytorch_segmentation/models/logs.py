from django.db import models

from .model_analyses import ModelAnalysis
from ..constants import LOG_EXECUTION_CHOICES


class LogFile(models.Model):
    log_file_path = models.CharField(
        verbose_name='Log filepath',
        max_length=500,
    )
    process_id = models.PositiveIntegerField(
        verbose_name='Process ID',
    )
    process_start_datetime = models.DateTimeField(
        verbose_name='Process Start Time',
    )
    execution_process = models.CharField(
        max_length=50,
        choices=LOG_EXECUTION_CHOICES,
    )
    log_text = models.TextField(
        verbose_name='Log',
        blank=True,
    )
    device = models.CharField(
        verbose_name='Main device of this log',
        max_length=12,
    )

    num_gpus = models.PositiveIntegerField(
        verbose_name='Parallel GPUs',
        default=None,
        help_text='Number of gpus used during training.',
        blank=True,
        null=True,
    )

    model_analysis = models.ForeignKey(
        ModelAnalysis,
        verbose_name='Related analysis',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    is_running = models.BooleanField(
        verbose_name='Is running',
        default=True,
    )
