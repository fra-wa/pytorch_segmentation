from django.db import models


class TrainingDataset(models.Model):
    name = models.CharField(
        verbose_name='Dataset Name',
        max_length=100,
        unique=True,
    )
    folder_name_on_device = models.CharField(
        verbose_name='Folder name on device',
        max_length=100,
        blank=True,
        null=True,
        unique=True,
    )
    number_training_images = models.PositiveIntegerField(
        verbose_name='Number of training images/volumes',
    )
    number_validation_images = models.PositiveIntegerField(
        verbose_name='Number of validation images/volumes',
    )
