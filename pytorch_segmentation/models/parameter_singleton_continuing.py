import torch

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from pytorch_segmentation import constants

"""
This is created once to store the users training continuation input over time.
"""


class TrainingContinuationParameters(models.Model):
    model_path = models.CharField(
        verbose_name='Model path',
        max_length=256,
        default='',
        blank=True,
        help_text='Path to the checkpoint.',
    )
    dataset = models.CharField(
        verbose_name='Dataset name',
        max_length=256,
        default='',
        blank=True,
    )
    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
        help_text='Leave empty if you want to train with the used batch size of the checkpoint. '
                  'If you are on a different machine, you might need/want to adjust the batch size here.',
        blank=True,
        null=True,
    )
    epochs = models.PositiveIntegerField(
        verbose_name='Further epochs',
        validators=[MinValueValidator(1)],
        default=50,
        help_text='Number of epochs to train further.',
    )
    save_every = models.PositiveIntegerField(
        verbose_name='Save every x checkpoint',
        validators=[MinValueValidator(0)],
        default=0,
        help_text='Save a checkpoint every x epochs. Best checkpoints regarding validation loss and accuracy are '
                  'always saved. Set to 0 to store only the last and the best 2 checkpoints.',
    )
    reproduce = models.BooleanField(
        verbose_name='Reproducible',
        default=False,
        help_text=f'Slower! Recommended for research.',
    )

    device_choices = [('cpu', 'CPU')]
    default_device = 'cpu'
    if torch.cuda.is_available():
        device_count = torch.cuda.device_count()
        if device_count == 1:
            device_choices.append(('cuda:0', 'GPU'))
            default_device = 'cuda:0'
        else:
            device_choices.append(('cuda', 'MultiGPU'))
            for i in range(device_count):
                device_choices.append((f'cuda:{i}', f'GPU:{i}'))
            default_device = 'cuda'

    device = models.CharField(
        verbose_name='Device',
        choices=device_choices,
        default=default_device,
        max_length=7,
        help_text='Device to process on.',
    )
    num_gpus = models.PositiveIntegerField(
        verbose_name='Parallel GPUs',
        default=None,
        validators=[MinValueValidator(2)],
        help_text='Set device to MultiGPU to enable. Number of gpus used during training. Must be >=2.',
        blank=True,
        null=True,
    )

