import torch

from decimal import Decimal

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.safestring import mark_safe

from pytorch_segmentation import constants

"""
This is created once to store the users training input over time.
"""


class TrainingParameters(models.Model):
    architecture = models.CharField(
        verbose_name='Architecture',
        max_length=128,
        choices=constants.ARCHITECTURE_CHOICES,
        default=constants.UNet_NAME,
        help_text='Choose a neural network.'
    )
    backbone = models.CharField(
        verbose_name='Backbone',
        max_length=128,
        blank=True,
        choices=constants.BACKBONE_MODEL_CHOICES,  # dynamically selected at frontend
        help_text='Choose a backbone if supported.'
    )
    pretrained = models.BooleanField(
        verbose_name='Pretrained',
        default=True,
        help_text='Only for rgb and if batch normalization is used. Otherwise training from scratch.'
    )
    dataset = models.CharField(
        verbose_name='Dataset name',
        max_length=256,
        default='',
        blank=True,
    )
    norm_on_dataset = models.BooleanField(
        verbose_name='Normalize on dataset',
        default=True,
        help_text='If True, the mean and std of the dataset will be used for normalization (recommended).'
    )
    batch_size = models.PositiveIntegerField(
        verbose_name='Batch size',
        default=16,
        help_text='Images/Volumes per forward pass during training.',
    )
    channels = models.PositiveSmallIntegerField(
        verbose_name='Input channels',
        default=1,
        choices=[(1, 1), (3, 3)],
        help_text='1 for gray, 3 for rgb.'
    )
    classes = models.PositiveIntegerField(
        verbose_name='Output classes',
        default=2,
        validators=[MinValueValidator(2)],
        help_text='Classes to be segmented. Binary classification: 2, multiclass: > 2.',
    )
    epochs = models.PositiveIntegerField(
        verbose_name='Total epochs',
        validators=[MinValueValidator(1)],
        default=100,
        help_text='Number of maximal epochs to run.',
    )
    save_every = models.PositiveIntegerField(
        verbose_name='Save every',
        validators=[MinValueValidator(0)],
        default=0,
        help_text='Save a checkpoint every x epochs. Best checkpoints regarding validation loss and accuracy are '
                  'always saved. Set to 0 to store only the last and the best 2 checkpoints.',
    )
    input_size = models.PositiveIntegerField(
        verbose_name='Input size',
        validators=[MinValueValidator(32)],
        default=256,
        help_text='height and width (h = w) in pixels/voxels',
    )
    input_depth = models.PositiveIntegerField(
        verbose_name='Input depth',
        default=0,
        help_text='For 3D networks: depth of the input block. Dimension is: (input_size x '
                  'input_size x input_depth) = (h, w, d)',
    )
    reproduce = models.BooleanField(
        verbose_name='Reproducible',
        default=False,
        help_text=f'Slower! Recommended for research.',
    )
    normalization = models.CharField(
        verbose_name='Normalization Layer',
        choices=constants.NORMALIZATION_CHOICES,
        default=constants.GROUP_NORM,
        max_length=12,
        help_text=mark_safe('Recommended: Group Normalization ('
                            '<a class="form-a" href="https://arxiv.org/abs/1803.08494">paper</a>).'),
    )
    learning_rate = models.DecimalField(
        verbose_name='Learning rate',
        decimal_places=4,
        max_digits=6,
        default=Decimal('0.001'),
        validators=[MinValueValidator(Decimal('0.000001'))],
        help_text='Initial learning rate. Will be adapted by Adam optimizer.',
    )

    device_choices = [('cpu', 'CPU')]
    default_device = 'cpu'
    device_help_text = 'No supported graphics card found.'
    if torch.cuda.is_available():
        device_count = torch.cuda.device_count()
        if device_count == 1:
            device_choices.append(('cuda:0', 'GPU'))
            default_device = 'cuda:0'
        else:
            for i in range(device_count):
                device_choices.append((f'cuda:{i}', f'GPU:{i}'))
            default_device = 'cuda'

        device_choices.append(('cuda', 'MultiGPU'))  # is set to disabled (frontend) if only 1 gpu is available
        device_help_text = 'Device to process on (GPU recommended).'

    device = models.CharField(
        verbose_name='Device',
        choices=device_choices,
        default=default_device,
        max_length=7,
        help_text=device_help_text,
    )
    online_aug = models.BooleanField(
        verbose_name='Use online augmentation',
        default=False,
        help_text='Training data will be augmented during the training.',
    )

    auto_weight = models.BooleanField(
        verbose_name='Auto weight classes',
        default=True,
        help_text='Recommended. Weights each class with respect to the occurrence in the dataset.',
    )

    num_gpus = models.PositiveIntegerField(
        verbose_name='Parallel GPUs',
        default=None,
        validators=[MinValueValidator(2)],
        help_text='Set device to MultiGPU to enable. Number of gpus used during training. Must be >=2.',
        blank=True,
        null=True,
    )
