import os

from pathlib import Path


def create_folder(folder):
    if not os.path.isdir(folder):
        os.makedirs(folder)


MULTICORE_RAM_RESERVATION = 5.5  # GB; minimum ram reservation to load all dependencies and process data
TESTING_RAM_RESERVATION = 6  # GB; minimum ram reservation to load all dependencies during testing
# the ram reservation is needed to determine the number of processes being spawned.
# 4 is too large though. However, better safe than sorry ;)
SUPPORTED_MASK_TYPES = ['.png', '.bmp', '.tif', '.tiff']
SUPPORTED_INPUT_IMAGE_TYPES = ['.jpg', '.png', '.bmp', '.tif', '.tiff']

PYTORCH_SEGMENTATION_APP_PATH = os.path.dirname(__file__)
PYTORCH_SEGMENTATION_PATH = Path(PYTORCH_SEGMENTATION_APP_PATH).parent
projects_directory = Path(PYTORCH_SEGMENTATION_APP_PATH).parent.parent

# Folder Structures
DL_MODELS = os.path.join(projects_directory, 'model_zoo')
TRAINED_MODELS_FOLDER = os.path.join(DL_MODELS, 'segmentation')

LOG_DIR = os.path.join(PYTORCH_SEGMENTATION_PATH, 'logs')
EXPORT_DIR = os.path.join(PYTORCH_SEGMENTATION_PATH, 'exports')

create_folder(DL_MODELS)
create_folder(TRAINED_MODELS_FOLDER)
create_folder(LOG_DIR)

STATIC_IMAGES_DIR = os.path.join(PYTORCH_SEGMENTATION_APP_PATH, 'static', 'pytorch_segmentation', 'images')
TEST_FILES_DIR = os.path.join(PYTORCH_SEGMENTATION_APP_PATH, 'tests', 'files')

# Data Loading
DATASET_FOLDER = os.path.join(projects_directory, 'segmentation_datasets')
create_folder(DATASET_FOLDER)

# Model Names 2D. EXACTLY like the class.__name__!!
AttentionUNet_NAME = 'AttentionUNet'
BiSeNet_NAME = 'BiSeNet'
BiSeNetV2_NAME = 'BiSeNetV2'
DANet_NAME = 'DANet'
Dran_NAME = 'Dran'
DeepLabV3Plus_NAME = 'DeepLabV3Plus'
DenseASPP121_NAME = 'DenseASPP121'
DenseASPP161_NAME = 'DenseASPP161'
DenseASPP169_NAME = 'DenseASPP169'
DenseASPP201_NAME = 'DenseASPP201'
DenseNet57_NAME = 'DenseNet57'
DenseNet67_NAME = 'DenseNet67'
DenseNet103_NAME = 'DenseNet103'
DFANet_NAME = 'DFANet'
DFN_NAME = 'DFN'
DeepLabDUCHDC_NAME = 'DeepLabDUCHDC'
ENet_NAME = 'ENet'
ERFNet_NAME = 'ERFNet'
ESPNet_NAME = 'ESPNet'
ExtremeC3Net_NAME = 'ExtremeC3Net'
FastSCNN_NAME = 'FastSCNN'
FCN8_NAME = 'FCN8'
FCN16_NAME = 'FCN16'
FCN32_NAME = 'FCN32'
GCN_NAME = 'GCN'
GSCNN_NAME = 'GSCNN'
HRNet_NAME = 'HRNet'
ICNet_NAME = 'ICNet'
LadderNet_NAME = 'LadderNet'
LEDNet_NAME = 'LEDNet'
OCNet_NAME = 'OCNet'
PSANet_NAME = 'PSANet'
PSPNet_NAME = 'PSPNet'
PSPDenseNet_NAME = 'PSPDenseNet'
R2AttentionUNet_NAME = 'R2AttentionUNet'
R2UNet_NAME = 'R2UNet'
SegNet_NAME = 'SegNet'
SegResNet_NAME = 'SegResNet'
SINet_NAME = 'SINet'
UNet_NAME = 'UNet'
UNetResNet_NAME = 'UNetResNet'
UNet2Plus_NAME = 'UNet2Plus'
UNet3Plus_NAME = 'UNet3Plus'
UNet3PlusBackboned_NAME = 'UNet3PlusBackboned'
UPerNet_NAME = 'UPerNet'

# Model Names 3D. EXACTLY like the class.__name__!!
DenseVoxelNet_NAME = 'DenseVoxelNet'
HighResNet3D_NAME = 'HighResNet3D'
ResNetMed3D_NAME = 'ResNetMed3D'
SFUNet_NAME = 'SFUNet'
SkipDenseNet3D_NAME = 'SkipDenseNet3D'
ResidualUNet3D_NAME = 'ResidualUNet3D'
UNet3D_NAME = 'UNet3D'
UNet3Plus3D_NAME = 'UNet3Plus3D'
VNet_NAME = 'VNet'
VNetLight_NAME = 'VNetLight'


STATEFUL_RECURRENT_UNETS = [
    SFUNet_NAME,
]
STATELESS_RECURRENT_UNETS = []  # only created one was dropped.

THREE_D_NETWORKS = [
    DenseVoxelNet_NAME,
    HighResNet3D_NAME,
    ResNetMed3D_NAME,
    SkipDenseNet3D_NAME,
    ResidualUNet3D_NAME,
    UNet3D_NAME,
    UNet3Plus3D_NAME,
    VNet_NAME,
    VNetLight_NAME,
]

TWO_D_NETWORKS = [
    AttentionUNet_NAME,
    BiSeNet_NAME,
    BiSeNetV2_NAME,
    DANet_NAME,
    Dran_NAME,
    DeepLabV3Plus_NAME,
    DenseASPP121_NAME,
    DenseASPP161_NAME,
    DenseASPP169_NAME,
    DenseASPP201_NAME,
    DenseNet57_NAME,
    DenseNet67_NAME,
    DenseNet103_NAME,
    DFANet_NAME,
    DFN_NAME,
    DeepLabDUCHDC_NAME,
    ENet_NAME,
    ERFNet_NAME,
    ESPNet_NAME,
    ExtremeC3Net_NAME,
    FastSCNN_NAME,
    FCN8_NAME,
    FCN16_NAME,
    FCN32_NAME,
    GCN_NAME,
    GSCNN_NAME,
    HRNet_NAME,
    ICNet_NAME,
    LadderNet_NAME,
    LEDNet_NAME,
    OCNet_NAME,
    PSANet_NAME,
    PSPNet_NAME,
    PSPDenseNet_NAME,
    R2AttentionUNet_NAME,
    R2UNet_NAME,
    SegNet_NAME,
    SegResNet_NAME,
    SINet_NAME,
    UNet_NAME,
    UNetResNet_NAME,
    UNet2Plus_NAME,
    UNet3Plus_NAME,
    UNet3PlusBackboned_NAME,
    UPerNet_NAME,
]

ARCHITECTURES_AND_BACKBONES = {
    # 2D, 3D follows below
    AttentionUNet_NAME: [''],
    BiSeNet_NAME: [''],
    BiSeNetV2_NAME: [''],
    DANet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    Dran_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    DeepLabV3Plus_NAME: ['drn', 'xception', 'resnet101'],
    DenseASPP121_NAME: [''],
    DenseASPP161_NAME: [''],
    DenseASPP169_NAME: [''],
    DenseASPP201_NAME: [''],
    DenseNet57_NAME: [''],
    DenseNet67_NAME: [''],
    DenseNet103_NAME: [''],
    DFANet_NAME: [''],
    DFN_NAME: [''],
    DeepLabDUCHDC_NAME: [''],
    ENet_NAME: [''],
    ERFNet_NAME: [''],
    ESPNet_NAME: [''],
    ExtremeC3Net_NAME: [''],
    FastSCNN_NAME: [''],
    FCN8_NAME: [''],
    FCN16_NAME: [''],
    FCN32_NAME: [''],
    GCN_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    GSCNN_NAME: [''],
    HRNet_NAME: [''],
    ICNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    LadderNet_NAME: [''],
    LEDNet_NAME: [''],
    OCNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSANet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSPNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    PSPDenseNet_NAME: ['densenet121', 'densenet161', 'densenet169', 'densenet201'],
    R2AttentionUNet_NAME: [''],
    R2UNet_NAME: [''],
    SegNet_NAME: [''],
    SegResNet_NAME: ['resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    SINet_NAME: [''],
    UNet_NAME: [''],
    UNetResNet_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    UNet2Plus_NAME: [''],
    UNet3Plus_NAME: [''],
    UNet3PlusBackboned_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    UPerNet_NAME: ['resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    # 3D
    DenseVoxelNet_NAME: [''],
    HighResNet3D_NAME: [''],
    ResNetMed3D_NAME: ['resnet10', 'resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnet200'],
    # SFUNet_NAME: ['', 'resnet18', 'resnet34', 'resnet50', 'resnet101', 'resnet152', 'resnext50', 'resnext101'],
    SkipDenseNet3D_NAME: [''],
    ResidualUNet3D_NAME: [''],
    UNet3D_NAME: [''],
    # UNet3Plus3D_NAME: [''],
    VNet_NAME: [''],
    VNetLight_NAME: [''],
}

BACKBONE_CHOICES_DICT = {}
__backbones = []
EMPTY_CHOICE = ('', '-------------')

for arch, backbones_list in ARCHITECTURES_AND_BACKBONES.items():
    __choices = []
    for backbone in backbones_list:
        if backbone:
            __choices.append((backbone, backbone))
        else:
            __choices.append(EMPTY_CHOICE)
    BACKBONE_CHOICES_DICT[arch] = [backbone for backbone in backbones_list]
    __backbones += backbones_list

__backbones = sorted(list(set(__backbones)))

BACKBONE_MODEL_CHOICES = [(backbone, backbone) for backbone in __backbones if backbone]
# no input has to be allowed
BACKBONE_MODEL_CHOICES.insert(0, EMPTY_CHOICE)

SUPPORTED_ARCHITECTURES = ARCHITECTURES_AND_BACKBONES.keys()

ARCHITECTURE_CHOICES = []
__arch_list = sorted(TWO_D_NETWORKS + THREE_D_NETWORKS)
for arch in __arch_list:
    if arch in SUPPORTED_ARCHITECTURES:
        if arch in THREE_D_NETWORKS:
            ARCHITECTURE_CHOICES.append((arch, f'{arch} (3D)'))
        else:
            ARCHITECTURE_CHOICES.append((arch, f'{arch} (2D)'))


DEFAULT_RGB_MEAN = [0.485, 0.456, 0.406]
DEFAULT_RGB_STD = [0.229, 0.224, 0.225]

DEFAULT_GRAY_MEAN = [0.5]
DEFAULT_GRAY_STD = [0.5]

# GLOBAL_SEED used if reproduce is true. Needed to get different augmentations for each worker but also
# keep reproducibility.
GLOBAL_SEED = 100


DL_EXECUTION_TRAIN = 'TR'
DL_EXECUTION_INFERENCE = 'INF'
DL_EXECUTION_AUG_IMG = 'AUG_IMG'
DL_EXECUTION_AUG_VOL = 'AUG_VOL'
DL_EXECUTION_IMPORT = 'IMPORT'
DL_EXECUTION_TEST = 'TEST'
DL_EXECUTION_TEST_INF = 'TEST_INF'
DL_EXECUTION_OTHER = 'OTHER'
DL_EXECUTION_PARAMETER_CALC = 'PARA_CALC'

DL_EXECUTION_TRAIN_NAME = 'Model Training'
DL_EXECUTION_INFERENCE_NAME = 'Inference'
DL_EXECUTION_AUG_IMG_NAME = 'Augmentation: Image'
DL_EXECUTION_AUG_VOL_NAME = 'Augmentation: Volume'
DL_EXECUTION_IMPORT_NAME = 'Model Import'
DL_EXECUTION_TEST_NAME = 'Test Run'
DL_EXECUTION_TEST_INF_NAME = 'Inference Timing'
DL_EXECUTION_OTHER_NAME = 'Other execution'
DL_EXECUTION_PARAMETER_CALC_NAME = 'Parameter calculation'

LOG_EXECUTION_CHOICES = (
    (DL_EXECUTION_TRAIN, DL_EXECUTION_TRAIN_NAME),
    (DL_EXECUTION_INFERENCE, DL_EXECUTION_INFERENCE_NAME),
    (DL_EXECUTION_AUG_IMG, DL_EXECUTION_AUG_IMG_NAME),
    (DL_EXECUTION_AUG_VOL, DL_EXECUTION_AUG_VOL_NAME),
    (DL_EXECUTION_IMPORT, DL_EXECUTION_IMPORT_NAME),
    (DL_EXECUTION_TEST, DL_EXECUTION_TEST_NAME),
    (DL_EXECUTION_TEST_INF, DL_EXECUTION_TEST_INF_NAME),
    (DL_EXECUTION_OTHER, DL_EXECUTION_OTHER_NAME),
    (DL_EXECUTION_PARAMETER_CALC, DL_EXECUTION_PARAMETER_CALC_NAME),
)

BATCH_NORM = 'bn'
GROUP_NORM = 'gn'

NORMALIZATION_CHOICES = [
    (BATCH_NORM, 'Batch normalization'),
    (GROUP_NORM, 'Group normalization'),
]

NORMALIZATION_DICT = {}
for choice, human_name in NORMALIZATION_CHOICES:
    NORMALIZATION_DICT[choice] = human_name

MAX_NUM_WORKERS = 10  # for online augmentation. why limit? see example:
# batch size: 32, h = w = 512, channels = 3 --> MB per batch = (512*512*3 Bytes)/1024/1024 = 24MB/Batch
# SSD read spead (approx) = 400MB / sec, nvme 4000MB/sec 10 workers: 240MB total --> less than 1 sec for 10 batches
# preload ok. Further: network will never process 10 batches per second. more likely: 1 sec per batch.
# Further: preprocessing of one batch takes around 1-5 seconds?! --> every 0.5 seconds, a batch can be loaded --> still
# fast enough

AUGMENTATION_POLICIES = [
    ('random', 'Random'),
    ('geo_first', 'Geometric first'),
    ('pix_first', 'Pixel first'),
    ('geo_only', 'Geometric only'),
    ('pix_only', 'Pixel only'),
]

AUGMENTATION_POLICIES_DICT = {}
for choice, human_name in AUGMENTATION_POLICIES:
    AUGMENTATION_POLICIES_DICT[choice] = human_name

# list of forbidden characters - used to prevent file creation containing those chars
FORBIDDEN_FILENAME_CHARACTERS = [
    # chars
    '/',
    '\\',
    '>',
    '<',
    ':',
    '"',
    '|',
    '?',
    '*',
]
RESERVED_FILE_NAMES = [
    # reserved file names
    'CON',
    'PRN',
    'AUX',
    'NUL',
    'COM1',
    'COM2',
    'COM3',
    'COM4',
    'COM5',
    'COM6',
    'COM7',
    'COM8',
    'COM9',
    'LPT1',
    'LPT2',
    'LPT3',
    'LPT4',
    'LPT5',
    'LPT6',
    'LPT7',
    'LPT8',
    'LPT9',
]

INFERENCE_WEIGHTING_STRATEGY_CHOICES = [
    ('gauss', 'Gaussian'),
    ('linear', 'Linear'),
    ('sum', 'Sum'),
]

MiB_VRAM_OFFSET = 800  # Real training differs a bit from calculation -- why?!

IN_SIZE_CHOICES_2D = [
    (64, 64),
    (128, 128),
    (192, 192),
    (256, 256),
    (320, 320),
    (384, 384),
    (448, 448),
    # important: the dataset images might be smaller --> needs to be checked before
    (512, 512),
    (640, 640),
    (768, 768),
    (896, 896),
    (1024, 1024),
]

# check for UNetResNet --> encoded bottom (depth=5) must be an even number
for choice in IN_SIZE_CHOICES_2D:
    if not (choice[0] / (2**5)) % 2 == 0:
        raise ValueError(
            f'Improperly configured: optimal config calculation will fail with input size {choice[0]} with UNet ResNet'
        )

BATCH_SIZE_CHOICES_2D = [
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (10, 10),
    (12, 12),
    (14, 14),
    (16, 16),
    (18, 18),
    (20, 20),
    (22, 22),
    (24, 24),
    (32, 32),
    (40, 40),
    (48, 48),
    (56, 56),
    (64, 64),
    (96, 96),
    (128, 128),
]

INFERENCE_BATCH_SIZE_CHOICES_2D = [
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (10, 10),
    (16, 16),
    (32, 32),
    (48, 48),
    (64, 64),
    (128, 128),
    (192, 192),
    (256, 256),
]

CPU_INFERENCE_BATCH_SIZE_CHOICES_2D = [
    (2, 2),
    (4, 4),
    (8, 8),  # more would take forever! even 8.. phew..
]

IN_SIZE_CHOICES_3D = [
    (64, 64),
    (128, 128),
    (192, 192),
    (256, 256),
]

DEPTH_CHOICES_3D = [
    (16, 16),
    (32, 32),
    (48, 48),
    (64, 64),
    (96, 96),
    (128, 128),
    (192, 192),
    (256, 256),
]

BATCH_SIZE_CHOICES_3D = [
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
    (10, 10),
    (12, 12),
    (14, 14),
    (16, 16),
    (24, 24),
    (32, 32),
    (40, 40),
    (48, 48),
    (56, 56),
    (64, 64),
]

INFERENCE_BATCH_SIZE_CHOICES_3D = [
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (8, 8),
    (10, 10),
    (16, 16),
    (32, 32),
    (48, 48),
    (64, 64),
]

CPU_INFERENCE_BATCH_SIZE_CHOICES_3D = [
    (2, 2),
    (4, 4),
]

NON_INTUITIVE_RAM_ERRORS = [
    'CUDA out of memory',
    'CUDA error: out of memory',
    'Unable to find a valid cuDNN algorithm to run convolution',
    'cuDNN error: CUDNN_STATUS_NOT_SUPPORTED',
    'DefaultCPUAllocator: not enough memory:'
]
