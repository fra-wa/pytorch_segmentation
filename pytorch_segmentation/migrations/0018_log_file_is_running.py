# Generated by Django 4.0 on 2022-07-25 08:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0017_updated_altered_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='logfile',
            name='is_running',
            field=models.BooleanField(default=True, verbose_name='Is running'),
        ),
    ]
