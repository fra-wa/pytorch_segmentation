import numpy as np
from django.db import migrations

from pytorch_segmentation.file_handling.utils import convert_binary_to_np_array, convert_np_array_to_binary


def forwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.history:
            history_data = convert_binary_to_np_array(analysis.history)
            new_history = history_data[:, 0:6]  # epochs, train_loss, val_loss, train_acc
            new_history[:, 5] = history_data[:, -1]  # augmentation_strengths
            header = f'epochs, train_loss, val_loss, train_acc, augmentation_strengths'
            analysis.history = convert_np_array_to_binary(new_history)
            analysis.history_header = header
            analysis.save()


def backwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.history:
            history_data = convert_binary_to_np_array(analysis.history)
            epochs = history_data.shape[0]
            new_history = np.zeros((epochs, 14))
            new_history[:, :5] = history_data[:, :5]
            new_history[:, -1] = history_data[:, 5]
            header = 'epochs, train_loss, val_loss, train_acc, val_acc, ma_train_acc, ma_valid_acc, ' \
                     'ma_train_acc_slopes, ma_valid_acc_slopes, training_ema, validation_ema, train_ema_slopes,' \
                     ' valid_ema_slopes, augmentation_strengths'
            analysis.history = convert_np_array_to_binary(new_history)
            analysis.history_header = header
            analysis.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0006_online_volume_aug_parameters'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
