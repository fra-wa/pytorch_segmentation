from django.db import migrations


def forwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.history_header == 'epochs, train_loss, val_loss, train_acc, augmentation_strengths':
            analysis.history_header = 'epochs, train_loss, val_loss, train_acc, val_acc, augmentation_strengths'
            analysis.save()


def backwards(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0024_inference_configuration'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
