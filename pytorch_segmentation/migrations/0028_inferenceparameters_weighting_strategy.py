# Generated by Django 4.1 on 2022-11-05 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0027_new_offline_augmentation'),
    ]

    operations = [
        migrations.AddField(
            model_name='inferenceparameters',
            name='weighting_strategy',
            field=models.CharField(choices=[('gauss', 'Gaussian'), ('linear', 'Linear'), ('sum', 'Sum')], default='gauss', help_text='Select the weighting strategy when using overlapping. Sum: each logit is equally weighted. Linear: ones at outer region, highest in center like [1,2,3,2,1] just in 2 or 3D (weighting border logits 1/(~h/2) to that of center). Gaussian: logits are weighted using a gaussian bell (2D or 3D), weighting border logits 1/3 to that of center (sigma h, w = h/3; sigma d = d/3.', max_length=255, verbose_name='Weighting strategy'),
        ),
    ]
