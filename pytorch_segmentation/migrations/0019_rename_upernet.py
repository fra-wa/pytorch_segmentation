from django.db import migrations


def forwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.architecture == 'UperNet':
            analysis.architecture = 'UPerNet'
            analysis.save()


def backwards(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0018_log_file_is_running'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
