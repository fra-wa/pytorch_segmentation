import os

from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import render

from .online_aug_example_utils import create_online_aug_examples
from .training import init_training
from .utils import start_process_and_create_log_file
from .. import constants
from ..file_handling.utils import remove_dir
from ..forms.online_augmentation import OnlineVolumeAugForm, OnlineImageAugForm, OnlineAugExampleForm
from ..models import TrainingParameters
from ..models import OnlineImageAugParameters
from ..models import OnlineVolumeAugParameters
from ..models.online_aug_example_images import OnlineImageAugExamples


def start_training_process():
    training_parameters = TrainingParameters.objects.last()
    log_file = start_process_and_create_log_file(
        execution_type=constants.DL_EXECUTION_TRAIN,
        device=training_parameters.device,
        process_func=init_training,
        parameters=training_parameters,
    )

    return redirect('pytorch_segmentation:monitor', log_pk=log_file.pk)


def online_aug_examples(request):

    form = OnlineAugExampleForm()
    examples_2d = None
    train_parameters = TrainingParameters.objects.last()
    is_3d = True if train_parameters.architecture in constants.THREE_D_NETWORKS else False
    if not is_3d:
        examples_2d = OnlineImageAugExamples.objects.all()

    example_folder = os.path.join(constants.DATASET_FOLDER, train_parameters.dataset, 'online_aug_examples')

    if request.method == 'POST':
        if 'delete_all' in request.POST:
            if is_3d:
                if os.path.isdir(example_folder):
                    remove_dir(example_folder)
            else:
                qs = OnlineImageAugExamples.objects.all()  # qs delete will not remove folders!
                for example in qs:
                    example.delete()
        else:
            form = OnlineAugExampleForm(request.POST)
            if form.is_valid():

                fcd = form.cleaned_data
                error_message = create_online_aug_examples(fcd['number_of_samples'], fcd['random'])
                if error_message is not None:
                    messages.add_message(request, messages.WARNING, error_message)
                if not is_3d:
                    examples_2d = OnlineImageAugExamples.objects.all()

    delete_active = False
    if is_3d:
        if os.path.isdir(example_folder):
            delete_active = True
    elif examples_2d is not None and examples_2d:
        delete_active = True

    context = {
        'form': form,
        'is_3d': True if train_parameters.architecture in constants.THREE_D_NETWORKS else False,
        'delete_active': delete_active,
        'examples_2d': examples_2d,
        'img_height': 300,
        'navbar_view': 'training',
    }

    return render(request, 'pytorch_segmentation/augmentation/online_aug_examples.html', context)


def online_image_aug(request):

    parameters = OnlineImageAugParameters.objects.last()
    if parameters is None:
        parameters = OnlineImageAugParameters.objects.create()

    form = OnlineImageAugForm(instance=parameters)
    if request.method == 'POST':
        form = OnlineImageAugForm(request.POST, instance=parameters)
        if form.is_valid():
            if form.has_changed():
                form.save()

            if 'sample_creation_submit' in request.POST:
                return redirect('pytorch_segmentation:online_aug_examples')
            return start_training_process()

    context = {
        'form': form,
        'navbar_view': 'training',
        'process_is_running': False,  # already killed job at training view when coming here
    }

    return render(request, 'pytorch_segmentation/augmentation/online_aug_image.html', context)


def online_volume_aug(request):

    parameters = OnlineVolumeAugParameters.objects.last()
    if parameters is None:
        parameters = OnlineVolumeAugParameters.objects.create()

    form = OnlineVolumeAugForm(instance=parameters)
    if request.method == 'POST':
        form = OnlineVolumeAugForm(request.POST, instance=parameters)
        if form.is_valid():
            if form.has_changed():
                form.save()

            if 'sample_creation_submit' in request.POST:
                return redirect('pytorch_segmentation:online_aug_examples')
            return start_training_process()

    context = {
        'form': form,
        'navbar_view': 'training',
        'process_is_running': False,  # already killed job at training view when coming here
    }

    return render(request, 'pytorch_segmentation/augmentation/online_aug_volume.html', context)
