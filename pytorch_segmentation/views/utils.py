import datetime
import os
import psutil
import sys

import torch.cuda
from django.contrib import messages
from django.utils import timezone
from django.utils.safestring import mark_safe

from torch import multiprocessing

try:
    multiprocessing.set_start_method('spawn')
except RuntimeError:
    pass

from pytorch_segmentation import constants
from pytorch_segmentation.logger.logger import LoggerSingleton
from pytorch_segmentation.models import LogFile
from pytorch_segmentation.utils import timezone_datetime, get_human_datetime


def get_running_logs(device=None):
    running_logs = list(LogFile.objects.filter(is_running=True))

    for idx, log_file in enumerate(running_logs):
        if isinstance(log_file, LogFile):
            log_active = True
            if psutil.pid_exists(log_file.process_id):
                # check if process with that id exists and if it was started at the same time as the log file process
                process = psutil.Process(log_file.process_id)
                # start time is already in local timezone
                local_started_datetime = timezone_datetime(
                    datetime.datetime.fromtimestamp(round(process.create_time()))
                )
                local_log_file_datetime = timezone.localtime(log_file.process_start_datetime)
                # log timezone is server --> utc. convert to local timezone

                if local_started_datetime != local_log_file_datetime:
                    log_active = False
                if sys.platform == 'linux' and process.status() == 'zombie':
                    log_active = False
            else:
                log_active = False

            if not log_active:
                log_file.is_running = False
                log_file.save()
                running_logs[idx] = None

    running_logs = [log for log in running_logs if log is not None]

    if device is not None:
        running_logs = [log for log in running_logs if log.device == device]

    return running_logs


def start_process_and_create_log_file(execution_type,
                                      device,
                                      process_func,
                                      parameters=None,
                                      process_func_args=None,
                                      return_process=False,
                                      ):
    """process_func must take logger as last argument. The logger will be created and appended inside this function!"""

    if parameters is not None and process_func_args is not None:
        raise ValueError('Either pass a parameters instance (db model) or process_func_args, not both')
    execution_choices = [choice[0] for choice in constants.LOG_EXECUTION_CHOICES]
    if execution_type not in execution_choices:
        raise ValueError(f'Execution type must be one of: {", ".join(execution_choices)}. You passed: {execution_type}')

    current_time = get_human_datetime(datetime_format='%Y%m%d_%H_%M_%S', time_ending='')
    logger_name = f'log_{execution_type}_{current_time}'
    log_file_path = os.path.join(constants.LOG_DIR, logger_name + '.txt')

    logger = LoggerSingleton.get_instance()
    process_logger = logger.create_logger(log_name=logger_name, log_handler=log_file_path)
    if process_func_args is None:
        process_func_args = (parameters, process_logger)
    else:
        process_func_args = list(process_func_args)
        process_func_args.append(process_logger)
        process_func_args = tuple(process_func_args)
    p = multiprocessing.Process(target=process_func, args=process_func_args, name=logger_name)
    p.start()

    psutil_p = psutil.Process(p.pid)

    process_started_timestamp = round(psutil_p.create_time())
    process_started_datetime = timezone_datetime(datetime.datetime.fromtimestamp(process_started_timestamp))

    log_file = LogFile.objects.create(
        log_file_path=log_file_path,
        process_id=psutil_p.pid,
        process_start_datetime=process_started_datetime,
        execution_process=execution_type,
        device=device,
    )
    if return_process:
        return p
    return log_file


def kill_process_and_children(process):
    if not isinstance(process, psutil.Process):
        raise ValueError('process must be a psutil Process instance!')

    children = process.children(recursive=True)
    for child in reversed(children):  # reverse to go backwards through children
        child.terminate()
    process.terminate()


def kill_running_process_if_needed(running_logs, device, request):
    """
    kills running logs/processes to free resources
    Args:
        running_logs:
        device:
        request:

    Returns:

    """
    for log in running_logs:
        if log.device == device or log.device == 'cuda':  # kill multi gpu in every case
            if psutil.pid_exists(log.process_id):
                # check if process with that id exists and if it was started at the same time as the log file
                # process
                process = psutil.Process(log.process_id)
                kill_process_and_children(process)

                execution_process = 'No info'
                for choice in constants.LOG_EXECUTION_CHOICES:
                    if choice[0] == log.execution_process:
                        execution_process = choice[1]
                        break

                messages.add_message(
                    request,
                    messages.INFO,
                    f'Killed {execution_process}.'
                )


def get_and_check_running_logs_and_warn(request):
    running_logs = get_running_logs()
    devices_in_use = [log.device for log in running_logs]

    multi_gpu = False
    for device in devices_in_use:
        if device == 'cuda':
            multi_gpu = True
            break

    if running_logs:
        if multi_gpu:
            warn_text = mark_safe(
                f'A multi GPU training is running!<b> If you start a new process, the training will be killed</b>'
            )
        else:
            warn_text = mark_safe(
                f'The following devices: <b>{", ".join(devices_in_use)} are in use</b>. '
                f'If you start a process on one of these devices, the process will be killed.<br>'
                f'WARNING: Running multiple processes on different devices <b>can cause out of memory crashes</b>.<br>'
                f'If the data preprocessing is running, wait for completion since multiprocessing requires a lot of '
                f'RAM!<br>If a training loop is running, you can safely start a new process.'
                f'<br>If augmentation or inference is running, DO NOT start a new process. This will crash very likely!'
            )

        messages.add_message(
            request,
            messages.WARNING,
            warn_text
        )

    return running_logs
