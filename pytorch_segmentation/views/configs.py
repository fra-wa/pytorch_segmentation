from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect

from pytorch_segmentation.forms import TrainingConfigurationForm, InferenceConfigurationForm
from pytorch_segmentation.models import TrainingConfiguration, InferenceConfiguration


def delete(request, config_class, config_pk):
    if isinstance(config_class, InferenceConfiguration):
        conf_type = 'inference'
    else:
        conf_type = 'training'
    try:
        configuration = config_class.objects.get(pk=config_pk)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.WARNING, f'The {conf_type} configuration does not exist.')
        return redirect('pytorch_segmentation:configurations_overview')

    configuration.delete()
    messages.add_message(request, messages.SUCCESS, f'The {conf_type}  configuration was deleted.')

    return redirect('pytorch_segmentation:configurations_overview')


def edit(request, config_class, config_pk):
    if isinstance(config_class, InferenceConfiguration):
        conf_type = 'inference'
        form_class = InferenceConfigurationForm
    else:
        conf_type = 'training'
        form_class = TrainingConfigurationForm
    try:
        configuration = config_class.objects.get(pk=config_pk)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.WARNING, f'The {conf_type} configuration does not exist')
        return redirect('pytorch_segmentation:configurations_overview')

    form = form_class(instance=configuration)

    if request.method == 'POST':
        form = form_class(request.POST, instance=configuration)

        if form.is_valid():
            if form.has_changed():
                form.save()
                messages.add_message(request, messages.SUCCESS, f'Saved {conf_type} configuration')

            return redirect('pytorch_segmentation:configurations_overview')

    context = {
        'form': form,
        'configuration': configuration,
        'is_training_config': True if conf_type == 'training' else False,
        'navbar_view': 'configurations',
    }
    return render(request, 'pytorch_segmentation/configuration/configuration_edit.html', context)


def edit_training_configuration(request, configuration_pk):
    return edit(request, TrainingConfiguration, configuration_pk)


def delete_training_configuration(request, configuration_pk):
    return delete(request, TrainingConfiguration, configuration_pk)


def edit_inference_configuration(request, configuration_pk):
    return edit(request, InferenceConfiguration, configuration_pk)


def delete_inference_configuration(request, configuration_pk):
    return delete(request, InferenceConfiguration, configuration_pk)


def configurations_overview(request):
    optimal_training_configurations = TrainingConfiguration.objects.order_by('architecture', 'backbone')
    optimal_inference_configurations = InferenceConfiguration.objects.order_by('architecture', 'backbone')

    context = {
        'training_configurations': optimal_training_configurations,
        'inference_configurations': optimal_inference_configurations,
        'navbar_view': 'configurations',
    }
    return render(request, 'pytorch_segmentation/configuration/configurations.html', context)
