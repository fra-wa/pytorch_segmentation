import json
import logging
import numpy as np
import os
import time
import torch
import traceback

from collections import OrderedDict
from decimal import Decimal

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect, resolve_url
from django.utils import timezone
from django.utils.safestring import mark_safe

from pytorch_segmentation import constants
from pytorch_segmentation.dl_models.loading import load_checkpoint_to_run_test, load_model_from_checkpoint
from pytorch_segmentation.file_handling.utils import remove_dir, convert_binary_to_np_array, create_folder
from pytorch_segmentation.forms import AnalysesCleanUpForm
from pytorch_segmentation.forms import RunTestForm
from pytorch_segmentation.forms.model_analysis import AnalysisCustomNameForm
from pytorch_segmentation.inference.predictors.image_predictor import SlidingImagePredictor
from pytorch_segmentation.inference.predictors.volume_predictor import SlidingVolumePredictor
from pytorch_segmentation.models import ModelAnalysis, LogFile, TrainingDataset
from pytorch_segmentation.models.model_analyses import fill_or_delete_analysis
from pytorch_segmentation.training.testing import run_test_loop
from pytorch_segmentation.temporary_parameters.parameters import FixedParameters
from pytorch_segmentation.utils import get_human_datetime, get_time_spent_string, validate_basename, open_file, \
    log_error_info_and_kill_children
from pytorch_segmentation.views.export_analysis import export_analysis
from pytorch_segmentation.views.optimal_parameter_calculation.inference_parameters import \
    get_optimal_inference_parameters
from pytorch_segmentation.views.utils import get_running_logs, start_process_and_create_log_file


def delete_model_analysis(request, analysis_pk):
    try:
        analysis = ModelAnalysis.objects.get(pk=analysis_pk)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.WARNING, 'The dataset does not exist.')
        return redirect('pytorch_segmentation:trained_model_overview')

    analysis.delete()
    messages.add_message(request, messages.SUCCESS, f'The analysis was deleted.')

    return redirect('pytorch_segmentation:trained_model_overview')


def run_inference_test(checkpoint_path, model_analysis, device, max_input_size, process_logger):
    """keep parameter max_input_size to fit run_test structure"""
    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        data_folder = os.path.join(constants.TEST_FILES_DIR, 'inference_timing_data')
        logging.info(f'Predicting files in: {data_folder}.')

        possible_relic_folders = [
            os.path.join(data_folder, 'original'),
            os.path.join(data_folder, 'temp'),
            os.path.join(data_folder, 'segmented'),
        ]
        for old_folder in possible_relic_folders:
            remove_dir(old_folder)

        model = load_model_from_checkpoint(checkpoint_path, device, False)
        image_channels = model.in_channels

        logging.info(f'Preparing data before test starts.')

        input_size, input_depth, batch_size = get_optimal_inference_parameters(
            checkpoint_path, device, max_input_size
        )

        if model.__class__.__name__ in constants.THREE_D_NETWORKS:
            batch_size = 1
            predictor = SlidingVolumePredictor(data_folder, input_size, model, batch_size, input_depth, image_channels,
                                               decode_to_color=False, logger=process_logger)
        elif model.__class__.__name__ in constants.TWO_D_NETWORKS:
            batch_size = 1
            predictor = SlidingImagePredictor(data_folder, input_size, model, batch_size, image_channels,
                                              decode_to_color=False, logger=process_logger)
        else:
            raise RuntimeError('The loaded model is not one of the preconfigured dl_models.')

        device = torch.device(device)
        if 'cuda' in device.type:
            if not torch.cuda.is_available():
                logging.info('Cuda is not available on your system. Using cpu.')
                device = torch.device('cpu')

        start_time = time.time()
        logging.info(f'Start time at: {get_human_datetime()}')
        predictor.predict(device, show_ram=True)
        time_string = get_time_spent_string(start_time)
        time_in_seconds = time.time() - start_time
        model_analysis.inference_time = Decimal(round(time_in_seconds, 2))
        model_analysis.save()

        logging.info(
            f'Finished inference timing test at: {get_human_datetime()} '
            f'(time spent: {time_string}; seconds: {time_in_seconds:.1f})'
        )
        logging.info('Cleaning created files')
        remove_dir(predictor.out_folder)
        remove_dir(predictor.temp_out_folder)
        logging.info('Finished.')

    except Exception:
        logging.error(traceback.format_exc())
        log_error_info_and_kill_children()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def run_test(checkpoint_path, model_analysis, device, process_logger):
    """
    Clean all input before calling this function! It does not validate, that the path relates to the analysis!
    """
    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting model test at: {get_human_datetime()}')

        params = FixedParameters()
        params.device = device
        params.model_path = checkpoint_path
        params.dataset_name = model_analysis.dataset.folder_name_on_device

        params, model, history_dict = load_checkpoint_to_run_test(params)

        testing_epoch = history_dict['epochs'][-1]

        testing_dir = os.path.join(constants.DATASET_FOLDER, params.dataset_name, 'test')
        storage = run_test_loop(params, checkpoint_path, testing_dir, logger=process_logger)
        if storage is not None:
            model_analysis.save_test_storage(storage, testing_epoch)

        model_analysis.save()

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished model test at: {get_human_datetime()} (time spent: {time_string})')
    except Exception:
        logging.error(traceback.format_exc())
        log_error_info_and_kill_children()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def __get_online_aug_table_data(model_analysis, rows):
    online_aug_parameters = model_analysis.get_online_aug_parameter_list(improve_readability=True)
    fill_count = 0
    if len(online_aug_parameters) % rows:
        fill_count = rows - len(online_aug_parameters) % rows

    for i in range(fill_count):
        online_aug_parameters.append(('', ''))

    online_aug_table_data = []
    for i in range(len(online_aug_parameters) // rows):
        index = rows * len(online_aug_table_data)
        names_and_values = online_aug_parameters[index:index + rows]
        heads = [name_value[0] for name_value in names_and_values]
        bodies = [name_value[1] for name_value in names_and_values]
        online_aug_table_data.append([heads, bodies])

    return online_aug_table_data


def __get_multiclass_testing_table_data(model_analysis):
    if not isinstance(model_analysis, ModelAnalysis):
        raise ValueError('model_analysis must be a ModelAnalysis instance!')
    if not model_analysis.test_accuracy:
        return []
    class_suited_for_calculation = convert_binary_to_np_array(model_analysis.testing_class_suited_for_calculation)
    class_suited_for_calculation = class_suited_for_calculation.astype(np.object)
    accuracy_per_class = list(convert_binary_to_np_array(model_analysis.testing_accuracy_per_class))
    tp_ratio_per_class = list(convert_binary_to_np_array(model_analysis.testing_true_positives_ratio_per_class))
    tn_ratio_per_class = list(convert_binary_to_np_array(model_analysis.testing_true_negatives_ratio_per_class))
    fp_ratio_per_class = list(convert_binary_to_np_array(model_analysis.testing_false_positives_ratio_per_class))
    fn_ratio_per_class = list(convert_binary_to_np_array(model_analysis.testing_false_negatives_ratio_per_class))
    precision_per_class = list(convert_binary_to_np_array(model_analysis.testing_precision_per_class))
    mean_iou_per_class = list(convert_binary_to_np_array(model_analysis.testing_mean_iou_per_class))
    dice_coefficient_per_class = list(convert_binary_to_np_array(model_analysis.testing_dice_coefficient_per_class))
    pixels_per_class = convert_binary_to_np_array(model_analysis.testing_pixels_per_class)
    pixel_ratio_per_class = list(pixels_per_class / model_analysis.testing_all_pixels)

    class_suited_for_calculation[class_suited_for_calculation == 1] = 'Yes'
    class_suited_for_calculation[class_suited_for_calculation == 0] = 'No'
    class_suited_for_calculation = list(class_suited_for_calculation)
    accuracy_per_class = [f'{ratio * 100:.2f} %' for ratio in accuracy_per_class]
    tp_ratio_per_class = [f'{ratio:.3f}' for ratio in tp_ratio_per_class]
    tn_ratio_per_class = [f'{ratio:.3f}' for ratio in tn_ratio_per_class]
    fp_ratio_per_class = [f'{ratio:.3f}' for ratio in fp_ratio_per_class]
    fn_ratio_per_class = [f'{ratio:.3f}' for ratio in fn_ratio_per_class]
    precision_per_class = [f'{precision:.3f}' for precision in precision_per_class]
    mean_iou_per_class = [f'{precision:.3f}' for precision in mean_iou_per_class]
    dice_coefficient_per_class = [f'{precision:.3f}' for precision in dice_coefficient_per_class]
    pixel_ratio_per_class = [f'{ratio * 100:.3f} %' for ratio in pixel_ratio_per_class]
    classes = [f'{class_id}' for class_id in list(range(model_analysis.classes))]
    classes[0] = classes[0] + ' (background)'

    header = [
        'Class (occurred)', 'Accuracy', 'TPR', 'TNR', 'FPR', 'FNR', 'Precision', 'IoU', 'Dice', '% of all pixels'
    ]

    combined = zip(
        class_suited_for_calculation,
        accuracy_per_class,
        tp_ratio_per_class,
        tn_ratio_per_class,
        fp_ratio_per_class,
        fn_ratio_per_class,
        precision_per_class,
        mean_iou_per_class,
        dice_coefficient_per_class,
        pixel_ratio_per_class,
    )

    table = [header]

    for c_id, (occurred, acc, tp, tn, fp, fn, precision, iou, dice, per_of_all) in enumerate(combined):
        if c_id == 0:
            c_id_and_usable = f'{c_id} ({occurred}, background)'
        else:
            c_id_and_usable = f'{c_id} ({occurred})'
        table_column = [c_id_and_usable, acc, tp, tn, fp, fn, precision, iou, dice, per_of_all]
        table.append(table_column)

    return table


def trained_model_analysis(request, analysis_pk):
    try:
        model_analysis = ModelAnalysis.objects.get(pk=analysis_pk)
    except (ObjectDoesNotExist, ValueError):
        messages.add_message(request, messages.INFO, 'The requested analysis does not exist.')
        return redirect('pytorch_segmentation:analysis_overview')

    running_logs = get_running_logs()
    devices_in_use = [log.device for log in running_logs]

    dataset_folder = os.path.join(constants.DATASET_FOLDER, model_analysis.dataset.folder_name_on_device)
    test_folder = os.path.join(dataset_folder, 'test')
    test_dataset_exists = True if os.path.isdir(test_folder) else False

    test_form = RunTestForm(
        dataset_folder_name=model_analysis.dataset.folder_name_on_device,
        architecture=model_analysis.architecture,
        backbone=model_analysis.backbone,
        disable=True if devices_in_use else False,
        disable_testing=not test_dataset_exists,
    )
    custom_name_form = AnalysisCustomNameForm(instance=model_analysis)
    if request.method == 'POST':
        if 'run_test' in request.POST or 'run_inference' in request.POST:
            test_form = RunTestForm(
                request.POST,
                dataset_folder_name=model_analysis.dataset.folder_name_on_device,
                architecture=model_analysis.architecture,
                backbone=model_analysis.backbone,
                disable=True if devices_in_use else False,
                disable_testing=not test_dataset_exists,
            )
            if test_form.is_valid():
                fcd = test_form.cleaned_data

                execution = constants.DL_EXECUTION_TEST if 'run_test' in request.POST else \
                    constants.DL_EXECUTION_TEST_INF
                exec_func = run_test if execution == constants.DL_EXECUTION_TEST else run_inference_test
                log_file = start_process_and_create_log_file(
                    execution_type=execution,
                    device=fcd['device'],
                    process_func=exec_func,
                    process_func_args=[fcd['checkpoint_path'], model_analysis, fcd['device']]
                )
                log_file.model_analysis = model_analysis
                log_file.save()

                return redirect('pytorch_segmentation:monitor', log_pk=log_file.pk)
        elif 'export' in request.POST:
            arch = model_analysis.architecture
            backbone = model_analysis.backbone
            custom_name = model_analysis.custom_name
            created_datetime = model_analysis.created_datetime
            created_datetime = timezone.localtime(created_datetime)
            folder_time_string = created_datetime.strftime('y%Y_m%m_d%d_h%H_m%M_s%S')
            if backbone:
                folder_name = f'{arch}_{backbone}_{folder_time_string}'
            else:
                folder_name = f'{arch}_{folder_time_string}'
            if custom_name:
                folder_name = f'{custom_name}_{folder_name}'
            folder_name = validate_basename(folder_name)

            out_folder = os.path.join(constants.EXPORT_DIR, 'single_exports')
            create_folder(out_folder)
            out_folder = os.path.join(out_folder, folder_name)
            info_data, accuracy_data, class_performance_data, class_performance_header, out_folder = export_analysis(
                model_analysis, out_folder
            )
            open_file(out_folder)
        else:
            custom_name_form = AnalysisCustomNameForm(request.POST, instance=model_analysis)
            if custom_name_form.is_valid():
                if custom_name_form.has_changed():
                    model_analysis = custom_name_form.save()
                    if model_analysis.custom_name:
                        messages.add_message(request, messages.SUCCESS, 'Saved custom name')
                    else:
                        messages.add_message(request, messages.SUCCESS, 'Reset custom name')
                    return redirect('pytorch_segmentation:trained_model_analysis', analysis_pk=model_analysis.pk)

    related_logs = OrderedDict({})

    for log_key, log_name_type in constants.LOG_EXECUTION_CHOICES:
        logs = LogFile.objects.filter(
            model_analysis=analysis_pk, execution_process=log_key).order_by('-process_start_datetime')
        if log_key in [constants.DL_EXECUTION_TEST, constants.DL_EXECUTION_TEST_INF] and logs and logs[0]:
            logs = [logs.first()]
        related_logs[log_name_type] = logs

    header, history = model_analysis.get_history()
    if header is not None:
        epochs = list(history[:, 0])
        rounded_train_loss = np.around(history[:, 1].astype(float), decimals=4)
        rounded_val_loss = np.around(history[:, 2].astype(float), decimals=4)
        train_loss = list(zip(epochs, list(rounded_train_loss)))
        valid_loss = list(zip(epochs, list(rounded_val_loss)))
        train_acc = list(zip(epochs, list(np.around(history[:, 3].astype(float), decimals=2))))
        valid_acc = list(zip(epochs, list(np.around(history[:, 4].astype(float), decimals=2))))
        max_loss_val = max(np.max(rounded_train_loss), np.max(rounded_val_loss))

    else:
        messages.add_message(
            request,
            messages.WARNING,
            mark_safe(
                f'The training of this model was stopped before the history could be saved! You might want to delete '
                f'this or '
                f'<a class="messages-a-warning" href="{resolve_url("pytorch_segmentation:trained_model_import")}">'
                f'import the checkpoint</a>.')
        )
        train_loss = []
        valid_loss = []
        train_acc = []
        valid_acc = []
        max_loss_val = 0

    train_loss = json.dumps(train_loss)
    valid_loss = json.dumps(valid_loss)
    train_acc = json.dumps(train_acc)
    valid_acc = json.dumps(valid_acc)

    try:
        inference_log_for_link = related_logs[constants.DL_EXECUTION_TEST_INF_NAME][0]
    except IndexError:
        inference_log_for_link = None

    online_aug_table_data = __get_online_aug_table_data(model_analysis, rows=5)

    testing_multiclass_table_data = __get_multiclass_testing_table_data(model_analysis)

    context = {
        'custom_name_form': custom_name_form,
        'test_form': test_form,
        'devices_in_use': devices_in_use,
        'model_analysis': model_analysis,
        'online_aug_table_data': online_aug_table_data,
        'testing_multiclass_table_data': testing_multiclass_table_data,
        'total_training_duration': model_analysis.get_total_training_duration_string(),
        'is_3d': True if model_analysis.architecture in constants.THREE_D_NETWORKS else False,
        'inference_log_for_link': inference_log_for_link,
        'train_loss': train_loss,
        'valid_loss': valid_loss,
        'train_acc': train_acc,
        'valid_acc': valid_acc,
        'max_loss_val': max_loss_val,
        'test_dataset_exists': test_dataset_exists,
        'related_logs': related_logs,
        'navbar_view': 'trained_models',
    }
    return render(request, 'pytorch_segmentation/analysis/model_analysis.html', context)


def trained_model_overview(request):
    datasets = TrainingDataset.objects.all().order_by('name')
    
    datasets_and_models = []
    for dataset in datasets:
        dataset_deleted = False if dataset.folder_name_on_device else True
        if not dataset_deleted \
                and not os.path.isdir(os.path.join(constants.DATASET_FOLDER, dataset.folder_name_on_device)):
            dataset_deleted = True

        models = ModelAnalysis.objects.filter(dataset=dataset.pk).order_by('-created_datetime')
        if models.count() > 0:
            dataset_context = {
                'name': dataset.name,
                'identifier': dataset.name.replace(' ', '_').lower(),
                'dataset_deleted': dataset_deleted,
                'models': models,
            }
            datasets_and_models.append(dataset_context)

    # last column of table must contain equal elements like largest element otherwise the last outer line
    # is not width = 100%
    try:
        fill_count = max(
            [entry['models'].count() for entry in datasets_and_models]) - datasets_and_models[-1]['models'].count()
    except (ValueError, IndexError):
        fill_count = 0

    context = {
        'datasets_and_models': datasets_and_models,
        'last_table_elements_to_fill': range(fill_count),
        'navbar_view': 'trained_models',
    }
    return render(request, 'pytorch_segmentation/analysis/model_overview.html', context)


def clean_model_analyses(request):
    if not ModelAnalysis.objects.all().count():
        messages.add_message(request, messages.INFO, f'No model analyses exists')
        return redirect('pytorch_segmentation:trained_model_overview')

    form = AnalysesCleanUpForm()

    if request.method == 'POST':
        form = AnalysesCleanUpForm(request.POST)

        if form.is_valid():
            fcd = form.cleaned_data
            removed = 0
            updated = 0
            analyses = ModelAnalysis.objects.all()
            running_logs = get_running_logs()

            for analysis in analyses:
                process_running = False
                for log in running_logs:
                    if log.execution_process == constants.DL_EXECUTION_TRAIN and log.model_analysis and \
                            log.model_analysis.pk == analysis.pk:
                        process_running = True
                        break
                if process_running:
                    # do not update currently running analyses
                    continue
                analysis, has_changed = fill_or_delete_analysis(analysis)
                if analysis is None:
                    removed += 1
                    continue
                if has_changed:
                    updated += 1
                header, history = analysis.get_history()
                if history is not None:
                    if history.shape[0] < fcd['remove_with_less_epochs_than']:
                        analysis.delete()
                        removed += 1
                        if has_changed:
                            updated -= 1

            if removed:
                messages.add_message(request, messages.SUCCESS, f'{removed} analyses were deleted.')
            if updated:
                messages.add_message(request, messages.SUCCESS, f'{updated} analyses were updated.')
            if not removed and not updated:
                messages.add_message(request, messages.SUCCESS, f'No analyses were removed or updated.')

            return redirect('pytorch_segmentation:trained_model_overview')

    context = {
        'form': form,
        'navbar_view': 'trained_models',
    }
    return render(request, 'pytorch_segmentation/analysis/analyses_clean_up.html', context)
