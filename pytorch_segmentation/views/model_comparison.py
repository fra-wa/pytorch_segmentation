import json

from collections import OrderedDict
from decimal import Decimal

from django.shortcuts import render
from django.utils.safestring import mark_safe

from pytorch_segmentation import constants
from pytorch_segmentation.models import TrainingDataset, ModelAnalysis


def __get_analyses_pks_to_remove(analyses_query, architecture, backbone):
    pk_s_to_exclude = []
    q = analyses_query.filter(architecture=architecture, backbone=backbone).order_by('validation_accuracy')
    if len(q) > 1:
        for i, analysis in enumerate(q):
            if i == 0:
                continue
            pk_s_to_exclude.append(analysis.pk)
    return pk_s_to_exclude


def __clean_analyses_query(analyses_query):
    # clean all analyses without information
    pks_to_exclude = []
    for analysis in analyses_query:
        if not analysis.validation_accuracy:
            pks_to_exclude.append(analysis.pk)

    analyses_query = analyses_query.exclude(pk__in=pks_to_exclude)

    # custom names are also a filter - this enables the comparison of multiple same networks (arch and backbone)
    # trained with different parameters on one dataset
    pk_s_to_exclude = []
    name_set = []
    for analysis in analyses_query:
        if analysis.custom_name:
            if analysis.custom_name in name_set:
                pk_s_to_exclude.append(analysis.pk)
            else:
                name_set.append(analysis.custom_name)

    cleaned_analyses_query = analyses_query.exclude(pk__in=pk_s_to_exclude)

    # now filter for same architectures and backbones, but exclude those with custom names
    # those are the vertical buttons to enable or disable single architectures or training runs
    pk_s_to_exclude = []
    for architecture, backbones in constants.ARCHITECTURES_AND_BACKBONES.items():
        if backbones[0]:
            for backbone in backbones:
                pk_s_to_exclude += __get_analyses_pks_to_remove(
                    cleaned_analyses_query.filter(custom_name=''), architecture, backbone
                )
        else:
            pk_s_to_exclude += __get_analyses_pks_to_remove(
                cleaned_analyses_query.filter(custom_name=''), architecture, None
            )

    cleaned_analyses_query = analyses_query.exclude(pk__in=pk_s_to_exclude)

    return cleaned_analyses_query.exclude(pk__in=pk_s_to_exclude)


def model_comparison(request):
    """
    Front end builds a chart like:
    https://www.highcharts.com/demo/column-basic/dark-unica

    There are multiple elements, the user can play with. The main data holder has the following structure:

    data_dict = OrderedDict({
        'dataset_a': OrderedDict({
            'readable_name: 'some name',
            'analysis_a': {
                'hz_navbar_element_id_1': 'some_value',
                'hz_navbar_element_id_2': 'some_value',
                'hz_navbar_element_id_3': 'some_value',
            },
            'analysis_b': {
                'hz_navbar_element_id_1': 'some_value',
            },
        }),
        'dataset_b': OrderedDict({
            'analysis_a': {...},
            'analysis_b': {...},
        }),
    })

    A highcharts series requires data like:
        series: dataset_name, [value arch 1, value arch 2, value arch 3, value arch 4, .....]
    will be build at frontend.

    Args:
        request:

    Returns:

    """
    hz_navbar_ids_unit_text = [
        ['validation_accuracy', '%', ''],
        ['test_accuracy', '%', ''],
        ['training_accuracy', '%', ''],
        ['training_loss', 'loss', ''],
        ['validation_loss', 'loss', ''],
        ['testing_precision', 'zero_to_one', ''],
        ['testing_mean_iou', 'zero_to_one', ''],
        ['testing_dice_coefficient', 'zero_to_one', ''],
        ['testing_true_positives_ratio', 'zero_to_one', ''],
        ['testing_true_negatives_ratio', 'zero_to_one', ''],
        ['testing_false_positives_ratio', 'zero_to_one', ''],
        ['testing_false_negatives_ratio', 'zero_to_one', ''],
        ['storage_size', 'MB', ''],
        ['inference_time', 's', ''],
    ]
    analysis_db_fields = ModelAnalysis._meta.fields

    for i, (attr, unit, text) in enumerate(hz_navbar_ids_unit_text):
        for field in analysis_db_fields:
            if field.attname == attr:
                hz_navbar_ids_unit_text[i][2] = field.verbose_name
                hz_navbar_ids_unit_text[i][0] = attr + '_id'

    datasets = TrainingDataset.objects.all().order_by('pk')

    data_dict = OrderedDict({})

    analyses_for_x_axis_labels = __clean_analyses_query(ModelAnalysis.objects.all()).order_by(
        'custom_name', 'architecture', 'backbone'
    )

    vertical_architecture_buttons = OrderedDict({})
    dataset_sorting_buttons = []
    dataset_filter_buttons = []

    for dataset in datasets:
        dataset_analyses = ModelAnalysis.objects.filter(dataset=dataset)
        dataset_analyses = __clean_analyses_query(dataset_analyses).order_by('custom_name', 'architecture', 'backbone')
        if not dataset_analyses:
            continue

        dataset_identifier = dataset.name.replace(" ", "_").lower()

        data_dict[dataset_identifier] = OrderedDict({'readable_name': dataset.name})
        dataset_sorting_buttons.append([dataset.name, dataset_identifier, f'js-sort-by-{dataset_identifier}'])
        dataset_filter_buttons.append([dataset.name, dataset_identifier, f'js-filter-by-{dataset_identifier}'])

        for analysis_used_as_x_label in analyses_for_x_axis_labels:

            if analysis_used_as_x_label.backbone:
                label = f'{analysis_used_as_x_label.architecture}<small> {analysis_used_as_x_label.backbone}</small'
                dom_element_id = f'{analysis_used_as_x_label.architecture}_{analysis_used_as_x_label.backbone}_id'
            else:
                label = f'{analysis_used_as_x_label.architecture}'
                dom_element_id = f'{analysis_used_as_x_label.architecture}_id'

            if analysis_used_as_x_label.custom_name:
                label += f'<small> ({analysis_used_as_x_label.custom_name})</small>'
                dom_element_id = f'{analysis_used_as_x_label.custom_name}_id'.replace(' ', '_')
            label = mark_safe(label)

            is_2d = True if analysis_used_as_x_label.architecture in constants.TWO_D_NETWORKS else False
            if dom_element_id not in vertical_architecture_buttons.keys():
                vertical_architecture_buttons[dom_element_id] = {
                    'label': label,
                    'dataset_filters': [],
                    'dimension': '2D' if is_2d else '3D',
                }

            current_analysis_q = dataset_analyses.filter(
                architecture=analysis_used_as_x_label.architecture,
                backbone=analysis_used_as_x_label.backbone,
                custom_name=analysis_used_as_x_label.custom_name,
            )

            data_dict[dataset_identifier][label] = {'dom_element_id': dom_element_id}

            if current_analysis_q:  # can only be the last one due to __clean_analyses_query
                vertical_architecture_buttons[dom_element_id]['dataset_filters'].append(
                    dataset_identifier
                )
                for attribute_id, unit, text in hz_navbar_ids_unit_text:
                    value = current_analysis_q[0].__getattribute__(attribute_id.split('_id')[0])
                    hz_navbar_id = f'{attribute_id}'
                    if isinstance(value, Decimal):
                        value = float(value)
                    data_dict[dataset_identifier][label][hz_navbar_id] = value
            else:
                for attribute_id, unit, text in hz_navbar_ids_unit_text:
                    hz_navbar_id = f'{attribute_id}'
                    data_dict[dataset_identifier][label][hz_navbar_id] = None

    context = {
        'navbar_view': 'model_comparison',
        'data_dict': json.dumps(data_dict),
        'hz_navbar_ids_unit_text': hz_navbar_ids_unit_text,  # for hz navbar creation and chart
        'hz_navbar_ids': json.dumps([id_unit_text[0] for id_unit_text in hz_navbar_ids_unit_text]),  # for js query
        'vertical_architecture_buttons': vertical_architecture_buttons,  # for vertical navbar creation
        'vt_navbar_ids': json.dumps(list(vertical_architecture_buttons.keys())),  # js query
        'dataset_sorting_buttons': dataset_sorting_buttons,
        'dataset_filter_buttons': dataset_filter_buttons,
    }
    return render(request, 'pytorch_segmentation/analysis/model_comparison.html', context)
