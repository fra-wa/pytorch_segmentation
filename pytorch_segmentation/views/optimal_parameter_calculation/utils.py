import cv2
import gc
import logging
import os

import torch
from torch.utils.data import DataLoader

from pytorch_segmentation import constants
from pytorch_segmentation.dl_models.loading import get_model
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, get_sub_folders
from pytorch_segmentation.multicore_utils import slice_multicore_parts, multiprocess_function
from pytorch_segmentation.training.data_loading import get_num_workers_and_prefetch
from pytorch_segmentation.temporary_parameters.parameters import FixedParameters
from pytorch_segmentation.views.optimal_parameter_calculation.simulation_datasets import Dataset2DSimulator,\
    Dataset3DSimulator


class ModelLoader:
    """Helper for usability --> easier usage of returned parameters"""

    def __init__(self, architecture, input_channels, classes, device, backbone, pretrained, normalization):
        self.architecture = architecture
        self.input_channels = input_channels
        self.classes = classes
        self.device = device
        self.backbone = backbone
        self.pretrained = pretrained
        self.normalization = normalization

        self.model = None
        self.optimizer = None
        self.loss = None
        self.in_size = None

    def setup_model_optimizer_loss(self):
        model = get_model(
            model_name=self.architecture,
            channels=self.input_channels,
            classes=self.classes,
            device=self.device,
            backbone=self.backbone,
            pretrained=self.pretrained,
            use_gru=True,
            normalization=self.normalization,
            in_size=self.in_size,
            log_information=False,
        )
        model.train()
        model.to(self.device)
        self.model = model

        self.optimizer = torch.optim.Adam(self.model.parameters())

        self.loss = torch.nn.BCEWithLogitsLoss().to(
            self.device) if self.classes == 2 else torch.nn.CrossEntropyLoss().to(self.device)

        return self.model, self.optimizer, self.loss

    def wipe_memory(self):
        self._optimizer_to(torch.device('cpu'))
        self.loss.cpu()
        self.model.cpu()
        del self.optimizer
        del self.model
        del self.loss
        gc.collect()
        torch.cuda.empty_cache()

    def _optimizer_to(self, device):
        for param in self.optimizer.state.values():
            if isinstance(param, torch.Tensor):
                param.data = param.data.to(device)
                if param._grad is not None:
                    param._grad.data = param._grad.data.to(device)
            elif isinstance(param, dict):
                for subparam in param.values():
                    if isinstance(subparam, torch.Tensor):
                        subparam.data = subparam.data.to(device)
                        if subparam._grad is not None:
                            subparam._grad.data = subparam._grad.data.to(device)


def multi_get_min_image_size(image_paths, process_nr):
    in_size_choices = constants.IN_SIZE_CHOICES_2D
    min_size = max([in_size_choice[0] for in_size_choice in in_size_choices])

    for i, path in enumerate(image_paths):
        if process_nr == 0:
            logging.info(f'Process 1: {i + 1}/{len(image_paths)}')
        h, w = cv2.imread(path, -1).shape[:2]
        min_image_dim = min([h, w])
        if min_size > min_image_dim:
            min_size = min_image_dim

    return min_size


def get_max_inference_in_size(model_path, data_folder, logger):
    """
    During inference, images might be enlarged -> this is bad if it can be avoided.
    This function checks all input images and gets the minimum dimension

    Args:
        model_path: path to checkpoint
        data_folder:
        logger:
    """
    logging.info('Checking images to get best inference parameters.')

    checkpoint = torch.load(model_path, map_location=torch.device('cpu'))
    is_2d = True if checkpoint['architecture'] in constants.TWO_D_NETWORKS else False

    image_paths = get_file_paths_in_folder(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    if not image_paths:
        raise FileNotFoundError(
            f'No files found. Images must be of type: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
        )
    size_choices = constants.IN_SIZE_CHOICES_2D if is_2d else constants.IN_SIZE_CHOICES_3D
    in_sizes = [in_choice[0] for in_choice in size_choices]

    max_in_depth = None
    max_in_size = None

    if len(image_paths) > 40:
        images_map = slice_multicore_parts(image_paths)
        process_nr_map = list(range(len(images_map)))

        min_sizes = multiprocess_function(
            multi_get_min_image_size, [images_map, process_nr_map], logger=logger
        )
        min_size = min(min_sizes)
    else:
        min_size = multi_get_min_image_size(image_paths, 1)

    for in_size in in_sizes:
        if min_size >= in_size:
            max_in_size = in_size
        else:
            break

    if max_in_size is None:
        if is_2d:
            # images will be resized to this size.
            max_in_size = min(in_sizes)
        else:
            # volume will not be resized
            raise ValueError(f'The volumes x or y dim is too small: x or y is smaller than {min(in_sizes)}.')

    if is_2d:
        max_in_depth = None
    else:
        volume_depth = len(image_paths)
        depths = [in_choice[0] for in_choice in constants.DEPTH_CHOICES_3D]
        for depth in depths:
            if volume_depth >= depth:
                max_in_depth = depth
            else:
                break

        if max_in_depth is None:
            raise ValueError(
                f'The volume contains too less slices: at least {min(depths)} slices are needed, got {len(image_paths)}'
                f'\nImages must be of type: {", ".join(constants.SUPPORTED_INPUT_IMAGE_TYPES)}.'
            )
    if max_in_depth is not None:
        logging.info(f'Max possible input size: {max_in_size}; max possible input depth: {max_in_depth}.')
    else:
        logging.info(f'Max possible input size: {max_in_size}.')

        logging.info('Tip: Larger input sizes are better! Resize your images if needed.')

    return max_in_size, max_in_depth


def get_max_training_in_size(architecture, data_folder_name, logger):
    """
    During training, images might be enlarged -> this is bad if it can be avoided.
    This function checks all input images and gets the minimum dimension

    Args:
        architecture: networks architecture
        data_folder_name:
        logger:
    """
    logging.info('Checking images to get best training parameters.')

    # masks loading is faster
    train_folder = os.path.join(os.path.join(constants.DATASET_FOLDER, data_folder_name), 'masks')

    is_2d = True if architecture in constants.TWO_D_NETWORKS else False

    depth_choices = constants.DEPTH_CHOICES_3D
    depth_choices = [in_choice[0] for in_choice in depth_choices]
    max_in_depth = max(depth_choices)

    logging.info('Loading images.')
    if is_2d:
        data_folder = train_folder
        image_paths = get_file_paths_in_folder(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
    else:
        image_paths = []
        for folder in get_sub_folders(train_folder, depth=1):
            paths = get_file_paths_in_folder(folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
            max_in_depth = min(len(paths), max_in_depth)
            image_paths += paths
    logging.info('Images loaded.')

    size_choices = constants.IN_SIZE_CHOICES_2D if is_2d else constants.IN_SIZE_CHOICES_3D
    in_sizes = [in_choice[0] for in_choice in size_choices]

    if is_2d and len(image_paths) > 40:
        images_map = slice_multicore_parts(image_paths)
        process_nr_map = list(range(len(images_map)))

        min_sizes = multiprocess_function(
            multi_get_min_image_size, [images_map, process_nr_map], logger=logger
        )
        min_size = min(min_sizes)
    elif is_2d:
        min_size = multi_get_min_image_size(image_paths, 1)
    else:
        min_size = min(cv2.imread(image_paths[0], -1).shape[:2])

    for i, in_size in enumerate(in_sizes):
        if i == 0:
            continue
        if min_size < in_size:
            return in_sizes[i - 1], max_in_depth

    return max(in_sizes), max_in_depth


def get_calculation_data():
    data_dir = os.path.join(constants.PYTORCH_SEGMENTATION_APP_PATH, 'tests', 'files', 'training_para_calc_data')
    images = get_file_paths_in_folder(os.path.join(data_dir, 'images'))
    masks = get_file_paths_in_folder(os.path.join(data_dir, 'masks'))
    return images, masks


def get_simulation_dataset(model, num_samples, input_size, channels, input_depth=None):
    params = FixedParameters()
    params.architecture = model.__class__.__name__
    if params.architecture in constants.TWO_D_NETWORKS:
        dataset = Dataset2DSimulator(num_samples, input_size, channels)
    elif params.architecture in constants.THREE_D_NETWORKS:
        dataset = Dataset3DSimulator(num_samples, input_size, input_depth, channels)
    else:
        raise NotImplementedError('RNNs are currently not supported')
    return dataset


def get_data_loader(model,
                    num_samples,
                    batch_size,
                    channels,
                    input_size,
                    input_depth=None,
                    custom_num_workers=None,
                    ):

    dataset = get_simulation_dataset(
        model, num_samples, input_size, channels, input_depth
    )

    num_workers, _, prefetch_factor, _ = get_num_workers_and_prefetch(
        online_aug=False,
        architecture=model.__class__.__name__,
        batch_size=batch_size,
        channels=channels,
        input_size=input_size,
        input_depth=input_depth,
        train_dataset=dataset,
        valid_dataset=dataset,
    )

    if custom_num_workers is not None:
        num_workers = custom_num_workers
    if num_workers == 0:
        prefetch_factor = 2

    data_loader = DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        drop_last=False,
        prefetch_factor=prefetch_factor,
    )
    return data_loader
