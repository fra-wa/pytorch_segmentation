import logging
import os
import time
import traceback

from django.shortcuts import render, redirect

from pytorch_segmentation import constants
from pytorch_segmentation.forms.inference import InferenceInputForm
from pytorch_segmentation.inference.predict import start_prediction
from pytorch_segmentation.models import InferenceParameters
from pytorch_segmentation.temporary_parameters.parameters import FixedParameters
from pytorch_segmentation.utils import get_human_datetime, get_time_spent_string, log_error_info_and_kill_children
from pytorch_segmentation.views.optimal_parameter_calculation.inference_parameters import \
    get_optimal_inference_parameters
from pytorch_segmentation.views.optimal_parameter_calculation.utils import get_max_inference_in_size
from pytorch_segmentation.views.utils import get_and_check_running_logs_and_warn
from pytorch_segmentation.views.utils import kill_running_process_if_needed
from pytorch_segmentation.views.utils import start_process_and_create_log_file


def init_inference(parameters, process_logger):
    if not isinstance(parameters, InferenceParameters):
        raise ValueError('params must be an InferenceParameters instance!')

    process_logger.set_up_root_logger()
    try:
        logging.info(f'ProcessID_{os.getpid()}')
        start_time = time.time()
        logging.info(f'Starting inference at: {get_human_datetime()}')

        # compatibility before django was used. Todo: use django only
        params = FixedParameters()
        params.model_path = parameters.model_path
        params.data_folder = parameters.data_folder
        params.device = parameters.device
        params.save_color = parameters.save_color
        params.overlap = parameters.overlap
        params.log_thresh = float(parameters.log_thresh)
        params.weighting_strategy = parameters.weighting_strategy

        max_h_w, max_depth = get_max_inference_in_size(params.model_path, params.data_folder, logger=process_logger)

        input_size, input_depth, batch_size = get_optimal_inference_parameters(
            parameters.model_path,
            parameters.device,
            max_in_size=max_h_w,
            max_in_depth=max_depth,
        )
        params.input_size = input_size
        params.batch_size = batch_size
        params.input_depth = input_depth

        start_prediction(params, logger=process_logger, interact_with_user=False)

        time_string = get_time_spent_string(start_time)
        logging.info(f'Finished inference at: {get_human_datetime()} (time spent: {time_string})')
    except Exception:
        logging.error(traceback.format_exc())
        log_error_info_and_kill_children()
    finally:
        process_logger.remove_logger_handlers()  # To free resources


def inference(request):
    running_logs = get_and_check_running_logs_and_warn(request)
    parameters = InferenceParameters.objects.last()
    if parameters is None:
        parameters = InferenceParameters.objects.create()

    form = InferenceInputForm(instance=parameters)
    if request.method == 'POST':
        form = InferenceInputForm(request.POST, instance=parameters)
        if form.is_valid():
            fcd = form.cleaned_data

            if form.has_changed():
                parameters = form.save()

            kill_running_process_if_needed(running_logs=running_logs, device='cpu', request=request)

            log_file = start_process_and_create_log_file(
                execution_type=constants.DL_EXECUTION_INFERENCE,
                device=fcd['device'],
                process_func=init_inference,
                parameters=parameters,
            )

            return redirect('pytorch_segmentation:monitor', log_pk=log_file.pk)

    context = {
        'navbar_view': 'inference',
        'form': form,
        'process_is_running': True if running_logs else False,
    }
    return render(request, 'pytorch_segmentation/inference.html', context)
