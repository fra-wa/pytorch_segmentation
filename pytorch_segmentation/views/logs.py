from collections import OrderedDict

from django.contrib import messages
from django.shortcuts import render

from .utils import get_running_logs
from .. import constants
from ..models import LogFile


def logs(request):

    if request.method == 'POST':
        # searching?
        pass

    if len(LogFile.objects.all()) == 0:
        messages.add_message(request, messages.INFO, 'There are no Logs.')

    log_dict = OrderedDict({})

    for log_key, log_name in constants.LOG_EXECUTION_CHOICES:
        objects = LogFile.objects.filter(execution_process=log_key).order_by('-process_start_datetime')
        if objects:
            log_dict[log_name] = objects

    context = {
        'logs': log_dict,
        'running_logs': get_running_logs(),
        'navbar_view': 'logs',
    }
    return render(request, 'pytorch_segmentation/logs.html', context)
