import cv2
import logging
import numpy as np
import os

from pytorch_segmentation.file_handling.read import read_csv_to_list
from pytorch_segmentation.file_handling.write import save_csv
from pytorch_segmentation.multicore_utils import slice_multicore_parts, multiprocess_function


def get_counts(target_images, classes, process_nr):
    class_counts = np.zeros(classes)

    for i, mask in enumerate(target_images):
        if process_nr == 0:
            logging.info(f'Process 1: {i+1}/{len(target_images)}')
        if process_nr == -1:
            logging.info(f'{i+1}/{len(target_images)}')
        mask = cv2.imread(mask, cv2.IMREAD_GRAYSCALE)
        for c in range(classes):
            class_counts[c] += np.sum(mask == c)
    return class_counts


def get_weights(dataset_folder, mask_paths, classes, input_size, logger):
    class_weights_file = os.path.join(dataset_folder, f'class_weights_in_size_{input_size}.csv')
    if os.path.isfile(class_weights_file):
        class_weights = read_csv_to_list(class_weights_file)
        if len(class_weights[1]) == classes:
            logging.info('Loaded calculated class weights:')
            classes_and_weights = zip(class_weights[0], class_weights[1])
            cls_w_string = ', '.join(f'{int(cls_and_w[0])}: {cls_and_w[1]}' for cls_and_w in classes_and_weights)
            logging.info(cls_w_string)
            return np.array(class_weights[1])

    logging.info('Calculating class weights...')

    if isinstance(mask_paths[0], list):
        new_paths = []
        for path_list in mask_paths:
            new_paths += path_list
        mask_paths = new_paths

    mask_paths_map = slice_multicore_parts(mask_paths)
    classes_map = len(mask_paths_map) * [classes]
    process_nr_map = list(range(len(mask_paths_map)))

    results = multiprocess_function(get_counts, [mask_paths_map, classes_map, process_nr_map], logger=logger)
    class_counts = np.zeros(classes)

    for result in results:
        class_counts += result

    if classes == 2:
        header = 'Positive weighting (weighting the ones -> positives). Calculated by: (counts cls 0) / (counts cls 1)'
        pos_weight = np.round(class_counts[0] / class_counts[1], 2)
        weights = np.array([1, pos_weight])
    else:
        header = 'Weighting per class. Calculated by: (total pixels) / (pixels of current class), ' \
                 'loss will be normalized by the sum of the weights'
        total = np.sum(class_counts)

        weights = np.round(total / class_counts, 2)

    class_weights_data = [
        list(range(classes)),
        list(weights),
    ]

    save_csv(class_weights_file, class_weights_data, header, number_format='.2f')

    return weights
