import cv2
import logging
import math
import numpy as np
import os

from pytorch_segmentation.file_handling.utils import create_folder, copy_or_move_file_or_files
from pytorch_segmentation.file_handling.write import save_csv
from pytorch_segmentation.multicore_utils import slice_multicore_parts, multiprocess_function
from pytorch_segmentation.utils import get_max_threads, resize_cv2_or_pil_image


def get_sub_images(img, target_size):
    """
    Splits the image in sub images of size = model_input_size

    if n * model_input_size != img.shape[x], the corresponding image will will start from
    img.shape[x] - model_input_size and end at img.shape[x]
    """
    try:
        h, w, c = img.shape
    except ValueError:
        h, w = img.shape

    if h < target_size or w < target_size:
        raise RuntimeError('Image is too small to slice into sub images!')

    images_horizontal = math.ceil(w / target_size)
    images_vertical = math.ceil(h / target_size)

    if images_horizontal > 1:
        offset_w_per_img = (images_horizontal * target_size - w) // (images_horizontal - 1)
    else:
        offset_w_per_img = 0
    if images_vertical > 1:
        offset_h_per_img = (images_vertical * target_size - h) // (images_vertical - 1)
    else:
        offset_h_per_img = 0

    w_coordinates = [i * (target_size - offset_w_per_img) for i in range(images_horizontal)]
    h_coordinates = [i * (target_size - offset_h_per_img) for i in range(images_vertical)]
    w_coordinates[-1] = w - target_size
    h_coordinates[-1] = h - target_size

    images = []
    sub_coordinate_origin_per_image = []
    for h_start in h_coordinates:
        # save images "line by line" to put them back together to one image
        for w_start in w_coordinates:
            h_end = h_start + target_size
            w_end = w_start + target_size

            sub_image = img[h_start:h_end, w_start:w_end]

            images.append(sub_image)
            sub_coordinate_origin_per_image.append((h_start, w_start))

    return images, sub_coordinate_origin_per_image


def slice_image_and_mask(img,
                         mask,
                         img_path,
                         mask_path,
                         input_size,
                         images_folder,
                         masks_folder):

    # create sub images and save sub images
    images, _ = get_sub_images(img, input_size)
    masks, _ = get_sub_images(mask, input_size)

    img_name, img_extension = os.path.basename(img_path).split('.')
    mask_name, mask_extension = os.path.basename(mask_path).split('.')

    new_image_paths = []
    new_mask_paths = []

    for nr, (new_img, new_mask) in enumerate(zip(images, masks)):
        new_img_name = f'{img_name}_{nr}.{img_extension}'
        new_mask_name = f'{mask_name}_{nr}.{mask_extension}'
        new_img_path = os.path.join(images_folder, new_img_name)
        new_mask_path = os.path.join(masks_folder, new_mask_name)

        cv2.imwrite(new_img_path, new_img)
        cv2.imwrite(new_mask_path, new_mask)

        new_image_paths.append(new_img_path)
        new_mask_paths.append(new_mask_path)
    return new_image_paths, new_mask_paths


def multiprocess_images_and_masks_simple(image_paths,
                                         mask_paths,
                                         images_folder,
                                         masks_folder,
                                         load_gray,
                                         input_size,
                                         process_nr):
    total_images = len(image_paths)

    new_image_paths = []
    new_mask_paths = []

    for idx, (img_path, mask_path) in enumerate(zip(image_paths, mask_paths)):
        if process_nr == 1:
            logging.info(f'Process {process_nr}: Processed {idx + 1}/{total_images} images    ')

        if load_gray:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        else:
            img = cv2.imread(img_path)

        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

        h, w, = img.shape[:2]
        h_mask, w_mask = mask.shape

        if h != h_mask or w != w_mask:
            raise ValueError('Masks and images are not of same shape. Make sure each image has a related mask.')

        if h < input_size or w < input_size:
            # enlarge image with min edge size = input_size
            img = resize_cv2_or_pil_image(
                img, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
            )
            mask = resize_cv2_or_pil_image(
                mask, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
            )
            h, w = img.shape[:2]

        if h > input_size or w > input_size:

            sliced_img_paths, sliced_mask_paths = slice_image_and_mask(
                img, mask, img_path, mask_path, input_size, images_folder, masks_folder,
            )
            new_image_paths += sliced_img_paths
            new_mask_paths += sliced_mask_paths

        else:
            assert h == w
            new_img_path = os.path.join(images_folder, os.path.basename(img_path))
            new_mask_path = os.path.join(masks_folder, os.path.basename(mask_path))

            if not os.path.isfile(new_img_path):
                cv2.imwrite(new_img_path, img)
            if not os.path.isfile(new_mask_path):
                cv2.imwrite(new_mask_path, mask)

            new_image_paths.append(new_img_path)
            new_mask_paths.append(new_mask_path)

    return new_image_paths, new_mask_paths


def multiprocess_sequences_for_recurrent_and_3d(images_folder,
                                                masks_folder,
                                                image_sequences,
                                                mask_sequences,
                                                load_gray,
                                                input_size,
                                                process_nr):
    total_datasets = len(image_sequences)

    for idx, img_paths in enumerate(image_sequences):
        if process_nr == 1:
            logging.info(f'Process {process_nr}: Processed {idx + 1}/{total_datasets} sub datasets  ')
        mask_paths = mask_sequences[idx]

        img_sequence_name = os.path.basename(os.path.dirname(img_paths[0]))
        mask_sequence_name = os.path.basename(os.path.dirname(mask_paths[0]))
        img_sequence_path = os.path.join(images_folder, img_sequence_name)
        mask_sequence_path = os.path.join(masks_folder, mask_sequence_name)

        create_folder(img_sequence_path)
        create_folder(mask_sequence_path)

        image_sequence_paths = [img_sequence_path]
        mask_sequence_paths = [mask_sequence_path]
        created_new_sequences = False

        for img_path, mask_path in zip(img_paths, mask_paths):
            if load_gray:
                img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
            else:
                img = cv2.imread(img_path)
            mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

            h, w, = img.shape[:2]
            h_mask, w_mask = mask.shape

            if h != h_mask or w != w_mask:
                raise ValueError('Masks and images are not of same shape. Make sure each image has a related mask.')

            if h < input_size or w < input_size:
                # TODO: for 3D --> load vol resize vol!
                # enlarge image with min edge size = input_size
                img = resize_cv2_or_pil_image(
                    img, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
                )
                mask = resize_cv2_or_pil_image(
                    mask, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
                )
                h, w = img.shape[:2]

            if h > input_size or w > input_size:
                # create sub images
                images, _ = get_sub_images(img, input_size)
                masks, _ = get_sub_images(mask, input_size)

                # create new sequence folders
                if not created_new_sequences:

                    for j in range(1, len(images)):
                        new_img_sequence_folder = image_sequence_paths[0] + '_' + str(j)
                        new_mask_sequence_folder = mask_sequence_paths[0] + '_' + str(j)
                        create_folder(new_img_sequence_folder)
                        create_folder(new_mask_sequence_folder)

                        image_sequence_paths.append(new_img_sequence_folder)
                        mask_sequence_paths.append(new_mask_sequence_folder)
                    created_new_sequences = True

                img_name = os.path.basename(img_path)
                mask_name = os.path.basename(mask_path)

                # and save sub images
                for nr, (new_img, new_mask) in enumerate(zip(images, masks)):
                    new_img_path = os.path.join(image_sequence_paths[nr], img_name)
                    new_mask_path = os.path.join(mask_sequence_paths[nr], mask_name)

                    cv2.imwrite(new_img_path, new_img)
                    cv2.imwrite(new_mask_path, new_mask)
            else:
                if h != w:
                    raise RuntimeError(f'Something went wrong at\n'
                                       f'img: {img_path}\n'
                                       f'mask: {mask_path}'
                                       f'Height after resizing is not equal to width: h = {h}, w = {w}')
                new_img_path = os.path.join(img_sequence_path, os.path.basename(img_path))
                new_mask_path = os.path.join(mask_sequence_path, os.path.basename(mask_path))
                if not os.path.isfile(new_img_path):
                    cv2.imwrite(new_img_path, img)
                if not os.path.isfile(new_mask_path):
                    cv2.imwrite(new_mask_path, mask)


def convert_color_masks_to_label_masks(mask_paths, mapped_labels_map, process_nr):
    """
    Takes a look onto the mask and saves the human readable mask to trainable masks

    Args:
        mask_paths:
        mapped_labels_map:
        process_nr: if 1, printing is enabled

    Returns:

    """
    if not isinstance(mapped_labels_map, dict):
        raise ValueError('mapped_labels_map must be a dict like: {label: color_or_gray_value}')
    label_list = sorted(mapped_labels_map.keys())
    color_list = [mapped_labels_map[label] for label in label_list]

    total_iterations = len(mask_paths)
    for idx, path in enumerate(mask_paths):
        if process_nr == 0:
            logging.info(f'Process {process_nr}: mask {idx + 1}/{total_iterations}')

        img = cv2.imread(path, -1)
        mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
        for label, color in zip(label_list, color_list):
            if color is None:
                continue

            if len(color) == 3:
                # np.where(img[:, :, :] == color[:]) wont work
                # this is the fastest way. see last answer:
                # https://stackoverflow.com/questions/25823608/find-matching-rows-in-2-dimensional-numpy-array
                indexes = np.where((img[:, :, 0] == color[0]) & (img[:, :, 1] == color[1]) & (img[:, :, 2] == color[2]))
                mask[indexes] = label
            else:
                mask[img == color[0]] = label

        folder = os.path.dirname(path)
        backup = os.path.join(folder, 'original')
        if not os.path.isdir(backup):
            raise FileNotFoundError(f'Backup folder {backup} was not created! Create before multiprocessing!')
        copy_or_move_file_or_files(path, backup)

        cv2.imwrite(path, mask)


def multicore_mask_check(mask_paths, process_nr):
    unique_colors = []
    total_iterations = len(mask_paths)
    for idx, path in enumerate(mask_paths):
        if process_nr == 1:
            logging.info(f'Process {process_nr}: mask {idx + 1}/{total_iterations}')

        img = cv2.imread(path, -1)  # -1 triggers: load as is and not default to bgr
        if len(img.shape) == 3:
            unique_labels = np.unique(img.reshape(-1, img.shape[2]), axis=0)
        else:
            unique_labels = np.unique(img)

        for color in unique_labels:
            try:
                color = list(color)
            except TypeError:
                color = [color]
            if color not in unique_colors:
                unique_colors.append(color)
    return unique_colors


def get_mapped_label_to_color_or_gray(mask_paths, label_count, logger, threads=None, process_nr=0):
    """
    Iterates over all masks and creates a dictionary containing the labels and related colors.

    Args:
        mask_paths:
        label_count: maximum classes to separate into
        logger: needed for multiprocessing
        threads: if this is already in a multiprocess call
        process_nr: if this is already in a multiprocess call

    Returns: dict like: {label: color_or_gray_value}

    """
    logging.info('Getting unique labels of whole dataset to ensure consistency.')

    if threads is None:
        threads = get_max_threads(logical=True)
    if threads == 1 or len(mask_paths) < 100:
        unique_colors = multicore_mask_check(mask_paths, 1)
    else:
        masks_map = slice_multicore_parts(mask_paths, total_maps=threads)
        process_nr_map = len(masks_map) * [0]
        process_nr_map[0] = 1
        maps = [masks_map, process_nr_map]

        results = multiprocess_function(multicore_mask_check, maps, logger=logger)
        unique_colors = []
        for result in results:
            for color in result:
                try:
                    color = list(color)
                except TypeError:
                    color = [color]
                if color not in unique_colors:
                    unique_colors.append(color)

    if label_count > len(unique_colors):
        logging.info('########################### WARNING ###########################')
        logging.info(f'Found less labels ({len(unique_colors)}) than class count ({label_count}).')
        logging.info(f'Folder: {os.path.dirname(mask_paths[0])}')
        logging.info('###############################################################')
    elif label_count < len(unique_colors):
        raise ValueError(f'Found {len(unique_colors)} labels, but {label_count} classes are set by the user.')

    mapped = {}
    for label in range(label_count):
        mapped[label] = None

    for label, color in enumerate(sorted(unique_colors)):
        mapped[label] = color
    if process_nr == 0:
        logging.info(f'Got labels for masks at: {os.path.dirname(mask_paths[0])}. Maps are:')
        logging.info('Label: [color or gray value]')
    label_list = sorted(mapped.keys())
    color_or_gray_list = [mapped[label] for label in label_list]
    for label, color_or_gray in zip(label_list, color_or_gray_list):
        if process_nr == 0:
            logging.info(f'{label}: {color_or_gray}')
    return mapped


def check_or_save_colorspace_valid_file(dataset_folder, label_count, save_valid_file=False):
    folder_is_valid_file = os.path.join(dataset_folder, 'masks_are_valid.txt')
    folder_is_valid_file_header = 'This file points out, that the masks are usable to train a neural network of ' \
                                  'pytorch_segmentation\n' \
                                  'project by Franz Wagner.\nClasses:'

    if save_valid_file:
        save_csv(folder_is_valid_file, [label_count], folder_is_valid_file_header)

    if os.path.isfile(folder_is_valid_file):
        logging.info(f'!!! If you get a warning, please remove the file: {folder_is_valid_file} and reprocess data !!!')
        return True


def check_correct_mask_color_space(mask_paths,
                                   label_count,
                                   threads=None,
                                   process_nr=0,
                                   logger=None):
    """

    Args:
        mask_paths:
        label_count:
        process_nr: if this function is already in a multiprocessing, log only process 0
        threads: if this function is already in a multiprocessing, set threads=1
        logger: for multiprocessing

    """
    if not mask_paths:
        raise ValueError('Masks are empty')

    if threads is None:
        threads = get_max_threads(logical=True)

    needs_conversion = False

    for path in mask_paths:
        img = cv2.imread(path, -1)
        if len(img.shape) == 3:
            needs_conversion = True
            break
        else:
            if np.max(np.unique(img)) >= label_count:  # eg.: 0 and 1 -> label count = 2 --> check for equal or greater
                needs_conversion = True
                break

    if not needs_conversion:
        return

    if process_nr == 0:
        logging.info(f'Performing color/grayscale to label correction for masks. Original data will be kept.')

    mapped_labels_to_color_or_gray = get_mapped_label_to_color_or_gray(
        mask_paths, label_count, logger=logger, threads=threads, process_nr=process_nr
    )

    # create backup folders for original data before multiprocessing
    for path in mask_paths:
        folder = os.path.dirname(path)
        backup = os.path.join(folder, 'original')
        create_folder(backup)
    if process_nr == 0:
        logging.info(f'Preparing masks for further usage: converting to grayscale in range: 0 to {label_count - 1}.')

    if threads == 1:
        convert_color_masks_to_label_masks(mask_paths, mapped_labels_to_color_or_gray, process_nr=0)
    else:
        masks_map = slice_multicore_parts(mask_paths, total_maps=threads, logical=True)
        mapped_labels_map = len(masks_map) * [mapped_labels_to_color_or_gray]
        mapped_process_nr_map = len(masks_map) * [0]
        mapped_process_nr_map[0] = 1

        multiprocess_function(
            convert_color_masks_to_label_masks, [masks_map, mapped_labels_map, mapped_process_nr_map], logger
        )
    if process_nr == 0:
        logging.info(f'Masks of folder: {os.path.dirname(mask_paths[0])} are now valid.')


def multi_check_mask_color_space_3d(target_sequences, classes, process_nr):
    """
    For 3d datasets --> faster than starting processes again and again if many volumes are given in the dataset.
    basically check_correct_mask_color_space but passing multiple folders for 3d for example.

    Args:
        target_sequences: list of files representing the training and validation masks
        classes:
        process_nr:
    """
    for i, files in enumerate(target_sequences):
        if process_nr == 0:
            logging.info(f'Process 0: Processing folder {i+1}/{len(target_sequences)}')
        check_correct_mask_color_space(files, classes, threads=1, process_nr=process_nr)


def multiprocess_enlarge_images_and_masks(image_paths,
                                          mask_paths,
                                          load_gray,
                                          input_size,
                                          process_nr):
    """
    Enlarges images if needed to fit into the network. Used for example at ground truth testing.

    Args:
        image_paths:
        mask_paths:
        load_gray:
        input_size:
        process_nr:

    Returns:

    """
    total_images = len(image_paths)

    backup_images_folder = os.path.join(os.path.dirname(image_paths[0]), 'original')
    if not os.path.isdir(backup_images_folder):
        raise FileNotFoundError(
            f'Could not find backup folder: {backup_images_folder}! please create before this funciton'
        )

    backup_masks_folder = os.path.join(os.path.dirname(mask_paths[0]), 'original')
    if not os.path.isdir(backup_masks_folder):
        raise FileNotFoundError(
            f'Could not find backup folder: {backup_masks_folder}! please create before this funciton'
        )

    for idx, (img_path, mask_path) in enumerate(zip(image_paths, mask_paths)):
        if process_nr == 1:
            logging.info(f'Process {process_nr}: Processed {idx + 1}/{total_images} images    ')

        if load_gray:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        else:
            img = cv2.imread(img_path)

        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

        h, w, = img.shape[:2]
        h_mask, w_mask = mask.shape

        if h != h_mask or w != w_mask:
            raise ValueError('Masks and images are not of same shape. Make sure each image has a related mask.')

        if h < input_size or w < input_size:
            # enlarge image with min edge size = input_size
            backup_img_path = os.path.join(backup_images_folder, os.path.basename(img_path))
            backup_mask_path = os.path.join(backup_masks_folder, os.path.basename(mask_path))
            cv2.imwrite(backup_img_path, img)
            cv2.imwrite(backup_mask_path, mask)

            img = resize_cv2_or_pil_image(
                img, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
            )
            mask = resize_cv2_or_pil_image(
                mask, input_size, use_pil=False, interpolation=cv2.INTER_NEAREST, invert=True
            )
            cv2.imwrite(img_path, img)
            cv2.imwrite(mask_path, mask)
