var backbone_choices;

function postCalculationRequestRequest() {
    document.getElementById("js-optimal-para-request-button").disabled = true;
    document.getElementById("js-run-button").disabled = true;
    try {
        document.getElementById("js-run-kill-button").disabled = true;
    }
    catch (TypeError) {}

    let backendForm = document.getElementById("input_form");

    let hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = 'calculate_optimal_parameters';
    hiddenField.value = 'true';
    backendForm.appendChild(hiddenField);

    backendForm.submit();
}

function updateBackboneSelect() {
    let architecture_select = document.getElementById("id_architecture");
    let backbone_select = document.getElementById("id_backbone");
    let initial_value = backbone_select.value;
    backbone_select.length = 0;

    let selected_architecture = architecture_select.options[architecture_select.selectedIndex].value;
    let backbone_list = backbone_choices[selected_architecture];

    for (let i = 0; i < backbone_list.length; i++) {
        let option = document.createElement("option");
        option.value = backbone_list[i];
        if (option.value === initial_value) {
            option.setAttribute('selected', '')
        }
        if (backbone_list[i]){
            option.text = backbone_list[i];
        } else {
            option.text = '-------------';
        }
        backbone_select.appendChild(option);
    }
}

function updateRequestButton(blocked_devices) {
    let device_select = document.getElementById("id_device");
    let selected_device = device_select.value;
    let request_button = document.getElementById("js-optimal-para-request-button");
    let calc_button_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_text;
    let calc_button_blocked_text = document.getElementsByClassName('js-data-holder')[0].dataset.calc_button_blocked_text;

    if (blocked_devices.includes(selected_device)) {
        request_button.innerHTML = calc_button_blocked_text;
        if (request_button.className.split(' ').includes('btn-primary-form')) {
            request_button.className = request_button.className.replace('btn-primary-form', 'btn-log-running');
        }
    } else {
        request_button.innerHTML = calc_button_text;
        if (request_button.className.split(' ').includes('btn-log-running')) {
            request_button.className = request_button.className.replace('btn-log-running', 'btn-primary-form');
        }
    }
}


window.addEventListener('load', function load(event) {
    let request_button = document.getElementById("js-optimal-para-request-button");
    let architecture_select = document.getElementById("id_architecture");
    let device_select = document.getElementById("id_device");
    backbone_choices = JSON.parse(document.getElementsByClassName('js-data-holder')[0].dataset.backbone_dict);
    let predicted_ram = document.getElementsByClassName('js-data-holder')[0].dataset.predicted_ram;
    let blocked_devices = document.getElementsByClassName('js-data-holder')[0].dataset.blocked_devices;
    blocked_devices = blocked_devices.replace(/ /g, '').replace(/'/g, '').replace(/\[/g, '').replace(/]/g, '').split(',')

    if (predicted_ram !== 'None') {
        let success_modal = new bootstrap.Modal(document.getElementById('js-ram-calc-success-modal'), {})
        success_modal.toggle()
    }

    updateBackboneSelect();
    updateRequestButton(blocked_devices);

    request_button.addEventListener("click", function () {
        postCalculationRequestRequest();
    });

    architecture_select.addEventListener("change", function () {
        updateBackboneSelect();
    });

    device_select.addEventListener("change", function () {
        updateRequestButton(blocked_devices);
    });

});
