
function dropdownFilter(input_field, parent_div_id) {
    let filter, ul, li, a, i;
    filter = input_field.value.toUpperCase();
    let div = document.getElementById(parent_div_id);
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        let text_value = a[i].textContent || a[i].innerText;
        if (text_value.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}


window.addEventListener('load', function load(event) {
    let dropdown_filters = document.getElementsByClassName("js-dropdown-filter-input");

    for (let i = 0; i < dropdown_filters.length; i++) {
        dropdown_filters[i].addEventListener("keyup", function () {
            dropdownFilter(this, this.dataset.parent_div_id);
        });
    }
});
