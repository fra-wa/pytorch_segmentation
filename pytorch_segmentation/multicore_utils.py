import warnings

import numpy as np
import sys

from concurrent import futures

from .utils import get_max_threads, get_possible_process_count

_MAX_WINDOWS_WORKERS = 60  # default of 61 by ProcessPoolExecutor() results in an error.
# Using 60 works --> current process?


def get_max_process_count(logical, max_ram_in_gb=None):
    """
    Created to prevent python/windows bugs

    Args:
        logical:
        max_ram_in_gb: if None, the projects default is used. For pytorch workers: set for example 0.1 - no offset is
            needed, pytorch is smart. Otherwise, the imported libs have to be copied to every process -> this is default

    Returns:

    """
    """python bug!"""
    # python bug: max allowed processes in multithreading is 61!: https://bugs.python.org/issue26903
    # In short: multiprocessing is using a thread per process to track the processes, which is quite wasteful on
    # both unix and windows where nonblocking primitives exist, but nevertheless, thats what it is doing. I
    # need to limit this to 60 processes to avoid the limit in question as a result.

    # I use the ProcessPoolExecutor as default to speed up cpu bound tasks! The Bug is also mentioned at the docs:
    # https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ProcessPoolExecutor

    process_count = get_possible_process_count(logical=logical, logging_active=True)
    usable_processes = process_count

    if sys.platform == 'win32' and process_count > _MAX_WINDOWS_WORKERS:
        usable_processes = 60

    max_threads = get_max_threads(logical, logging_active=False, max_ram_in_gb=max_ram_in_gb)  # already logged before
    if usable_processes > max_threads:
        usable_processes = max_threads
    if usable_processes < 1:
        usable_processes = 1
    return usable_processes, process_count


def get_slice_counts(data_length, part_count):
    """
    Calculates the length per part to slice the data

    Examples:
        data_length = 5, part_count = 2 --> returns [3, 2]
        data_length = 5, part_count = 8 --> returns [1, 1, 1, 1, 1, 0, 0, 0]

    Args:
        data_length: length of the data to slice
        part_count: number of parts to slice the data in

    Returns:

    """
    if part_count < 1:
        raise ValueError('You cant slice data into 0 or less parts..')

    count_per_process = int(data_length / part_count)
    rest = data_length % part_count

    slice_counts = [count_per_process for i in range(part_count)]
    idx = 0
    while rest > 0:
        slice_counts[idx] += 1
        rest -= 1
        idx += 1

    return slice_counts


def get_multicore_counts(data_length, logical=True):
    """
    Calculates even slices of a given total amount of steps for a calculation for multicore processing.

    Uses -2 threads or -1 logical core so that you can use your computer for other stuff.

    Args:
        data_length: e.g. length of a list which will be processed by multiple processes
        logical: True for logical, False for physical

    Returns: calculation steps per process as list

    Examples:
        Suppose you want to process 1000 images, represented as a list
        Suppose you have 4 cores
        Then this will slice 1000 into 3 even parts (one process left for current process):
        [334, 334, 332]

    """
    usable_processes, total_processes_count = get_max_process_count(logical)

    if logical and usable_processes == total_processes_count:
        usable_processes -= 2
    elif usable_processes == total_processes_count:
        usable_processes -= 1

    return get_slice_counts(data_length, usable_processes)


def slice_list_equally(data, parts):
    """
    Slices a list into equally sized sub lists.

    Examples:
        data = [1, 2, 3, 4, 5], parts = 2 --> returns [[1, 2, 3], [4, 5]]
        data = [1, 2, 3, 4, 5], parts = 7 --> returns [[1], [2], [3], [4], [5], [], []]

    Args:
        data: data to slice
        parts: number of parts to slice the data in

    Returns: list of lists containing the sliced data

    """
    if not isinstance(data, list):
        raise ValueError('data must be a list instance.')

    slice_counts = get_slice_counts(len(data), parts)

    slice_ranges = [(0, slice_counts[0])]
    for i in range(1, len(slice_counts)):
        previous_end = slice_ranges[i - 1][1]
        slice_ranges.append((previous_end, previous_end + slice_counts[i]))

    sliced_data = []
    for start, end in slice_ranges:
        sliced_data.append(data[start:end])
    return sliced_data


def slice_volume_equally(volume, parts=None):
    """
     Slices a volume into equally sized sub volumes.

    Examples:
        E1:
        volume = [
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 1, 1],
        ]
        parts = 2
        --> returns [sub_vol_1, sub_vol_2] like:
                [
                    np.ndarray([
                        [1, 1, 1, 1],
                        [1, 1, 1, 1]
                    ]),
                    np.ndarray([
                        [1, 1, 1, 1]
                    ]),
                ]
        E2:
        volume = [
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 1, 1],
        ]
        parts = 4
        --> returns [sub_vol_1, sub_vol_2, sub_vol_3, sub_vol_4] like:
                [
                    np.ndarray([[1, 1, 1, 1]]),
                    np.ndarray([[1, 1, 1, 1]]),
                    np.ndarray([[1, 1, 1, 1]]),
                    np.ndarray([[]])
                ]

    Args:
        volume: volume to slice
        parts: number of parts to slice the data in

    Returns: list of lists containing the sliced volumes

    """
    if not isinstance(volume, np.ndarray):
        raise ValueError('volume must be a np.ndarray instance')

    d, h, w = volume.shape
    if parts is None:
        sliced_depths_list = get_multicore_counts(d)
    else:
        sliced_depths_list = get_slice_counts(d, parts)

    volumes = []
    slice_index = 0

    for depth_part in sliced_depths_list:
        start = slice_index
        stop = slice_index + depth_part
        sliced_volume = volume[start:stop]
        volumes.append(sliced_volume)

        slice_index += depth_part

    return volumes


def slice_equal_parts(data, parts):
    """

    Args:
        data: list/np.array of data or a 3D numpy array
        parts: number of how many parts the data should split in

    Returns: sliced data as nested list

    Examples:
        part = 2
        data = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        result = [[0, 1, 2, 3, 4], [5, 6, 7, 8]]

        A volume will be sliced by it's depth.

    """
    if isinstance(data, np.ndarray):
        if len(data.shape) == 1:
            data = list(data)

    if isinstance(data, list):
        sliced_data = slice_list_equally(data, parts)
    else:
        if len(data.shape) != 3:
            warnings.warn(
                f'Got numpy array for slicing. Array is of shape: {data.shape} but expected 3D array. '
                f'Ensure correct slicing or add code.'
            )
        sliced_data = slice_volume_equally(data, parts)

    return sliced_data


def slice_multicore_parts(data, logical=True, total_maps=None):
    """
    Slices the data into equally sized lists for multicore processing.
    pass logical false if you use heavy calculations and less IO operations.

    Uses -2 threads or -1 logical core so that you can use your computer for other stuff.

    Args:
        data:
        logical: True for logical, False for physical
        total_maps: If given a number of total maps, you can specify the amount of processes spawned by yourself.

    Returns: data sliced into equal sub lists for multiprocessing

    """
    if total_maps is not None:
        if not isinstance(total_maps, int) or total_maps < 1:
            raise ValueError('total maps must be 1 or larger!')
        if total_maps > 60 and sys.platform == 'win32':
            print('Max usable processes on windows: 60 due to python bug!: https://bugs.python.org/issue26903')
            total_maps = 60
        sliced_parts = slice_equal_parts(data, total_maps)
        return [part for part in sliced_parts if part]

    usable_processes, total_processes_count = get_max_process_count(logical)

    if logical and usable_processes == total_processes_count:
        usable_processes -= 2
    elif usable_processes == total_processes_count:
        usable_processes -= 1

    if isinstance(data, np.ndarray):
        data_size = data.shape[0]
    else:
        data_size = len(data)
    if usable_processes > data_size:
        usable_processes = data_size

    return slice_equal_parts(data, usable_processes)


def subprocess_logging_wrapper(logging_manager, func, *args):
    logging_manager.set_up_root_logger()  # Set up the root logger to send messages to events queue
    return func(*args)


def multiprocess_function(func, maps, logger=None):
    """

    Args:
        func: function to use multiprocessing
        maps: mapped attributes as list of lists. Length of one map should not be > 60 on Windows:
            https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ProcessPoolExecutor
        logger: log process information as well

    Returns: r_p = results process --> [r_p_1, r_p_2, r_p_3, ..., r_p_n]

    """
    results = []
    if not isinstance(maps, list):
        raise ValueError('maps must be a list of lists')
    for element in maps:
        if not isinstance(element, list):
            raise ValueError('maps must be a list of lists')

    if sys.platform == 'win32' and len(maps[0]) > _MAX_WINDOWS_WORKERS:
        """python bug / windows bug"""
        # python bug: max allowed processes in multithreading is 61!: https://bugs.python.org/issue26903
        # In short: multiprocessing is using a thread per process to track the processes, which is quite wasteful on
        # both unix and windows where nonblocking primitives exist, but nevertheless, thats what it is doing. I
        # need to limit this to 60 processes to avoid the limit in question as a result.

        # I use the ProcessPoolExecutor as default to speed up cpu bound tasks! The Bug is also mentioned at the docs:
        # https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ProcessPoolExecutor
        max_workers = _MAX_WINDOWS_WORKERS
    else:
        max_workers = len(maps[0])

    maps_aux = []  # don't use maps to keep it immutable in this function
    if logger is not None:
        repeat = len(maps[0])
        maps_aux.append([logger] * repeat)
        maps_aux.append([func] * repeat)
        func = subprocess_logging_wrapper
    maps_aux += maps
    with futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
        for result in executor.map(func, *maps_aux):
            results.append(result)

    return results
