from .logging_manager import LoggingManager


class LoggerSingleton:
    
    _loggerManager = None

    @staticmethod
    def get_instance():
        if LoggerSingleton._loggerManager is None:
            LoggerSingleton._loggerManager = LoggingManager()  # set for all instances
            LoggerSingleton._loggerManager.run_logging_listener_thread()
        return LoggerSingleton._loggerManager
