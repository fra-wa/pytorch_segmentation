import logging
import os
import time
import torch

from pytorch_segmentation.inference.predictors.image_predictor import SlidingImagePredictor
from pytorch_segmentation.inference.predictors.volume_predictor import SlidingVolumePredictor

from .. import constants
from ..file_handling.utils import get_file_paths_in_folder
from pytorch_segmentation.dl_models.loading import load_model_from_checkpoint
from ..utils import get_human_datetime, get_time_spent_string


def predict(model_path,
            data_folder,
            batch_size,
            input_size,
            input_depth,
            device,
            decode_to_color,
            overlap,
            binary_logit_threshold=None,
            weight_mode='gauss',
            logger=None,
            ):
    """

    Args:
        model_path:
        data_folder:
        batch_size:
        input_size:
        input_depth:
        device:
        decode_to_color:
        overlap:
        binary_logit_threshold:
        weight_mode: 'sum', 'linear', 'gauss', see inference.utils for implementation
        logger:

    Returns:

    """
    if binary_logit_threshold is None:
        binary_logit_threshold = 0.5

    start_time = time.time()
    logging.info(f'Start time at: {get_human_datetime()}')

    model = load_model_from_checkpoint(model_path, device, True)
    image_channels = model.in_channels

    # sequenced model
    if model.__class__.__name__ in constants.STATEFUL_RECURRENT_UNETS + constants.STATELESS_RECURRENT_UNETS:
        raise RuntimeError('Currently not supported')
    elif model.__class__.__name__ in constants.THREE_D_NETWORKS:
        predictor = SlidingVolumePredictor(
            folder=data_folder,
            input_size=input_size,
            input_depth=input_depth,
            model=model,
            batch_size=batch_size,
            channels=image_channels,
            decode_to_color=decode_to_color,
            overlap=overlap,
            binary_logit_threshold=binary_logit_threshold,
            weight_mode=weight_mode,
            logger=logger
        )
    elif model.__class__.__name__ in constants.TWO_D_NETWORKS:
        predictor = SlidingImagePredictor(
            folder=data_folder,
            input_size=input_size,
            model=model,
            batch_size=batch_size,
            channels=image_channels,
            decode_to_color=decode_to_color,
            overlap=overlap,
            binary_logit_threshold=binary_logit_threshold,
            weight_mode=weight_mode,
            logger=logger,
        )
    else:
        raise RuntimeError('The loaded model is not one of the preconfigured dl_models.')

    device = torch.device(device)
    if 'cuda' in device.type:
        if not torch.cuda.is_available():
            logging.info('Cuda is not available on your system. Using cpu.')
            device = torch.device('cpu')

    predictor.predict(device)
    time_string = get_time_spent_string(start_time)
    time_in_seconds = time.time() - start_time
    logging.info(
        f'Finished prediction at: {get_human_datetime()} (time spent: {time_string}; seconds: {time_in_seconds:.1f})'
    )
    predictor.combine_images_and_masks()

    return predictor.out_folder


def start_prediction(parameters, interact_with_user=True, logger=None):
    if interact_with_user:
        logging.info(
            'Please set the model path and the folder! Other values are good by default but you can play around.\n'
            'The quality of prediction will not change but you might speed up the process.'
        )
        params = parameters.start_interaction_guide()
    else:
        try:
            params = parameters.get_fixed_parameters()
        except AttributeError:
            # already got fixed parameters
            params = parameters

    model_path = params.model_path
    data_folder = params.data_folder
    batch_size = params.batch_size
    input_size = params.input_size
    input_depth = params.input_depth
    device = params.device
    decode_to_color = params.save_color
    overlap = params.overlap
    logit_thresh = params.log_thresh
    weighting_strategy = params.weighting_strategy

    if not os.path.isfile(model_path):
        raise ValueError('Could not find model path')
    if not os.path.isdir(data_folder):
        raise ValueError('Could not find folder to predict')
    if not get_file_paths_in_folder(data_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES):
        raise ValueError(
            f"Could not find images in folder. Images must be of type: "
            f"{', '.join([constants.SUPPORTED_INPUT_IMAGE_TYPES])}!")

    predict(model_path=model_path, data_folder=data_folder, batch_size=batch_size, input_size=input_size,
            input_depth=input_depth, device=device, decode_to_color=decode_to_color, overlap=overlap,
            binary_logit_threshold=logit_thresh, weight_mode=weighting_strategy, logger=logger)
