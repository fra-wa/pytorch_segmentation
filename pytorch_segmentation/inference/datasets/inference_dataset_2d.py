import cv2
import logging
import math
import os


from torch.utils.data.dataset import Dataset

from pytorch_segmentation.multicore_utils import slice_multicore_parts, multiprocess_function
from pytorch_segmentation.training.transformations import get_gray_image_transformation
from pytorch_segmentation.training.transformations import get_rgb_image_transformation


class InferenceDataset2D(Dataset):
    def __init__(self,
                 img_paths,
                 out_folder,
                 channels,
                 input_size,
                 first_layer_padding,
                 overlap_w=0,
                 overlap_h=0,
                 dataset_mean=None,
                 dataset_std=None,
                 logger=None,):
        """
        Preprocesses images as follows:
        Examples:
            overlap = 0
            padding = 0
            input size = 500
            image size = 1500 x 1000

            idea of inference data:
            inference_data = [
                # (img_name, h, w, num_sub_images) like:
                (name, 0, 0, 6),
                (name, 500, 0, 6),
                (name, 1000, 0, 6),
                (name, 0, 500, 6),
                (name, 500, 500, 6),
                (name, 1000, 500, 6), --> last val is key for recreation
                (next img, ....), ....
            ]

            listener process checks whether an image is finished --> if num_sub_images == len(images_with_that_name).
            out images (actually logits) names are: imgName_h_w_numSubImages.bin
            if true, all logit images with same name are loaded and segmentation is recreated by listener process.

        Args:
            img_paths:
            channels:
            input_size:
            first_layer_padding: of the neural network
            overlap_w: in percent
            overlap_h: in percent
            dataset_mean:
            dataset_std:
        """

        if channels not in [1, 3]:
            raise ValueError('Channels must be 1 or 3')
        self.channels = channels

        self.img_paths = img_paths
        self.out_folder = out_folder

        self.inference_data = [
            # (img_path, h, w, num_images),
        ]
        self.logger = logger

        if channels == 1:
            self.img_transform = get_gray_image_transformation(dataset_mean, dataset_std)
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation(dataset_mean, dataset_std)
        else:
            raise NotImplementedError(f'{channels} channels are not supported.')

        self.input_size = input_size
        self.overlap_w = math.ceil(
            (self.input_size - 2 * first_layer_padding) * overlap_w / 100 + 2 * first_layer_padding
        )
        self.overlap_h = math.ceil(
            (self.input_size - 2 * first_layer_padding) * overlap_h / 100 + 2 * first_layer_padding
        )

        self.setup_items()

    def __getitem__(self, i):
        img_path, h, w, num_sub_images = self.inference_data[i]
        if self.channels == 1:
            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        elif self.channels == 3:
            img = cv2.cvtColor(cv2.imread(img_path, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        img = img[h:h+self.input_size, w:w+self.input_size]

        img = self.img_transform(img)
        img_name = os.path.basename(img_path).split('.')[0]
        out_path = os.path.join(self.out_folder, f'{img_name}_{h}_{w}_{num_sub_images}.bin')
        return img, out_path

    def __len__(self):
        return len(self.inference_data)

    def multi_items_setup_helper(self, img_paths, process_nr):
        out_data = []
        for i, img_path in enumerate(img_paths):
            if process_nr == 0:
                logging.info(f'Process 1: Image {i + 1}/{len(img_paths)}')
            elif process_nr == -1:
                logging.info(f'Image {i + 1}/{len(img_paths)}')
            if self.channels == 1:
                img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
            else:
                img = cv2.imread(img_path, cv2.IMREAD_COLOR)

            img_w = img.shape[1]
            img_h = img.shape[0]

            if img_w < self.input_size or img_h < self.input_size:
                raise RuntimeError(
                    f'Image too small! Image is of size: {img_h}x{img_w}, input size is: {self.input_size}!'
                )

            increment_w = self.input_size - self.overlap_w
            increment_h = self.input_size - self.overlap_h

            if increment_w <= 0:
                raise RuntimeError(
                    f'Can not width-overlap {self.overlap_w} pixels with an input size of {self.input_size}'
                )
            if increment_h <= 0:
                raise RuntimeError(
                    f'Can not height-overlap {self.overlap_h} pixels with an input size of {self.input_size}'
                )

            w_sub_indexes = [w for w in range(0, img_w - self.input_size, increment_w)]
            w_sub_indexes.append(img_w - self.input_size)
            h_sub_indexes = [h for h in range(0, img_h - self.input_size, increment_h)]
            h_sub_indexes.append(img_h - self.input_size)

            total_images = len(w_sub_indexes) * len(h_sub_indexes)

            for h_index in h_sub_indexes:
                for w_index in w_sub_indexes:
                    out_data.append((img_path, h_index, w_index, total_images))
        return out_data

    def setup_items(self):
        logging.info('Preparing images for inference.')

        if len(self.img_paths) > 20:
            images_paths_map = slice_multicore_parts(self.img_paths)
            process_nr_map = list(range(len(images_paths_map)))
            results = multiprocess_function(
                self.multi_items_setup_helper, [images_paths_map, process_nr_map], logger=self.logger
            )
            for result in results:
                self.inference_data += result

            self.inference_data = sorted(self.inference_data, key=lambda x: x[0])
        else:
            self.inference_data = self.multi_items_setup_helper(self.img_paths, -1)
