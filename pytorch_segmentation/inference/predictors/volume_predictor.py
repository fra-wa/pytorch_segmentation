import logging
import numpy as np
import os
import time

from torch import multiprocessing

try:
    multiprocessing.set_start_method('spawn')
except RuntimeError:
    pass

from pytorch_segmentation.file_handling.read import read_binary_to_numpy_array
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder, remove_file
from pytorch_segmentation.file_handling.write import save_volume
from pytorch_segmentation.inference.datasets.inference_dataset_3d import InferenceDataset3D, Section
from pytorch_segmentation.inference.predictor_base import PredictorBase
from pytorch_segmentation.inference.utils import decode_logits, decode_segmentation_map, get_logits_weight


def get_section_index_values(section, logits_d_start_global, logits_d_end_global, volume_end, padding):
    """

    Args:
        section:
        logits_d_start_global: padding already removed -> where current recreation volume starts
        logits_d_end_global: padding already removed -> where current recreation volume ends
        volume_end:
        padding:

    Returns:

    """
    section_start = 0
    if section.d_start != 0:
        section_start += padding
    section_end = section.d_end - section.d_start
    if section.d_end != volume_end:
        section_end -= padding

    section_d_start_global = section.d_start + section_start  # position without padding in full volume
    section_d_end_global = section.d_start + section_end
    if section_d_end_global > logits_d_end_global:
        section_end = logits_d_end_global - section.d_start
        if section_end > section.input_depth - padding:
            section_end = section.input_depth - padding

    if section_d_start_global < logits_d_start_global:
        section_start = logits_d_start_global - section.d_start
        if section_start < padding:
            section_start = padding

    section_d_start_global = section.d_start + section_start  # update
    result_volume_d_start = section_d_start_global - logits_d_start_global
    return section_start, section_end, result_volume_d_start


def combine_sections(target_sections,
                     sections_for_recreation,
                     model_out_channels,
                     padding,
                     bin_folder,
                     volume_end,
                     logits_d_start_global,
                     weight_mode,
                     ):
    """

    Args:
        target_sections: this is/are the section/s which will be deleted
        sections_for_recreation: all overlapping sections needed to recreated the next few slices
        model_out_channels: clear
        padding: clear
        bin_folder: where to store binaries
        volume_end: the depth of the full volume
        logits_d_start_global: start of the current logits volume in the global context
        weight_mode: 'gauss', 'linear' or 'sum'

    Returns:

    """
    # padding removal
    logits_d_end_global = target_sections[-1].d_end
    if logits_d_end_global != volume_end:
        logits_d_end_global -= padding

    out_logits_depth = logits_d_end_global - logits_d_start_global

    height = target_sections[0].height
    width = target_sections[0].width

    result_volume = SlidingVolumeCalculator(model_out_channels, out_logits_depth, height, width)
    target_section_ids = [s.section_id for s in target_sections]
    for section in sections_for_recreation:
        section_d_start, section_d_end, result_volume_d_start = get_section_index_values(
            section, logits_d_start_global, logits_d_end_global, volume_end, padding
        )
        for i, sample in enumerate(section.samples):
            logits_file = os.path.join(bin_folder, sample.out_name)
            if not os.path.isfile(logits_file):
                raise FileNotFoundError(f'Section {section.section_id}: Missing file: {logits_file}')

            sample_data = read_binary_to_numpy_array(logits_file)
            n, d, h, w = sample_data.shape[:]
            weight_volume = get_logits_weight(weight_mode, h, w, depth=d)

            sample_data = sample_data[:, section_d_start:section_d_end]  # shape: Classes, D, H, W
            weight_volume = weight_volume[section_d_start:section_d_end]
            result_volume.add_current_logits(
                sample_data, result_volume_d_start, sample.h_start, sample.w_start, padding, False, weight_volume
            )
            if section.section_id in target_section_ids:
                remove_file(logits_file)

    return result_volume.get_avg_logits_volume(), logits_d_start_global


def recreate_if_possible(bin_folder,
                         out_folder,
                         sections,
                         files,
                         model_out_channels,
                         padding,
                         decode_to_color,
                         logit_threshold,
                         files_on_device,
                         total_volume_depth,  # used for naming of slices
                         weight_mode,
                         ):
    volume_depth = sections[-1].d_end
    for current_section, section_files in zip(sections, files):
        if current_section.ends_sections:
            start_recreation = True
            for section_file in section_files:
                if section_file not in files_on_device:
                    start_recreation = False
                    break

            if start_recreation:
                target_section_ids = current_section.ends_sections
                target_sections = sections[current_section.ends_sections[0]:current_section.ends_sections[-1] + 1]
                sections_for_recreation = sections[target_section_ids[0]: current_section.section_id + 1]

                if min(target_section_ids) - 1 >= 0:
                    logits_volume_d_start = sections[min(target_section_ids) - 1].d_end - padding
                else:
                    logits_volume_d_start = target_sections[0].d_start

                logits_volume, logits_d_start_global = combine_sections(
                    target_sections,
                    sections_for_recreation,
                    model_out_channels,
                    padding,
                    bin_folder,
                    volume_depth,
                    logits_volume_d_start,
                    weight_mode,
                )
                final_logits = np.expand_dims(logits_volume, 0)
                segmentation_map, classes = decode_logits(final_logits, model_out_channels, logit_threshold)
                final_volume = decode_segmentation_map(segmentation_map[0], classes, decode_to_color)
                save_volume(
                    final_volume,
                    out_folder,
                    multiprocessing=False,
                    start_index=logits_volume_d_start,
                    max_slices=total_volume_depth,
                )
                current_section.ends_sections = []


def volume_recreation_process(bin_folder,
                              out_folder,
                              sections,
                              model_out_channels,
                              padding,
                              decode_to_color,
                              logit_threshold,
                              segmentation_finished_file,
                              weight_mode):
    files = []  # [[section, files_list_of_section], ..]
    sections = sorted(sections, key=lambda s: s.section_id)  # simple double check of storing order
    total_depth = sections[-1].d_start + sections[-1].input_depth
    for section in sections:
        if not isinstance(section, Section):
            raise ValueError('section must be a Section instance.')
        sample_paths = []
        for sample in section.samples:
            sample_paths.append(os.path.join(bin_folder, sample.out_name))
        files.append(sample_paths)
    while True:
        try:
            files_on_device = get_file_paths_in_folder(bin_folder, extension='bin')
            if not files_on_device and os.path.isfile(segmentation_finished_file):
                remove_file(segmentation_finished_file)
                break
            elif not files_on_device:
                time.sleep(1)
                continue
        except OSError:
            # folder deleted --> likely by process or user
            break
        recreate_if_possible(
            bin_folder,
            out_folder,
            sections,
            files,
            model_out_channels,
            padding,
            decode_to_color,
            logit_threshold,
            files_on_device,
            total_depth,
            weight_mode,
        )


class SlidingVolumeCalculator:

    def __init__(self, model_out_channels, depth, height, width):
        self.logits_volume = np.zeros((model_out_channels, depth, height, width), dtype=np.float32)
        self.weighting_volume = np.zeros((model_out_channels, depth, height, width), dtype=np.uint16)

    def add_current_logits(self,
                           logits,
                           d_start,
                           h_start,
                           w_start,
                           padding,
                           reduce_depth_padding=True,
                           weight=None,
                           weight_mode='gauss'):
        """
        adds logits to the sliding volume calculator,
        reduces by padding in depth if wanted,
        padding of height and width is always reduced

        Args:
            logits: current logits representing a sub image
            d_start: start coordinate in depth
            h_start: start coordinate in height
            w_start: start coordinate in width
            padding: first layer padding -> will be removed from calculation
            reduce_depth_padding: False if the depth padding was already removed
            weight: pass a custom weighting image or use the default.
            weight_mode: 'sum', 'linear' or 'gauss'; where:
                sum: simply adds the logits together and divides by n
                linear: creates a pyramid like weighting
                gauss: uses a gaussian bell with 3 sigma and my is the center

        Returns:

        """
        n_logits, d_logits, h_logits, w_logits = logits.shape

        if weight is None:
            weight = get_logits_weight(weight_mode, h_logits, w_logits, depth=d_logits)

        weight = weight.astype(self.weighting_volume.dtype)
        if n_logits == 1:
            weight = np.expand_dims(weight, 0)
        else:
            weight = np.repeat(weight[np.newaxis, :, :, :], n_logits, axis=0)

        d_end = d_start + d_logits
        h_end = h_start + h_logits
        w_end = w_start + w_logits

        # zero out padding values
        if reduce_depth_padding:
            if d_start != 0:
                weight[:, :padding, :, :] = 0
            if d_end != self.logits_volume.shape[1]:
                weight[:, d_logits - padding:, :, :] = 0

        if h_start != 0:
            weight[:, :, :padding, :] = 0
        if h_end != self.logits_volume.shape[2]:
            weight[:, :, h_logits - padding:, :] = 0

        if w_start != 0:
            weight[:, :, :, :padding] = 0
        if w_end != self.logits_volume.shape[3]:
            weight[:, :, :, w_logits - padding:] = 0

        padding_removed_logits = weight * logits

        self.logits_volume[:, d_start:d_end, h_start:h_end, w_start:w_end] += padding_removed_logits
        self.weighting_volume[:, d_start:d_end, h_start:h_end, w_start:w_end] += weight

    def get_avg_logits_volume(self):
        return np.divide(self.logits_volume, self.weighting_volume)


class SlidingVolumePredictor(PredictorBase):

    def __init__(self,
                 folder,
                 input_size,
                 input_depth,
                 model,
                 batch_size,
                 channels=1,
                 decode_to_color=True,
                 overlap=0,
                 binary_logit_threshold=0.5,
                 weight_mode='gauss',
                 logger=None,
                 ):
        """
        Predicts small sub volumes of shape: input_size x input_size x depth

        Args:
            folder: folder of the volume
            input_size: input size of the model
            input_depth: input depth of the model
            model: the model
            batch_size: in this case: how many mini sub volumes will be predicted simultaneously
                -> sub volume is shaped (width, height, unet_depth): input_size x input_size x unet3d depth
            channels: 1 for gray, 3 for rgb, ..
            decode_to_color: if True, the segmented output of the model will be converted to rgb colors
            overlap: overlap in percentage
            binary_logit_threshold: Threshold value for the logits of a binary classification.
                Above is foreground, beneath background.
        """
        super(SlidingVolumePredictor, self).__init__(
            folder=folder,
            input_size=input_size,
            input_depth=input_depth,
            model=model,
            channels=channels,
            decode_to_color=decode_to_color,
            binary_logit_threshold=binary_logit_threshold,
            overlap=overlap,
            logger=logger,
            batch_size=batch_size,
            weight_mode=weight_mode,
        )

        assert self.model.first_layer_padding[0] == self.model.first_layer_padding[1]
        assert self.model.first_layer_padding[0] == self.model.first_layer_padding[2]

        self.inference_dataset = InferenceDataset3D(
            img_paths=self.input_images,
            out_folder=self.temp_out_folder,
            input_size=self.input_size,
            input_depth=self.input_depth,
            first_layer_padding=self.first_layer_padding,
            overlap_w=self.overlap,
            overlap_h=self.overlap,
            overlap_d=self.overlap,
            dataset_mean=self.model.dataset_mean,
            dataset_std=self.model.dataset_std,
            logger=self.logger,
        )

        if self.batch_size > len(self.inference_dataset):
            self.batch_size = len(self.inference_dataset)

        logging.info('Prediction parameters:')
        logging.info(f'Network input size:')
        logging.info(f'Height and width: {input_size}')
        logging.info(f'Depth: {input_depth}')
        logging.info(f'Batch size: {batch_size}')
        logging.info(f'Image channels: {channels}')
        logging.info(f'Overlap: {overlap} %')
        logging.info(f'Overlap weighting strategy: {weight_mode}')
        logging.info(f'Logit threshold if binary classification: {binary_logit_threshold}')

    def start_recreation_process(self):
        recreation_arguments = (
            self.temp_out_folder,
            self.out_folder,
            self.inference_dataset.sections,
            self.model.out_channels,
            self.first_layer_padding,
            self.decode_to_color,
            self.binary_logit_threshold,
            self.prediction_done_file,
            self.weight_mode,
        )
        p = multiprocessing.Process(target=volume_recreation_process, args=recreation_arguments)
        p.start()
        self.process = p
