import numpy as np

# beautiful colors
BEAUTIFUL_COLORS_RGB = np.array([
    (42, 54, 59),  # almost black background
    (232, 23, 93),  # reddish pink
    (69, 173, 168),  # darker teal
    (247, 219, 79),  # yellow
    (168, 167, 167),  # gray
    (53, 92, 125),  # darker blue
    (242, 107, 56),  # orange
    (168, 230, 206),  # mint green
    (198, 164, 119),  # dirt
    (194, 100, 154),  # brighter purple
    (255, 140, 148),  # salomon
    (232, 74, 95),  # smooth red
    (219, 209, 209),  # a brighter gray with some purple in it
    (225, 245, 196),  # olive or so.. i don't know some bright green
    (3, 27, 136),  # strong blue
    (231, 231, 231),  # bright gray
    (249, 173, 106),  # smooth orange
    (93, 110, 30),  # grass green
    (254, 206, 171),  # kind of yellow
    (153, 184, 152),  # smooth green
    (50, 50, 50),  # more colors for more classes
    (128, 0, 0),
    (0, 128, 0),
    (128, 128, 0),
    (0, 0, 128),
    (128, 0, 128),
    (0, 128, 128),
    (128, 128, 128),
    (64, 0, 0),
    (192, 0, 0),
    (64, 128, 0),
    (192, 128, 0),
    (64, 0, 128),
    (192, 0, 128),
    (64, 128, 128),
    (192, 128, 128),
    (0, 64, 0),
    (128, 64, 0),
    (0, 192, 0),
    (0, 64, 128)
])


DECODING_COLORS_RGB = np.array([
    (0, 0, 0),  # 0=background
    (128, 0, 0),
    (0, 128, 0),
    (128, 128, 0),
    (0, 0, 128),
    (128, 0, 128),
    (0, 128, 128),
    (128, 128, 128),
    (64, 0, 0),
    (192, 0, 0),
    (64, 128, 0),
    (192, 128, 0),
    (64, 0, 128),
    (192, 0, 128),
    (64, 128, 128),
    (192, 128, 128),
    (0, 64, 0),
    (128, 64, 0),
    (0, 192, 0),
    (0, 64, 128)
])
