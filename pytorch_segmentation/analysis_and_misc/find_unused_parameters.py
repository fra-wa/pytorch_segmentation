import logging
import os
import traceback

import torch
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django
django.setup()

from pytorch_segmentation.dl_models.loading import get_model
from pytorch_segmentation.views.optimal_parameter_calculation.utils import get_data_loader
from pytorch_segmentation import constants
from pytorch_segmentation.logger.logging_manager import LoggingManager


def get_targets_outputs_and_loss(input_batch, target_batch, model, loss_func, device, output_channels):
    """
    Used by train and validation loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    input_batch = input_batch.to(device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if output_channels == 1 else torch.long
    target_batch = target_batch.to(device, dtype=masks_type)

    output_batch = model(input_batch)

    if output_channels == 1:
        loss = loss_func(output_batch.squeeze(dim=1), target_batch)
    else:
        loss = loss_func(output_batch, target_batch)

    return target_batch, output_batch, loss


def train_loop(model, batch_size, input_size, input_depth=None, custom_num_workers=None):
    device = torch.device('cuda:0')
    model.train()
    model.to(device)

    loss_func = torch.nn.BCEWithLogitsLoss().to(device)

    device = torch.device('cuda:0')
    channels = model.in_channels
    output_channels = model.out_channels

    iterations = 5  # ram usage with workers is stable after 3 to 4 iterations. 5 for safety
    num_samples = iterations * batch_size
    data_loader = get_data_loader(
        model, num_samples, batch_size, channels, input_size, input_depth, custom_num_workers
    )

    model.train()
    scaler = torch.cuda.amp.GradScaler()
    with torch.cuda.amp.autocast():
        for i, (input_batch, target_batch) in enumerate(data_loader):
            targets, out_logits, loss = get_targets_outputs_and_loss(
                input_batch, target_batch, model, loss_func, device, output_channels
            )
            scaler.scale(loss).backward()
            for name, param in model.named_parameters():
                if param.grad is None:
                    logging.info(name)
            break


def main():
    input_size = 128
    kwargs = {
        'channels': 1,
        'classes': 2,
        'normalization': 'bn',
        'in_size': input_size,
        'pretrained': False,
        'device': torch.device('cuda:0'),
        'log_information': False,
    }
    model = None

    logger_name = f'log_unused_parameters'
    log_file_path = os.path.join(current_dir, logger_name + '.txt')

    print('setup logger')
    logger_manager = LoggingManager()
    logger_manager.run_logging_listener_thread()

    process_logger = logger_manager.create_logger(log_name=logger_name, log_handler=log_file_path)
    process_logger.set_up_root_logger()

    print('setup done')

    try:
        for i, (architecture, backbones) in enumerate(constants.ARCHITECTURES_AND_BACKBONES.items()):
            # if architecture != 'SegResNet':
            #     continue
            print(f'Net {i+1}/{len(constants.ARCHITECTURES_AND_BACKBONES.keys())}')
            if backbones and backbones[0] != '':
                for backbone in backbones:
                    # if backbone != 'resnext101':
                    #     continue
                    model = get_model(model_name=architecture, backbone=backbone, **kwargs)
                    logging.info(f'################## {i+1}')
                    logging.info(f'{architecture}, {backbone}')
            else:
                model = get_model(model_name=architecture, backbone=None, **kwargs)
                logging.info(f'################## {i+1}')
                logging.info(f'{architecture}')

            if architecture in constants.THREE_D_NETWORKS:
                train_loop(model, batch_size=2, input_size=input_size, input_depth=16, custom_num_workers=0)
            else:
                train_loop(model, batch_size=4, input_size=input_size, custom_num_workers=0)
        logging.info('##################')

    except Exception:
        logging.error(traceback.format_exc())
        # log_error_info_and_kill_children()
    finally:
        process_logger.remove_logger_handlers()  # To free resources
        logger_manager.stop_logging()


if __name__ == '__main__':
    main()
