import warnings

import cv2
import numpy as np
import random

from scipy.ndimage.filters import gaussian_filter

from pytorch_segmentation.data_augmentation.geometric_volume_transformations import rotate_x, rotate_z, rotate_y
from pytorch_segmentation.data_augmentation.rgb_manipulation import random_shadow


def add_noise(volume, mask):
    # increase dtype to store possible smaller and larger values than 0 and 255
    std = random.randint(5, 25)
    volume = volume.astype(np.float16)
    volume = volume + np.random.normal(0, std, volume.shape)
    volume[volume > 255] = 255
    volume[volume < 0] = 0
    volume = np.around(volume, decimals=0)
    volume = volume.astype(np.uint8)
    return volume, mask


def add_blur_3d(volume, mask, kernel_size=5, sigma=4):
    """
    Smooths a volume
    Args:
        volume:
        mask:
        kernel_size: int, this value will be applied to x, y and z
        sigma: int
    """
    truncate = (((kernel_size - 1) / 2) - 0.5) / sigma
    return gaussian_filter(volume, sigma=sigma, truncate=truncate), mask


def sharpen_3d_with_blur(volume, mask, kernel_size=5, sigma=4):
    volume = volume.astype(np.float32)
    truncate = (((kernel_size - 1) / 2) - 0.5) / sigma
    new_volume = gaussian_filter(volume, sigma=sigma, truncate=truncate)
    new_volume = volume - new_volume  # difference of blurred and normal
    new_volume = volume + new_volume  # add difference to initial image
    new_volume = np.around(new_volume, 0)
    new_volume[new_volume > 255] = 255
    new_volume[new_volume < 0] = 0
    new_volume = new_volume.astype(np.uint8)
    return new_volume, mask


def contrast(volume, mask, contrast_factor=None):
    """
    calculates the mean, subtracts the mean, multiplies with the contrast_factor
    -> negative values will get smaller, positive larger if > 1
    adds the mean again

    Args:
        volume:
        mask:
        contrast_factor:

    Returns:

    """
    if contrast_factor is None:
        contrast_factor = random.randint(50, 150) / 100
    mean = np.mean(volume)
    new_vol = volume.astype(np.float16)
    new_vol -= mean
    new_vol = new_vol * contrast_factor
    new_vol += mean
    new_vol = np.around(new_vol)
    new_vol[new_vol < 0] = 0
    new_vol[new_vol > 255] = 255
    new_vol = new_vol.astype(np.uint8)
    return new_vol, mask


def brightness(volume, mask, brightness_offset=None):
    if brightness_offset is None:
        brightness_offset = random.randint(10, 30)
        if random.randint(0, 1):
            brightness_offset *= -1
    volume = volume + brightness_offset
    volume[volume < 0] = 0
    volume[volume > 255] = 255
    volume = volume.astype(np.uint8)
    return volume, mask


def histogram_normalization_3d(volume, mask):
    """
    Standard normalization.
    Hard to explain in words, easy to understand with a video:
    https://www.youtube.com/watch?v=Y864d3kZnQI
    """
    warnings.warn('Applying 3D histogram normalization. Results might not be suited to train neural networks!')
    p = np.arange(0, 256)
    pn = np.histogram(volume, bins=p)

    d, h, w = volume.shape
    total_pixels = d * h * w

    new_volume = np.zeros_like(volume)
    cumulated_value = 0

    for gray_value, intensity_count in enumerate(pn[0]):
        cumulated_value += intensity_count
        current_value = cumulated_value / total_pixels * 255
        new_volume[volume == gray_value] = int(round(current_value))

    return new_volume, mask


def random_shadow_3d(volume, mask):
    """
    First: randomly rotates the volume
    Generates random 2D shadow of size rotated_volume height, width
    Subtracts random shadow from each 3d slice
    Rotates back to initial
    """
    assert volume.shape[0] == mask.shape[0]
    assert volume.shape[1] == mask.shape[1]
    assert volume.shape[2] == mask.shape[2]

    angle_multiplier = random.randint(0, 3)
    angle = 90 * angle_multiplier

    rotation_axis = random.randint(1, 3)  # x, y, z
    if rotation_axis == 1:
        rot_func = rotate_x
    elif rotation_axis == 2:
        rot_func = rotate_y
    else:
        rot_func = rotate_z

    new_volume, new_mask = rot_func(volume, mask, angle)

    d, h, w = new_volume.shape
    shadow_image = cv2.cvtColor(new_volume[0, :, :], cv2.COLOR_GRAY2BGR)
    shadow_image, _ = random_shadow(shadow_image, None)
    shadow_image = cv2.cvtColor(shadow_image, cv2.COLOR_BGR2GRAY)
    shadow_image = shadow_image.astype(np.float32)

    # results in negative values for shadow or 0
    shadow = shadow_image - new_volume[0, :, :]
    new_volume = new_volume.astype(np.float32)
    for i in range(d):
        new_volume[i, :, :] += shadow

    new_volume[new_volume > 255] = 255
    new_volume[new_volume < 0] = 0
    new_volume = new_volume.astype(np.uint8)

    new_volume, new_mask = rot_func(new_volume, new_mask, -angle)

    return new_volume, new_mask


def random_erasing(volume, mask, max_erasing=None):

    if max_erasing is None:
        max_erasing = random.randint(1, 5)

    d, h, w = volume.shape

    scale_factor = 1 / 10
    patch_max_d = int(scale_factor * d)
    patch_max_h = int(scale_factor * h)
    patch_max_w = int(scale_factor * w)

    if patch_max_d < 2 and patch_max_h < 2 and patch_max_w < 2:
        # volume too small
        return None, None

    patch_max_d = 2 if patch_max_d < 2 else patch_max_d
    patch_max_h = 2 if patch_max_h < 2 else patch_max_h
    patch_max_w = 2 if patch_max_w < 2 else patch_max_w

    new_volume = np.copy(volume)

    for i in range(max_erasing):
        patch_type = random.randint(1, 3)

        patch_w = random.randint(2, patch_max_w)
        patch_h = random.randint(2, patch_max_h)
        patch_d = random.randint(2, patch_max_d)

        if patch_type == 1:  # black
            patch = np.zeros((patch_d, patch_h, patch_w), dtype=np.uint8)

        elif patch_type == 2:  # white
            patch = np.full((patch_d, patch_h, patch_w), 255, dtype=np.uint8)

        else:  # noise
            patch = np.random.randint(low=0, high=255, size=(patch_d, patch_h, patch_w), dtype=np.uint8)

        d, h, w = volume.shape
        end_w = w - patch_w
        end_h = h - patch_h
        end_d = d - patch_d

        start_w = random.randint(0, end_w)
        start_h = random.randint(0, end_h)
        start_d = random.randint(0, end_d)

        new_volume[start_d:start_d + patch_d, start_h:start_h + patch_h, start_w:start_w + patch_w] = patch[:, :, :]
    return new_volume, mask
