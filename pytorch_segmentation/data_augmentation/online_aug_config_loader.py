import logging
import os

from pytorch_segmentation.constants import ONLINE_AUG_CONFIGS_FOLDER
from pytorch_segmentation.file_handling.read import read_csv_to_list
from pytorch_segmentation.file_handling.utils import get_file_paths_in_folder


def load_parameters_to_dict(file_path):
    kwargs = {}
    config_parameters = read_csv_to_list(file_path, ignore_header=True)
    for name, value in config_parameters:
        kwargs[name] = round(float(value), 3)
    return kwargs


def load_online_aug_config(dataset_name):
    kwargs = {}

    if not os.path.isdir(ONLINE_AUG_CONFIGS_FOLDER):
        logging.info(f'Could not find augmentation config for dataset: {dataset_name}, using default.')
        return kwargs

    files = get_file_paths_in_folder(ONLINE_AUG_CONFIGS_FOLDER, extension='.csv')
    file = None
    for f in files:
        if dataset_name == os.path.basename(f).split('.')[0]:
            file = f
            break

    if file is not None:
        kwargs = load_parameters_to_dict(file)
    if not kwargs:
        logging.info(f'Could not find/load augmentation config for dataset: {dataset_name}, using default.')
    return kwargs
