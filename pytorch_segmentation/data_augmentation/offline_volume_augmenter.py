from copy import deepcopy

import cv2
import logging
import numpy as np
import os
import random

from .geometric_volume_transformations import random_flip, random_tilt, random_squeeze
from .geometric_volume_transformations import random_crop
from .geometric_volume_transformations import random_resize
from .geometric_volume_transformations import rotate_x
from .geometric_volume_transformations import rotate_y
from .geometric_volume_transformations import rotate_z

from .pixel_volume_transformations import add_blur_3d
from .pixel_volume_transformations import add_noise
from .pixel_volume_transformations import contrast
from .pixel_volume_transformations import brightness
from .pixel_volume_transformations import random_erasing
from .pixel_volume_transformations import random_shadow_3d
from .pixel_volume_transformations import sharpen_3d_with_blur
from .. import constants

from ..file_handling.read import load_volume_from_folder
from ..file_handling.utils import create_folder, get_sub_folders, get_file_paths_in_folder
from ..file_handling.write import save_volume
from ..multicore_utils import slice_multicore_parts, multiprocess_function, get_max_process_count


class VolumeAugmentChain:
    def __init__(self, vol_folder, mask_folder, out_folder, operations_and_kwargs, augment_id, augment_seed=None):

        self.vol_folder = vol_folder
        self.mask_folder = mask_folder
        self.operations_and_kwargs = operations_and_kwargs
        self.augment_id = augment_id
        self.py_init_random_state = random.getstate()
        self.np_init_random_seed = np.random.get_state()
        self.augment_seed = random.randint(0, 2 ** 32 - 1) if augment_seed is None else augment_seed

        base_name = os.path.basename(vol_folder) + f'_aug_id_{self.augment_id}'
        self.out_vol_folder = os.path.join(out_folder, 'images', base_name)
        self.out_mask_folder = os.path.join(out_folder, 'masks', base_name)

    def augment(self, min_size=None):
        """
        Args:
            min_size: tuple or list or nd array. Must be able to be indexed! --> D, H, W
        """
        np.random.seed(self.augment_seed)
        random.seed(self.augment_seed)
        volume = load_volume_from_folder(self.vol_folder, multiprocessing=False)
        target = load_volume_from_folder(self.mask_folder, multiprocessing=False)

        for operation, kwargs in self.operations_and_kwargs:
            volume, target = operation(volume, target, **kwargs)
            if volume is None:
                np.random.set_state(self.np_init_random_seed)
                random.setstate(self.py_init_random_state)
                return

        d, h, w = volume.shape[:3]
        if min_size is not None:
            if d < min_size[0] or h < min_size[1] or w < min_size[2]:
                np.random.set_state(self.np_init_random_seed)
                random.setstate(self.py_init_random_state)
                return

        create_folder(self.out_vol_folder)
        create_folder(self.out_mask_folder)
        save_volume(volume, self.out_vol_folder, multiprocessing=False)
        save_volume(target, self.out_mask_folder, multiprocessing=False)
        np.random.set_state(self.np_init_random_seed)
        random.setstate(self.py_init_random_state)


class VolumeAugmenter:
    def __init__(self,
                 images_folder,
                 masks_folder,
                 min_dimension,
                 max_augmentations_per_volume,
                 rotation_angle,
                 total_augmentations=None,
                 operation_skip_list=None,
                 out_folder=None,
                 logger=None):
        """

        Args:
            images_folder:
            masks_folder:
            min_dimension: min 3D dimension, the resulting volume should have. shape -> depth, height, width
            max_augmentations_per_volume: upper border for one augmented volume.
            rotation_angle: increment of angle per rotating step in degrees until a full 360° rotation is completed
            out_folder:
            total_augmentations: number of maximum augmentations -- not all combinations are stored
            logger:
        """
        if not os.path.isdir(images_folder):
            raise FileNotFoundError(f'There is no folder named {images_folder}!')

        self.volume_folders = get_sub_folders(images_folder, depth=1)

        if not self.volume_folders:
            raise FileNotFoundError(f'The folder {images_folder} does not contain volumes')

        self.target_folders = get_sub_folders(masks_folder, depth=1)

        if len(self.volume_folders) != len(self.target_folders):
            raise RuntimeError('There must be an equal amount of volumes and masks')

        max_voxels = 0  # d, h, w

        for vol_dir, target_dir in zip(self.volume_folders, self.target_folders):
            images_count = len(get_file_paths_in_folder(vol_dir, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES))
            masks = get_file_paths_in_folder(target_dir, extension=constants.SUPPORTED_MASK_TYPES)
            masks_count = len(masks)

            if images_count != masks_count:
                raise RuntimeError(f'Unequal images and masks: Found {images_count} images and {masks_count} of volume'
                                   f' {os.path.basename(vol_dir)}')

            h, w = cv2.imread(masks[0], cv2.IMREAD_GRAYSCALE).shape[:2]
            d = masks_count

            if h * w * d > max_voxels:
                max_voxels = h * w * d

        if total_augmentations == 0:
            total_augmentations = None
        if total_augmentations is not None and total_augmentations < 1:
            raise ValueError('Setting total_augmentations < 0 is not allowed!')

        # up to 4 times: 1 volume, 1 mask, and copies, maybe more
        ram_reservation = constants.MULTICORE_RAM_RESERVATION + 4 * max_voxels / 2**30
        self.max_threads, _ = get_max_process_count(logical=False, max_ram_in_gb=ram_reservation)

        self.logger = logger

        self.out_folder = out_folder
        if self.out_folder is not None:
            create_folder(out_folder)
            images_folder = os.path.join(out_folder, 'images')
            masks_folder = os.path.join(out_folder, 'masks')
            create_folder(images_folder)
            create_folder(masks_folder)
        else:
            self.out_folder = os.path.dirname(images_folder)

        logging.info(f'Saving files to: {self.out_folder}')

        self.initial_volumes_count = len(self.volume_folders)

        self.min_dimension = min_dimension
        self.max_augmentations_per_volume = max_augmentations_per_volume
        self.total_augmentations = total_augmentations
        self.rotation_angle = rotation_angle

        self.rotation_operations = {
            'rotate_x': rotate_x,
            'rotate_y': rotate_y,
            'rotate_z': rotate_z,
        }
        self.base_operations = {
            'resize': random_resize,
        }
        self.additional_operations = {
            'random_flip': random_flip,
            'random_tilt': random_tilt,
            'random_squeeze': random_squeeze,
            'random_crop': random_crop,
            'noise': add_noise,
            'blur': add_blur_3d,
            'sharpen': sharpen_3d_with_blur,
            'contrast': contrast,
            'brightness': brightness,
            'random_shadow': random_shadow_3d,
            'random_erasing': random_erasing,
        }

        for operation in operation_skip_list:
            if operation in self.base_operations.keys():
                self.base_operations.pop(operation)
            if operation in self.additional_operations.keys():
                self.additional_operations.pop(operation)

        if operation_skip_list is None:
            self.operation_skip_list = []
        else:
            self.operation_skip_list = operation_skip_list

        logging.info('Creating augmentation chains with current settings..')
        self.augment_chains = []
        self.create_chains()
        logging.info(f'Finished. Total possible augmentations: {len(self.augment_chains)}.')
        if self.min_dimension is not None:
            logging.info(
                f'Requirement: min dimension >= '
                f'{self.min_dimension[0]}x{self.min_dimension[1]}x{self.min_dimension[2]} (DxHxW).'
            )

    def create_chain_from_existing(self, chain, func, aug_id, kwargs=None):
        """
        Creates a chain based on a previous.
        Adds an augmentation func to the previous chain funcs and ensures reproducible results by copying the seed
        and appending the new func
        """
        if kwargs is None:
            kwargs = {}
        if not isinstance(chain, VolumeAugmentChain):
            raise RuntimeError
        operations_and_kwargs = chain.operations_and_kwargs
        operations_and_kwargs.append((func, kwargs))
        vol_folder = chain.vol_folder
        mask_folder = chain.mask_folder
        seed = chain.augment_seed

        return VolumeAugmentChain(vol_folder, mask_folder, self.out_folder, operations_and_kwargs, aug_id, seed)

    def multi_create_chains(self, volumes_and_targets, process_nr):
        chains = []
        for i, (vol_dir, target_dir) in enumerate(volumes_and_targets):
            if process_nr == 0:
                logging.info(f'Process 1: Creating chain {i+1}/{len(volumes_and_targets)}')
            elif process_nr == -1:
                logging.info(f'Creating chain {i+1}/{len(volumes_and_targets)}')
            aug_count = 0

            resizing_chains = []
            if 'resize' in self.base_operations.keys():
                operations_and_kwargs = [(self.base_operations['resize'], {})]
                resizing_chains.append(
                    VolumeAugmentChain(vol_dir, target_dir, self.out_folder, operations_and_kwargs, aug_count)
                )
                aug_count += 1

            # rotate
            rotation_chains = []
            for operation, func in self.rotation_operations.items():
                for current_angle in range(self.rotation_angle, 360, self.rotation_angle):
                    operations_and_kwargs = [(func, {'angle': current_angle})]
                    rotation_chains.append(
                        VolumeAugmentChain(vol_dir, target_dir, self.out_folder, operations_and_kwargs, aug_count)
                    )
                    aug_count += 1

            # resize
            if 'resize' in self.base_operations.keys():
                for chain in rotation_chains:
                    current_chain = deepcopy(chain)  # do not overwrite existing
                    resizing_chains.append(
                        self.create_chain_from_existing(current_chain, self.base_operations['resize'], aug_count)
                    )
                    aug_count += 1

            # additional manipulation on original and all previous
            additional_chains = []
            for operation, func in self.additional_operations.items():
                if operation == 'random_crop':
                    operations_and_kwargs = [(func, {'min_size': min(self.min_dimension[1:])})]
                else:
                    operations_and_kwargs = [(func, {})]
                additional_chains.append(
                    VolumeAugmentChain(vol_dir, target_dir, self.out_folder, operations_and_kwargs, aug_count)
                )
                aug_count += 1

            for chain in rotation_chains + resizing_chains:
                for operation, func in self.additional_operations.items():
                    current_chain = deepcopy(chain)  # do not overwrite existing
                    if operation == 'random_crop':
                        kwargs = {'min_size': min(self.min_dimension[1:])}
                    else:
                        kwargs = None
                    additional_chains.append(self.create_chain_from_existing(current_chain, func, aug_count, kwargs))
                    aug_count += 1

            current_chains = rotation_chains + resizing_chains + additional_chains
            # if self.max_augmentations_per_volume:
            #     random.shuffle(current_chains)
            #     current_chains = current_chains[:self.max_augmentations_per_volume]
            chains += current_chains
        return chains

    def create_chains(self):
        volumes_and_targets = list(zip(self.volume_folders, self.target_folders))
        volumes_and_targets_map = slice_multicore_parts(volumes_and_targets)
        if len(volumes_and_targets_map) > 1:
            logging.info(f'Using {len(volumes_and_targets_map)} processes.')
            process_nr_map = list(range(len(volumes_and_targets_map)))

            results = multiprocess_function(
                self.multi_create_chains, [volumes_and_targets_map, process_nr_map], self.logger
            )

            for result in results:
                self.augment_chains += result
        else:
            logging.info(
                f'Using 1 process. A single process needs around {constants.MULTICORE_RAM_RESERVATION}GB ram. '
                f'Close applications to get more processes.'
            )
            self.augment_chains = self.multi_create_chains(volumes_and_targets, -1)
        random.shuffle(self.augment_chains)

        logging.info(
            f'Created {len(self.augment_chains)} possible chains ('
            f'{len(self.augment_chains) / self.initial_volumes_count} per volume).'
        )

    @staticmethod
    def multi_augment(aug_chains, min_size, total_augmentations, process_nr):
        count_out_volumes = True
        if total_augmentations is None:
            total_augmentations = len(aug_chains)
            count_out_volumes = False

        for i, chain in enumerate(aug_chains):
            if count_out_volumes:
                out_volumes = len(get_sub_folders(os.path.dirname(chain.out_vol_folder), depth=1))
                if process_nr == 0:
                    logging.info(
                        f'Process 1: calculating chain {i + 1}/{len(aug_chains)}. '
                        f'Valid volumes saved: {out_volumes} (target: {total_augmentations})'
                    )
                elif process_nr == -1:
                    logging.info(
                        f'Calculating chain {i + 1}/{len(aug_chains)}. '
                        f'Valid volumes saved: {out_volumes} (target: {total_augmentations})'
                    )
                if out_volumes >= total_augmentations:
                    break
            else:
                if process_nr <= 0:
                    logging.info(f'Volume {i}/{total_augmentations}')
            chain.augment(min_size)

    def augment(self):
        if self.total_augmentations is not None:
            logging.info(f'Creating {self.total_augmentations} samples.')
        else:
            logging.info(f'Creating {len(self.augment_chains)} samples.')

        aug_chain_map = slice_multicore_parts(self.augment_chains, logical=False, total_maps=self.max_threads)
        if len(aug_chain_map) == 1:
            logging.info(f'Using 1 process.')
            self.multi_augment(self.augment_chains, self.min_dimension, self.total_augmentations, -1)
        else:
            logging.info(f'Using {len(aug_chain_map)} processes.')
            min_size_map = len(aug_chain_map) * [self.min_dimension]
            total_augmentations_map = len(aug_chain_map) * [self.total_augmentations]
            process_nr_map = list(range(len(aug_chain_map)))

            multiprocess_function(
                self.multi_augment, [aug_chain_map, min_size_map, total_augmentations_map, process_nr_map], self.logger
            )

        out_volumes = len(get_sub_folders(os.path.join(self.out_folder, 'images'), depth=1))
        if self.total_augmentations is not None and out_volumes > self.total_augmentations:
            logging.info(f'Created {out_volumes} volumes. Slightly more than wanted due to multiprocessing.')
        logging.info('Finished.')
