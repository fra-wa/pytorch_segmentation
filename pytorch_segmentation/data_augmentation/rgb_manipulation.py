import warnings

import albumentations as A
import cv2
import numpy as np
import random


def channel_shuffle(img, mask):
    if len(img.shape) != 3:
        return img, mask

    channels = [0, 1, 2]
    random.shuffle(channels)

    shuffled = np.zeros_like(img, dtype=np.uint8)
    shuffled[:, :, 0] = img[:, :, channels[0]]
    shuffled[:, :, 1] = img[:, :, channels[1]]
    shuffled[:, :, 2] = img[:, :, channels[2]]
    return shuffled, mask


def color_to_hsv(img, mask):
    if len(img.shape) != 3:
        return img, mask
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    return img, mask


def to_gray(img, mask, keep_channels=True):
    """
    Args:
        img:
        mask:
        keep_channels: if True, out img will have 3 channels as well
    """
    if len(img.shape) != 3:
        return img, mask
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if keep_channels:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return img, mask


def iso_noise(img, mask):
    if len(img.shape) != 3:
        return img, mask
    transform = A.Compose([
        A.ISONoise(always_apply=True),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def rgb_shift(img, mask):
    if len(img.shape) != 3:
        return img, mask
    transform = A.Compose([
        A.RGBShift(always_apply=True),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def random_rain(img, mask):
    if len(img.shape) != 3:
        return img, mask
    if img.shape[0] <= 20 or img.shape[1] <= 20:
        return img, mask
    transform = A.Compose([
        A.RandomRain(always_apply=True, drop_length=20),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def random_fog(img, mask):
    if len(img.shape) != 3:
        return img, mask
    transform = A.Compose([
        A.RandomFog(always_apply=True),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def random_snow(img, mask):
    if len(img.shape) != 3:
        return img, mask
    transform = A.Compose([
        A.RandomSnow(always_apply=True),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def random_sun_flair(img, mask):
    if len(img.shape) != 3:
        return img, mask
    if img.shape[0] < 20 or img.shape[1] < 20:
        return img, mask
    radius = random.randint(10, img.shape[0] // 2)
    transform = A.Compose([
        A.RandomSunFlare(
            always_apply=True,
            src_radius=radius,
        ),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def random_shadow(img, mask):
    if len(img.shape) != 3:
        return img, mask
    transform = A.Compose([
        A.RandomShadow(always_apply=True),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def solarize(img, mask):
    if len(img.shape) != 3:
        return img, mask

    threshold = np.mean(img) + 10
    if threshold > 250:
        threshold = 250
    transform = A.Compose([
        A.Solarize(always_apply=True, threshold=threshold),
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def to_sepia(img, mask):
    if len(img.shape) != 3:
        return img, mask
    with warnings.catch_warnings(record=True):
        transform = A.Compose([
            A.ToSepia(always_apply=True),
        ])
        # function ToSepia uses deprecated np.matrix so i suppress it:
        # PendingDeprecationWarning: the matrix subclass is not the recommended way to represent matrices or deal with
        # linear algebra (see https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html). Please adjust your
        # code to use regular ndarray.
        #   self.sepia_transformation_matrix = np.matrix(
        transformed = transform(image=img)
        img = transformed['image']
    return img, mask


def multiplicative_noise(img, mask):
    version = random.randint(1, 5)
    if version == 1:
        operation = A.MultiplicativeNoise(multiplier=0.5, p=1)
    elif version == 2:
        operation = A.MultiplicativeNoise(multiplier=1.5, p=1)
    elif version == 3:
        operation = A.MultiplicativeNoise(multiplier=(0.5, 1.5), per_channel=True, p=1)
    elif version == 4:
        operation = A.MultiplicativeNoise(multiplier=(0.5, 1.5), elementwise=True, p=1)
    else:
        operation = A.MultiplicativeNoise(multiplier=(0.5, 1.5), elementwise=True, per_channel=True, p=1)

    transform = A.Compose([
        operation,
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask


def jpeg_compression(img, mask):
    version = random.randint(1, 5)
    if version == 1:
        operation = A.JpegCompression(quality_lower=99, quality_upper=100, p=1)
    elif version == 2:
        operation = A.JpegCompression(quality_lower=59, quality_upper=60, p=1)
    elif version == 3:
        operation = A.JpegCompression(quality_lower=39, quality_upper=40, p=1)
    elif version == 4:
        operation = A.JpegCompression(quality_lower=19, quality_upper=20, p=1)
    else:
        operation = A.JpegCompression(quality_lower=0, quality_upper=1, p=1)
    transform = A.Compose([
        operation,
    ])
    transformed = transform(image=img)
    img = transformed['image']
    return img, mask
