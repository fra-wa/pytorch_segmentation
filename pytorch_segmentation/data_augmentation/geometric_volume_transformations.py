import math
import numpy as np
import random

from scipy.ndimage import map_coordinates
from scipy.ndimage import zoom
from scipy.spatial.transform import Rotation as R

from .geometric_transformations import rotate_by_angle
from .geometric_transformations import tilt_backwards
from .geometric_transformations import tilt_forward
from .geometric_transformations import tilt_left
from .geometric_transformations import tilt_right


def apply_2d_function_to_volume(volume, masked_volume, function, **kwargs):
    """
    Iterates through the volume in z direction. Manipulate your volume if you want a different axis
    """
    d, h, w = volume.shape

    img = volume[0, :, :]
    mask = masked_volume[0, :, :]

    first_manipulated_image, first_manipulated_mask = function(img, mask, **kwargs)

    new_volume = np.zeros((d, first_manipulated_image.shape[0], first_manipulated_image.shape[1]), dtype=np.uint8)
    new_mask = np.zeros((d, first_manipulated_image.shape[0], first_manipulated_image.shape[1]), dtype=np.uint8)

    new_volume[0, :, :] = first_manipulated_image[:, :]
    new_mask[0, :, :] = first_manipulated_mask[:, :]

    for z in range(1, d):
        img = volume[z, :, :]
        mask = masked_volume[z, :, :]
        img, mask = function(img, mask, **kwargs)
        new_volume[z, :, :] = img[:, :]
        new_mask[z, :, :] = mask[:, :]
    return new_volume, new_mask


def random_resize(volume, mask, scale_factor=None):
    if scale_factor is None:
        percentage = random.randint(80, 120)
        scale_factor = percentage / 100

    new_volume = zoom(volume, (scale_factor, scale_factor, scale_factor))
    new_mask = zoom(mask, (scale_factor, scale_factor, scale_factor), mode='nearest', order=0)
    return new_volume, new_mask


def rotate_3d(volume, rot_x, rot_y, rot_z, crop_volume=True, interpolation='nearest'):
    """
    Rotates the whole volume using rotation matrices
    Args:
        volume: volume to rotate
        rot_x: rotation in x in degrees (corresponds to rotate clockwise around width direction)
        rot_y: rotation in y in degrees (corresponds to rotate clockwise around height direction)
        rot_z: rotation in z in degrees (corresponds to rotate clockwise around depth direction)
        crop_volume: whether or not to crop the resulting volume so that no zeros occur
        interpolation: interpolation used by scipy

    Returns: rotated volume

    """
    raise NotImplementedError('this is WIP')
    # if not isinstance(volume, np.ndarray) or not len(volume.shape) == 3:
    #     raise ValueError('Volume must be a 3d np array shaped like: depth x height x width')
    #
    # # convert rotation angles to rotation matrix
    #
    # # create mesh grid
    # d, h, w = volume.shape
    # ax = np.arange(w)
    # ay = np.arange(h)
    # az = np.arange(d)
    # coordinates = np.meshgrid(ax, ay, az)
    #
    # # stack the mesh grid to position vectors, center them around 0 by subtracting dim/2
    # xyz = np.vstack([
    #     coordinates[2].reshape(-1) - float(w) / 2,  # x coordinate, centered
    #     coordinates[1].reshape(-1) - float(h) / 2,  # y coordinate, centered
    #     coordinates[0].reshape(-1) - float(d) / 2   # z coordinate, centered
    # ])
    #
    # # create transformation matrix
    # r = R.from_euler('zyx', [rot_z, rot_y, rot_x], degrees=True)
    # rot_mat = r.as_matrix()
    #
    # # apply transformation
    # transformed_xyz = np.dot(rot_mat, xyz)
    #
    # # extract coordinates
    # x = transformed_xyz[0, :] + float(w) / 2
    # y = transformed_xyz[1, :] + float(h) / 2
    # z = transformed_xyz[2, :] + float(d) / 2
    #
    # mapped_coordinates = [z, y, x]
    #
    # rotated_volume = map_coordinates(volume, mapped_coordinates, order=1)
    # rotated_volume = np.round(rotated_volume)
    # rotated_volume[rotated_volume < 0] = 0
    # rotated_volume[rotated_volume > 255] = 255
    # return rotated_volume


def rotate_x(volume, mask, angle):
    """
    Rotate around x by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    if angle == 0:
        return volume, mask
    d, h, w = volume.shape

    first_image = volume[:, :, 0]
    first_mask = mask[:, :, 0]
    first_rotated_image, first_rotated_mask = rotate_by_angle(first_image, first_mask, angle)

    new_volume = np.zeros((first_rotated_image.shape[0], first_rotated_image.shape[1], w), dtype=np.uint8)
    new_mask = np.zeros((first_rotated_image.shape[0], first_rotated_image.shape[1], w), dtype=np.uint8)

    new_volume[:, :, 0] = first_rotated_image[:, :]
    new_mask[:, :, 0] = first_rotated_mask[:, :]

    for x in range(1, w):
        rotated_image = volume[:, :, x]
        rotated_mask = mask[:, :, x]
        rotated_image, rotated_mask = rotate_by_angle(rotated_image, rotated_mask, angle)
        new_volume[:, :, x] = rotated_image[:, :]
        new_mask[:, :, x] = rotated_mask[:, :]
    return new_volume, new_mask


def rotate_y(volume, mask, angle):
    """
    Rotate around y by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    if angle == 0:
        return volume, mask
    d, h, w = volume.shape
    d_mask, h_mask, w_mask = mask.shape
    assert d == d_mask
    assert h == h_mask
    assert w == w_mask

    first_image = volume[:, 0, :]
    first_mask = mask[:, 0, :]
    first_rotated_image, first_rotated_mask = rotate_by_angle(first_image, first_mask, angle)

    new_volume = np.zeros((first_rotated_image.shape[0], h, first_rotated_image.shape[1]), dtype=np.uint8)
    new_mask = np.zeros((first_rotated_image.shape[0], h, first_rotated_image.shape[1]), dtype=np.uint8)

    new_volume[:, 0, :] = first_rotated_image[:, :]
    new_mask[:, 0, :] = first_rotated_mask[:, :]

    for y in range(1, h):
        rotated_image = volume[:, y, :]
        rotated_mask = mask[:, y, :]
        rotated_image, rotated_mask = rotate_by_angle(rotated_image, rotated_mask, angle)
        new_volume[:, y, :] = rotated_image[:, :]
        new_mask[:, y, :] = rotated_mask[:, :]
    return new_volume, new_mask


def rotate_z(volume, mask, angle):
    """
    Rotate around z by a given angle. I recommend using multiprocessing and stacking the images later.
    """
    if angle == 0:
        return volume, mask
    d, h, w = volume.shape

    first_image = volume[0, :, :]
    first_mask = mask[0, :, :]
    first_rotated_image, first_rotated_mask = rotate_by_angle(first_image, first_mask, angle)

    new_volume = np.zeros((d, first_rotated_image.shape[0], first_rotated_image.shape[1]), dtype=np.uint8)
    new_mask = np.zeros((d, first_rotated_image.shape[0], first_rotated_image.shape[1]), dtype=np.uint8)

    new_volume[0, :, :] = first_rotated_image[:, :]
    new_mask[0, :, :] = first_rotated_mask[:, :]

    for z in range(1, d):
        rotated_image = volume[z, :, :]
        rotated_mask = mask[z, :, :]
        rotated_image, rotated_mask = rotate_by_angle(rotated_image, rotated_mask, angle)
        new_volume[z, :, :] = rotated_image[:, :]
        new_mask[z, :, :] = rotated_mask[:, :]
    return new_volume, new_mask


def execute_random_tilt(volume, mask, *args, **kwargs):
    start_factor = 1 / 4
    end_factor = 3 / 4

    if len(args) == 2 and ('start_factor' in kwargs.keys() or 'end_factor' in kwargs.keys()):
        raise ValueError('execute_random_tilt takes either arguments or keyword arguments. Not both')

    if args and len(args) == 2:
        start_factor = min(args)
        end_factor = max(args)
        assert start_factor >= 0
        assert end_factor > start_factor
        assert end_factor <= 1

    if 'start_factor' in kwargs.keys():
        start_factor = kwargs['start_factor']
    if 'end_factor' in kwargs.keys():
        end_factor = kwargs['end_factor']

    if 'func' in kwargs.keys():
        function = kwargs['func']
    else:
        functions = [tilt_forward, tilt_backwards, tilt_left, tilt_right]
        function = random.choice(functions)

    f_kwargs = {}

    if random.randint(0, 1):
        start_factor = start_factor
        end_factor = end_factor
    else:
        if random.randint(0, 1):
            start_factor = 0
            end_factor = end_factor
        else:
            start_factor = start_factor
            end_factor = 1

    f_kwargs['start_factor'] = start_factor
    f_kwargs['end_factor'] = end_factor

    return apply_2d_function_to_volume(volume, mask, function, **f_kwargs)


def tilt_volume_backwards(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_backwards, **kwargs)


def tilt_volume_forwards(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_forward, **kwargs)


def tilt_volume_left(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_left, **kwargs)


def tilt_volume_right(volume, mask, **kwargs):
    return execute_random_tilt(volume, mask, func=tilt_right, **kwargs)


def random_squeeze_execution(volume, mask, axis, scale_factor=None):
    if scale_factor is None:
        scale_factor = random.randint(20, 80) / 100

    if axis == 0:
        zoom_factors = (scale_factor, 1, 1)
    elif axis == 1:
        zoom_factors = (1, scale_factor, 1)
    else:
        zoom_factors = (1, 1, scale_factor)

    new_volume = zoom(volume, zoom_factors)
    new_mask = zoom(mask, zoom_factors, mode='nearest', order=0)
    return new_volume, new_mask


def squeeze_x(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 2, scale_factor)


def squeeze_y(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 1, scale_factor)


def squeeze_z(volume, mask, scale_factor=None):
    return random_squeeze_execution(volume, mask, 0, scale_factor)


def squeeze_xy(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_y(volume, mask, scale_factor)
    return volume, mask


def squeeze_xz(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def squeeze_yz(volume, mask, scale_factor=None):
    volume, mask = squeeze_y(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def squeeze_xyz(volume, mask, scale_factor=None):
    volume, mask = squeeze_x(volume, mask, scale_factor)
    volume, mask = squeeze_y(volume, mask, scale_factor)
    volume, mask = squeeze_z(volume, mask, scale_factor)
    return volume, mask


def random_squeeze(volume, mask, scale_factor=None):
    functions = [squeeze_x, squeeze_y, squeeze_z, squeeze_xy, squeeze_xz, squeeze_yz, squeeze_xyz]
    return random.choice(functions)(volume, mask, scale_factor)


def random_crop(volume, mask, min_size=16, cropped_size=None):
    """
    Crops random height and width but not depth
    Args:
        volume:
        mask:
        min_size: min dim of the new volume
        cropped_size: none or: (depth, width, height)

    Returns:

    """
    d, h, w = volume.shape

    if cropped_size is None:
        try:
            h_start = random.randint(0, h // 4 - min_size // 2)
            w_start = random.randint(0, w // 4 - min_size // 2)

            h_end = random.randint(h // 4 + min_size // 2, h)
            w_end = random.randint(w // 4 + min_size // 2, w)
        except ValueError:
            # raised for example when: d // 2 - min_size // 2 <= 0. randint(0, 0) fails
            return None, None

    else:
        if isinstance(cropped_size, int):
            cropped_size = (cropped_size, cropped_size, cropped_size)

        try:
            max_start_height = h - cropped_size[1]
            max_start_width = w - cropped_size[2]

            h_start = random.randint(0, max_start_height)
            w_start = random.randint(0, max_start_width)

            h_end = h_start + cropped_size[1]
            w_end = w_start + cropped_size[2]
        except ValueError:
            return None, None

    return volume[:, h_start:h_end, w_start:w_end], mask[:, h_start:h_end, w_start:w_end]


def flip_x(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_x = 0
    for x in range(w - 1, -1, -1):
        new_volume[:, :, new_x] = volume[:, :, x]
        new_mask[:, :, new_x] = mask[:, :, x]
        new_x += 1
    return new_volume, new_mask


def flip_y(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_y = 0
    for y in range(h - 1, -1, -1):
        new_volume[:, new_y, :] = volume[:, y, :]
        new_mask[:, new_y, :] = mask[:, y, :]
        new_y += 1
    return new_volume, new_mask


def flip_z(volume, mask):
    d, h, w = volume.shape

    new_volume = np.zeros_like(volume)
    new_mask = np.zeros_like(mask)

    new_z = 0
    for z in range(d - 1, -1, -1):
        new_volume[new_z, :, :] = volume[z, :, :]
        new_mask[new_z, :, :] = mask[z, :, :]
        new_z += 1
    return new_volume, new_mask


def flip_xy(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_y(volume, mask)
    return volume, mask


def flip_xz(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def flip_yz(volume, mask):
    volume, mask = flip_y(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def flip_xyz(volume, mask):
    volume, mask = flip_x(volume, mask)
    volume, mask = flip_y(volume, mask)
    volume, mask = flip_z(volume, mask)
    return volume, mask


def random_flip(volume, mask, depth_flip_allowed=True):
    functions = [flip_x, flip_y, flip_z, flip_xy, flip_xz, flip_yz, flip_xyz]
    if not depth_flip_allowed:
        functions = [flip_x, flip_y, flip_xy]
    return random.choice(functions)(volume, mask)


def random_tilt(volume, mask):
    functions = [tilt_volume_forwards, tilt_volume_backwards, tilt_volume_left, tilt_volume_right]
    return random.choice(functions)(volume, mask)
