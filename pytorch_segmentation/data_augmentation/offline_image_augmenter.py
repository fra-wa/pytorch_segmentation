from copy import deepcopy

import cv2
import logging
import numpy as np
import os
import random

from collections import OrderedDict

from .geometric_transformations import elastic_distortion
from .geometric_transformations import flip_x
from .geometric_transformations import flip_y
from .geometric_transformations import grid_distortion
from .geometric_transformations import optical_distortion
from .geometric_transformations import random_crop
from .geometric_transformations import random_grid_shuffle
from .geometric_transformations import resize
from .geometric_transformations import rotate_by_angle
from .geometric_transformations import squeeze_height
from .geometric_transformations import squeeze_width
from .geometric_transformations import tilt_backwards
from .geometric_transformations import tilt_forward
from .geometric_transformations import tilt_left
from .geometric_transformations import tilt_right

from .pixel_manipulation import to_gray_and_adaptive_histogram
from .pixel_manipulation import brightness
from .pixel_manipulation import contrast
from .pixel_manipulation import gaussian_filter
from .pixel_manipulation import gaussian_noise
from .pixel_manipulation import to_gray_and_histogram_normalization
from .pixel_manipulation import to_gray_and_non_local_means_filter
from .pixel_manipulation import sharpen
from .pixel_manipulation import random_erasing
from .rgb_manipulation import channel_shuffle
from .rgb_manipulation import color_to_hsv
from .rgb_manipulation import iso_noise
from .rgb_manipulation import random_fog
from .rgb_manipulation import random_rain
from .rgb_manipulation import random_shadow
from .rgb_manipulation import random_snow
from .rgb_manipulation import random_sun_flair
from .rgb_manipulation import rgb_shift
from .rgb_manipulation import solarize
from .rgb_manipulation import to_gray
from .rgb_manipulation import to_sepia

from .. import constants
from ..file_handling.utils import get_file_paths_in_folder, create_folder
from ..multicore_utils import slice_multicore_parts, multiprocess_function


class ImgAugmentChain:
    def __init__(self, img_path, mask_path, out_folder, operations_and_kwargs, augment_id, augment_seed=None):

        self.img_path = img_path
        self.mask_path = mask_path
        self.operations_and_kwargs = operations_and_kwargs
        self.augment_id = augment_id
        self.py_init_random_state = random.getstate()
        self.np_init_random_seed = np.random.get_state()
        self.augment_seed = random.randint(0, 2**32 - 1) if augment_seed is None else augment_seed

        self.out_img_folder = os.path.join(out_folder, 'images')
        out_mask_folder = os.path.join(out_folder, 'masks')
        base_name = os.path.basename(self.img_path).split('.')[0]
        self.out_img_path = os.path.join(self.out_img_folder, f'{base_name}_aug_id_{self.augment_id}.jpg')
        self.out_mask_path = os.path.join(out_mask_folder, f'{base_name}_aug_id_{self.augment_id}.png')

    def augment(self, min_size=None):
        np.random.seed(self.augment_seed)
        random.seed(self.augment_seed)
        img = cv2.imread(self.img_path, -1)
        mask = cv2.imread(self.mask_path, -1)

        for operation, kwargs in self.operations_and_kwargs:
            img, mask = operation(img, mask, **kwargs)

        h, w = img.shape[:2]
        if min_size is not None:
            if h < min_size or w < min_size:
                np.random.set_state(self.np_init_random_seed)
                random.setstate(self.py_init_random_state)
                return

        cv2.imwrite(self.out_img_path, img)
        cv2.imwrite(self.out_mask_path, mask)
        np.random.set_state(self.np_init_random_seed)
        random.setstate(self.py_init_random_state)


class ImageAugmenter:
    def __init__(self,
                 images_folder,
                 masks_folder,
                 min_dimension,
                 max_augmentations_per_image,
                 max_rotation_angle,
                 total_augmentations=None,
                 operation_skip_list=None,
                 out_folder=None,
                 logger=None):
        """
        Calculation differs a bit from paper. Newer, more efficient version.

        Args:
            images_folder:
            masks_folder:
            min_dimension: if an image was geometrically manipulated so that either h or w is < min_dimension, the file
                will not be saved
            max_augmentations_per_image:
            max_rotation_angle:
            out_folder:
            total_augmentations: number of maximum augmentations -- not all combinations are stored
            logger:
        """
        if not os.path.isdir(images_folder):
            raise FileNotFoundError(f'There is no folder named {images_folder}!')

        self.images = get_file_paths_in_folder(images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)

        if not self.images:
            raise FileNotFoundError(f'The folder {images_folder} does not contain images')

        self.masks = get_file_paths_in_folder(masks_folder, extension=constants.SUPPORTED_MASK_TYPES)

        if len(self.images) != len(self.masks):
            raise RuntimeError('There must be an equal amount of images and masks')

        if total_augmentations == 0:
            total_augmentations = None
        if total_augmentations is not None and total_augmentations < 0:
            raise ValueError('Setting total_augmentations < 0 is not allowed!')

        self.channels = 1
        for img in self.images[:5]:
            img = cv2.imread(img, -1)
            if len(img.shape) == 3 and img.shape[2] == 3:
                self.channels = 3
                break

        self.logger = logger

        self.images_folder = images_folder
        self.masks_folder = masks_folder
        self.out_folder = out_folder

        if self.out_folder is not None:
            out_images_folder = os.path.join(out_folder, 'images')
            out_masks_folder = os.path.join(out_folder, 'masks')
            create_folder(out_images_folder)
            create_folder(out_masks_folder)
        else:
            self.out_folder = os.path.dirname(images_folder)

        logging.info(f'Saving files to: {self.out_folder}')

        self.initial_images_count = len(
            get_file_paths_in_folder(self.images_folder, extension=constants.SUPPORTED_INPUT_IMAGE_TYPES)
        )

        self.min_dimension = min_dimension
        self.max_rotation_angle = max_rotation_angle if max_rotation_angle is not None else 30
        self.rotation_angle_list = [0]

        self.geo_base_operations = OrderedDict({
            'rotate': rotate_by_angle,
            'resize': resize,
        })

        # afterwards all additional operations will be performed once on top of each already created image
        self.geo_additional_operations = {
            'tilt_backwards': tilt_backwards,
            'tilt_forward': tilt_forward,
            'tilt_left': tilt_left,
            'tilt_right': tilt_right,
            'elastic_distortion': elastic_distortion,
            'grid_distortion': grid_distortion,
            'optical_distortion': optical_distortion,
            'random_crop': random_crop,
            'random_grid_shuffle': random_grid_shuffle,
            'squeeze_height': squeeze_height,
            'squeeze_width': squeeze_width,
            'flip_horizontal': flip_x,
            'flip_vertical': flip_y,
        }

        self.pix_operations = {
            'to_gray_and_adaptive_histogram': to_gray_and_adaptive_histogram,
            'brightness': brightness,
            'contrast': contrast,
            'gaussian_filter': gaussian_filter,
            'gaussian_noise': gaussian_noise,
            'to_gray_and_histogram_normalization': to_gray_and_histogram_normalization,
            'to_gray_and_non_local_means_filter': to_gray_and_non_local_means_filter,
            'sharpen': sharpen,
            'random_erasing': random_erasing,
        }

        self.pix_rgb_operations = {
            'channel_shuffle': channel_shuffle,
            'color_to_hsv': color_to_hsv,
            'iso_noise': iso_noise,
            'random_fog': random_fog,
            'random_rain': random_rain,
            'random_shadow': random_shadow,
            'random_snow': random_snow,
            'random_sun_flair': random_sun_flair,
            'rgb_shift': rgb_shift,
            'solarize': solarize,
            'to_gray': to_gray,
            'to_sepia': to_sepia,
        }

        if self.channels == 3:
            self.pix_operations.update(self.pix_rgb_operations)

        if operation_skip_list is None:
            self.operation_skip_list = []
        else:
            self.operation_skip_list = operation_skip_list

        for operation_to_skip in operation_skip_list:
            if operation_to_skip in self.geo_base_operations.keys():
                self.geo_base_operations.pop(operation_to_skip)
            if operation_to_skip in self.geo_additional_operations.keys():
                self.geo_additional_operations.pop(operation_to_skip)
            if operation_to_skip in self.pix_operations.keys():
                self.pix_operations.pop(operation_to_skip)

        max_created_geo_base_images = 0
        if 'rotate' not in self.operation_skip_list:
            max_created_geo_base_images = len(self.rotation_angle_list)
        if 'resize' not in self.operation_skip_list:
            max_created_geo_base_images = max_created_geo_base_images * 2 + 1  # + resized original one

        # max_created_base_images plus original -> at least 1
        max_created_geo_additional_images = (max_created_geo_base_images + 1) * \
            len(self.geo_additional_operations.keys())
        max_geo_images = max_created_geo_base_images + max_created_geo_additional_images
        max_pix_images = max_geo_images * len(self.pix_operations.keys()) + len(self.pix_operations.keys())

        if max_geo_images + max_pix_images < max_augmentations_per_image:
            max_augmentations_per_image = max_geo_images + max_pix_images

        self.max_augmentations_per_image = max_augmentations_per_image
        self.total_augmentations = total_augmentations
        if self.out_folder == os.path.dirname(self.images_folder) and total_augmentations is not None:
            self.total_augmentations += self.initial_images_count

        logging.info('Creating augmentation chains with current settings..')
        self.augment_chains = []
        self.create_chains()
        logging.info(f'Finished. Total possible augmentations with current settings: {len(self.augment_chains)}.')
        if self.min_dimension is not None:
            logging.info(f'Requirement: min dimension >= {self.min_dimension}.')
        if self.total_augmentations is not None:
            logging.info(f'Requirement: total augmentations {self.total_augmentations}.')

    def setup_rotation_angle_list(self):
        # set up fixed rotation angles.
        # make a list containing rotation angles. The rotation step size should be at least 5 to prevent angles like:
        # 5, 6, 7 from happen (almost no difference).
        # All values will be randomly selected between -max angle and + max angle with a minimum distance of 5
        # see: also https://stackoverflow.com/a/66938460/13439260
        # if max angle is >= 30, choose 2 random rotations, >= 60: 4, 90: 6, ...
        max_rotations = self.max_rotation_angle // 10 - self.max_rotation_angle // 30
        if max_rotations == 0:
            max_rotations = 1
        min_step_size = 5
        max_val = self.max_rotation_angle - (min_step_size * (max_rotations - 1))
        possible_values = list(range(max_val))
        for i in range(len(possible_values)):
            possible_values.append(-1 * possible_values[i])
        valid_angle_choices = sorted(random.choices(possible_values, k=max_rotations))
        self.rotation_angle_list = sorted([(min_step_size * i + x) for i, x in enumerate(valid_angle_choices)])

    def create_chain_from_existing(self, chain, func, aug_id, kwargs=None):
        """
        Creates a chain based on a previous.
        Adds an augmentation func to the previous chain funcs and ensures reproducible results by copying the seed
        and appending the new func
        """
        if kwargs is None:
            kwargs = {}
        if not isinstance(chain, ImgAugmentChain):
            raise RuntimeError
        operations_and_kwargs = chain.operations_and_kwargs
        operations_and_kwargs.append((func, kwargs))
        img_path = chain.img_path
        mask_path = chain.mask_path
        seed = chain.augment_seed

        return ImgAugmentChain(img_path, mask_path, self.out_folder, operations_and_kwargs, aug_id, seed)

    def get_img_geometric_operation_chain(self, img_path, mask_path, aug_count):
        """
        First: rotate image by all angles
        Second: resize all rotated and original images
        Third: apply additional operations on original image and all previous manipulated images
        """
        # 1. rotate original by all angles
        rotation_chains = []
        if 'rotate' in self.geo_base_operations.keys():
            self.setup_rotation_angle_list()  # different angles for every image
            for rotation_angle in self.rotation_angle_list:
                operations_and_kwargs = [(self.geo_base_operations['rotate'], {'angle': rotation_angle})]
                rotation_chains.append(
                    ImgAugmentChain(img_path, mask_path, self.out_folder, operations_and_kwargs, aug_count)
                )
                aug_count += 1

        # 2. resize all rotated images
        resizing_chains = []
        if 'resize' in self.geo_base_operations.keys():
            for chain in rotation_chains:
                chain = deepcopy(chain)  # do not overwrite existing
                new_chain = self.create_chain_from_existing(chain, self.geo_base_operations['resize'], aug_count)
                resizing_chains.append(new_chain)
                aug_count += 1

            # resize original as well
            operations_and_kwargs = [(self.geo_base_operations['resize'], {})]
            resizing_chains.append(ImgAugmentChain(img_path, mask_path, self.out_folder, operations_and_kwargs, aug_count))
            aug_count += 1

        # 3. add additional
        additional_chains = []
        for operation, func in self.geo_additional_operations.items():
            for chain in rotation_chains + resizing_chains:
                chain = deepcopy(chain)
                additional_chains.append(self.create_chain_from_existing(chain, func, aug_count))
                aug_count += 1

        aug_chains = rotation_chains + resizing_chains + additional_chains

        return aug_chains, aug_count

    def get_img_pix_operation_chain(self, img_path, mask_path, geo_aug_chain, aug_count):
        aug_chains = []
        for geo_chain in geo_aug_chain:
            for operation, func in self.pix_operations.items():
                chain = deepcopy(geo_chain)
                aug_chains.append(self.create_chain_from_existing(chain, func, aug_count))
                aug_count += 1

        for operation, func in self.pix_operations.items():
            operations_and_kwargs = [(func, {})]
            aug_chains.append(
                ImgAugmentChain(img_path, mask_path, self.out_folder, operations_and_kwargs, aug_count)
            )
            aug_count += 1
        return aug_chains, aug_count

    def multi_create_chains(self, images_and_masks, process_nr):
        chains = []
        for i, (img_path, mask_path) in enumerate(images_and_masks):
            if process_nr == 0:
                logging.info(f'Process 1: Creating chains for image {i+1}/{len(images_and_masks)}')
            elif process_nr == -1:
                logging.info(f'Creating chains for image {i + 1}/{len(images_and_masks)}')
            aug_count = 0  # used to pass id's to all chains --> used for naming of out images
            geo_chain, aug_count = self.get_img_geometric_operation_chain(img_path, mask_path, aug_count)
            pix_chain, aug_count = self.get_img_pix_operation_chain(img_path, mask_path, geo_chain, aug_count)

            current_chains = geo_chain + pix_chain
            assert aug_count == len(current_chains)
            chains += current_chains

        return chains

    def create_chains(self):
        images_masks = list(zip(self.images, self.masks))
        images_masks_map = slice_multicore_parts(images_masks)
        if len(images_masks_map) > 1:
            logging.info(f'Using {len(images_masks_map)} processes.')
            process_nr_map = list(range(len(images_masks_map)))

            results = multiprocess_function(self.multi_create_chains, [images_masks_map, process_nr_map], self.logger)

            for result in results:
                self.augment_chains += result
        else:
            logging.info(
                f'Using 1 process. A single process needs around {constants.MULTICORE_RAM_RESERVATION}GB ram. '
                f'Close applications to get more processes.'
            )
            self.augment_chains = self.multi_create_chains(images_masks, -1)
        random.shuffle(self.augment_chains)
        logging.info(
            f'Created {len(self.augment_chains)} possible chains ('
            f'{len(self.augment_chains)/self.initial_images_count} per image).'
        )

    @staticmethod
    def multi_augment(aug_chains, min_size, total_augmentations, process_nr):
        count_out_files = True
        if total_augmentations is None:
            total_augmentations = len(aug_chains)
            count_out_files = False

        for i, chain in enumerate(aug_chains):
            if not isinstance(chain, ImgAugmentChain):
                raise RuntimeError
            if count_out_files:
                out_images = len(get_file_paths_in_folder(chain.out_img_folder))
                if process_nr == 0:
                    logging.info(
                        f'Process 1: calculating chain {i+1}/{len(aug_chains)}. '
                        f'Valid images saved: {out_images} (target: {total_augmentations})'
                    )
                elif process_nr == -1:
                    logging.info(
                        f'Calculating chain {i + 1}/{len(aug_chains)}. '
                        f'Valid images saved: {out_images} (target: {total_augmentations})'
                    )
                if out_images >= total_augmentations:
                    break
            else:
                if process_nr <= 0:
                    logging.info(f'{i}/{total_augmentations}')
            chain.augment(min_size)

    def augment(self):
        if self.total_augmentations is not None:
            if self.total_augmentations > len(self.augment_chains):
                self.total_augmentations = len(self.augment_chains)
            logging.info(f'Creating {self.total_augmentations} samples.')
        else:
            logging.info(f'Creating {len(self.augment_chains)} samples.')

        aug_chain_map = slice_multicore_parts(self.augment_chains)
        if len(aug_chain_map) > 1:
            logging.info(f'Using {len(aug_chain_map)} processes.')
            min_size_map = len(aug_chain_map) * [self.min_dimension]
            total_augmentations_map = len(aug_chain_map) * [self.total_augmentations]
            process_nr_map = list(range(len(aug_chain_map)))

            multiprocess_function(
                self.multi_augment, [aug_chain_map, min_size_map, total_augmentations_map, process_nr_map], self.logger
            )
        else:
            logging.info(f'Using 1 process.')
            self.multi_augment(self.augment_chains, self.min_dimension, self.total_augmentations, -1)

        out_images = len(get_file_paths_in_folder(os.path.join(self.out_folder, 'images')))
        if self.total_augmentations is not None and out_images > self.total_augmentations:
            logging.info(f'Created {out_images} images. Slightly more than wanted due to multiprocessing.')
        logging.info('Finished.')
