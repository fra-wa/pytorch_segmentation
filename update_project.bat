@echo off
if exist .git\ (
    echo Updating project
    git pull --rebase
    echo #######################################################################
    echo ##############################  Finished  #############################
    echo #######################################################################
) else (
    echo Cannot find git, please download and install from here:
    echo https://gitforwindows.org/
    echo Use defaults for installation.
    echo Ensure, that you downloaded the software using git:
    echo git clone https://gitlab.com/fra-wa/pytorch_segmentation.git
)
pause